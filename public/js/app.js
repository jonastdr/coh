$(function() {
    /*pesquisa*/
    var openPesquisa = function (self) {
        var pesquisa = $(self).attr('data-pesquisa');

        $.post(urlBase + "laboratorio/requisitos", {id: pesquisa}, function (data) {
            $('#Laboratorio .requisitos').html(data);
        });

        $.post(urlBase + "laboratorio/sidebar", {id: pesquisa}, function (data) {
            $('#Laboratorio .sidebar').html(data);
        });
    };

    $(document).on('click', '#Laboratorio .pesquisas.links li', function () {
        openPesquisa(this);
    });

    /*barraca*/
    var openUnidade = function (self) {
        var unidade = $(self).attr('data-unidade');

        $.post(urlBase + "barraca/unidade", {id: unidade}, function (data) {
            $('#Barraca .sobre_unidade').html(data);
        });

        $.get(urlBase + "barraca/recrutamento", function (data) {
            $('#Barraca .recrutamento').html(data);
        });
    };

    $(document).on('click', '#Barraca .lista .unidades.links li', function () {
        openUnidade(this);
    });

    /*Academia*/
    var openHeroi = function (self) {
        var heroi = $(self).attr('data-heroi');

        $.post(urlBase + "academia/funcoes", {id: heroi}, function (data) {
            $('#Academia .funcoes').html(data);
        });

        $.post(urlBase + "academia/sidebar", {id: heroi}, function (data) {
            $('#Academia .sidebar').html(data);
        });
    };

    $(document).on('click', '#Academia .lista .herois.links li', function () {
        openHeroi(this);
    });

    /*sidebar*/
    var execTimer = setInterval(function () {
        var tempoAttr = $('.tempo');

        $.each(tempoAttr, function(index, el){

            var timer = $(el);

            timer.attr('data-tempo', parseInt(timer.attr('data-tempo')) - 1);

            var restante = timer.attr('data-tempo');

            var min = parseInt(restante / 60);
            var hor = parseInt(min / 60);
            var seg = restante % 60;

            var min = min % 60;

            if (hor < 10) {
                hor = '0' + hor;
                hor = hor.substr(0, 2);
            }

            if (min < 10) {
                min = '0' + min;
                min = min.substr(0, 2);
            }
            if (seg < 10) {
                seg = '0' + seg;
            }

            if(restante < 0) {
                timer.html('Concluído.');

                setTimeout(function () {
                    if (timer.closest('ul').length == 1) {
                        timer.closest('ul').prev("h3").remove();
                        timer.closest('ul').remove();
                    } else {
                        timer.parent().remove();
                    }
                }, 2000);
            } else {
                timer.html(hor + ':' + min + ':' + seg);
            }
        });
    }, 1000);
});