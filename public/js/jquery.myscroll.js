/*Desenvolvido por JonasTDR - projectws*/

;(function($){
	$.fn.myscroll = function(){
		var argumentos = arguments[0];

		var seletor = this.selector;

        //valores padroes
        var defaults = {
            valor: 0
        };

        if(argumentos){
            if(argumentos && typeof argumentos == 'object'){
                $.each(argumentos, function(index, val) {
                    defaults.index = val;
                });
            } else {
                defaults.valor = argumentos;
            }
        }

        var scroll = {
            exec: function () {
                var main = this;

                var content;

                //Cria slider
                var scrollBkg = $(seletor).find(".scrollbarbkg").length == 0 ? $('<div>').addClass('scrollbarbkg') : $(seletor).find(".scrollbarbkg"),
                    scrollbar;

                if($(seletor).find(".content").length == 0) {
                    content = $('<div>').addClass('content').append($(seletor).html());

                    $(seletor).html(content).addClass('myscroll');

                    $(seletor).append(scrollBkg);

                    content.css({
                        position: 'absolute',
                        overflow: 'hidden'
                    });

                    content.height($(seletor).height());

                    content.width($(seletor).width());
                } else {
                    content = $(seletor).find(".content");
                }

                var maxScroll = content[0].scrollHeight - $(seletor).height();

                if (maxScroll > 0) {
                    if($(seletor).find(".scrollbar").length == 0)
                        scrollbar = $('<span>').addClass('scrollbar').appendTo($(seletor).find(".scrollbarbkg"));
                    else
                        scrollbar = $(seletor).find(".scrollbar");
                }

                scrollBkg.css({
                    height: parseInt($(seletor).height()) + 'px'
                });

                if(scrollbar){
                    var porcent,
                        block = (scrollbar.outerHeight() / scrollBkg.outerHeight()) * 1.1;

                    scrollbar.draggable({
                        containment: 'parent',
                        axis: 'y',
                        drag: function(e, ui){
                            porcent = parseInt($(this).css('top')) / (scrollBkg.height() / 100);

                            porcent = Math.ceil(porcent+(porcent * block));

                            defaults.valor = porcent;

                            main.setScroll(scrollBkg, scrollbar, maxScroll, content);
                        },
                        create: function(e, ui){
                            main.setScroll(scrollBkg, scrollbar, maxScroll, content);
                        }
                    });

                    scrollBkg.click(function(e) {
                        porcent = e.offsetY / (scrollBkg.height() / 100);

                        defaults.valor = porcent;

                        main.setScroll(scrollBkg, scrollbar, maxScroll, content);
                    });

                    content.off('mousewheel');

                    content.on('mousewheel', function(e){
                        var value = content.scrollTop();

                        if(e.originalEvent.wheelDelta >= 0){
                            defaults.valor = (defaults.valor <= 5 ? 0 : defaults.valor-5);

                            main.setScroll(scrollBkg, scrollbar, maxScroll, content);
                        } else {
                            defaults.valor = (defaults.valor >= 95 ? 100 : defaults.valor+5);

                            main.setScroll(scrollBkg, scrollbar, maxScroll, content);
                        }

                        value = maxScroll * defaults.valor / 100;

                        content.scrollTop(value);
                    });
                }
            },
            setScroll: function (scrollBkg, scrollbar, maxScroll, content) {
                scrollbar.css({
                    top: (scrollBkg.height() * defaults.valor / 100) - (scrollbar.outerHeight() * defaults.valor / 100)
                });

                var value = maxScroll * defaults.valor / 100;

                content.scrollTop(value);
            },
            getPosition: function () {
                return defaults.valor;
            },
            setPosition: function (valor) {
                this.exec();

                if(valor > 100 || valor < 0)
                    throw "Valor incorreto. deve ser entre 0 e 100.";

                var scrollBkg = $(seletor).find(".scrollbarbkg"),
                    scrollbar = $(seletor).find(".scrollbar"),
                    content = $(seletor).find(".content"),
                    maxScroll = content[0].scrollHeight - $(seletor).height();


                this.setScroll(scrollBkg, scrollbar, maxScroll, content);

                return this;
            },
            refresh: function () {
                this.exec();
            }
        };

        scroll.exec();

        return scroll;
	};
})(jQuery);
