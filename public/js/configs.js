$(document).off('click');

$(document).on('click', 'a.abrir_dialog', function(e) {
    var title = $(this).attr('dialog-title'),
        id = removeAcentos(title),
        url = $(this).attr('href');

    if(title) {
        for (i = 0; i < title.length; i++) {
            if (id.charAt(i) == " " || id.charAt(i) == ":") {
                id = id.replace(id.charAt(i), "_");
            }
        }
    } else {
        throw 'Deve informar um titulo!';
    }

    $( 'body' ).find( '#' + id ).remove();

    $( 'body' ).append( $( '<div id="'+ id + '">' ) );

    var preload,
        tempo = 1000,
        timeout = setTimeout(function(){
            preload = game.mensagem();
        }, tempo);

    $( '#'+ id ).load(url, function (){
        var self = $( this ),
            ifTab = false;

        self.dialog({
            width: 'auto',
            resizable: false,
            close: function(){
                $(this).remove();
            },
            open: function(){
                //verificacao timeout
                if(timeout){
                    clearTimeout(timeout);
                    timeout = false;
                }
                if(preload)
                    preload.remove();

                //variaveis
                var titulo = self.closest('.ui-dialog').find('.ui-dialog-titlebar'),
                    tabs = self.children('.dialog-tabs'),
                    allTabs = $('<div>').addClass('dialog-tabs').append(tabs),
                    tituloText = $('<div>').addClass('dialog-header-title').html(title),
                    header = $('<div>').addClass('dialog-header'),
                    controles;

                self.closest('.ui-dialog').find('.ui-dialog-title').remove();

                allTabs.width(
                    self.width() - parseInt(self.width() * 25 / 100)
                );

                if(tabs.length > 0){
                    //para tirar animacoes
                    ifTab = true;

                    //cria botoes de navegacao
                    var mascara = $('<span>').addClass('dialog-tabs-mask').appendTo(allTabs);

                    var maxScroll = tabs.outerWidth() - allTabs.outerWidth(),
                        atual = 1,
                        soma = 0;

                    controles = $('<div>').addClass('dialog-tabs-controls').appendTo(allTabs);

                    var botoaoA = $('<a>').html('<i class="fa fa-chevron-circle-left"></i>').click(function(){
                        var pos = $(tabs).find('li:nth-child(' + (atual-1) + ')'),
                            anterior = $(tabs).find('li:nth-child(' + (atual) + ')');

                        if(pos.length > 0){
                            soma -= pos.outerWidth();

                            allTabs.animate({
                                scrollLeft: soma
                            }, 200);

                            atual--;
                        }

                    }).appendTo(controles);

                    var botoaoB = $('<a>').html('<i class="fa fa-chevron-circle-right"></i>').click(function(){
                        var pos = $(tabs).find('li:nth-child(' + (atual+1) + ')'),
                            anterior = $(tabs).find('li:nth-child(' + (atual) + ')');

                        if(pos.length > 0){
                            soma += anterior.outerWidth();

                            allTabs.animate({
                                scrollLeft: soma
                            }, 200);

                            atual++;
                        }

                    }).appendTo(controles);

                    header.append(allTabs);

                    //abrir link como active
                    var ativo = $(tabs).find('li a.ativo');

                    if(ativo.length > 0){
                        $(self).load(ativo.attr('href'));
                    } else {
                        ativo = $(tabs).find('li:first a').addClass('ativo');

                        timeout = setTimeout(function(){
                            preload = game.mensagem();
                        }, tempo);

                        $(self).load(ativo.attr('href'), function(){
                            if(timeout){
                                clearTimeout(timeout);
                                timeout = false;
                            }
                            if(preload)
                                preload.remove();
                        });
                    }

                    $(tabs).off('click');

                    $(tabs).on('click', 'li a:not(.abrir_dialog)', function(e){
                        e.preventDefault();

                        var link = $(this).attr('href');

                        $(tabs).find('a').removeClass('ativo');

                        $(this).addClass('ativo');

                        timeout = setTimeout(function(){
                            preload = game.mensagem();
                        }, tempo);

                        $(self).load(link, function(){
                            if(timeout){
                                clearTimeout(timeout);
                                timeout = false;
                            }
                            if(preload)
                                preload.remove();
                        });
                    });
                }

                header.prepend(tituloText);
                titulo.prepend(header);

                //eliminar botoes se for o caso
                var tamanho = 0,
                    max = allTabs.width() - (allTabs.width() * 25 / 100);

                tabs.find('li').each(function(index, el) {
                    tamanho += $(this).width();
                });

                if(max >= tamanho && typeof controles !== 'undefined')
                    controles.remove();

                tabs.width(tamanho + $(self).width());
            }
        });

        self.parent().css({
            top: '10%'
        });

        $(self).off('click');

        $(self).on('click', 'a:not(.abrir_dialog)', function(e){
            if($(this).attr('ignoredefault')!='true'){
                e.preventDefault();

                var link = $(this).attr('href');

                timeout = setTimeout(function(){
                    preload = game.mensagem();
                }, tempo);

                $(self).load(link, function(){
                    if(timeout){
                        clearTimeout(timeout);
                        timeout = false;
                    }
                    if(preload)
                        preload.remove();
                });
            }
        });

        self.off('submit');

        self.on('submit', 'form', function(event) {
            var $this = $(this);

            if($this.attr('ignoredefault')!='true'){
                var formObj = $(this),
                    formURL = formObj.attr("action"),
                    formData = new FormData(this);

                timeout = setTimeout(function(){
                    preload = game.mensagem();
                }, tempo);

                $.ajax({
                    url: formURL,
                    type: 'POST',
                    data:  formData,
                    mimeType:"multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data, textStatus, jqXHR)
                    {
                        if($this.is("#expedicao")) {
                            app.atividades();
                        }

                        if(timeout){
                            clearTimeout(timeout);
                            timeout = false;
                        }
                        if(preload)
                            preload.remove();

                        self.html(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        if(timeout){
                            clearTimeout(timeout);
                            timeout = false;
                        }
                        if(preload)
                            preload.remove();

                        console.log('error');
                    }
                });
                event.preventDefault();
            }
        });

        self.off('change');

        self.on('change', 'div.select select', function(e){
            var value = $(this).val();

            var msgBox = game.mensagem();

            self.load(value, function(){
                msgBox.remove();
            });
        });
    });

    return false;
});

var removeAcentos = function (texto) {
    if(!texto)
        return null;

    texto = texto.replace(/[á|ã|â|à]/gi, "a");
    texto = texto.replace(/[é|ê|è]/gi, "e");
    texto = texto.replace(/[í|ì|î]/gi, "i");
    texto = texto.replace(/[õ|ò|ó|ô]/gi, "o");
    texto = texto.replace(/[ú|ù|û]/gi, "u");
    texto = texto.replace(/[ç]/gi, "c");
    texto = texto.replace(/[ñ]/gi, "n");
    texto = texto.replace(/[á|ã|â]/gi, "a");
    texto = texto.replace(/W/gi, "-");

    return texto;
};

var game = {

    mensagem: function(mensagem, classe, delay){
        classe = (typeof classe == 'undefined' ? '' : ' ' + classe);

        var msgBox;

        msgBox = $('<span>').addClass('alerta' + classe).appendTo('body');

        if(typeof mensagem == 'undefined' || mensagem === '')
            mensagem = $('<i>').addClass('fa fa-cog fa-spin fa-3x fa-fw').appendTo(msgBox);
        else
            msgBox.html(mensagem);

        if(typeof delay !== 'undefined'){
            msgBox.delay(delay)
                .fadeOut(500, function() {
                    $(this).remove();
                });
        } else {
            msgBox.delay(3000)
                .fadeOut(500, function() {
                    $(this).remove();
                });
        }

        var opcoes = {
            remove: function(){
                msgBox.fadeOut(500, function() {
                    $(this).remove();
                });
            },
            removeNow: function(){
                msgBox.remove();
            },
            removeDelay: function(delay){
                msgBox.delay(delay).fadeOut(500, function() {
                    $(this).remove();
                });
            },
            alterar: function(html, classe){
                if(typeof classe !== 'undefined')
                    msgBox.addClass(classe);

                msgBox.html(html);

                return opcoes;
            }
        };

        return opcoes;
    }

};
