/*Desenvolvido por JonasTDR - projectws*/

;(function($){
	$.fn.textcomplete = function(){
		var argumentos = arguments[0],
			seletor = this.selector;

		var source = argumentos.source,
			url = argumentos.url,
			delay = (typeof argumentos.delay == 'undefined' || argumentos.delay < 500 ? 500 : argumentos.delay),
			multi = (typeof argumentos.multi == 'undefined' ? false : true),
			repeat = false;

		//resultados
		var resultado = function(result){
			if(typeof timeout !== 'undefined')
				clearTimeout(timeout);

			ul.insertAfter(seletor);

			var nomes = $(seletor).val().split(', ');

			//remove e insere

			//arrow
			$('<div>').addClass('arrow').appendTo(ul);

			//Lista os resultados
			$.each(result, function(index, texto) {
				var nome = $('<li>' + texto + '</li>').text(),
					verif = nomes.indexOf(nome);

				if(verif < 0 || nomes.length == 1){
					$('<li>').attr('data-parent', seletor).html(texto).appendTo(ul).click(function(){
						if(multi){
							var value = [];

							if($(seletor).val() !== '')
								value = $(seletor).val().split(', ');

							var i = value.length - 1;

							value[i] = $(this).text() + ', ';

							//verifica se é possivel repetir nome
							if(value.indexOf($(this).text()) < 0 || repeat === true)
								$(seletor).val( value.join(', ') );

							//remove o item
							$(this).remove();

							//se não existir mais itens remove todos
							if(ul.children('*:not(.arrow)').length === 0){
								ul.remove();
							}
						} else {
							$(seletor).val( $(this).text() );
						}
					});
				}
			});

			var position = $(seletor).position();

			ul.css({
				top: position.top + 10,
				left: parseInt(position.left - (position.left/2.5)) + ($(seletor)[0].selectionStart * 5)
			});
		};

		//get
		var gets = function(){
			$.getJSON(url + value, function(json) {
				ul.empty();

				if(json.length > 0)
					resultado(json);
			});
		};

		//executa o timer
		var timer = function(){

			timeout = setTimeout(function(){
				ul.remove();
				criarElementos();

				gets();

				clearTimeout(timeout);
			}, delay);
		};

		//cria elementos
		var ul;

		var criarElementos = function(){
			ul = $('<ul>').addClass('textcomplete').attr('data-parent', seletor);
		};
		criarElementos();

		if(typeof url !== 'undefined'){
			var timeout = 'undefined',
				value;

			$(seletor).keyup(function(){
				if(multi){
					value = $(seletor).val().split(', ');

					value = value[value.length-1];
				} else {
					value = $(seletor).val();
				}

				if(value !== ''){
					if(typeof timeout == 'undefined'){
						timer();
					} else{
						clearTimeout(timeout);
						timer();
					}
				} else {
					clearTimeout(timeout);
				}
			});

			$(document).click(function(e){
				if($(e.target).attr('data-parent') !== seletor){
					ul.remove();
				}
			});

			$(document).off('focus');

			$(document).on('focus', 'input',function(e){
				if($(e.target).attr('data-parent') !== seletor){
					ul.remove();
				}
			});
		}

		return $(seletor);
	};
})(jQuery);
