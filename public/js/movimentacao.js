var anterior = 0,
    ultimo = 0;

var aberta = [],
    fechada = [];

function Adjacentes(atual, destino, G){

  var x = atual[0],
      y = atual[1];

  var diagonal = 0;

  var adjacentes = [
    {
      x: 0,
      y: -1
    },
    {
      x: 1,
      y: 0
    },
    {
      x: 0,
      y: 1
    },
    {
      x: -1,
      y: 0
    }
  ];

  $.each(adjacentes, function(index, adj) {
    var AdX = x+adj.x,
        AdY = y+adj.y;

    if((AdX>=0 && AdX<tileMap.length) && (AdY>=0 && AdY<tileMap.length) && !Pesquisa([AdX,AdY]) && tileMap[AdX][AdY]>0){
      aberta.push({
        x: AdX,
        y: AdY,
        f: G + (Math.abs(AdX-destino[0]) + Math.abs(AdY-destino[1])) * 10,
        pai: {
          x: x,
          y: y
        },
        g: G
      });
    } else if((AdX>=0 && AdX<tileMap.length) && (AdY>=0 && AdY<tileMap.length) && tileMap[AdX][AdY]==0){
      diagonal++;
    }
  });

  if(diagonal==0){
    var diagonais = [
      {
        x: 1,
        y: -1
      },
      {
        x: 1,
        y: 1
      },
      {
        x: -1,
        y: 1
      },
      {
        x: -1,
        y: -1
      }
    ];

    $.each(diagonais, function(index, adj) {
      var AdX = x+adj.x,
          AdY = y+adj.y;

      if((AdX>=0 && AdX<tileMap.length) && (AdY>=0 && AdY<tileMap.length) && !Pesquisa([AdX,AdY]) && tileMap[AdX][AdY]>0){
        aberta.push({
          x: AdX,
          y: AdY,
          f: G+4 + (Math.abs(AdX-destino[0]) + Math.abs(AdY-destino[1])) * 10,
          pai: {
            x: x,
            y: y
          },
          g: G+4
        });
      }
    });
  }

  //reordena decrescente sem o valor adicionado na lista dos fechados
  aberta.sort(OrdenarPor('f'));

  var max = aberta.length-1;

  fechada.push(aberta[max]);

  resultado = [aberta[max].x, aberta[max].y, aberta[max].g];

  aberta.pop();

  aberta.sort(OrdenarPor('f'));

  return resultado;
}

//Pesquisa se o bloco já esta na lista fechada/aberta
function Pesquisa(pesquisado){
  var retorno = 0;
  $.each(aberta, function(index, val) {
    if(val.x == pesquisado[0] && val.y == pesquisado[1]){
      retorno++;//encontrou o valor na lista aberta/bloqueado
    }
  });

  $.each(fechada, function(index, val) {
    if(val.x == pesquisado[0] && val.y == pesquisado[1]){
      retorno++;//encontrou o valor na lista aberta/bloqueado
    }
  });

  if(retorno>0){
    return true;
  } else {
    return false;
  }
}

function movimentacao(atual, destino){
  limit = 0;
  G = 0;

  anterior = 0;
  ultimo = 0;

  aberta = [];
  fechada = [];

  fechada.push({
    x: atual[0],
    y: atual[1],
    f: 10 + (Math.abs(atual[0]-destino[0]) + Math.abs(atual[0]-destino[1])) * 10,
    pai: {
      x: atual[0],
      y: atual[1]
    },
    g: 0
  });

  proximo = [fechada[0].x,fechada[0].y,fechada[0].g];

  while(atual[0] != destino[0] || atual[1] != destino[1]){

    G += proximo[2];

    proximo = Adjacentes(atual, destino, G);

    atual[0] = proximo[0];
    atual[1] = proximo[1];

    limit++;
  }

  return fechada;
}

function OrdenarPor(property){
  return function(a, b){
    if(a[property] > b[property]){
      return -1;
    }else if(a[property] < b[property]){
      return 1;
    }else{
      return 0;
    }
  }
}
