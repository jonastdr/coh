import { NgModule }      from '@angular/core';
import {BrowserModule, platformBrowser} from '@angular/platform-browser';
import {AppComponent} from "./dev/components/app.component";
import {MapaComponent} from "./dev/components/mapa.component";
import {MenuComponent} from "./dev/components/menu.component";
import {LaboratorioComponent} from "./dev/components/construcoes/laboratorio.component";
import {LaboratorioService} from "./dev/services/laboratorio.service";
import {HttpModule, JsonpModule} from "@angular/http";
import {RecursosComponent} from "./dev/components/recursos.component";
import {GameService} from "./dev/services/game.service";
import {RecursoDirective} from "./dev/directives/recurso.directive";
import {SidebarComponent} from "./dev/components/sidebar.component";
import {TimerDirective} from "./dev/directives/timer.directive";
import {BarracaComponent} from "./dev/components/construcoes/barraca.component";
import {BarracaService} from "./dev/services/barraca.service";
import {DefesaService} from "./dev/services/defesa.service";
import {MensagemComponent} from "./dev/components/mensagem.component";
import {MensagemService} from "./dev/services/mensagem.service";
import {RelatorioComponent} from "./dev/components/relatorio.component";
import {RelatorioService} from "./dev/services/relatorio.service";
import {RecursoTempoPipe} from "./dev/pipes/recurso.tempo.pipe";
import {AliancaComponent} from "./dev/components/alianca.component";
import {AliancaService} from "./dev/services/alianca.service";
import {ROUTER_DIRECTIVES} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {ScrollDirective} from "./dev/directives/scroll.directive";
import {EstatisticasComponent} from "./dev/components/estatisticas.component";
import {EstatisticasService} from "./dev/services/estatisticas.service";
import {ExpedicaoComponent} from "./dev/components/expedicao.component";
import {ExpedicaoService} from "./dev/services/expedicao.service";
import {CidadesComponent} from "./dev/components/cidades.component";
import {ClickoutDirective} from "./dev/directives/clickout.directive";
import {OverviewComponent} from "./dev/components/overview.component";
import {OverviewService} from "./dev/services/overview.service";
import {DragConstrucaoDirective} from "./dev/directives/drag.construcao.directive";
import {DroppableDirective} from "./dev/directives/droppable.directive";
import {ConstrucaoComponent} from "./dev/components/construcao.component";
import {ConstrucaoService} from "./dev/services/construcao.service";
import {DefesaCidadeComponent} from "./dev/components/construcoes/radar/defesa-cidade.component";
import {DefesaExpedicaoComponent} from "./dev/components/construcoes/radar/defesa-expedicao.component";
import {FilterUserDirective} from "./dev/directives/filter.user.directive";
import {AcademiaComponent} from "./dev/components/construcoes/academia.component";
import {AcademiaService} from "./dev/services/academia.service";
import {UsuarioComponent} from "./dev/components/usuario.component";
import {UsuarioService} from "./dev/services/usuario.service";
import {TitleDirective} from "./dev/directives/title.directive";
import {EqualValidator} from "./dev/directives/equal-validator.directive";
import {CidadeDirective} from "./dev/directives/cidade.directive";
import {ExpPosDirective} from "./dev/directives/exp-pos.directive";
import {MapaService} from "./dev/services/mapa.service";
import {PagerService} from "./dev/services/pager.service";
import {InputTagComponent} from "./dev/input-tag/input-tag.component";
import {NotificacaoComponent} from "./dev/components/notificacao.component";
import {EspionagemComponent} from "./dev/components/espionagem.component";
import {SliderComponent} from "./dev/components/display/slider.component";
import {EspionagemService} from "./dev/services/espionagem.service";
import {EspionagemCidadeComponent} from "./dev/components/construcoes/radar/espionagem-cidade.component";
import {RelatorioEspionagemComponent} from "./dev/components/relatorio-espionagem.component";
import {IntPipe} from "./dev/pipes/int.pipe";
import {AliancaNivelComponent} from "./dev/components/alianca/alianca-nivel.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule
    ],
    declarations: [
        AppComponent,
        MapaComponent,
        OverviewComponent,
        RecursosComponent,
        NotificacaoComponent,
        MenuComponent,
        CidadesComponent,
        SidebarComponent,
        LaboratorioComponent,
        BarracaComponent,
        MensagemComponent,
        RelatorioComponent,
        AliancaComponent,
        AliancaNivelComponent,
        EstatisticasComponent,
        ExpedicaoComponent,
        ConstrucaoComponent,
        DefesaCidadeComponent,
        DefesaExpedicaoComponent,
        AcademiaComponent,
        UsuarioComponent,
        EspionagemComponent,
        EspionagemCidadeComponent,
        RelatorioEspionagemComponent,
        InputTagComponent,
        SliderComponent,
        //DIRECTIVES
        RecursoDirective,
        TimerDirective,
        ScrollDirective,
        ClickoutDirective,
        DragConstrucaoDirective,
        DroppableDirective,
        FilterUserDirective,
        TitleDirective,
        EqualValidator,
        CidadeDirective,
        ExpPosDirective,
        //PIPES
        RecursoTempoPipe,
        IntPipe
    ],
    providers: [
        {
            provide: platformBrowser,
            useValue: [ROUTER_DIRECTIVES],
            multi: true
        },
        GameService,
        OverviewService,
        DefesaService,
        LaboratorioService,
        BarracaService,
        MensagemService,
        RelatorioService,
        AliancaService,
        EstatisticasService,
        ExpedicaoService,
        ConstrucaoService,
        AcademiaService,
        UsuarioService,
        MapaService,
        EspionagemService,
        PagerService
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }