export class City {
    private _island: number;
    private _city: number;

    private _x: number;
    private _y: number;

    private left: number;
    private top: number;

    constructor(left, top) {
        this.left = left;
        this.top = top;
    }

    set island(islandId) {
        this._island = islandId;
    }

    set city(cityId) {
        this._city = cityId;
    }

    set x(x) {
        this._x = x;
    }

    set y(y) {
        this._y = y;
    }

    get island() {
        return this._island;
    }

    get city() {
        return this._city;
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    get id() {
        return 'i' + this._island + 'c' + this._city;
    }

}