import {City} from "./city";
export class Island {

    cities = [];

    constructor(left:Array, top:Array) {

        for (let i = 0; i < left.length; i++) {
            this.cities.push(new City(left[i], top[i]));
        }

    }

}