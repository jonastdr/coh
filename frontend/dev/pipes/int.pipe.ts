import {Pipe, PipeTransform} from "@angular/core";
@Pipe({
    name: 'int'
})

export class IntPipe implements PipeTransform {

    transform(value) {
        return Math.round(value);
    }

}