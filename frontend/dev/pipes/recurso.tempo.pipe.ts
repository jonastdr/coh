import {Pipe, PipeTransform} from "@angular/core";
@Pipe({
    name: 'recursoTempo'
})

export class RecursoTempoPipe implements PipeTransform {

    transform(value: number) {
        var min = parseInt(value / 60);
        var hor = parseInt(min / 60);
        var seg = value % 60;

        var min = min % 60;

        if (hor < 10) {
            hor = '0' + hor;
            hor = (""+hor).substr(0, 2);
        }

        if (min < 10) {
            min = '0' + min;
            min = min.substr(0, 2);
        }
        if (seg < 10) {
            seg = '0' + seg;
        }

        if(value >= 0) {
            if(hor == '00')
                return min + ':' + seg;

            return hor + ':' + min + ':' + seg;
        }

        return '00:00';
    }

}