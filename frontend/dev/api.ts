import {URLSearchParams, Headers} from "@angular/http";
export class Api {
    private static _url = 'http://127.0.0.1/coh/public/';

    public body: URLSearchParams;
    public headers: Headers;

    constructor() {
        this.body = new URLSearchParams();
        this.headers = new Headers();

        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }

    static url(path?: string) {
        return this._url + (path ? path : '');
    }

}