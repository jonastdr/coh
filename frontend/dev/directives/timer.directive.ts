import {Directive, ElementRef, Input, Output, EventEmitter} from "@angular/core";
@Directive({
    selector: '[coh-timer]'
})
export class TimerDirective {

    @Input('coh-timer') timer;
    @Output('callback') callback = new EventEmitter();

    interval;

    private $timer;

    constructor(el: ElementRef) {
        this.$timer = $(el.nativeElement);

        this.interval = setInterval(() => {
            this.clock();
        }, 1000);
    }

    ngOnInit() {
        this.clock();
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

    /**
     * Constrói o timer
     */
    clock() {
        var min = parseInt(this.timer / 60);
        var hor = parseInt(min / 60);
        var seg = this.timer % 60;

        var min = min % 60;

        if (hor < 10) {
            hor = '0' + hor;
            hor = (""+hor).substr(0, 2);
        }

        if (min < 10) {
            min = '0' + min;
            min = min.substr(0, 2);
        }
        if (seg < 10) {
            seg = '0' + seg;
        }

        if(this.timer >= 0) {
            this.$timer.html(hor + ':' + min + ':' + seg);
        } else {
            this.$timer.html('Concluído.');

            setTimeout(() => {
                this.callback.emit();
            }, 1000);

            clearInterval(this.interval);
        }

        this.timer--;
    }

}