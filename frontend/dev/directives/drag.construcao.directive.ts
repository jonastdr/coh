import {Directive, ElementRef, Output, EventEmitter, Input} from "@angular/core";
@Directive({
    selector: '[coh-drag-construcao]'
})
export class DragConstrucaoDirective {

    @Input('coh-drag-construcao')
    dragConstrucao;

    @Output('drag-stop')
    dragStop = new EventEmitter();

    constructor(private elem: ElementRef) {
        var itemSelecionado = null;

        var selectItem = function(e){
            itemSelecionado = $(e.target);
        };

        var effectIn = function(self) {
            $(self).css({
                zIndex: 2,
                backgroundColor: 'rgba(0,255,0,.2)'
            });

            itemSelecionado = $(self);
        };

        var effectOut = function(self) {
            $(self).css({
                zIndex: 1,
                backgroundColor: ''
            });

            itemSelecionado = null;
        };

        var removerEventos = function() {
            $( ".ov_bloco" ).off('mouseenter');
            $( ".ov_bloco" ).off('mouseleave');
            $(document).off('mousemove', selectItem);

            $('.ov_bloco').css({
                zIndex: ''
            });

            $('.ov_bloco_bloq').css({
                zIndex: ''
            });
        };

        $(elem.nativeElement).draggable({
            cursorAt: {
                left: 50,
                top: 50
            },
            helper: 'clone',
            appendTo: 'body',
            revert: () => {
                removerEventos();

                if(itemSelecionado && itemSelecionado.is('.ov_bloco'))
                    return false;

                return true;
            },
            start: (event, ui) => {
                $('.ov_bloco').css({
                    zIndex: 1
                });

                $( ".ov_bloco" ).on('mouseenter', function() {
                    effectIn(this);
                });

                $( ".ov_bloco" ).on('mouseleave', function() {
                    effectOut(this)
                });

                $(document).on('mousemove', selectItem);

                $('.ov_bloco_bloq').css({
                    zIndex: 1,
                    backgroundColor: 'rgba(255,0,0,.5)'
                });
            },
            drag: (e: JQueryEventObject, ui) => {
                $(this).css('opacity', '.5');

                ui.helper
                    .width(100)
                    .height(100)
                    .css({
                        backgroundSize: '100px 100px',
                    }).mouseup(function(event) {
                    $( this ).width(50)
                        .height(50)
                        .css({
                            backgroundSize: '50px 50px'
                        });
                });
            },
            stop: (e: any, ui) => {
                if(itemSelecionado.is('.ov_bloco')) {
                    this.dragConstrucao.slot = itemSelecionado.attr('id').replace('slot_', '');

                    this.dragStop.emit(this.dragConstrucao);
                }

                //retorna estilos padroes
                $( ".ov_bloco" ).css({
                    backgroundColor: '',
                    zIndex: ''
                });

                $( ".ov_bloco_bloq" ).css({
                    backgroundColor: ''
                });

                $(this.elem.nativeElement).css('opacity', '');

                removerEventos();
            }
        });
    }



}