import {Directive, ElementRef, Input} from "@angular/core";
@Directive({
    selector: '[coh-scroll]'
})
export class ScrollDirective {

    @Input('coh-scroll') scroll;

    constructor(private elem: ElementRef) {
    }

    ngOnInit() {
        $(this.elem.nativeElement)
            .addClass('scrollbar-macosx')
            .scrollbar();
    }

}