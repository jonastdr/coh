import {Directive, ElementRef, Input} from "@angular/core";
@Directive({
    selector: '[coh-cidade]'
})
export class CidadeDirective {

    @Input('coh-cidade') cidade;

    constructor(el: ElementRef) {
        var self = this;

        $(el.nativeElement).tooltip({
            content: function() {
                var conteudo = $('<div>')
                    .addClass('cidade_tooltip');

                if(self.cidade) {
                    conteudo.append(
                        $('<h4>').html(self.cidade.cidade)
                    );

                    conteudo.append(
                        $('<p>').html('<i class="fa fa-user"></i> ' + self.cidade.usuario + '')
                    );

                    if(self.cidade.alianca) {
                        conteudo.append(
                            $('<p>').html('<i class="fa fa-users"></i> ' + self.cidade.alianca + '')
                        );
                    }

                    conteudo.append(
                        $('<p>').html('<i class="fa fa-star-o"></i> ' + self.cidade.pontos + '')
                    );
                } else {
                    conteudo.append(
                        $('<h4>').html('Colonizar?')
                    );
                }

                return conteudo;
            }
        });
    }

    ngOnDestroy() {
        $('.ui-tooltip').remove();
    }

}