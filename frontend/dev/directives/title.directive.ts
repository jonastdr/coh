import {Directive, ElementRef, Input} from "@angular/core";
@Directive({
    selector: '[coh-title]'
})
export class TitleDirective {

    @Input('coh-title')
    title;

    constructor(private elem: ElementRef) {
    }

    ngOnInit() {
        $(this.elem.nativeElement).attr('title', this.title).tooltip();
    }

    ngOnDestroy() {
        $('.ui-tooltip').remove();
    }

}