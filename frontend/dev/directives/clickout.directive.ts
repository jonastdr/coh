import {Directive, ElementRef, EventEmitter, Output, HostListener} from "@angular/core";
@Directive({
    selector: '[coh-clickout]'
})
export class ClickoutDirective {
    @Output('coh-clickout')
    public clickOut = new EventEmitter<MouseEvent>();

    constructor(private _elementRef: ElementRef) {
    }

    @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if (!targetElement) {
            return;
        }

        const clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.clickOut.emit(event);
        }
    }
}