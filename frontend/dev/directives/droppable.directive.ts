import {Directive, ElementRef, Output, EventEmitter, Input} from "@angular/core";
@Directive({
    selector: '[coh-droppable]'
})
export class DroppableDirective {

    @Input('coh-droppable')
    droppableI = new EventEmitter();

    @Output('coh-droppable')
    droppableO = new EventEmitter();

    constructor(private elem: ElementRef) {
        $(elem.nativeElement).droppable({
            drop: () => {
                this.droppableO.emit(this.droppableI);
            }
        });
    }

}