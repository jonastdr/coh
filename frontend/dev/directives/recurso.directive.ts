import {Directive, ElementRef, Input} from "@angular/core";
@Directive({
    selector: '[coh-recurso]'
})
export class RecursoDirective {

    @Input('coh-recurso') recurso;

    constructor(el: ElementRef) {
        var self = this;

        $(el.nativeElement).tooltip({
            content: function() {
                var titulo = $(this).attr('title');

                var conteudo = $('<div>')
                    .addClass('recursos_tooltip')
                    .append(
                        $('<p>').text(titulo)
                    );

                if(self.recurso) {
                    conteudo.append(
                        $('<p>').text('Produção por hora: ' + Math.round(self.recurso.valor * (3600 / 60)))
                    );

                    conteudo.append(
                        $('<p>').text('Armazenamento: ' + self.recurso.max)
                    );
                }

                return conteudo;
            }
        });
    }

}