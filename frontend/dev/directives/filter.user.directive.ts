import {Directive, ElementRef, Input} from "@angular/core";
import {Api} from "../api";
@Directive({
    selector: '[filter-user]'
})
export class FilterUserDirective {

    @Input('filter-user')
    filterUser;

    constructor(private elem: ElementRef) {

    }

    ngOnInit() {
        $(this.elem.nativeElement).select2();
    }

}