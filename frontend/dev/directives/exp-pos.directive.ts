import {Directive, Input} from "@angular/core";
@Directive({
    selector: '[coh-exp-pos]',
    host: {
        '[style.left]': 'getPercentLeft()+\'%\'',
        '[style.right]': 'getPercentRight()+\'%\''
    }
})
export class ExpPosDirective {

    @Input('coh-exp-pos')
    atividade = {
        duracao: 1,
        tempo_corrido: 0,
        volta: false
    };

    incremento = 0;

    interval;

    constructor() {
    }

    ngOnInit() {
        this.interval = setInterval(() => {
            this.incremento++;
        }, 1000);
    }

    /**
     * Constroi a porcentagem
     * @returns {number}
     */
    getPercentLeft() {
        if(this.atividade.volta)
            return;

        var calc = (((this.atividade.tempo_corrido + this.incremento) / this.atividade.duracao) * 100);

        if(this.atividade.volta)
            return Math.abs(calc - 100);

        return calc;
    }

    /**
     * Constroi a porcentagem
     * @returns {number}
     */
    getPercentRight() {
        if(!this.atividade.volta)
            return;

        var calc = (((this.atividade.tempo_corrido + this.incremento) / this.atividade.duracao) * 100);

        if(this.atividade.volta)
            return Math.abs(calc - 100);

        return calc;
    }

}