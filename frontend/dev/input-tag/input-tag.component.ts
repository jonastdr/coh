import {Component, Input, ElementRef, forwardRef} from "@angular/core";
import {InputTagService} from "./input-tag.service";
import {InputTagCompleteComponent} from "./input-tag-complete.component";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: 'input-tag',
    template: `
        <tag *ngFor="let tag of tags">
          {{tag.label}}
          <button (click)="removeTag(tag)">&times;</button>
        </tag>
        <input [(ngModel)]="result" (keydown)="keydown($event)" (ngModelChange)="change($event)" />
        <input-tag-complete *ngIf="mostrarItems"
                            [input]="inputElem"
                            [items-complete]="items"
                            [sel-item]="selItem"
                            (select)="select($event)"></input-tag-complete>
    `,
    styles: [
        `:host {
            display: inline-block;
            position: relative;
        }`,
        `input, tag {
            float: left;
            margin: 2px;
            padding: 4px;
        }`,
        `input {
            border: 0;
            background: transparent;
        }`,
        `tag {
            border: 1px solid #ddd;
            border-radius: 10px;
        }`,
        `button {
            border: 0;
            border-radius: 50%;
            width: 14px;
            height: 14px;
        }`
    ],
    providers: [
        InputTagService,
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InputTagComponent),
            multi: true
        }
    ],
    directives: [
        InputTagCompleteComponent
    ]
})
export class InputTagComponent implements ControlValueAccessor {

    private onTouchedCallback: () => void = () => {};
    private onChangeCallback: (_: any) => void = () => {};

    @Input('list-key')
    listKey = 'list';

    @Input('timer')
    timer = 1000;

    /**
     * Url para a busca dos items
     */
    @Input('url')
    url;

    /**
     * Se for um ?GET
     * @type {boolean}
     */
    @Input('is-get')
    isGet = false;

    /**
     * Items disponíveis
     * @type {Array}
     */
    @Input('items')
    items: Array = [];

    /**
     * Texto a pesquisar
     */
    result;

    /**
     * Tags selecionadas
     * @type {Array}
     */
    @Input('tags')
    tags: Array = [];

    /**
     * Indica que a requisição está em progresso
     */
    inprogress;

    /**
     * Define se irá mostrar os itens ao selecionar
     * @type {boolean}
     */
    mostrarItems = false;

    /**
     * Elemento ao que se escreve
     */
    inputElem;

    selItem = 0;

    constructor(private elem: ElementRef, private inputTagService: InputTagService) {
    }

    ngOnInit() {
        this.inputElem = this.elem.nativeElement.querySelector('input');
    }

    /**
     * Adiciona o texto as tags
     * @param ev
     */
    keydown(ev) {
        if(ev.keyCode == 13) {
            var item = this.items[this.selItem];

            if(item && this.mostrarItems) {
                this.tags.push(item);

                this.mostrarItems = false;

                this.result = '';
            }

            return false;
        }
        else if(ev.keyCode == 38) {//UP ARROW
            if(this.selItem > 0)
                this.selItem--;

            return false;
        }
        else if(ev.keyCode == 40) {//DOWN ARROW
            if(this.selItem < (this.items.length - 1))
                this.selItem++;

            return false;
        }
        else if (ev.keyCode == 8) {
            this.mostrarItems = false;

            if(!this.result) {
                if (this.tags.length > 0)
                    this.tags.pop();

                return false;
            }
        }
    }

    /**
     * Seleciona o item
     * @param item
     */
    select(item) {
        this.tags.push(item);

        this.result = '';

        this.mostrarItems = false;

        this.focus();
    }

    /**
     * Coloca o foco no input
     */
    focus() {
        this.elem.nativeElement.querySelector('input').focus();
    }

    /**
     * Ao alterar o texto é executado
     */
    change() {
        if(!this.url) {
            return;
        }

        if(this.inprogress) {
            clearTimeout(this.inprogress);

            this.inprogress = null;
        }

        if(!this.result) {
            return;
        }

        this.inprogress = setTimeout(() => {
            this.inputTagService.autocomplete(this.url, this.result, this.isGet).subscribe((res: any) => {
                if(res.success === true) {
                    this.selItem = 0;

                    this.items = this.itemsFilter(res[this.listKey]);

                    if(this.items.length > 0)
                        this.mostrarItems = true;
                    else
                        this.mostrarItems = false;
                }
            });
        }, this.timer);
    }

    /**
     * Filtra os items
     * @param allItems
     * @returns {Array}
     */
    itemsFilter(allItems) {
        var items = [];

        for (let i = 0; i < allItems.length; i++) {
            var item = JSON.stringify(allItems[i]);

            var encontrado = false;

            for (let j = 0; j < this.tags.length; j++) {
                var tag = JSON.stringify(this.tags[j]);

                if(tag == item) {
                    encontrado = true;

                    break;
                }
            }

            if(!encontrado) {
                items.push(allItems[i]);
            }
        }

        return items;
    }

    /**
     * Remove uma tag já selecionada
     * @param tag
     */
    removeTag(tag) {
        var i = this.tags.indexOf(tag);

        if(i > -1) {
            this.tags.splice(i, 1);
        }

        this.focus();
    }


    /**
     *
     *
     * NgModel
     *
     *
     */
    //From ControlValueAccessor interface
    writeValue(value: any) {
        this.tags = value;
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
}