import {Component, Input, Output, EventEmitter, ElementRef} from "@angular/core";
@Component({
    selector: 'input-tag-complete',
    template: `
    <ul>
      <li *ngFor="let item of items; let i = index" [class]="selItem == i ? 'active' : ''">
        <a (click)="selectItem(item)">{{item.label}}</a>
      </li>
    </ul>
    `,
    styles: [
        `:host {
            position: absolute;
        }`,
        `ul {
            list-style: none;
            color: #21221e;
            border: 1px solid #1f1e1c;
            background-color: #f2f1b4;
            box-shadow: 0 2px 10px -5px #000;
        }`,
        `li {
            padding: 4px 10px;
        }`,
        `li.active {
            color: #FF6100;
        }`
    ]
})
export class InputTagCompleteComponent {

    @Input('items-complete')
    items;

    @Input('input')
    inputElem;

    @Input('sel-item')
    selItem;

    @Output('select')
    select = new EventEmitter();

    constructor(private elem: ElementRef) {
    }

    ngOnInit() {
        var input: any = this.inputElem;

        var elem: any = this.elem.nativeElement;

        elem.style.top = input.clientHeight;
        elem.style.left = input.offsetLeft;
    }

    /**
     * Ação ao selecionar um item
     * @param item
     */
    selectItem(item) {
        this.select.emit(item);
    }

}