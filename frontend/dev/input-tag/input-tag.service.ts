import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
@Injectable()
export class InputTagService {

    constructor(private http: Http) {
    }

    /**
     * Executa o autocomplete trazendo os dados para o input tag
     * @param url
     * @param search
     * @param get
     * @returns {Observable<R>}
     */
    autocomplete(url: string, search: string, get: boolean) {
        var separador = get ? '?' : '/';

        return this.http.get(url + separador + search).map(res => {
            try {
                return res.json();
            } catch (err) {
                return {
                    success: false
                };
            }
        });
    }

}