import {Injectable, EventEmitter} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
import {MainService} from "./main.service";
@Injectable()
export class GameService extends MainService {

    refreshRecurso$: EventEmitter;
    refreshSidebar$: EventEmitter;
    refreshCidades$: EventEmitter;
    refreshNotificacoes$: EventEmitter;
    refreshCity$: EventEmitter;

    constructor(
        private http: Http
    ) {
        this.refreshRecurso$ = new EventEmitter();
        this.refreshSidebar$ = new EventEmitter();
        this.refreshCidades$ = new EventEmitter();
        this.refreshNotificacoes$ = new EventEmitter();
        this.refreshCity$ = new EventEmitter();
    }

    /**
     * Traz os dados de recursos
     * @returns {Observable<R>}
     */
    recursos() {
        return this.http.get(Api.url('game/recursos/data')).map(res => {
            return res.json();
        });
    }

    /**
     * Traz todas as notificações
     * @returns {Observable<R>}
     */
    notificacoes() {
        return this.http.get(Api.url('game/notificacoes/data')).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Remove uma notificação
     * @param notificacao
     * @returns {Observable<R>}
     */
    removeNotificacao(notificacao) {
        var api = new Api();

        api.body.set('id_notificacao', notificacao.id);

        return this.http.post(Api.url('game/notificacoes/remover'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        })
    }

    /**
     * Traz os dados das cidades
     * @returns {Observable<R>}
     */
    cidades() {
        return this.http.get(Api.url('game/cidades/data')).map(res => {
            return res.json();
        });
    }

    /**
     * Seleciona a cidade
     * @param cidade
     * @returns {Observable<R>}
     */
    selecionarCidade(cidade) {
        var api = new Api();

        api.body.set('id', cidade.id);

        return this.http.post(Api.url('game/cidade/selecionar'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Traz os dados do sidebar
     * @returns {Observable<R>}
     */
    sidebar() {
        return this.http.get(Api.url('sidebar/data')).map(res => {
            return res.json();
        })
    }

    /**
     * Retorna uma lista de usuários
     * @param nome
     * @returns {Observable<R>}
     */
    buscarUsuarios(nome) {
        return this.http.get(Api.url('buscar_usuarios/' + nome)).map(res => {
            return res.json();
        });
    }

    /**
     * Altera o nome da cidade que está selecionada
     * @param nome
     * @returns {Observable<R>}
     */
    alterarNomeCidade(nome) {
        var api = new Api();

        api.body.set('nome', nome);

        return this.http.post(Api.url('game/alterar_nome_cidade'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Finaliza a sessão
     * @returns {Observable<R>}
     */
    logout() {
        return this.http.get(Api.url('logout')).map(res => {
            return res.json();
        });
    }

}