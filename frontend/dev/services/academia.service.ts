import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class AcademiaService {

    constructor(private http: Http) {

    }

    /**
     * Retorna os dados inicias da tela
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('academia/get')).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna os dados do heroi
     * @param heroi
     * @returns {Observable<R>}
     */
    getHeroi(heroi) {
        return this.http.get(Api.url('academia/get_heroi/' + heroi.id)).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna os dados da funcao
     * @param funcao
     * @returns {Observable<R>}
     */
    getFuncao(funcao) {
        return this.http.get(Api.url('academia/get_funcao/' + funcao.id)).map(res => {
            return res.json();
        });
    }

    /**
     * Ativa um heroi em uma cidade
     * @param heroi
     * @returns {Observable<R>}
     */
    ativarHeroi(heroi) {
        var api = new Api();

        api.body.set('id', heroi.id);

        return this.http.post(Api.url('academia/ativar/heroi'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Ativa uma função de um heroi
     * @param funcao
     * @returns {Observable<R>}
     */
    ativarFuncao(funcao) {
        var api = new Api();

        api.body.set('id', funcao.id);

        return this.http.post(Api.url('academia/ativar/funcao'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

}