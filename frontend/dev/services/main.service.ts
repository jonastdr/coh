export abstract class MainService {

    /**
     * Caso a requisição não seja JSON retorna uma mensagem de erro para o usuário
     * @param res
     * @returns {any}
     */
    intercept(res) {
        try {
            return res.json();
        } catch (err) {
            return {
                success: false,
                msg: 'Parece que algo deu errado aqui.'
            };
        }
    }

}