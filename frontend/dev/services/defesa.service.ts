import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()

export class DefesaService {

    constructor(
        private http: Http
    ) {

    }

    /**
     * Retorna os dados da tela de defesa
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('defesa/get')).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna os dados referentes ao que estão na cidade
     * @returns {Observable<R>}
     */
    cidade() {
        return this.http.get(Api.url('defesa/cidade_data')).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna os dados referentes ao que estão na fora cidade
     * @returns {Observable<R>}
     */
    expedicao() {
        return this.http.get(Api.url('defesa/expedicao_data')).map(res => {
            return res.json();
        });
    }

    /**
     * Manda a ação de regresso das unidades
     * @param defesa
     * @returns {Observable<R>}
     */
    regressar(defesa) {
        return this.http.get(Api.url('defesa/regressar/' + defesa.id)).map(res => {
            return res.json();
        });
    }

}