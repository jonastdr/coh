import {Injectable, EventEmitter} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class OverviewService {

    overview$ = new EventEmitter();

    constructor(private http: Http) {
    }

    /**
     * Retorna o json do layout da cidade
     * @param id
     * @returns {Observable<R>}
     */
    getLayout(id) {
        return this.http.get(Api.url('vista_geral/' + id + '.map')).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna todas as informações da tela
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('overview/get')).map(res => {
            return res.json();
        });
    }

    /**
     * Insere no mapa uma nova construção
     * @param construcao
     * @returns {Observable<R>}
     */
    insert(construcao) {
        return this.http.get(Api.url('overview/insere/' + construcao.id + '/' + construcao.slot)).map(res => {
            return res.json();
        });
    }

}