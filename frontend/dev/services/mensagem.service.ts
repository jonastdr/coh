import {Injectable} from "@angular/core";
import {Api} from "../api";
import {Http} from "@angular/http";
import {MainService} from "./main.service";
@Injectable()

export class MensagemService extends MainService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('mensagem/get')).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Lista as mensagens por um determinado tipo
     * @param tipo
     * @param pag
     * @param qtde
     * @returns {Observable<R>}
     */
    list(tipo? : number, pag?: number, qtde?: number) {
        var strGet: string = "";

        if(tipo)
            strGet += "/" + tipo;

        if(pag)
            strGet += "/" + pag;

        if(qtde)
            strGet += "/" + qtde;

        return this.http.get(Api.url('mensagem/listar' + strGet)).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna o grupo de mensagens na visualização
     * @param mensagem
     * @returns {Observable<R>}
     */
    mensagensVisualizar(mensagem) {
        return this.http.get(Api.url('mensagem/visualizar/' + mensagem.id)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Responde um grupo de mensagens
     * @param mensagem
     * @returns {Observable<R>}
     */
    responder(mensagem) {
        var api = new Api();

        api.body.set('assunto', mensagem.assunto);
        api.body.set('mensagem', mensagem.mensagem);

        return this.http.post(Api.url('mensagem/responder/' + mensagem.id), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Remove o grupo ode mensagens do usuário
     * @param mensagens
     * @returns {Observable<R>}
     */
    excluir(mensagens) {
        var api: Api = new Api();

        for (var i in mensagens) {
            var mensagem = mensagens[i];

            api.body.append('msg[]', mensagem.id);
        }

        return this.http.post(Api.url('mensagem/excluir'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Cria uma nova mensagem
     * @param mensagem
     * @returns {Observable<R>}
     */
    nova(mensagem) {
        var api: Api = new Api();

        for (var i in mensagem.usuarios) {
            var usuario = mensagem.usuarios[i];

            api.body.append('usuario[]', usuario.id);
        }

        api.body.set('assunto', mensagem.assunto);
        api.body.set('mensagem', mensagem.mensagem);

        return this.http.post(Api.url('mensagem/nova'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        })
    }
}