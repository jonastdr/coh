import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class ConfiguracoesService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('configuracoes/get')).map(res => {
            return res.json();
        });
    }

}