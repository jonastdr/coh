import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
import {MainService} from "./main.service";
@Injectable()
export class AliancaService extends MainService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('alianca/get')).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Altera alguma propriedade da aliança
     * @param acao
     * @param value
     * @returns {Observable<R>}
     */
    propriedades(acao, value) {
        let api = new Api();

        api.body.set('acao', acao);
        api.body.set('value', value);

        return this.http.post(Api.url('alianca/propriedades'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        })
    }

    /**
     * Realiza uma busca por alianças
     * @param texto
     * @returns {Observable<R>}
     */
    buscarAlianca(texto) {
        return this.http.get(Api.url('buscar_aliancas/' + texto)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Cria uma nova aliança
     * @param texto
     * @returns {Observable<R>}
     */
    criarAlianca(texto) {
        let api = new Api();

        api.body.set('texto', texto);

        return this.http.post(Api.url('alianca/nova'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Envia um pedido de recrutamento
     * @param alianca
     * @param mensagem
     * @returns {Observable<R>}
     */
    alistar(alianca, mensagem: string) {
        let api = new Api();

        api.body.set('id_alianca', alianca.id);
        api.body.set('mensagem', mensagem);

        return this.http.post(Api.url('alianca/alistar'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Aceita o recrutamento
     * @param usuario
     * @returns {Observable<R>}
     */
    aceitarRecrutamento(usuario) {
        return this.http.get(Api.url('alianca/recrutamento_aceitar/' + usuario.id)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Rejeita o recrutamento
     * @param usuario
     * @returns {Observable<R>}
     */
    rejeitarRecrutamento(usuario) {
        return this.http.get(Api.url('alianca/recrutamento_rejeitar/' + usuario.id)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Retorna os níveis cadastrados na aliança
     * @returns {Observable<R>}
     */
    niveis() {
        return this.http.get(Api.url('alianca/nivel/data')).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Cria um novo nível
     * @param form
     * @returns {Observable<R>}
     */
    novoNivel(form) {
        let api = new Api();

        api.body.set('nome', form.nome);

        return this.http.post(Api.url('alianca/nivel/novo'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Edita um nível
     * @param nivel
     * @returns {Observable<R>}
     */
    editaNivel(nivel) {
        let api = new Api();

        api.body.set('nome', nivel.nome);

        for(let i in nivel.permissoes) {
            var permissao = nivel.permissoes[i];

            if(permissao.checked === true)
                api.body.append('permissoes[]', permissao.cod);
        }

        return this.http.post(Api.url('alianca/nivel/editar/' + nivel.id), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Remove um nível
     * @param nivel
     * @returns {Observable<R>}
     */
    removerNivel(nivel) {
        return this.http.get(Api.url('alianca/nivel/remover/' + nivel.id)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Altera o nível de um membro
     * @param membro
     * @returns {Observable<R>}
     */
    alterarMembroNivel(membro) {
        let api = new Api();

        api.body.set('id_nivel', membro.id_nivel);

        return this.http.post(Api.url('alianca/membro_nivel/' + membro.id), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Abandona a aliança
     * @returns {Observable<R>}
     */
    abandonar() {
        return this.http.get(Api.url('alianca/abandonar')).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Exclui a aliança
     * @returns {Observable<R>}
     */
    excluir() {
        return this.http.get(Api.url('alianca/excluir')).map(res => {
            return this.intercept(res);
        });
    }

}