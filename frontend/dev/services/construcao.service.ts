import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class ConstrucaoService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados da tela de construção
     * @returns {Observable<R>}
     */
    getAll(construcao) {
        return this.http.get(Api.url('construcao/get/' + construcao.id)).map(res => {
            return res.json();
        });
    }

    /**
     * Faz uma melhoria na construção
     * @param construcao
     * @returns {Observable<Response>}
     */
    melhorar(construcao) {
        var api = new Api();

        api.body.set('id', construcao.id);

        return this.http.post(Api.url('construcao/melhorar'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Cancela uma melhoria
     * @param construcao
     * @returns {Observable<Response>}
     */
    cancelar(construcao) {
        var api = new Api();

        api.body.set('id', construcao.id);

        return this.http.post(Api.url('construcao/cancelar'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Destrói uma construção
     * @param construcao
     * @returns {Observable<Response>}
     */
    destruir(construcao) {
        var api = new Api();

        api.body.set('id', construcao.id);

        return this.http.post(Api.url('construcao/destruir'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

}