import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
import {MainService} from "./main.service";
@Injectable()
export class ExpedicaoService extends MainService {

    constructor(private http: Http) {

    }

    /**
     * Retorna os dados inicias da tela
     * @param cidade
     * @returns {Observable<R>}
     */
    getAll(cidade) {
        return this.http.get(Api.url('expedicao/missao/' + cidade.cod_ilha + '/' + cidade.cod_cidade)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Faz o envio da expedição
     * @param form
     * @returns {Observable<R>}
     */
    enviar(form) {
        var api = new Api();

        api.body.set('cod_ilha', form.cod_ilha);
        api.body.set('cod_cidade', form.cod_cidade);

        for (let i in form.unidades) {
            var qt = form.unidades[i];

            if(qt)
                api.body.set('unidades[' + i + ']', qt);
        }

        api.body.set('dinheiro', form.dinheiro);
        api.body.set('alimento', form.alimento);
        api.body.set('metal', form.metal);
        api.body.set('petroleo', form.petroleo);

        api.body.set('missao', form.missao);

        return this.http.post(Api.url('expedicao/enviar'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

}