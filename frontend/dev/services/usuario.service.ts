import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class UsuarioService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @returns {Observable<R>}
     */
    getAll(usuario) {
        return this.http.get(Api.url('usuario/get/' + usuario.id)).map(res => {
            return res.json();
        });
    }

    /**
     * Altera as configurações do usuário
     * @param usuario
     * @returns {Observable<R>}
     */
    alterarConfig(usuario) {
        var api = new Api();

        api.body.set('nome', usuario.usuario);
        api.body.set('email', usuario.email);
        api.body.set('senha', usuario.senha);

        return this.http.post(Api.url('usuario/alterar'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Faz a alteração de senha de um usuário
     * @param usuario
     * @returns {Observable<R>}
     */
    alterarSenha(usuario) {
        var api = new Api();

        api.body.set('senha_anterior', usuario.senha_anterior);
        api.body.set('senha', usuario.senha);

        return this.http.post(Api.url('usuario/alterar_senha'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    /**
     * Usuário é inativado do jogo
     * @returns {Observable<R>}
     */
    abandonar() {
        return this.http.get(Api.url('usuario/abandonar')).map(res => {
            return res.json();
        });
    }

    /**
     * Ativa a opção de férias para o jogador
     * @returns {Observable<R>}
     */
    ferias() {
        return this.http.get(Api.url('usuario/ferias')).map(res => {
            return res.json();
        });
    }

}