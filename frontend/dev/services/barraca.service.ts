import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class BarracaService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @param id_tipo
     * @returns {Observable<R>}
     */
    getAll(id_tipo: number) {
        return this.http.get(Api.url('barraca/get/' + id_tipo)).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna os dados da unidade
     * @param unidade
     * @returns {Observable<R>}
     */
    get(unidade) {
        return this.http.get(Api.url('barraca/get_unidade/' + unidade.id)).map(res => {
            return res.json();
        });
    }

    /**
     * Lista as unidades que estão em recrutamento
     * @returns {Observable<R>}
     */
    recrutamento() {
        return this.http.get(Api.url('barraca/recrutamento')).map(res => {
            return res.json();
        });
    }

    /**
     * Recruta as undiades
     * @param unidade
     * @returns {Observable<R>}
     */
    recrutar(unidade) {
        var api = new Api();

        api.body.set('id', unidade.id);
        api.body.set('quantidade', unidade.quantidade);

        return this.http.post(Api.url('barraca/recrutar'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json()
        });
    }

}