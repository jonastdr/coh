import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
import {MainService} from "./main.service";
@Injectable()
export class EspionagemService extends MainService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @param cidade
     * @returns {Observable<R>}
     */
    getAll(cidade) {
        return this.http.get(Api.url('espionagem/missao/' + cidade.cod_ilha + '/' + cidade.cod_cidade)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Retorna todos os dados para a construção do relatório de espionagem
     * @param codigo
     * @returns {Observable<R>}
     */
    getRelatorio(codigo) {
        return this.http.get(Api.url('espionagem/relatorio/' + codigo)).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Retorna os dados de armazenamento
     * @returns {Observable<R>}
     */
    getArmazem() {
        return this.http.get(Api.url('espionagem/armazem')).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Faz o envio da espionagem
     * @param form
     * @returns {Observable<R>}
     */
    enviar(form) {
        var api = new Api();

        api.body.set('cod_ilha', form.cod_ilha);
        api.body.set('cod_cidade', form.cod_cidade);

        api.body.set('dinheiro', form.dinheiro);

        return this.http.post(Api.url('espionagem/enviar'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

    /**
     * Armazena dinheiro na cidade para missões de espionagem
     * @param form
     * @returns {Observable<R>}
     */
    armazenar(form) {
        var api = new Api();

        api.body.set('quantidade', form.quantidade);

        return this.http.post(Api.url('espionagem/armazenar'), api.body, {
            headers: api.headers
        }).map(res => {
            return this.intercept(res);
        });
    }

}