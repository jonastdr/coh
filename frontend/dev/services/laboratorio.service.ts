import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";

@Injectable()
export class LaboratorioService {

    constructor(private http: Http) {
    }

    /**
     * Retorna os dados inicias da tela
     * @returns {Observable<R>}
     */
    getAll() {
        return this.http.get(Api.url('laboratorio/get')).map(res => {
            return res.json();
        });
    }

    /**
     * Retorna os dados da pesquisa
     * @param pesquisa
     * @returns {Observable<R>}
     */
    get(pesquisa) {
        return this.http.get(Api.url('laboratorio/get/' + pesquisa.id)).map(res => {
            return res.json();
        });
    }

    /**
     * Coloca a pesquisa em fila
     * @param pesquisa
     * @returns {Observable<R>}
     */
    pesquisar(pesquisa) {
        var api = new Api();

        api.body.set('id', pesquisa.id);

        return this.http.post(Api.url('laboratorio/pesquisar'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json()
        })
    }

}