import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
import {MainService} from "./main.service";
@Injectable()
export class RelatorioService extends MainService {

    constructor(private http: Http) {

    }

    /**
     * Retorna todos os dados para construção do relatório
     * @returns {Observable<R>}
     */
    getAll(codigo) {
        return this.http.get(Api.url('relatorio/get/' + codigo)).map(res => {
            return this.intercept(res);
        });
    }

}