import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Api} from "../api";
@Injectable()
export class MapaService {

    constructor(private http: Http) {
    }

    /**
     *
     * @param radius
     * @returns {Observable<R>}
     */
    push(radius) {
        var api = new Api();

        api.body.set('x_start', radius.x_start);
        api.body.set('x_end', radius.x_end);

        api.body.set('y_start', radius.y_start);
        api.body.set('y_end', radius.y_end);

        return this.http.post(Api.url('mapa/push'), api.body, {
            headers: api.headers
        }).map(res => {
            return res.json();
        });
    }

    pushInitial() {
        return this.http.get(Api.url('mapa/push_initial')).map(res => {
            return res.json();
        });
    }

}