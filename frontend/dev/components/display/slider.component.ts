import {Component, ElementRef, Input, Output, EventEmitter} from "@angular/core";

@Component({
    selector: 'coh-slider',
    template: `
        <div class="value">{{value}}</div>
        <div class="slider"></div>
    `,
    styles: [`
        .value {
            text-align: center;
            font-size: 15px;
            margin: 5px;
        }
        .slider.ui-slider {
            font-size: 10px
        }
    `]
})

export class SliderComponent {

    value: number = 0;

    @Output()
    change = new EventEmitter();

    @Input('min')
    min;

    @Input('max')
    max;

    constructor(private elementRef: ElementRef) {
    }

    ngOnInit() {
        $(this.elementRef.nativeElement).find(".slider").slider({
            range: false,
            min: this.min,
            max: this.max,
            value: this.value,
            slide: (event, ui) => {
                this.value = ui.value;
            },
            stop: (event, ui) => {
                this.change.emit(ui.value);
            }
        });
    }
}