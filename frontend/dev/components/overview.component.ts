import {Component} from "@angular/core";
import {Api} from "../api";
import {OverviewService} from "../services/overview.service";
import {DragConstrucaoDirective} from "../directives/drag.construcao.directive";
import {DroppableDirective} from "../directives/droppable.directive";
import {Game} from "../game";
import {GameService} from "../services/game.service";
import {AppComponent} from "./app.component";
@Component({
    selector: 'coh-overview',
    templateUrl: Api.url('overview'),
    directives: [
        DragConstrucaoDirective,
        DroppableDirective
    ]
})
export class OverviewComponent {

    canvas;
    c;

    telhas = [];

    masks = [];

    construcoes = [];
    construcoesPendentes = [];

    emiterOverview;

    constructor(private overviewService: OverviewService, private gameService: GameService) {
        this.carregarImagens();
    }

    ngOnInit() {
        this.startOverview();

        /**
         * Cria uma escuta para atualização da vista geral em outra tela
         */
        this.emiterOverview = this.overviewService.overview$.subscribe(() => {
            this.startOverview();
        });
    }

    ngOnDestroy() {
        this.emiterOverview.unsubscribe();
    }

    /**
     * Cria os dados da tela
     */
    startOverview() {
        this.masks = [];

        this.canvas = document.getElementById('overview_canvas');
        this.c = this.canvas.getContext('2d');

        $('#overview_content').draggable({
            drag: (e, ui) => {
                //resize container here
                var w1 = ui.helper.outerWidth(),
                    w2 = -1400 + $(window).width();

                var h1 = ui.helper.outerHeight(),
                    h2 = -800 + $(window).height();
                ui.position.left = Math.min(Math.max(ui.position.left, w2 - w1), 100);

                ui.position.top = Math.min(Math.max(ui.position.top, h2 - h1), 100);
            }
        });

        this.overviewService.getAll().subscribe(res => {
            if(res.success === true) {
                this.construcoes = res.construcoes;
                this.construcoesPendentes = res.construcoesPendentes;

                this.criarCanvas();
            }
        });
    }

    /**
     * Insere as construções nos slots
     */
    slotConstrucoes() {
        for (var i in this.construcoes) {
            var construcao = this.construcoes[i];

            var slot = construcao.slot;

            this.masks[slot - 1].classe = 'ov_bloco_bloq';
            this.masks[slot - 1].construcao = construcao;
        }
    }

    /**
     * Cria o canvas que é o fundo da vista geral da cidade
     */
    criarCanvas() {
        this.overviewService.getLayout(1).subscribe(mapa => {
            var grid = {
                width: mapa[0].length,
                height: mapa.length
            };

            // Desenhando o mapa
            this.c.clearRect (0, 0, this.canvas.width, this.canvas.height);

            var slot = 0;

            // Array responsavel pela construcao do mapa
            for (var x = 0; x < grid.height; x++) {
                for (var y = 0; y < grid.width; y++) {

                    var tilePositionX = (y - x) * this.telhas[0].height;

                    tilePositionX += (this.canvas.width / 2) - (this.telhas[0].width / 2);

                    var tilePositionY = (y + x) * (this.telhas[0].height / 2);

                    for (let i in this.telhas) {
                        if(i == mapa[x][y]) {
                            var telha = this.telhas[i];

                            this.c.drawImage(telha, Math.round(tilePositionX), Math.round(tilePositionY), telha.width, telha.height);

                            this.createMasks(++slot, x, y, i);
                        }
                    }
                }
            }

            /**
             * Insere as construções na vista geral
             */
            this.slotConstrucoes();
        });
    }

    /**
     * Cria as mascaras do mapa
     * @param slot
     * @param i
     * @param j
     * @param telha
     */
    createMasks(slot, i, j, telha) {
        var left = (j - i) * 64;
        left += (2560 / 2) - (128 / 2);

        var top = (j + i) * (64 / 2);

        var classe;

        if(telha == 0) {
            classe = 'ov_bloco_bloq';
        } else {
            classe = 'ov_bloco';
        }

        this.masks.push({
            slot: slot,
            left: left,
            top: top,
            classe: classe
        });
    }

    /**
     * Insere uma construção no mapa
     * @param construcao
     */
    insereConstrucao(construcao) {
        this.overviewService.insert(construcao).subscribe(res => {
            if(res.success === true) {
                this.construcoes = res.construcoes;
                this.construcoesPendentes = res.construcoesPendentes;

                this.gameService.refreshRecurso$.emit();
                this.gameService.refreshSidebar$.emit();

                this.slotConstrucoes();
            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * Ação ao terminar uma construção
     */
    terminaConstrucao() {
        this.overviewService.getAll().subscribe(res => {
            if(res.success === true) {
                this.construcoes = res.construcoes;
                this.construcoesPendentes = res.construcoesPendentes;

                this.slotConstrucoes();
            }
        });
    }

    /**
     * Carrega as imagens no DOM
     */
    carregarImagens() {

        this.telhas[0] = new Image();
        this.telhas[0].src = "imagens/overview/water.png";

        this.telhas[1] = new Image();
        this.telhas[1].src = "imagens/overview/grass.png";

        this.telhas[2] = new Image();
        this.telhas[2].src = "imagens/overview/grass2.png";

        this.telhas[3] = new Image();
        this.telhas[3].src = "imagens/overview/grass3.png";

        this.telhas[4] = new Image();
        this.telhas[4].src = "imagens/overview/grass10.png";

        this.telhas[5] = new Image();
        this.telhas[5].src = "imagens/overview/watera.png";

        this.telhas[6] = new Image();
        this.telhas[6].src = "imagens/overview/waterb.png";
    }

    construcaoImage(construcao) {
        return 'url(imagens/construcoes/' + construcao.id + '.png)';
    }

    /**
     * Abre a janela da construção
     * @param construcao
     */
    openConstrucao(construcao) {
        AppComponent.openWindow('construcao', construcao);
    }

}