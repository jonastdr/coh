import {Component} from "@angular/core";
import {Api} from "../api";
import {GameService} from "../services/game.service";
import {Game} from "../game";
import {TimerDirective} from "../directives/timer.directive";
import {ClickoutDirective} from "../directives/clickout.directive";
import {AppComponent} from "./app.component";
import {OverviewService} from "../services/overview.service";
@Component({
    selector: 'coh-cidades',
    templateUrl: Api.url('game/cidades'),
    directives: [
        TimerDirective,
        ClickoutDirective
    ]
})
export class CidadesComponent {

    cidadeEdit = false;
    cidadeNome;

    usuario = {
        cidade_nome: ''
    };
    cidades = [];
    atividades = [];

    timeout;

    menuCidadeAberto = false;
    menuAtividadeAberto = false;

    constructor(private gameService: GameService,
                private overviewService: OverviewService) {
    }

    ngOnInit() {
        /**
         * escuta da atualização dos recursos
         */
        this.gameService.refreshCidades$.subscribe(() => {
            this.get();
        });

        this.get();
    }

    /**
     * Retorna os dados para o painel
     */
    get() {
        this.gameService.cidades().subscribe(res => {
            if(res.success === true) {
                this.usuario = res.usuario;
                this.cidades = res.cidades;
                this.atividades = res.atividades;
            }
        });
    }

    /**
     * Aciona ao terminar uma expedição
     */
    terminou() {
        this.get();

        this.gameService.refreshNotificacoes$.emit();
    }

    /**
     * Mostra as cidades do usuário logado
     * @param ev
     */
    mostrarCidades(ev) {
        $("#menu_cidades").addClass("ativo");

        this.timeout = setTimeout(() => {
            this.menuCidadeAberto = true;
        }, 100);
    }

    /**
     * Mostra as cidades do usuário logado
     * @param ev
     */
    mostrarAtividades(ev) {
        $("#menu_atividade").addClass("ativo");

        this.timeout = setTimeout(() => {
            this.menuAtividadeAberto = true;
        }, 100);
    }

    /**
     * Altera a cidade
     * @param cidade
     */
    selecionarCidade(cidade) {
        this.hideMenuCidades();

        this.gameService.selecionarCidade(cidade).subscribe(res => {
            if(res.success === true) {
                this.gameService.refreshCity$.emit(res.cidade);

                AppComponent.destroyAllWindows();

                Game.mensagem(res.msg);

                this.gameService.refreshRecurso$.emit();
                this.gameService.refreshSidebar$.emit();
                this.gameService.refreshCidades$.emit();
                this.overviewService.overview$.emit();
            }
        });
    }

    /**
     * escode as cidades
     */
    hideMenuCidades() {
        if(this.menuCidadeAberto) {
            $("#menu_cidades").removeClass("ativo");

            this.menuCidadeAberto = false;
        }
    }

    /**
     * Esconde as atividades
     */
    hideAtividades() {
        if(this.menuAtividadeAberto) {
            $("#menu_atividade").removeClass("ativo");

            this.menuAtividadeAberto = false;
        }
    }

    /**
     * Faz a ação de renomear a cidade
     */
    renomearCidade() {
        if(this.cidadeEdit === false) {
            this.cidadeEdit = true;

            this.cidadeNome = this.usuario.cidade_nome;
        } else {
            this.gameService.alterarNomeCidade(this.cidadeNome).subscribe(res => {
                if(res.success === true) {
                    this.cidadeEdit = false;

                    this.gameService.refreshCidades$.emit();
                }

                Game.mensagem(res.msg);
            });
        }
    }

    /**
     * Ao pressionar ENTER altera o nome da cidade
     * @param ev
     */
    cidadeEditPress(ev) {
        if(ev.keyCode == 13) {
            this.gameService.alterarNomeCidade(this.cidadeNome).subscribe(res => {
                if(res.success === true) {
                    this.cidadeEdit = false;

                    this.gameService.refreshCidades$.emit();
                }

                Game.mensagem(res.msg);
            });
        }
    }

}