import {Component, ElementRef, OnInit} from "@angular/core";
import {Api} from "../../api";
import {BarracaService} from "../../services/barraca.service";
import {Game} from "../../game";
import {FORM_DIRECTIVES} from "@angular/forms";
import {TimerDirective} from "../../directives/timer.directive";
import {GameService} from "../../services/game.service";
@Component({
    selector: 'coh-barraca',
    templateUrl: Api.url('barraca'),
    attributes: [

    ],
    directives: [
        FORM_DIRECTIVES,
        TimerDirective
    ]
})

export class BarracaComponent implements OnInit {

    params;

    recrutados = [];
    unidades = [];
    unidade = {
        construcao_requisitos: [],
        pesquisa_requisitos: []
    };

    quantidade: number = 1;

    constructor(private elem: ElementRef,
                private barracaService: BarracaService,
                private gameService: GameService) {
        $(elem.nativeElement).css({
            visibility: 'hidden'
        });
    }

    ngOnInit() {
        this.barracaService.getAll(this.params.id_tipo).subscribe(res => {
            if(res.success === true) {
                this.recrutados = res.recrutados;
                this.unidades = res.unidades;
                this.unidade = res.unidade;

                $(this.elem.nativeElement).css({
                    visibility: ''
                });
            }
        });
    }

    /**
     * abre os dados de uma unidade
     * @param unidade
     */
    openUnidade(unidade) {
        this.barracaService.get(unidade).subscribe(res => {
            if(res.success === true) {
                this.unidade = res.unidade;
            }
        });
    }

    /**
     * Recruta unidade
     * @param unidade
     */
    recrutar(unidade) {
        unidade.quantidade = this.quantidade;

        this.barracaService.recrutar(unidade).subscribe(res => {
            if(res.success == true) {
                this.gameService.refreshRecurso$.emit();
                this.gameService.refreshSidebar$.emit();

                this.recrutados = res.recrutados;

                this.quantidade = 1;
            }

            Game.mensagem(res.msg, '', 2000);
        });
    }

    /**
     * Incrementa a quantidade
     */
    mais() {
        if(this.quantidade > 20)
            return;

        this.quantidade++;
    }

    /**
     * Decrementa a quantidade
     */
    menos() {
        if(this.quantidade <= 1)
            return;

        this.quantidade--;
    }

    /**
     * Imagem da unidade
     * @param unidade
     * @returns {string}
     */
    unidadeImage(unidade) {
        return "url('imagens/unidades/" + unidade.id + ".png')";
    }

    /**
     * Imagem da pesquisa
     * @param unidade
     * @returns {string}
     */
    pesquisaImage(unidade) {
        return "url('imagens/pesquisas/" + unidade.id + ".png')";
    }

    /**
     * Imagem da unidade recrutado
     * @param unidade
     * @returns {string}
     */
    unidadeRecrutamentoImage(unidade) {
        return "url('imagens/unidades/" + unidade.id_unidade + ".png')";
    }

    /**
     * Imagem da construção
     * @param construcao
     * @returns {string}
     */
    construcaoImage(construcao) {
        return 'url(imagens/construcoes/' + construcao.id + '.png)';
    }

    /**
     * Imagem de quando não é uma unidade | usado no recrutamento
     * @returns {string}
     */
    unidadeEmpty() {
        return "url('imagens/unidades/empty.png')";
    }

    /**
     * Traz os dados de recrutamento
     */
    recrutamento() {
        this.barracaService.getAll(this.params.id_tipo).subscribe(res => {
            if(res.success === true) {
                this.recrutados = res.recrutados;
                this.unidades = res.unidades;
                this.unidade = res.unidade;
            }
        });
    }

    /**
     * Ação ao terminar de recrutar
     */
    termino(ev) {
        this.recrutamento();
    }

}