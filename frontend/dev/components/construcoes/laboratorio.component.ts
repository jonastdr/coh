import {Component, ElementRef} from "@angular/core";
import {Api} from "../../api";
import {LaboratorioService} from "../../services/laboratorio.service";
import {Game} from "../../game";
import {GameService} from "../../services/game.service";
import {TimerDirective} from "../../directives/timer.directive";

@Component({
    selector: 'coh-laboratorio',
    templateUrl: Api.url('laboratorio'),
    directives: [
        TimerDirective
    ]
})

export class LaboratorioComponent {

    /**
     * Toas as pesquisas disponíveis
     */
    public pesquisas: Array;

    /**
     * Pesquisa selecionada
     */
    public pesquisa = {
        construcao_requisitos: [],
        pesquisa_requisitos: []
    };

    public acao: string;
    public tempo: string;

    constructor(private laboratorioService: LaboratorioService,
                private gameService: GameService) {
    }

    ngOnInit() {
        this.getAll();
    }

    getAll() {
        this.laboratorioService.getAll().subscribe(res => {
            if(res.success === true) {
                this.pesquisas = res.pesquisas;
                this.pesquisa = res.pesquisa;
                this.tempo = res.tempo;
                this.acao = res.acao;
            } else {
            }
        });
    }

    /**
     * Abre os dados de uma determinada pesquisa
     * @param pesquisa
     */
    openPesquisa(pesquisa) {
        this.laboratorioService.get(pesquisa).subscribe(res => {
            if(res.success === true) {
                this.pesquisa = res.pesquisa;
                this.acao = res.acao;
                this.tempo = res.tempo;
            }
        });
    }

    /**
     * Aciona para subir de level a pesquisa
     * @param pesquisa
     */
    pesquisar(pesquisa) {
        if(this.acao == 'Pesquisando') {
            Game.mensagem('Existe uma pesquisa em andamento.');

            return;
        }

        this.laboratorioService.pesquisar(pesquisa).subscribe(res => {
            if(res.success == true) {
                this.gameService.refreshRecurso$.emit();
                this.gameService.refreshSidebar$.emit();
            }

            this.tempo = res.tempo;
            this.acao = res.acao;

            Game.mensagem(res.msg);
        });
    }

    /**
     * Imagem da pesquisa
     * @param pesquisa
     * @returns {string}
     */
    pesquisaImage(pesquisa) {
        return "url('imagens/pesquisas/" + pesquisa.id + ".png')";
    }

    /**
     * Imagem da construção
     * @param construcao
     * @returns {string}
     */
    construcaoImage(construcao) {
        return "url('imagens/construcoes/" + construcao.id + ".png')";
    }

}