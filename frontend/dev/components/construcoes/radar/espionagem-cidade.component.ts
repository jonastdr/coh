import {Component} from "@angular/core";
import {Api} from "../../../api";
import {EspionagemService} from "../../../services/espionagem.service";
import {Game} from "../../../game";
@Component({
    selector: 'coh-espionagem-cidade',
    templateUrl: Api.url('espionagem/cidade')
})

export class EspionagemCidadeComponent {

    imagem: string = 'url("imagens/construcoes/7.png")';

    quantidade_maxima: number = null;
    pesquisa_nivel: number = 0;
    armazenado: number = 0;

    form = {
        quantidade: 0
    };

    constructor(private espionagemService: EspionagemService) {
    }

    ngOnInit() {
        this.espionagemService.getArmazem().subscribe(res => {
            if(res.success === true) {
                this.quantidade_maxima = res.quantidade_maxima;
                this.pesquisa_nivel = res.pesquisa_nivel;
                this.armazenado = res.armazenado;
            }
        });
    }

    /**
     * Armazena uma quantidade de dinheiro para uso de espionagem
     * @param form
     */
    armazenar(form) {
        if(form.quantidade == 0) {
            Game.mensagem('Informe uma quantidade.');

            return;
        }

        this.espionagemService.armazenar(form).subscribe(res => {
            if(res.success === true) {
                this.quantidade_maxima = res.quantidade_maxima;
                this.form.quantidade = 0;
            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * Atualiza a quantidade do formulário
     * @param quantidade
     */
    quantidade(quantidade) {
        this.form.quantidade = quantidade;
    }

}