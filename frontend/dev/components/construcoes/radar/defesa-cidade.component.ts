import {Component} from "@angular/core";
import {DefesaService} from "../../../services/defesa.service";
import {Game} from "../../../game";
import {Api} from "../../../api";
import {GameService} from "../../../services/game.service";
@Component({
    selector: 'coh-defesa-cidade',
    templateUrl: Api.url('defesa/cidade')
})
export class DefesaCidadeComponent {

    defesas: Array = [];

    constructor(private defesaService: DefesaService,
                private gameService: GameService) {
    }

    ngOnInit() {
        this.getAll();
    }

    getAll() {
        this.defesaService.cidade().subscribe(res => {
            if(res.success === true) {
                this.defesas = res.defesas;
            }
        });
    }

    /**
     * Faz o regresso das unidades
     * @param defesa
     */
    regressar(defesa) {
        this.defesaService.regressar(defesa).subscribe(res => {
            if(res.success === true) {
                this.getAll();
                
                this.gameService.refreshCidades$.emit();
            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * Imagem da unidade
     * @param unidade
     * @returns {string}
     */
    unidadeImage(unidade) {
        return "url(imagens/unidades/" + unidade.id + ".png)";
    }

}