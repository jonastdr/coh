import {Component} from "@angular/core";
import {Api} from "../../api";
import {AcademiaService} from "../../services/academia.service";
import {Game} from "../../game";
import {TimerDirective} from "../../directives/timer.directive";
import {GameService} from "../../services/game.service";
@Component({
    selector: 'coh-academia',
    templateUrl: Api.url('academia'),
    directives: [
        TimerDirective
    ]
})
export class AcademiaComponent {

    herois = [];
    funcoes = [];

    heroi_cidade = {};

    heroi = {};
    funcao;

    heroi_cidade_desc = '';

    constructor(private academiaService: AcademiaService,
                private gameService: GameService) {
    }

    ngOnInit() {
        this.academiaService.getAll().subscribe(res => {
            if(res.success === true) {
                this.herois = res.herois;
                this.heroi = res.heroi;
                this.funcoes = res.funcoes;

                this.heroi_cidade = res.heroi_cidade;
                this.heroi_cidade_desc = res.heroi_cidade_desc;
            }
        });
    }

    /**
     *
     * @param heroi
     */
    openHeroi(heroi) {
        this.academiaService.getHeroi(heroi).subscribe(res => {
            if(res.success === true) {
                this.heroi = res.heroi;
                this.funcoes = res.funcoes;
                this.funcao = null;
            }
        });
    }

    /**
     * Retorna os dados de uma função de heroi
     * @param funcao
     */
    openFuncao(funcao) {
        this.academiaService.getFuncao(funcao).subscribe(res => {
            if(res.success === true) {
                this.funcao = res.funcao;
                this.heroi = null;
            }
        });
    }

    /**
     * Ativa um heroi na cidade selecionada
     * @param heroi
     */
    ativarHeroi(heroi) {
        this.academiaService.ativarHeroi(heroi).subscribe(res => {
            if(res.success === true) {
                this.herois = res.herois;
                this.heroi = res.heroi;
                this.funcoes = res.funcoes;

                this.gameService.refreshRecurso$.emit();
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Ativa uma função do heroi
     * @param funcao
     */
    ativarFuncao(funcao) {
        this.academiaService.ativarFuncao(funcao).subscribe(res => {
            if(res.success === true) {
                this.herois = res.herois;
                this.funcao = res.funcao;
                this.funcoes = res.funcoes;

                this.gameService.refreshRecurso$.emit();
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    heroiImage(heroi) {
        return 'url(' + this.heroiImageSrc(heroi) + ')';
    }

    heroiImageSrc(heroi) {
        return 'imagens/unidades/' + heroi.id + '.png';
    }

    funcaoImage(funcao) {
        return 'url(' + this.funcaoImageSrc(funcao) + ')';
    }

    funcaoImageSrc(funcao) {
        return 'imagens/unidades/' + funcao.id + '.png';
    }

}