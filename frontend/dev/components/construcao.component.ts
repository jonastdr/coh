import {
    Component, ElementRef, ViewChild, ViewContainerRef, ComponentFactoryResolver,
    ComponentFactory
} from "@angular/core";
import {WindowDialog} from "./window.dialog";
import {Api} from "../api";
import {ConstrucaoService} from "../services/construcao.service";
import {OverviewService} from "../services/overview.service";
import {Game} from "../game";
import {GameService} from "../services/game.service";
import {TimerDirective} from "../directives/timer.directive";
import {LaboratorioComponent} from "./construcoes/laboratorio.component";
import {BarracaComponent} from "./construcoes/barraca.component";
import {DefesaExpedicaoComponent} from "./construcoes/radar/defesa-expedicao.component";
import {DefesaCidadeComponent} from "./construcoes/radar/defesa-cidade.component";
import {AcademiaComponent} from "./construcoes/academia.component";
import {EspionagemCidadeComponent} from "./construcoes/radar/espionagem-cidade.component";
@Component({
    selector: 'coh-construcao',
    templateUrl: Api.url('construcao'),
    directives: [
        TimerDirective
    ],
    entryComponents: [
        LaboratorioComponent,
        BarracaComponent,
        DefesaCidadeComponent,
        DefesaExpedicaoComponent,
        AcademiaComponent,
        EspionagemCidadeComponent
    ]
})
export class ConstrucaoComponent extends WindowDialog {

    @ViewChild('components', {read: ViewContainerRef})
    viewContainerRef: ViewContainerRef;

    aba = 'construcao';

    params;

    construindo;
    construcao = {
        construcao_requisitos: [],
        pesquisa_requisitos: []
    };
    tempoGerado;

    constructor(private elem: ElementRef,
                private componentFactoryResolver: ComponentFactoryResolver,
                private construcaoService: ConstrucaoService,
                private overviewService: OverviewService,
                private gameService: GameService) {
        super('Construção', elem);
    }

    ngOnInit() {
        this.construcaoService.getAll(this.params).subscribe(res => {
            if(res.success === true) {
                this.construcao = res.construcao;
                this.construindo = res.construindo;
                this.tempoGerado = res.tempoGerado;

                this.setTitle(res.construcao.nome);

                this.showWindow();
            } else {
                this.closeWindow();
            }
        });
    }

    alternarAba(aba: string, param: string) {
        var component;

        if(aba == 'laboratorio')
            component = LaboratorioComponent;

        if(aba == 'barraca')
            component = BarracaComponent;

        if(aba == 'defesa-cidade')
            component = DefesaCidadeComponent;

        if(aba == 'defesa-expedicao')
            component = DefesaExpedicaoComponent;

        if(aba == 'espionagem-cidade')
            component = EspionagemCidadeComponent;

        if(aba == 'academia')
            component = AcademiaComponent;

        if(component) {
            var factory = this.componentFactoryResolver.resolveComponentFactory(component);

            /**
             * Só pode carregar 1 component por vez
             */
            if(this.viewContainerRef.length > 0) {
                /**
                 * Verifica se o component não está aberto
                 */
                var comp: any = this.viewContainerRef.get(0);

                /**
                 * Se a janela já estiver aberta não abre novamente
                 */
                if(comp.rootNodes[0].tagName.toLowerCase() == factory.selector)
                    return;


                this.viewContainerRef.clear();
            }

            let other = this.viewContainerRef.createComponent(factory);

            let instance:any = other.instance;

            instance.params = {
                id_tipo: param
            };
        } else {
            this.viewContainerRef.clear();
        }

        this.aba = aba;
    }

    /**
     * Traz os dados para a tela
     */
    get() {
        this.construcaoService.getAll(this.params).subscribe(res => {
            if(res.success === true) {
                this.construcao = res.construcao;
                this.construindo = res.construindo;
                this.tempoGerado = res.tempoGerado;
            }
        });
    }

    /**
     * Registra uma melhoria na construção
     * @param construcao
     */
    melhorar(construcao) {
        this.construcaoService.melhorar(construcao).subscribe(res => {
            if(res.success === true) {
                this.construcao = res.construcao;
                this.construindo = res.construindo;
                this.tempoGerado = res.tempoGerado;

                this.gameService.refreshRecurso$.emit();
                this.gameService.refreshSidebar$.emit();
                this.overviewService.overview$.emit();
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Cancela a melhoria
     * @param construcao
     */
    cancelar(construcao) {
        this.construcaoService.cancelar(construcao).subscribe((res: any) => {
            if(res.success === true) {
                this.construcao = res.construcao;
                this.construindo = res.construindo;
                this.tempoGerado = res.tempoGerado;

                this.overviewService.overview$.emit();
                this.gameService.refreshSidebar$.emit();
                this.gameService.refreshRecurso$.emit();
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Volta um level da construção
     * @param construcao
     */
    destruir(construcao) {
        this.construcaoService.destruir(construcao).subscribe((res: any) => {
            if(res.success === true) {
                this.construcao = res.construcao;
                this.construindo = res.construindo;
                this.tempoGerado = res.tempoGerado;

                this.overviewService.overview$.emit();
                this.gameService.refreshSidebar$.emit();
                this.gameService.refreshRecurso$.emit();
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Imagem da construção
     * @param construcao
     * @returns {string}
     */
    construcaoImage(construcao) {
        return 'url(imagens/construcoes/' + construcao.id + '.png)';
    }

    /**
     * Imagem de pesquisa
     * @returns {string}
     */
    pesquisaImage(pesquisa) {
        return 'url(imagens/pesquisas/' + pesquisa.id + '.png)';
    }

}