import {Component, ElementRef} from "@angular/core";
import {Api} from "../api";
import {UsuarioService} from "../services/usuario.service";
import {Game} from "../game";
import {WindowDialog} from "./window.dialog";
import {IDialog} from "../interfaces/dialog.interface";
import {IDialogTabs} from "../interfaces/dialog.tabs.interface";
import {EqualValidator} from "../directives/equal-validator.directive";
import {GameService} from "../services/game.service";
@Component({
    selector: 'coh-usuario',
    templateUrl: Api.url('usuario'),
    directives: [
        EqualValidator
    ]
})
export class UsuarioComponent extends WindowDialog implements IDialog, IDialogTabs {

    aba = 'geral';

    params;

    cidades = [];

    usuario = {};
    usuarioLogado = {};

    form = {};
    formSenha = {};

    constructor(private usuarioService: UsuarioService, elem: ElementRef, private gameService: GameService) {
        super('Usuário', elem);
    }

    ngOnInit() {
        /**
         * Usado para abrir diretamente uma aba especifica
         */
        if(this.params.aba) {
            this.aba = this.params.aba;
        }

        var usuario = {
            id: this.params.id_usuario
        };

        this.usuarioService.getAll(usuario).subscribe(res => {
            if(res.success === true) {
                this.usuario = res.usuario;
                this.usuarioLogado = res.usuarioLogado;
                this.cidades = res.cidades;

                this.showWindow();
            } else {
                Game.mensagem(res.msg);

                this.closeWindow();
            }
        });
    }

    alternarAba(aba: string) {
        this.aba = aba;
    }

    /**
     * Altera os dados do usuário
     * @param form
     * @param valid
     */
    alterarConfig(form, valid) {
        if(!valid) {
            Game.mensagem('Favor verificar os campos em destaque.');

            return;
        }

        this.usuarioService.alterarConfig(form).subscribe(res => {
            if(res.success === true) {
                this.form = {};

                this.gameService.logout().subscribe((res:any) => {
                    if (res.success === true) {
                        setTimeout(() => {
                            location.href = res.redirect;
                        }, 1000);
                    }
                });
            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * Altera a senha do usuário
     * @param form
     * @param valid
     */
    alterarSenha(form, valid) {
        if(!valid) {
            Game.mensagem('Favor verificar os campos em destaque.');

            return;
        }

        this.usuarioService.alterarSenha(form).subscribe(res => {
            if(res.success === true) {
                this.formSenha = {};

                this.gameService.logout().subscribe((res:any) => {
                    if (res.success === true) {
                        setTimeout(() => {
                            location.href = res.redirect;
                        }, 1000);
                    }
                });
            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * Desativa o usuário
     */
    abandonar() {
        var confirmacao = confirm('Você tem certeza que deseja abandonar? Não será mais possível acessar esta conta novamente.');

        if(confirmacao) {
            this.usuarioService.abandonar().subscribe((res:any) => {
                if(res.success === true) {
                    location.href = res.redirect;
                } else {
                    Game.mensagem(res.msg);
                }
            });
        }
    }

    /**
     * Coloca o usuário em período de férias
     */
    ferias() {
        var confirmacao = confirm('Você tem certeza que deseja ativar o modo férias? não será possível acessar essa conta por 3 dias.');

        if(confirmacao) {
            this.usuarioService.ferias().subscribe((res:any) => {
                if(res.success === true) {
                    location.href = res.redirect;
                } else {
                    Game.mensagem(res.msg);
                }
            });
        }
    }

}