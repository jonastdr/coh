import {Component} from "@angular/core";
import {Api} from "../api";
import {AppComponent} from "./app.component";
import {GameService} from "../services/game.service";
import {timeout} from "rxjs/operator/timeout";
import {Game} from "../game";

@Component({
    selector: 'coh-menu',
    templateUrl: Api.url('game/menu')
})

export class MenuComponent {

    private view = 'mapa';

    constructor(private gameService: GameService) {
        $('.mapa_construtor').click(function(e){
            e.preventDefault();

            var link = $(this).attr('href');

            $( '#mapa_construtor' ).load(link, function(){
                $('.mapa_construtor').parent().toggle();

                $('.minimap').toggle();
            });
        });

        $('[title]').tooltip();
    }

    /**
     * Faz toggle do menu
     */
    toggleMenu() {
        var $menu = $('coh-menu');

        var calc = - ($menu.width() - 5);

        if($menu.css('right')!=calc + 'px'){
            $menu.animate({
                right: calc
            }).effect("bounce", { direction:'left', times:5 }, 500);
        } else {
            $menu.animate({
                right: '0px'
            });
        }
    }

    /**
     * Abre um menu
     * @param id
     */
    openModal(id:string) {
        AppComponent.openWindow(id);
    }

    /**
     * Vai até a tela de vista geral da cidade
     */
    openOverview() {
        this.view = 'overview';

        AppComponent.changeView('overview');
    }

    /**
     * Vai até a tela de mapa
     */
    openMapa() {
        this.view = 'mapa';

        AppComponent.changeView('mapa');
    }

    openConfig(id_usuario: number) {
        AppComponent.openWindow('usuario', {
            id_usuario: id_usuario,
            aba: 'config'
        });
    }

    /**
     * Finaliza a sessão
     */
    logout() {
        var confirmacao = confirm('Tem certeza que deseja sair?');

        if(confirmacao) {
            this.gameService.logout().subscribe((res:any) => {
                if (res.success === true) {
                    setTimeout(() => {
                        location.href = res.redirect;
                    }, 1000);

                    Game.mensagem(res.msg);
                }
            });
        }
    }

}