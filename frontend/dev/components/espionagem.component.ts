import {Component, ElementRef} from "@angular/core";
import {Api} from "../api";
import {WindowDialog} from "./window.dialog";
import {EspionagemService} from "../services/espionagem.service";
import {GameService} from "../services/game.service";
import {Game} from "../game";
@Component({
    selector: 'coh-espionagem',
    templateUrl: Api.url('espionagem')
})

export class EspionagemComponent extends WindowDialog {

    params;

    quantidade_maxima;
    pesquisa_nivel;

    form = {
        cod_ilha: null,
        cod_cidade: null,
        dinheiro: 0
    };

    /**
     * interval
     */
    interval;
    tempoUpd = 0;
    duracao = 0;

    info = {
        duracao: '00:00:00',
        chegada: '00:00:00'
    };

    now = 0;

    constructor(private elem: ElementRef,
                private espionagemService: EspionagemService,
                private gameService: GameService) {
        super('Espionagem', elem);
    }

    ngOnInit() {
        var city = this.params;

        this.espionagemService.getAll(city).subscribe(res => {
            if(res.success === true) {
                this.form.cod_ilha = res.cod_ilha;
                this.form.cod_cidade = res.cod_cidade;

                this.quantidade_maxima = res.quantidade_maxima;
                this.pesquisa_nivel = res.pesquisa_nivel;

                this.duracao = res.duracao;

                this.now = res.now;

                this.interval = setInterval(() => {
                    this.calculaTempo();
                }, 1000);

                this.calculaTempo();

                this.showWindow();
            } else {
                this.closeWindow();
            }
        });
    }

    closeWindow(): any {
        clearTimeout(this.interval);

        return super.closeWindow();
    }

    /**
     * Faz o clock da duração da expedição
     */
    calculaTempo() {
        var time = this.now + this.duracao + this.tempoUpd;

        var horarioAtual = new Date(time * 1000);

        /**
         * Calcula a duração
         */
        var min: any = parseInt(this.duracao / 60);
        var hor: any = parseInt(min / 60);
        var seg: any = this.duracao % 60;

        var min = min % 60;

        if (hor < 10) {
            hor = '0' + hor;
            hor = hor.substr(0, 2);
        }

        if (min < 10) {
            min = '0' + min;
            min = min.substr(0, 2);
        }
        if (seg < 10) {
            seg = '0' + seg;
        }

        this.info.duracao = hor + ":" + min + ":" + seg;

        var horaAtual = horarioAtual.getHours(),
            minutosAtual = horarioAtual.getMinutes(),
            segundosAtual = horarioAtual.getSeconds();

        if (horaAtual < 10) horaAtual = '0' + horaAtual;
        if (minutosAtual < 10) minutosAtual = '0' + minutosAtual;
        if (segundosAtual < 10) segundosAtual = '0' + segundosAtual;

        this.info.chegada = horaAtual + ':' + minutosAtual + ':' + segundosAtual;

        this.tempoUpd++;
    }

    ngOnDestroy() {

    }

    quantidade(dinheiro) {
        this.form.dinheiro = dinheiro;
    }

    enviar(form) {
        this.espionagemService.enviar(form).subscribe(res => {
            if(res.success === true) {
                this.gameService.refreshCidades$.emit();
                this.gameService.refreshRecurso$.emit();

                this.closeWindow();
            }

            Game.mensagem(res.msg);
        });
    }

}