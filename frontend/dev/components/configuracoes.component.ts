import {Component, ElementRef} from "@angular/core";
import {Api} from "../api";
import {WindowDialog} from "./window.dialog";
import {Game} from "../game";
@Component({
    selector: 'coh-configuracoes',
    templateUrl: Api.url('configuracoes')
})
export class ConfiguracoesComponent extends WindowDialog {

    constructor(private elem: ElementRef) {
        super('Configurações', elem);

        this.closeWindow();

        Game.mensagem('Tela não implementada');
    }

}