import {Component, ElementRef} from "@angular/core";
import {Api} from "../../api";
import {AliancaService} from "../../services/alianca.service";
import {WindowDialog} from "../window.dialog";
import {Game} from "../../game";
@Component({
    selector: 'coh-alianca-nivel',
    templateUrl: Api.url('alianca/nivel')
})

export class AliancaNivelComponent extends WindowDialog {

    form = {
        nome: ''
    };

    niveis = [];

    timeout;

    constructor(elem: ElementRef, private aliancaService: AliancaService) {
        super('Níveis', elem);
    }

    ngOnInit() {
        this.aliancaService.niveis().subscribe(res => {
            if(res.success === true) {
                this.niveis = res.niveis;

                this.showWindow();
            } else {
                this.closeWindow();
            }
        });
    }

    /**
     * Edita as permissões de um nível
     * @param n
     */
    marcarPermissao(n) {
        this.aliancaService.editaNivel(n).subscribe(res => {
            if(res.success === false) {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Faz a edição de um nível
     * @param n
     */
    editaNivel(n) {
        if(this.timeout) {
            clearTimeout(this.timeout);
        }

        this.timeout = setTimeout(() => {
            this.aliancaService.editaNivel(n).subscribe(res => {
                if(res.success === false) {
                    Game.mensagem(res.msg);
                }
            });

            this.timeout = null;
        }, 2000);
    }

    /**
     * Cria um novo nível
     * @param form
     */
    novoNivel(form) {
        this.aliancaService.novoNivel(form).subscribe(res => {
            if(res.success === true) {
                this.niveis = res.niveis;

                this.form.nome = '';
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Remove um nível
     * @param nivel
     */
    removerNivel(nivel) {
        var confirma = confirm('Tem certeza que deseja remover?');

        if(confirma) {
            this.aliancaService.removerNivel(nivel).subscribe(res => {
                if(res.success === true) {
                    this.niveis = res.niveis;
                } else {
                    Game.mensagem(res.msg);
                }
            });
        }
    }

}