import {Component, ElementRef, OnInit} from "@angular/core";
import {Api} from "../api";
import {IDialog} from "../interfaces/dialog.interface";
import {Game} from "../game";
import {WindowDialog} from "./window.dialog";
import {EspionagemService} from "../services/espionagem.service";
import {AppComponent} from "./app.component";
@Component({
    selector: 'coh-relatorio-espionagem',
    templateUrl: Api.url('espionagem/relatorio')
})

export class RelatorioEspionagemComponent extends WindowDialog implements OnInit, IDialog {

    params;

    exito = false;

    investimento = 0;
    cidade = {};
    construcoes = [];
    pesquisas = [];
    data = new Date().toDateString();
    detalhes = {};
    unidades = [];

    constructor(private elementRef: ElementRef, private espionagemService: EspionagemService) {
        super('Relatório', elementRef);
    }

    ngOnInit() {
        this.espionagemService.getRelatorio(this.params.link).subscribe(res => {
            if(res.success === true) {
                this.exito = res.exito;
                this.investimento = res.investimento;
                this.cidade = res.cidade;
                this.data = res.data;
                this.pesquisas = res.pesquisas;
                this.construcoes = res.construcoes;
                this.unidades = res.unidades;

                this.showWindow();
            } else {
                Game.mensagem(res.msg);

                this.closeWindow();
            }
        });
    }

    /**
     * Imagem da unidade
     * @param unidade
     * @returns {string}
     */
    unidadeImage(unidade) {
        return "url(imagens/unidades/" + unidade.id + ".png)";
    }

    /**
     * Imagem da construção
     * @param construcao
     * @returns {string}
     */
    construcaoImage(construcao) {
        return "url(imagens/construcoes/" + construcao.id + ".png)";
    }

    /**
     * Imagem da pesquisa
     * @param unidade
     * @returns {string}
     */
    pesquisasImage(pesquisa) {
        return "url(imagens/pesquisas/" + pesquisa.id + ".png)";
    }

    openCidade(id_usuario) {
        AppComponent.openWindow('usuario', {
            id_usuario: id_usuario,
            aba: 'cidades'
        });
    }

}