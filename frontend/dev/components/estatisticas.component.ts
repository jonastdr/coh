import {Component, ElementRef} from "@angular/core";
import {WindowDialog} from "./window.dialog";
import {EstatisticasService} from "../services/estatisticas.service";
import {Api} from "../api";
import {IDialogTabs} from "../interfaces/dialog.tabs.interface";
@Component({
    selector: 'coh-estatisticas',
    templateUrl: Api.url('estatisticas')
})
export class EstatisticasComponent extends WindowDialog implements IDialogTabs {

    aba: string = 'usuarios';

    private usuarios = [];
    private aliancas = [];
    private ataques = [];
    private defesas = [];

    constructor(private elem: ElementRef,
                private estatisticasService: EstatisticasService) {
        super('Estatísticas', elem);
    }

    ngOnInit() {
        this.estatisticasService.getAll().subscribe(res => {
            if(res.success == true) {
                this.usuarios = res.usuarios;
                this.aliancas = res.aliancas;
                this.ataques = res.ataques;
                this.defesas = res.defesas;

                this.showWindow();
            } else {
                this.closeWindow();
            }
        });
    }

    alternarAba(aba: string) {
        this.aba = aba;
    }

}