import {Component, ElementRef} from "@angular/core";
import {WindowDialog} from "./window.dialog";
import {Api} from "../api";
import {ExpedicaoService} from "../services/expedicao.service";
import {Game} from "../game";
import {GameService} from "../services/game.service";
@Component({
    selector: 'coh-expedicao',
    templateUrl: Api.url('expedicao')
})
export class ExpedicaoComponent extends WindowDialog {

    params;

    form = {
        cod_ilha: null,
        cod_cidade: null,
        missao: 1,
        dinheiro: '',
        alimento: '',
        metal: '',
        petroleo: '',
        unidades: []
    };

    cidade = {};
    unidades = [];
    missoes = [];
    minha_posicao = {
        x: 0,
        y: 0
    };
    destino_posicao = {
        x: 0,
        y: 0
    };
    now = 0;

    /**
     * interval
     */
    interval;
    tempoUpd = 0;
    distancia = 0;
    duracao = 0;

    jogoVelocidade = 1;

    info = {
        missao: 'Atacar',
        duracao: '00:00:00',
        chegada: '00:00:00',
        capacidade: 0,
        capacidade_restante: 0,
        quantidade: 0,
        velocidade: null,
        velocidade_aereo_naval: null
    };

    constructor(private elem: ElementRef,
                private expedicaoService: ExpedicaoService,
                private gameService: GameService) {
        super('Expedição', elem);
    }

    ngOnInit() {
        var city = this.params;

        this.expedicaoService.getAll(city).subscribe(res => {
            if(res.success === true) {
                this.form.cod_ilha = res.cod_ilha;
                this.form.cod_cidade = res.cod_cidade;

                this.cidade = res.cidade;
                this.unidades = res.unidades;
                this.missoes = res.missoes;
                this.minha_posicao = res.minha_posicao;
                this.destino_posicao = res.destino_posicao;
                this.now = res.now;

                this.jogoVelocidade = 1;

                this.showWindow();

                this.interval = setInterval(() => {
                    this.calculaTempo();
                }, 1000);

                this.velocidadeMinima();
                this.calculaDistancia();
                this.calculaDuracao();
                this.calculaTempo();
            } else {
                Game.mensagem(res.msg);

                this.closeWindow();
            }
        })
    }

    closeWindow(): any {
        clearTimeout(this.interval);

        return super.closeWindow();
    }

    /**
     * Imagem da unidade
     * @param unidade
     * @returns {string}
     */
    unidadeImage(unidade) {
        return "url(imagens/unidades/" + unidade.id + ".png)";
    }

    /**
     * Faz o clock da duração da expedição
     */
    calculaTempo() {
        var time = this.now + this.duracao + this.tempoUpd;

        var horarioAtual = new Date(time * 1000);

        /**
         * Calcula a duração
         */
        var min: any = parseInt(this.duracao / 60);
        var hor: any = parseInt(min / 60);
        var seg: any = this.duracao % 60;

        var min = min % 60;

        if (hor < 10) {
            hor = '0' + hor;
            hor = hor.substr(0, 2);
        }

        if (min < 10) {
            min = '0' + min;
            min = min.substr(0, 2);
        }
        if (seg < 10) {
            seg = '0' + seg;
        }

        this.info.duracao = hor + ":" + min + ":" + seg;

        var horaAtual = horarioAtual.getHours(),
            minutosAtual = horarioAtual.getMinutes(),
            segundosAtual = horarioAtual.getSeconds();

        if (horaAtual < 10) horaAtual = '0' + horaAtual;
        if (minutosAtual < 10) minutosAtual = '0' + minutosAtual;
        if (segundosAtual < 10) segundosAtual = '0' + segundosAtual;

        this.info.chegada = horaAtual + ':' + minutosAtual + ':' + segundosAtual;

        this.tempoUpd++;
    }

    /**
     * Faz o calculo da distância total
     */
    calculaDistancia() {
        this.distancia = Math.sqrt(
            Math.pow((this.destino_posicao.x - this.minha_posicao.x), 2)
            +
            Math.pow((this.destino_posicao.y - this.minha_posicao.y), 2)
        );
    }

    /**
     * Calcula a duração total da expedição
     */
    calculaDuracao() {
        /**
         * Calculo de distancia em horas
         */
        var horas = this.distancia / this.info.velocidade;

        /**
         * Horas para segundos
         */
        this.duracao = Math.round(horas * 3600) / this.jogoVelocidade;
    }

    /**
     * Calcula a velocidade minima possivel
     */
    velocidadeMinima() {
        this.info.velocidade = null;
        this.info.velocidade_aereo_naval = null;

        for (var i in this.unidades) {
            var unidade = this.unidades[i];

            if(unidade.velocidade < this.info.velocidade || this.info.velocidade == null)
                this.info.velocidade = unidade.velocidade;

            if([2, 3].indexOf(unidade.id_tipo_classe) > -1) {
                if(unidade.velocidade < this.info.velocidade_aereo_naval || this.info.velocidade_aereo_naval == null)
                    this.info.velocidade_aereo_naval = unidade.velocidade;
            }
        }

        if(this.info.velocidade_aereo_naval !== null) {
            this.info.velocidade = this.info.velocidade_aereo_naval;
        }
    }

    /**
     * Recalcula as informações
     */
    calculaRecurso(name) {
        if(this.form[name])
            this.form[name] = this.form[name].replace(/[^0-9]/, '');

        var recursos = 0;

        recursos += +this.form.dinheiro;
        recursos += +this.form.alimento;
        recursos += +this.form.metal;
        recursos += +this.form.petroleo;

        this.info.capacidade_restante = this.info.capacidade - recursos;
    }

    /**
     * Recalcula as informações
     */
    calculaUnidade(id) {
        if(this.form.unidades[id])
            this.form.unidades[id] = this.form.unidades[id].replace(/[^0-9]/, '');

        this.info.capacidade = 0;
        this.info.velocidade = null;
        this.info.velocidade_aereo_naval = null;

        this.info.quantidade = 0;

        for (var i in this.unidades) {
            var unidade = this.unidades[i];

            var unidadeForm = this.form.unidades[unidade.id] ? +this.form.unidades[unidade.id] : 0;

            /**
             * Não pode passar da quantidade que tem
             */
            if(unidadeForm > unidade.quantidade)
                this.form.unidades[unidade.id] = unidade.quantidade;

            this.info.quantidade += unidadeForm;

            this.info.capacidade += unidade.capacidade * unidadeForm;

            if(unidadeForm > 0) {
                if(unidade.velocidade < this.info.velocidade || this.info.velocidade == null)
                    this.info.velocidade = unidade.velocidade;

                if([2, 3].indexOf(unidade.id_tipo_classe) > -1) {
                    if(unidade.velocidade < this.info.velocidade_aereo_naval || this.info.velocidade_aereo_naval == null)
                        this.info.velocidade_aereo_naval = unidade.velocidade;
                }
            }
        }

        if(this.info.velocidade_aereo_naval !== null) {
            this.info.velocidade = this.info.velocidade_aereo_naval;
        }

        if(this.info.quantidade == 0)
            this.velocidadeMinima();

        this.calculaDistancia();
        this.calculaDuracao();
    }

    /**
     * Muda o texto da expedição
     * @param id
     */
    changeMissao(id) {
        if(id == 1)
            this.info.missao = 'Atacar';
        else if(id == 2)
            this.info.missao = 'Transportar';
        else if(id == 3)
            this.info.missao = 'Defender';
        else if(id == 4)
            this.info.missao = 'Colonização';
    }

    /**
     * Faz o envio da expedição
     * @param form
     */
    enviar(form) {
        if(this.info.capacidade_restante < 0) {
            Game.mensagem('A quantidade de recursos ultrapassa a quantidade total.');

            return;
        }

        if(this.info.quantidade <= 0) {
            Game.mensagem('O que irá enviar para esta missão?');

            return;
        }

        form.cidade = this.cidade;

        this.expedicaoService.enviar(form).subscribe(res => {
            if(res.success === true) {
                this.gameService.refreshCidades$.emit();
                this.gameService.refreshRecurso$.emit();

                this.closeWindow();
            }

            Game.mensagem(res.msg);
        });
    }

}