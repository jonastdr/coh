import {Component} from "@angular/core";
import {GameService} from "../services/game.service";
import {Api} from "../api";
import {AppComponent} from "./app.component";
@Component({
    selector: 'coh-notificacao',
    templateUrl: Api.url('game/notificacoes')
})
export class NotificacaoComponent {

    notificacoes: Array = [];

    interval;

    constructor(private gameService: GameService) {

    }

    ngOnInit() {
        this.getAll();

        /**
         * Escuta se houver atualização de notificações
         */
        this.gameService.refreshNotificacoes$.subscribe(() => {
            this.getAll();

            /**
             * Recria o intervalo das notificações
             */
            this.makeInterval();
        });

        this.makeInterval();
    }

    /**
     * Cria o intervalo para as notificações
     */
    makeInterval() {
        clearInterval(this.interval);

        this.interval = setInterval(() => {
            this.getAll();
        }, 60 * 1000);
    }

    /**
     * Destroi o component e o intervalo para execução de notificações
     */
    ngOnDestroy() {
        clearInterval(this.interval);
    }

    /**
     * Retorna todas as notificações do usuário para a aplicação
     */
    getAll() {
        this.gameService.notificacoes().subscribe(res => {
            if(res.success === true) {
                this.notificacoes = res.notificacoes;
            }
        });

        /**
         * Atualização das movimentações na cidade
         */
        this.gameService.refreshCidades$.emit();

        /**
         * Atualização dos recursos da cidade
         */
        this.gameService.refreshRecurso$.emit();
    }

    /**
     * Abre alguma tela configurada na notificação
     * @param notificacao
     */
    abrir(notificacao) {
        if(notificacao.component)
            AppComponent.openWindow(notificacao.component, notificacao.params);

        this.gameService.removeNotificacao(notificacao).subscribe(res => {
            if(res.success === true) {
                this.notificacoes = res.notificacoes;
            }
        });
    }

}