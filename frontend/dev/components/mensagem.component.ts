import {Component, OnInit, ElementRef} from "@angular/core";
import {Api} from "../api";
import {IDialogTabs} from "../interfaces/dialog.tabs.interface";
import {MensagemService} from "../services/mensagem.service";
import {Game} from "../game";
import {AppComponent} from "./app.component";
import {WindowDialog} from "./window.dialog";
import {GameService} from "../services/game.service";
import {PagerService} from "../services/pager.service";
import {Observable} from "rxjs";

@Component({
    selector: 'coh-mensagem',
    templateUrl: Api.url('mensagem')
})

export class MensagemComponent extends WindowDialog implements OnInit, IDialogTabs {

    form = {
        usuarios: [],
        assunto: '',
        mensagem: '',
        alianca: 0
    };

    /**
     * Parametros recebidos de outro component
     */
    params;

    aba: string = 'usuarios';

    usuarios: Array = [];
    relatorios: Array = [];
    aliancas: Array = [];

    usuariosPag = {};
    relatoriosPag = {};
    aliancasPag = {};
    usuariosMostrar = 10;
    relatoriosMostrar = 10;
    aliancasMostrar = 10;

    visualizar;

    constructor(private elementRef: ElementRef,
                private mensagemService: MensagemService,
                private gameService: GameService,
                private pagerService: PagerService) {
        super('Mensagem', elementRef);
    }

    ngOnInit() {
        if(this.params) {
            if(this.params.aba) {
                if(this.params.aba == 'visualizar') {
                    /**
                     * Abre diretamente a mensagem
                     */
                    this.mensagemService.mensagensVisualizar({id: this.params.id}).subscribe(res => {
                        if(res.success === true) {
                            this.visualizar = res.visualizar;

                            this.alternarAba('visualizar');

                            this.showWindow();
                        }
                    });
                } else {
                    this.aba = this.params.aba;
                }

            }

            if(this.params.usuarios) {
                this.form.usuarios = this.params.usuarios;
            }

            if(this.params.alianca) {
                this.form.alianca = 1;
            }
        }

        /**
         * Lista as mensagens todas as mensagens
         */
        this.getAll().then((res) => {
            if(res.success === true) {
                if(!this.params || (this.params && this.params.aba != 'visualizar'))
                this.showWindow();
            } else {
                this.closeWindow();
            }
        });
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getAll() {
        return new Promise((resolve, reject) => {
            this.mensagemService.getAll().subscribe((res: any) => {
                if(res.success === true) {
                    this.usuariosPag = this.pagerService.getPager(res.usuariosCount, 1);
                    this.relatoriosPag = this.pagerService.getPager(res.relatoriosCount, 1);
                    this.aliancasPag = this.pagerService.getPager(res.aliancasCount, 1);

                    this.usuarios = res.usuarios;
                    this.relatorios = res.relatorios;
                    this.aliancas = res.aliancas;

                    /**
                     * Responde o conteudo retornado do service
                     */
                    resolve(res);
                }
            });
        });
    }

    alternarAba(aba: string) {
        if(aba == 'nova') {
            this.resetForm();
        }

        this.aba = aba;
    }

    /**
     * Abre a mensagem para visualização
     * @param mensagem
     */
    visualizarMensagem(mensagem) {
        this.mensagemService.mensagensVisualizar(mensagem).subscribe(res => {
            if(res.success === true) {
                this.visualizar = res.visualizar;

                this.gameService.refreshNotificacoes$.emit();

                this.resetForm();

                this.alternarAba('visualizar');
            }
        });
    }

    /**
     * Reseta o formulário em visualizar
     */
    resetForm() {
        this.form = {
            usuarios: [],
            assunto: '',
            mensagem: '',
            alianca: 0
        };
    }

    /**
     * Responde uma mensagem
     * @param id
     * @param mensagem
     */
    responder(id, mensagem) {
        mensagem.id = id;

        this.mensagemService.responder(mensagem).subscribe(res => {
            if(res.success === true) {
                this.resetForm();

                this.visualizar = res.visualizar;

                this.usuarios = res.usuarios;
                this.relatorios = res.relatorios;
                this.aliancas = res.aliancas;
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Insere uma nova mensagem
     * @param id
     * @param mensagem
     */
    nova(mensagem) {
        this.mensagemService.nova(mensagem).subscribe(res => {
            if(res.success == true) {
                let aba = this.form.alianca ? 'aliancas' : 'usuarios';

                this.resetForm();

                this.usuarios = res.usuarios;
                this.relatorios = res.relatorios;
                this.aliancas = res.aliancas;

                this.alternarAba(aba);
            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * Abre a tela de relatório
     * @param mensagem
     */
    abrirRelatorio(mensagem) {
        if(mensagem.tipo == 2) {
            AppComponent.openWindow('relatorio', mensagem);
        } else if(mensagem.tipo == 4) {
            AppComponent.openWindow('relatorio-espionagem', mensagem);
        }
    }

    /**
     * Atualiza a paginação e os dados
     * @param tipo 1 = Usuários, 2 = Relatórios, 3 = Alianças
     * @param p
     */
    pageChange(tipo: number, p: number) {
        var qtde: number = 10;
        var variable: string = '';

        if(tipo == 1) {
            variable = 'usuarios';
            qtde = this.usuariosMostrar;
        }
        else if(tipo == 2) {
            variable = 'relatorios';
            qtde = this.relatoriosMostrar;
        }
        else if(tipo == 3) {
            variable = 'aliancas';
            qtde = this.aliancasMostrar;
        }

        this.mensagemService.list(tipo, p, qtde).subscribe((res: any) => {
            if(res.success === true) {
                this[variable] = res.mensagens;

                this[variable + 'Pag'] = this.pagerService.getPager(res.count, p, qtde);
            }
        });
    }

    /**
     * Remove um grupo de mensagens
     * @param tipo
     * @param mensagens
     */
    excluir(tipo: number, mensagens: Array) {
        var variable: string = '';
        var mensagensDeletadas: Array = [];

        if(tipo == 1) {
            variable = 'usuarios';
        }
        else if(tipo == 2) {
            variable = 'relatorios';
        }
        else if(tipo == 3) {
            variable = 'aliancas';
        }

        /**
         * Verifica as mensagens que serão deletadas e acrescenta ao array mensagensDeletadas
         */
        for(var i in mensagens) {
            if(mensagens[i].checked) {
                mensagensDeletadas.push(mensagens[i]);
            }
        }

        if(mensagensDeletadas.length == 0) {
            alert('Deve selecionar pelo menos uma mensagem.');

            return;
        }

        if(!confirm('Você tem certeza que deseja remover as mensagens?')) {
            return;
        }

        this.mensagemService.excluir(mensagensDeletadas).subscribe(res => {
            if(res.success === true) {
                this.pageChange(tipo, this[variable + 'Pag'].currentPage);
            } else {
                Game.mensagem(res.msg);
            }
        });
    }

    /**
     * Marca todas as mensagens
     * @param tipo
     * @param ev
     */
    checkAll(tipo: number, ev: MouseEvent) {
        /**
         * Nome da variavel
         * @type {any}
         */
        var variable = null;
        /**
         * Se irá marcar como verdadeiro ou falso
         * @type {boolean}
         */
        var checkedVal = $(ev.target).prop('checked');

        if(tipo == 1) {
            variable = 'usuarios';
        }

        else if(tipo == 2) {
            variable = 'relatorios';
        }

        else if(tipo == 3) {
            variable = 'aliancas';
        }
        else {
            throw "Tipo de mensagem incorreto.";
        }

        for (let i in this[variable]) {
            this[variable][i].checked = checkedVal;
        }
    }

}