import {
    Component, ComponentFactoryResolver, ViewContainerRef, ComponentFactory,
    ViewChild, ComponentRef,
} from '@angular/core';
import {Api} from "../api";
import {MensagemComponent} from "./mensagem.component";
import {RelatorioComponent} from "./relatorio.component";
import {AliancaComponent} from "./alianca.component";
import {EstatisticasComponent} from "./estatisticas.component";
import {ConfiguracoesComponent} from "./configuracoes.component";
import {ExpedicaoComponent} from "./expedicao.component";
import {ConstrucaoComponent} from "./construcao.component";
import {UsuarioComponent} from "./usuario.component";
import {EspionagemComponent} from "./espionagem.component";
import {RelatorioEspionagemComponent} from "./relatorio-espionagem.component";
import {AliancaNivelComponent} from "./alianca/alianca-nivel.component";
@Component({
    selector: 'coh-game',
    templateUrl: Api.url('game/init'),
    entryComponents: [
        MensagemComponent,
        RelatorioComponent,
        AliancaComponent,
        AliancaNivelComponent,
        EstatisticasComponent,
        ExpedicaoComponent,
        ConstrucaoComponent,
        UsuarioComponent,
        EspionagemComponent,
        RelatorioEspionagemComponent
    ]
})
export class AppComponent {
    @ViewChild('dialog', {read: ViewContainerRef})
    viewContainerRef: ViewContainerRef;

    private view = 'mapa';

    private componentFactory: ComponentFactory<any>;

    private static instance: AppComponent;

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver) {

        AppComponent.instance = this;
    }

    static destroyAllWindows() {
        var instance: AppComponent = AppComponent.instance;

        instance.viewContainerRef.clear();
    }

    /**
     * Abre uma janela
     * @param id
     * @param params
     */
    static openWindow(id: string, params?: any) {
        var component;

        if(id == 'mensagem')
            component = MensagemComponent;

        else if(id == 'relatorio')
            component = RelatorioComponent;

        else if(id == 'alianca')
            component = AliancaComponent;

        else if(id == 'alianca-nivel')
            component = AliancaNivelComponent;

        else if(id == 'estatisticas')
            component = EstatisticasComponent;

        else if(id == 'expedicao')
            component = ExpedicaoComponent;

        else if(id == 'configuracoes')
            component = ConfiguracoesComponent;

        else if(id == 'construcao')
            component = ConstrucaoComponent;

        else if(id == 'usuario')
            component = UsuarioComponent;

        else if(id == 'espionagem')
            component = EspionagemComponent;

        else if(id == 'relatorio-espionagem')
            component = RelatorioEspionagemComponent;
        else
            throw 'Component ' + id + ' não encontrado.';

        var instance: AppComponent = AppComponent.instance;

        instance.componentFactory = instance.componentFactoryResolver.resolveComponentFactory(component);

        /**
         * Verifica se o component não está aberto
         */
        for(let i = 0; i < instance.viewContainerRef.length; i++) {
            var comp: any = instance.viewContainerRef.get(i);

            /**
             * Se a janela já estiver aberta não abre novamente
             */
            if(comp.rootNodes[0].tagName.toLowerCase() == instance.componentFactory.selector)
                return;
        }

        /**
         * Instancia o component
         * @type {ComponentRef<any>}
         */
        let dialogWindow = instance.viewContainerRef.createComponent(instance.componentFactory);

        /**
         * Escuta de evento disparado pela janela eventos
         */
        dialogWindow.instance.event.subscribe(type => {
            if(type == 'close')
                dialogWindow.destroy();
        });

        /**
         * Caso necessário passa os parametros para o component
         * que deve ser acessado apartir do ngOnInit
         * @type {any}
         */
        dialogWindow.instance.params = params;
    }

    static changeView(view) {
        var instance = AppComponent.instance;

        if(['mapa', 'overview'].indexOf(view) > -1)
            instance.view = view;
    }
}