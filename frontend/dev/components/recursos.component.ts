import {Component} from "@angular/core";
import {Api} from "../api";
import {GameService} from "../services/game.service";
import {RecursoDirective} from "../directives/recurso.directive";
@Component({
    selector: 'coh-recursos',
    templateUrl: Api.url('game/recursos'),
    directives: [
        RecursoDirective
    ]
})
export class RecursosComponent {
    recursos;
    incremento;
    armazenamento;
    tempo;

    constructor(private gameService: GameService) {
        this.recursos = {};
        this.incremento = {};
        this.armazenamento = {};
        this.tempo = 0;
    }

    ngOnInit() {
        this.get();

        /**
         * escuta da atualização dos recursos
         */
        this.gameService.refreshRecurso$.subscribe(() => {
            this.get();
        });

        setInterval(() => {
            this.atualizacao();
        }, 1000);
    }

    /**
     * Pega os dados de recursos
     */
    get() {
        this.gameService.recursos().subscribe(res => {
            if(res.success) {
                this.recursos = res.recursos;
                this.incremento = res.incremento;
                this.armazenamento = res.armazenamento;
                this.tempo = res.tempo;
            }
        });
    }

    /**
     * Faz a construção do canvas
     * @param id
     * @param recurso
     * @param incremento
     * @param max
     * @param corA
     * @param corB
     */
    private canvas(id, recurso, incremento, max, corA, corB) {
        var Now = new Date();
        var tempoCalc = Math.round(Now.getTime()/1000) - this.tempo;

        var recursoCalculado = Math.round(recurso + ((tempoCalc/60) * incremento));

        var canvasElem: any = document.getElementById(id);

        var RecursoValor = canvasElem.getContext('2d');

        var armazenamento = Math.round((recursoCalculado/max)*44);

        if(recursoCalculado > max)
            armazenamento = 44;

        $('#' + id).html(recursoCalculado);

        var gradient = RecursoValor.createLinearGradient(0, 0, 44, 0);
        gradient.addColorStop(0, corA);
        gradient.addColorStop(1, corB);

        RecursoValor.beginPath();
        RecursoValor.clearRect(0, 0, 44, 12);
        RecursoValor.rect(0, 0, armazenamento, 12);

        RecursoValor.fillStyle = gradient;

        RecursoValor.fill();
        RecursoValor.font = '12pt';
        RecursoValor.textAlign = 'center';

        if(+$('#' + id).text() < max) {
            RecursoValor.fillStyle = 'white';

            RecursoValor.fillText(Math.round(recursoCalculado, 0), 22, 10);
        } else {
            RecursoValor.fillStyle = 'white';

            RecursoValor.fillText(Math.round(max, 0), 22, 10);
        }
    }

    /**
     * Atualiza o canvas de honra | heroi
     */
    private canvasHonra() {
        var max = this.armazenamento.honra;
        var recurso = +this.recursos.honra;
        var incremento = this.incremento.honra;

        var Now = new Date();
        var tempoCalc = Math.round(Now.getTime()/1000) - this.tempo;

        var recursoCalculado = Math.round(recurso + ((tempoCalc/60) * incremento));

        var canvasElem: any = document.getElementById('honra');

        var RecursoValor = canvasElem.getContext('2d');

        var armazenamento = Math.round((recursoCalculado/max) * 48);

        if(recursoCalculado > max)
            armazenamento = 48;

        $('#honra').html(recursoCalculado);

        var gradient = RecursoValor.createLinearGradient(0, 0, 0, 48);
        gradient.addColorStop(0, '#fbf306');
        gradient.addColorStop(1, '#FF9800');

        RecursoValor.beginPath();
        RecursoValor.clearRect(0, 0, 48, 48);
        RecursoValor.rect(0, (48 - armazenamento), 48, 48);

        RecursoValor.fillStyle = gradient;

        RecursoValor.fill();
        RecursoValor.font = '12pt';
        RecursoValor.textAlign = 'center';

        if(+$('#honra').text() < max) {
            RecursoValor.fillStyle = 'white';

            RecursoValor.fillText(recursoCalculado, 24, 44);
        } else {
            RecursoValor.fillStyle = 'white';

            RecursoValor.fillText(max, 24, 44);
        }
    }

    /**
     * Atualiza os recursos
     */
    private atualizacao() {
        if(this.recursos) {
            this.canvas('dinheiro', +this.recursos.dinheiro, this.incremento.dinheiro, this.armazenamento.dinheiro, '#1FEB0D', '#005206');
            this.canvas('alimento', +this.recursos.alimento, this.incremento.alimento, this.armazenamento.alimento, '#B40000', '#4D0000');
            this.canvas('metal', +this.recursos.metal, this.incremento.metal, this.armazenamento.metal, '#52B3AE', '#4D7E8D');
            this.canvas('petroleo', +this.recursos.petroleo, this.incremento.petroleo, this.armazenamento.petroleo, '#464646', '#141414');
            this.canvas('ouro', +this.recursos.ouro, 0, +this.recursos.ouro * 1.1, '#F5FF00', '#614A00');
            this.canvas('energia', +this.recursos.energia, 0, +this.recursos.energia * 1.1, '#FFA300', '#611300');
            this.canvas('populacao', +this.recursos.populacao, 0, +this.recursos.populacao * 1.1, '#00B8FF', '#003661');

            this.canvasHonra();
        }
    }

}