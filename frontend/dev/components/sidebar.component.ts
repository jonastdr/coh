import {Component, OnInit} from "@angular/core";
import {Api} from "../api";
import {GameService} from "../services/game.service";
import {TimerDirective} from "../directives/timer.directive";
@Component({
    selector: 'coh-sidebar',
    templateUrl: Api.url('sidebar'),
    directives: [
        TimerDirective
    ]
})
export class SidebarComponent implements OnInit {

    construcao;
    pesquisa;
    unidade;

    constructor(private gameService: GameService) {
    }

    ngOnInit() {
        this.get();

        //escuta de atualização do sidebar
        this.gameService.refreshSidebar$.subscribe(() => {
            this.get();
        });
    }

    /**
     * Pega os dados de sidebar
     */
    get() {
        this.gameService.sidebar().subscribe(res => {
            if(res.success) {
                this.construcao = res.construcao;
                this.pesquisa = res.pesquisa;
                this.unidade = res.unidade;
            }
        });
    }

    /**
     * Imagem da pesquisa
     * @param pesquisa
     * @returns {string}
     */
    pesquisaImage(pesquisa) {
        return "url('imagens/pesquisas/" + pesquisa.id + ".png')";
    }

    /**
     * Imagem do de construcao
     * @param construcao
     * @returns {string}
     */
    construcaoImage(construcao) {
        return "url('imagens/construcoes/" + construcao.id + ".png')";
    }

    /**
     * Imagem de unidade
     * @param requisito
     * @returns {string}
     */
    unidadeImage(unidade) {
        return "url('imagens/unidades/" + unidade.id + ".png')";
    }

    termino() {
        this.get();
    }

}