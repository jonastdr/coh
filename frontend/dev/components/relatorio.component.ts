import {Component, ElementRef, OnInit} from "@angular/core";
import {Api} from "../api";
import {IDialog} from "../interfaces/dialog.interface";
import {RelatorioService} from "../services/relatorio.service";
import {Game} from "../game";
import {WindowDialog} from "./window.dialog";
import {AppComponent} from "./app.component";
@Component({
    selector: 'coh-relatorio',
    templateUrl: Api.url('relatorio')
})

export class RelatorioComponent extends WindowDialog implements OnInit, IDialog {

    params;

    recursos = {};
    status = '';
    pontos = 0;
    data = new Date().toDateString();
    detalhes = {};
    unidades = {
        ataque: {
            primeiro: [],
            ultimo: []
        },
        defesa: {
            primeiro: [],
            ultimo: []
        }
    };

    constructor(private elementRef: ElementRef, private relatorioService: RelatorioService) {
        super('Relatório', elementRef);
    }

    ngOnInit() {
        this.relatorioService.getAll(this.params.link).subscribe(res => {
            if(res.success === true) {
                this.recursos = res.recursos;
                this.status = res.status;
                this.pontos = res.pontos;
                this.data = res.data;
                this.detalhes = res.detalhes;
                this.unidades = res.unidades;

                this.showWindow();
            } else {
                Game.mensagem(res.msg);

                this.closeWindow();
            }
        });
    }

    /**
     * Imagem da unidade
     * @param unidade
     * @returns {string}
     */
    unidadeImage(unidade) {
        return "url(imagens/unidades/" + unidade.id + ".png)";
    }

    /**
     * Faz o calculo das perdas
     * @param ultimo
     * @param primeiro
     * @returns {number}
     */
    calcPerdas(ultimo, primeiro) {
        return ultimo.quantidade - primeiro.quantidade;
    }

    openUsuario(id_usuario) {
        AppComponent.openWindow('usuario', {
            id_usuario: id_usuario
        });
    }

    openCidade(id_usuario) {
        AppComponent.openWindow('usuario', {
            id_usuario: id_usuario,
            aba: 'cidades'
        });
    }

}