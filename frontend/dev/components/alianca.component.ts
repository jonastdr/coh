import {Component, OnInit, ElementRef} from "@angular/core";
import {Api} from "../api";
import {IDialogTabs} from "../interfaces/dialog.tabs.interface";
import {AliancaService} from "../services/alianca.service";
import {WindowDialog} from "./window.dialog";
import {Game} from "../game";
import {ScrollDirective} from "../directives/scroll.directive";
import {AppComponent} from "./app.component";
@Component({
    selector: 'coh-alianca',
    templateUrl: Api.url('alianca'),
    directives: [
        ScrollDirective
    ]
})

export class AliancaComponent extends WindowDialog implements OnInit, IDialogTabs {

    aba: string = 'inicial';

    busca: string = '';

    alianca = {};
    membros = [];
    usuarios = [];

    niveis = [];

    /**
     * Permissões do usuário na aliança
     * @type {Array}
     */
    permissoes = [];

    /**
     * Usado em buscas de alianças
     * @type {Array}
     */
    aliancas = [];

    constructor(elementRef: ElementRef,
                private aliancaService: AliancaService) {
        super('Aliança', elementRef);
    }

    ngOnInit() {
        this.getAll();
    }

    /**
     * Realiza uma busca por aliança
     * @param ev
     */
    buscarAlianca(ev?: KeyboardEvent) {
        /**
         * Função que realiza um busca em si
         */
        let exec = () => {
            this.aliancaService.buscarAlianca(this.busca).subscribe(res => {
                if(res.success === true) {
                    this.aliancas = res.aliancas;
                } else {
                    Game.mensagem(res.msg);
                }
            });
        }

        /**
         * Se for pressionado enter ou clicado no botão de submissão faz o envio
         */
        if(ev) {
            if(ev.keyCode == 13) {
                exec();

                return;
            }
        } else {
            exec();
        }
    }

    /**
     * Cria uma aliança
     * @param ev
     */
    criarAlianca() {
        if(this.busca == '') {
            Game.mensagem('Informe o nome da aliança.');

            return;
        }

        this.aliancaService.criarAlianca(this.busca).subscribe(res => {
            Game.mensagem(res.msg);

            if(res.success === true) {
                setTimeout(() => {
                    location.href = res.redirect;
                }, 1000);
            }
        });
    }

    /**
     * Faz o alistamento em uma aliança
     * @param alianca
     */
    alistar(alianca) {
        var mensagem = prompt('Qual a mensagem de alistamento irá enviar?');

        if(mensagem === '') {
            Game.mensagem('Informe uma mensagem de alistamento.');
        }
        else if(mensagem !== null) {
            this.aliancaService.alistar(alianca, mensagem).subscribe(res => {
                if(res.success === true) {

                }

                Game.mensagem(res.msg);
            });
        }
    }

    /**
     * Aceita um novo membro
     * @param usuario
     */
    aceitarRecrutamento(usuario?) {
        if(usuario) {
            if(confirm('Tem certeza que deseja realizar esta ação?')) {
                this.aliancaService.aceitarRecrutamento(usuario).subscribe(res => {
                    if(res.success == true) {
                        this.alianca = res.alianca;
                        this.membros = res.membros;
                        this.usuarios = res.usuarios;
                        this.permissoes = res.permissoes;
                    }

                    Game.mensagem(res.mensagem);
                });
            }
        } else {

        }
    }

    /**
     * Rejeita um pedido de recrutamento
     * @param usuario
     */
    rejeitarRecrutamento(usuario?) {
        if(usuario) {
            if(confirm('Tem certeza que deseja realizar esta ação?')) {
                this.aliancaService.rejeitarRecrutamento(usuario).subscribe(res => {
                    if(res.success == true) {
                        this.alianca = res.alianca;
                        this.membros = res.membros;
                        this.usuarios = res.usuarios;
                    }

                    Game.mensagem(res.mensagem);
                });
            }
        } else {

        }
    }

    /**
     * Traz todos os dados iniciais
     */
    getAll() {
        this.aliancaService.getAll().subscribe(res => {
            if(res.success === true) {
                this.alianca = res.alianca;
                this.membros = res.membros;
                this.usuarios = res.usuarios;
                this.permissoes = res.permissoes;
                this.niveis = res.niveis;

                this.aliancas = res.aliancas;

                this.showWindow();
            } else {
                this.closeWindow();
            }
        });
    }

    alternarAba(aba: string) {
        this.aba = aba;
    }

    /**
     * Altera uma configuração
     * @param acao
     * @param value
     */
    alterarPropriedade(acao, value) {
        this.aliancaService.propriedades(acao, value).subscribe(res => {
            if(res.success === true) {

            }

            Game.mensagem(res.msg);
        });
    }

    /**
     * bloqueia um usuário da aliança
     * @param membro
     */
    bloquearMembro(membro) {
        if(confirm('Tem certeza que deseja bloquear este membro?')) {
            this.aliancaService.propriedades('bloquear', membro.id_usuario).subscribe(res => {
                if(res.success === true) {
                    this.getAll();
                }

                Game.mensagem(res.msg);
            });
        }
    }

    /**
     * Expulsa um usuário da aliança
     * @param membro
     */
    expulsarMembro(membro) {
        if(confirm('Tem certeza que deseja expulsar este membro?')) {
            this.aliancaService.propriedades('expulsar', membro.id_usuario).subscribe(res => {
                if(res.success === true) {
                    this.getAll();
                }

                Game.mensagem(res.msg);
            });
        }
    }

    /**
     * Abre a tela de estatísticas na pagina do usuário selecionado
     * @param membro
     */
    abrirEstatisticas(membro) {
        AppComponent.openWindow('estatisticas', {
            'usuarios': membro.id_usuario
        });
    }

    /**
     * Abre a tela de mensagens na aba de nova mensagem
     * @param membro
     */
    abrirMensagem(membro) {
        AppComponent.openWindow('mensagem', {
            /**
             * Para qual aba irá
             */
            aba: 'nova',
            /**
             * Indica que é uma mensagem de aliança
             */
            alianca: true,
            /**
             * Usuários que irá pre-selecionado
             */
            usuarios: [
                {
                    id: membro.id_usuario,
                    label: membro.nome
                }
            ]
        });
    }

    /**
     * Membro abandona a aliança
     */
    abandonar() {
        if(confirm("Tem certeza que deseja abandonar a aliança?")) {
            this.aliancaService.abandonar().subscribe(res => {
                Game.mensagem(res.msg);

                if(res.success === true) {
                    this.closeWindow();

                    setTimeout(() => {
                        location.href = res.redirect;
                    }, 1000);
                }
            });
        }
    }

    /**
     *
     */
    excluir() {
        if(confirm("Tem certeza que deseja excluir esta aliança?")) {
            this.aliancaService.excluir().subscribe(res => {
                Game.mensagem(res.msg);

                if(res.success === true) {
                    this.closeWindow();

                    setTimeout(() => {
                        location.href = res.redirect;
                    }, 1000);
                }
            });
        }
    }

    /**
     * Abre a tela de níveis
     */
    abrirNiveis() {
        AppComponent.openWindow('alianca-nivel');
    }

    /**
     * Altera o nível de um membro
     * @param membro
     */
    alterarNivel(membro) {
        this.aliancaService.alterarMembroNivel(membro).subscribe(res => {
            if(res.success === false) {
                this.membros = res.membros;

                Game.mensagem(res.msg);
            }
        });
    }

}