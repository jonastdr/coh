import {Component} from "@angular/core";
import {Api} from "../api";
import {Game} from "../game";
import {AppComponent} from "./app.component";
import {GameService} from "../services/game.service";
import {MapaService} from "../services/mapa.service";
@Component({
    selector: 'coh-mapa',
    templateUrl: Api.url('mapa')
})
export class MapaComponent {

    menuId: string = '';

    cidadeAtual = {
        id: null,
        x: null,
        y: null
    };

    ilhas = [];

    posx = 0;
    posy = 0;

    radius = {
        x_start: 0,
        x_end: 0,
        y_start: 0,
        y_end: 0
    };

    timeout = null;

    emitterMapa;

    constructor(private gameService: GameService, private mapaService: MapaService) {
    }

    ngOnInit() {
        $('#mapa_fundo').draggable({
            containment: [
                -99560,
                -59560,
                0,
                0
            ],
            cursor: 'move',
            drag: () => {
                var width = $(window).width();
                var height = $(window).height();

                var pos = $('#mapa_fundo').position();

                var x = Math.abs(pos.left);
                var y = Math.abs(pos.top);

                if(
                    ((x - width) < this.radius.x_start || (x + width) > this.radius.x_end)
                    ||
                    ((y - height) < this.radius.y_start || (y + height) > this.radius.y_end)
                ) {
                    this.push();
                }

                this.calcCoords();
            },
            stop: () => {

            }
        });

        this.emitterMapa = this.gameService.refreshCity$.subscribe(cidade => {
            this.cidadeAtual = cidade;

            this.getPosition();

            this.calcCoords();
        });

        //let loading = Game.mensagem('Carregando...', '', false);

        this.mapaService.pushInitial().subscribe(res => {
            if(res.success) {
                this.cidadeAtual = res.cidadeAtual;

                this.getPosition();

                this.calcCoords();
            }

            /**
             * Termino de carregamento
             */
            setTimeout(() => {
                //loading.remove();
            }, 1000);
        });
    }

    ngOnDestroy() {
        this.emitterMapa.unsubscribe();
    }

    calcCoords() {
        var posx = parseInt($('#mapa_fundo').css('left'));
        var posy = parseInt($('#mapa_fundo').css('top'));

        var xcalc = (posx / 10)*-1;
        var ycalc = (posy / 10)*-1;

        this.posx = Math.round(xcalc);
        this.posy = Math.round(ycalc);
    }

    /**
     * Cria o raio para encontrar as ilhas
     * @returns {{x_start: number, x_end: number, y_start: number, y_end: number}}
     */
    getRadius() {
        var widthX2 = $(window).width() * 2;
        var heightX2 = $(window).height() * 2;

        var pos = $('#mapa_fundo').position();

        var radius = {
            x_start: Math.abs(pos.left) - widthX2,
            x_end: Math.abs(pos.left) + widthX2,
            y_start: Math.abs(pos.top) - heightX2,
            y_end: Math.abs(pos.top) + heightX2
        };

        this.radius = radius;

        return radius;
    }

    /**
     * Traz os dados para o mapa
     */
    push() {
        if(this.timeout)
            return;

        this.timeout = setTimeout(() => {
            Game.mensagem();
        }, 2000);

        this.mapaService.push(this.getRadius()).subscribe(res => {
            if(res.success === true) {
                this.ilhas = res.ilhas;
                this.cidadeAtual = res.cidadeAtual;
            }

            clearTimeout(this.timeout);
            this.timeout = null;
        });
    }

    /**
     *
     * @param city
     */
    showOptions(cidade) {
        this.menuId = cidade.cod_ilha + '' + cidade.cod_cidade;
    }

    /**
     * Se clicar no mapa fecha o menu
     * @param e
     */
    hideOptions(e) {
        if(!!$(e.target).has('.menucity').length) {
            setTimeout(() => {
                this.menuId = '';
            }, 500);

            $('.menucity').addClass('remove');
        }
    }

    /**
     * Fecha o menu
     */
    hideOptionsImediatly() {
        setTimeout(() => {
            this.menuId = '';
        }, 500);

        $('.menucity').addClass('remove');
    }

    /**
     *
     * @param $event
     */
    changeCoords($event) {
        var x, y;

        if($event.keyCode == 13) {
            if(this.posx >= 9956){
                x = (9956 * 10)*-1;
            }
            else if(this.posx > 0){
                x = (this.posx * 10)*-1;
            } else {
                x = 0;
            }
            $('#mapa_fundo').css('left', x + 'px');

            if(this.posy >= 5956){
                y = (5956 * 10)*-1;
            }
            else if(this.posy > 0){
                y = (this.posy * 10)*-1;
            } else {
                y = 0;
            }

            $('#mapa_fundo').css('top', y + 'px');

            this.push();
        }
    }

    /**
     * retorna a posição da cidade atualmente selecionada
     */
    getPosition() {
        var widthX2 = $(window).width() / 2;
        var heightX2 = $(window).height() / 2;

        var left = - this.cidadeAtual.x + widthX2;
        var top = - this.cidadeAtual.y + heightX2;

        if(left > 0) {
            left = 0;
        }

        if(top > 0) {
            top = 0;
        }

        $('#mapa_fundo').css({
            left: left,
            top: top
        });

        this.push();
    }

    /**
     * Abre uma expedição
     * @param cidade
     */
    openExpedicao(cidade) {
        AppComponent.openWindow('expedicao', cidade);

        this.hideOptionsImediatly();
    }

    /**
     * Abre o menu de um usuário
     * @param cidade
     */
    openUsuario(cidade) {
        AppComponent.openWindow('usuario', cidade);

        this.hideOptionsImediatly();
    }

    /**
     * Abre a tela de espionagem
     * @param cidade
     */
    openEspionagem(cidade) {
        AppComponent.openWindow('espionagem', cidade);

        this.hideOptionsImediatly();
    }

    /**
     * Retorna a imagem da ilha
     * @param ilha
     * @returns {string}
     */
    imageIsland(ilha) {
        return "url('map/" + ilha.img + ".png')";
    }

}