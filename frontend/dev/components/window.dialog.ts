import {ElementRef, EventEmitter} from "@angular/core";
import {Game} from "../game";
export abstract class WindowDialog {

    aba: string;

    /**
     * Evento para emitir a saída do modal
     * @type {EventEmitter}
     */
    event: EventEmitter = new EventEmitter();

    /**
     *
     * @param title
     * @param elem
     */
    constructor(private title, private elem: ElementRef) {
        /**
         * Esconde até que seja executado o método showWindow()
         */
        $(this.elem.nativeElement).hide();
    }

    /**
     * Ação ao remover o component
     */
    ngOnDestroy() {
        $(this.elem.nativeElement).remove();
    }

    /**
     * Cria o dialog
     */
    protected showWindow() {
        $(this.elem.nativeElement).dialog(this.config());
    }

    /**
     * Fecha o dialog
     */
    closeWindow() {
        this.event.emit('close');

        $(this.elem.nativeElement).remove();
    }

    /**
     * Altera o título do dialog
     */
    setTitle(title: string) {
        this.title = title;
    }

    /**
     * Define se a aba está ativa
     * @param aba
     * @returns {string|string}
     */
    abaAtiva(aba: string) {
        return this.aba == aba ? 'ativo' : '';
    }

    /**
     * Carrega a configuração do dialog
     * @returns {{width: string, resizable: boolean, close: (()), open: (()=>undefined)}}
     */
    config() {
        return {
            width: 'auto',
            resizable: false,
            close: () => {
                this.closeWindow();
            },
            open: () => {
                var self = $(this.elem.nativeElement);

                var ifTab = false;

                //variaveis
                var titulo = self.closest('.ui-dialog').find('.ui-dialog-titlebar'),
                    tabs = self.children('.dialog-tabs'),
                    allTabs = $('<div>').addClass('dialog-tabs').append(tabs),
                    tituloText = $('<div>').addClass('dialog-header-title').html(this.title),
                    header = $('<div>').addClass('dialog-header'),
                    controles;

                self.closest('.ui-dialog').find('.ui-dialog-title').remove();

                allTabs.width(
                    self.width() - parseInt(self.width() * 25 / 100)
                );

                // verificar se irá adicionar as abas da janela
                if(tabs.length > 0) {
                    //para tirar animacoes
                    ifTab = true;

                    //cria botoes de navegacao
                    var mascara = $('<span>').addClass('dialog-tabs-mask').appendTo(allTabs);

                    var maxScroll = tabs.outerWidth() - allTabs.outerWidth(),
                        atual = 1,
                        soma = 0;

                    controles = $('<div>').addClass('dialog-tabs-controls').appendTo(allTabs);

                    var botoaoA = $('<a>').html('<i class="fa fa-chevron-circle-left"></i>').click(function(){
                        var pos = $(tabs).find('li:nth-child(' + (atual-1) + ')'),
                            anterior = $(tabs).find('li:nth-child(' + (atual) + ')');

                        if(pos.length > 0){
                            soma -= pos.outerWidth();

                            allTabs.animate({
                                scrollLeft: soma
                            }, 200);

                            atual--;
                        }

                    }).appendTo(controles);

                    var botoaoB = $('<a>').html('<i class="fa fa-chevron-circle-right"></i>').click(function(){
                        var pos = $(tabs).find('li:nth-child(' + (atual+1) + ')'),
                            anterior = $(tabs).find('li:nth-child(' + (atual) + ')');

                        if(pos.length > 0){
                            soma += anterior.outerWidth();

                            allTabs.animate({
                                scrollLeft: soma
                            }, 200);

                            atual++;
                        }

                    }).appendTo(controles);

                    header.append(allTabs);
                }

                header.prepend(tituloText);
                titulo.prepend(header);

                //eliminar botoes se for o caso
                var tamanho = 0,
                    max = allTabs.width() - (allTabs.width() * 25 / 100);

                tabs.find('li').each(function(index, el) {
                    tamanho += $(this).width();
                });

                if(max >= tamanho && typeof controles !== 'undefined')
                    controles.remove();

                tabs.width(tamanho + $(self).width());
            }
        };
    }

}