export class Game {

    static mensagem(mensagem?:string, classe?:string, delay?:any) {
        var anterior = $('.alerta');

        if(anterior.length) {
            anterior.remove();
        }

        classe = (typeof classe == 'undefined' ? '' : ' ' + classe);

        var msgBox;

        msgBox = $('<span>').addClass('alerta' + classe).appendTo('body');

        if(typeof mensagem == 'undefined' || mensagem === '')
            mensagem = $('<i>').addClass('fa fa-cog fa-spin fa-3x fa-fw').appendTo(msgBox);
        else
            msgBox.html(mensagem);

        msgBox.css({
            left: ($(window).width() / 2) - (msgBox.width() / 2),
            top: ($(window).height() / 2) - (msgBox.height() / 2)
        });

        if(typeof delay !== 'undefined') {
            msgBox.delay(delay)
                .fadeOut(500, function () {
                    $(this).remove();
                });
        } else if(delay === false) {
            //não remove
        } else {
            msgBox.delay(3000)
                .fadeOut(500, function() {
                    $(this).remove();
                });
        }

        var opcoes = {
            remove: function(){
                msgBox.fadeOut(500, function() {
                    $(this).remove();
                });
            },
            removeNow: function(){
                msgBox.remove();
            },
            removeDelay: function(delay){
                msgBox.delay(delay).fadeOut(500, function() {
                    $(this).remove();
                });
            },
            alterar: function(html, classe){
                if(typeof classe !== 'undefined')
                    msgBox.addClass(classe);

                msgBox.html(html);

                return opcoes;
            }
        };

        return opcoes;
    }

}