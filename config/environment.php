<?php

/**
 * Configuraçoes de Ambiente - ProjectWS
 */

return [
    'NAME' => 'game',//nome do projeto
    'ENV' => 'system',//ambiente de desenvolvimento
    'BASE' => 'coh/public/',//usado para informar a url base do projeto
    'FOLDERS' => [
        'APP' => 'app/',
        'CONTROLLERS' => 'controllers/',
        'EVENTS' => 'events/',
        'HELPERS' => 'helpers/',
        'LANGUAGES' => 'languages/',
        'MIDDLEWARES' => 'middlewares/',
        'MODELS' => 'models/',
        'VIEWS' => 'views/',
        'ASSETS' => 'assets/'//arquivos publicos da aplicação
    ],
    'ALIAS' => [//acesso 'curto' das classes com namespaces
        'Request' => 'projectws\libs\Request',
        'Response' => 'projectws\libs\Response',
        'Form' => 'projectws\libs\Form',
        'Router' => 'projectws\libs\Router'
    ],
    'LOG' => true,//para registros de logs
    'KEY_ENCRYPT' => 'htdhgjdjtjjf',//codigo definido usado para criptografia
    'DATE_FORMAT_PREVIEW' => 'd/m/Y',//formato para visualização da data
    'TIME_FORMAT_PREVIEW' => 'H:i:s',//formato para visualização de hora
    'TIME_ZONE' => 'America/Sao_Paulo',//Define o timezone da aplicação
    'DROP_CACHE_EXPIRE' => true//Se estiver ativo irá deletar os arquivos de cache se não for mais usado o cache do objeto atual
];