<?php

return [
    'timestamp_created' => 'data_criacao',
    'timestamp_updated' => 'data_modificacao',
    'trash' => 'data_removido'
];