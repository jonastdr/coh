<?php

/**
 * Carrega automaticamente as classes
 */

return [
    'request' => 'projectws\libs\Request',
    'cookie' => 'projectws\libs\Cookie',
    'response' => 'projectws\libs\Response',
    'tag' => 'projectws\libs\Tag'
];