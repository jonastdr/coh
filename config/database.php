<?php

return [
    'DRIVER' => 'pgsql',
    'HOST' => '127.0.0.1',
    'DBNAME' => 'coh',
    'USERNAME' => 'postgres',
    'PASSWORD' => 'postgres',
    'CHARSET' => 'utf8',
    'PORT' => '5432',
    'DEBUG' => false
];