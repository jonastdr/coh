var app = {
    tempo: 0,
    atividades: function (callback) {
        this.notificacao.procurar();

        $.post(urlBase + "expedicao/atividade", function (data) {
            $("#menu_atividade").html(data);

            var count = $("#menu_atividade ul li").length;

            if(count == 1) {
                $("#cidade-atividade").attr("title", "1 atividade").addClass("atividade");
            } else if(count > 1) {
                $("#cidade-atividade").attr("title", count + " atividades!").addClass("atividade");
            } else {
                $("#cidade-atividade").attr("title", "Nenhuma atividade!").removeClass("atividade");
            }

            if(callback != undefined)
                callback();
        });
    },
    recursos: function(id, corA, corB) {
        var el = $("#" + id);

        if(el.length > 0) {
            var props = el.closest(".recursos_painel"),
                recurso = parseFloat(props.attr("data-quantidade")),
                max = parseFloat(props.attr("data-max")),
                incremento = parseFloat(props.attr("data-incremento"));

            if (isNaN(max)) max = recurso;

            if (isNaN(incremento)) incremento = 0;

            var Now = new Date();
            var tempoCalc = Math.round(Now.getTime() / 1000) - app.tempo;

            var RecursoValor = document.getElementById(id).getContext('2d');

            var armazenamento = Math.round((recurso / max) * 44);

            $('#' + id).html(Math.round(recurso + ((tempoCalc / 60) * incremento)));

            var gradient = RecursoValor.createLinearGradient(0, 0, 44, 0);
            gradient.addColorStop(0, corA);
            gradient.addColorStop(1, corB);

            RecursoValor.beginPath();
            RecursoValor.clearRect(0, 0, 44, 12);
            RecursoValor.rect(0, 0, armazenamento, 13);

            RecursoValor.fillStyle = gradient;

            RecursoValor.fill();
            RecursoValor.fillStyle = 'white';
            RecursoValor.font = '12pt';
            RecursoValor.textAlign = 'center';

            if ($('#' + id).text() < max) {
                RecursoValor.fillText(Math.round(recurso + ((tempoCalc / 60) * incremento), 0), 22, 10);
            } else {
                RecursoValor.fillText(Math.round(recurso, 0), 22, 10);
            }
        }
    },
    //geração de mapas
    mapa: {
        cidades: []
    },
    //Tela de expedição
    expedicao: {
        capacidade: 0,
        unidadesCount: 0,
        duracao: 0,
        tempoCalculado: 0,
        velocidadeMaxima: null,
        unidades: [],
        //verifica a capacidade total da frota
        verificaCapacidade: function () {
            var self = this;

            self.capacidade = 0;
            self.unidadesCount = 0;

            self.velocidadeMaxima = null;

            $("#expedicao .unidade input").each(function () {
                var quantidade = parseInt($(this).val() == '' ? 0 : $(this).val());

                var id = $(this).attr("id").replace(/unidade\[(.*)\]/g, "$1");

                if(quantidade > self.unidades[id].quantidade) {
                    $(this).val(self.unidades[id].quantidade);

                    quantidade = self.unidades[id].quantidade;
                }

                if (quantidade > 0) {
                    self.capacidade += self.unidades[id].capacidade * quantidade;

                    if(self.unidades[id].velocidade < self.velocidadeMaxima || self.velocidadeMaxima == null)
                        self.velocidadeMaxima = self.unidades[id].velocidade;

                    self.verificaRestante();
                }

                self.unidadesCount += quantidade;

                $("#expedicao .unidades_quantidade").text(self.unidadesCount);
            });

            if(self.velocidadeMaxima == null)
                self.velocidadeMinimaInicial();

            //Atualiza informação da velocidade máxima
            $('#expedicao .velocidade_maxima').text(self.velocidadeMaxima);
        },
        //Verifica quantidade de recursos que pra armazenamento
        verificaRestante: function () {
            var recursos = 0;
            $("#expedicao .recursos input").each(function () {
                var valor = parseInt($(this).val() == '' ? 0 : $(this).val());

                recursos += valor;
            });

            var restante = this.capacidade - recursos;

            if(restante < 0) {
                var breake = false;

                restante = Math.abs(restante);

                $("#expedicao .recursos input").each(function () {
                    if(breake)
                        return false;

                    if($(this).val() > restante) {
                        $(this).val( parseInt($(this).val()) - restante );

                        restante = 0;
                    } else {
                        $(this).val( 0 );

                        restante -= parseInt($(this).val());
                    }

                    if($(this).val() == 0)
                        $(this).val('');

                    if(restante == 0)
                        breake = true;
                });
            }


            $("#expedicao .capacidade_restante").text(restante);
        },
        //Renderiza a visualização da capacidade
        defineCapacidade: function () {
            $("#expedicao .capacidade").text(this.capacidade);
        },
        //Seleciona a menor velocidade
        calculaDuracao: function (velocidadeMaxima, distancia, fator) {
            var jogoVelocidade = 10;

            var duracao = Math.round((5 / jogoVelocidade) * ( ( Math.sqrt( (distancia/1000)/velocidadeMaxima)) * 3600) / fator);

            return duracao;
        },
        //Faz o calculo da distância
        calculaDistancia: function () {
            var destino = [$("#expedicao #posicao_0").val(), $("#expedicao #posicao_1").val()],
                atacante = [$("#expedicao #minha_posicao_0").val(), $("#expedicao #minha_posicao_1").val()];

            return Math.sqrt(Math.pow((destino[0]-atacante[0]), 2) + Math.pow((destino[1] - atacante[1]), 2)) * 1000;
        },
        //Faz a atualização da duração da viagem
        atualizaDuracao: function () {
            var self = this;
            var distancia = self.calculaDistancia();

            self.duracao = self.calculaDuracao(self.velocidadeMaxima, distancia, 1);
        },
        //Atualizacao do tempo de chegada
        atualizaTempo: function() {
            var self = this;

            var calculaTempo = setInterval(function () {
                if ($('.tempo_chegada').length == 0) {
                    clearInterval(calculaTempo);

                    return false;
                }

                var horarioChegada = parseInt($("#expedicao #horarioChegada").val());

                var horarioAtual = new Date((horarioChegada + self.duracao + self.tempoCalculado) * 1000);

                var min = parseInt(self.duracao / 60);
                var hor = parseInt(min / 60);
                var seg = self.duracao % 60;

                var min = min % 60;

                if (hor < 10) {
                    hor = '0' + hor;
                    hor = hor.substr(0, 2);
                }

                if (min < 10) {
                    min = '0' + min;
                    min = min.substr(0, 2);
                }
                if (seg < 10) {
                    seg = '0' + seg;
                }

                $('.tempo_restante').html(hor + ":" + min + ":" + seg);

                var horaAtual = horarioAtual.getHours(),
                    minutosAtual = horarioAtual.getMinutes(),
                    segundosAtual = horarioAtual.getSeconds();

                if (minutosAtual < 10) minutosAtual = "0" + minutosAtual;
                if (segundosAtual < 10) segundosAtual = "0" + segundosAtual;

                $('.tempo_chegada').html(horaAtual + ':' + minutosAtual + ':' + segundosAtual);

                self.tempoCalculado++;
            }, 1000);
        },
        //Aciona os eventos da tela de expedição
        eventos: function () {
            var self = this;

            var soma = function (el, event, quantidade) {
                $(el).val( $(el).val().replace(/[^0-9]/, '') );

                var valor = parseInt($(el).val() == '' ? 0 : $(el).val());

                if(event.keyCode == 38 || event.keyCode == 40) {
                    //para cima
                    if (event.keyCode == 38) {
                        valor += quantidade;
                    }

                    //para baixo
                    if (event.keyCode == 40 && valor > 0) {
                        valor -= quantidade;
                    }
                }

                if(valor <= 0)
                    valor = '';

                $(el).val(valor);
            };

            //Calculos de capacidade
            $("#expedicao .unidade input").off("keyup");

            $("#expedicao .unidade input").on("keyup", function (e) {
                soma(this, e, 1);

                self.verificaCapacidade();

                self.defineCapacidade();

                self.verificaRestante();

                self.atualizaDuracao();
            });

            //Calculos de recursos
            $("#expedicao .recursos input").off("keyup");

            $("#expedicao .recursos input").on("keyup", function (e) {
                if(self.unidadesCount == 0) {
                    e.preventDefault();
                } else {
                    soma(this, e, 10);

                    self.verificaRestante();
                }
            });

            $("#expedicao").submit(function () {
                if(self.unidadesCount == 0) {
                    game.mensagem("Deve informar pelo menos 1 unidade!", "", 1000);

                    return false;
                }

                if(parseInt($("#expedicao .capacidade_restante").text()) < 0) {
                    game.mensagem("Os recursos selecionados ultrapassam o limite de armazenamento!", "", 3000);

                    return false;
                }

                $(this).closest(".ui-dialog").hide();
            });
        },
        //velocidade Minima || nenhum selecionado
        velocidadeMinimaInicial: function () {
            var self = this;

            self.velocidadeMaxima = null;

            $.each(self.unidades, function (id) {
                if(self.unidades[id].velocidade < self.velocidadeMaxima || self.velocidadeMaxima == null)
                    self.velocidadeMaxima = self.unidades[id].velocidade;
            });

            //Atualiza informação da velocidade máxima
            $("#expedicao .velocidade_maxima").text(self.velocidadeMaxima);
        },
        //inicializa a tela de expedição
        init: function () {
            this.tempoCalculado = 0;

            this.velocidadeMinimaInicial();
            this.eventos();
            this.atualizaDuracao();
            this.atualizaTempo();
        }
    },
    notificacao: {
        ultima: null,
        init: function () {
            this.ultima = Date.now() / 1000;

            $("#notificacoes li").tooltip();

            $("#notificacoes").on("click", "a", function () {
                var notificacao = $(this).parent().attr("notificacao");

                $(this).parent().fadeOut(1000, function () {
                    $.post(urlBase + "game/notificacao", { id_notificacao: notificacao });
                });
            });
        },
        procurar: function () {
            var $this = this;

            $.getJSON(urlBase + "game/notificacao", {ultima: this.ultima}, function (data) {
                $this.ultima = Date.now() / 1000;

                $.each(data, function (index, notificacao) {
                    var li = $("<li>"),
                        a = $("<a>");

                    li.attr({
                        title: notificacao.mensagem,
                        notificacao: notificacao.id
                    });

                    a.attr({
                        href: notificacao.link,
                        "dialog-title": notificacao.mensagem
                    }).addClass("abrir_dialog");

                    li.append(a);

                    $("#notificacoes ul").prepend(li);
                });
            });
        }
    }
};

/*pesquisa*/
var openPesquisa = function (self) {
    var pesquisa = $(self).attr('data-pesquisa');

    $.post(urlBase + "laboratorio/requisitos", {id: pesquisa}, function (data) {
        $('#Laboratorio .requisitos').html(data);
    });

    $.post(urlBase + "laboratorio/sidebar", {id: pesquisa}, function (data) {
        $('#Laboratorio .sidebar').html(data);
    });
};

$(document).on('click', '#Laboratorio .pesquisas.links li', function () {
    openPesquisa(this);
});

/*barraca*/
var openUnidade = function (self) {
    var unidade = $(self).attr('data-unidade');

    $.post(urlBase + "barraca/unidade", {id: unidade}, function (data) {
        $('#Barraca .sobre_unidade').html(data);
    });

    $.get(urlBase + "barraca/recrutamento", function (data) {
        $('#Barraca .recrutamento').html(data);
    });
};

$(document).on('click', '#Barraca .lista .unidades.links li', function () {
    openUnidade(this);
});

/*Academia*/
var openHeroi = function (self) {
    var heroi = $(self).attr('data-heroi');

    $.post(urlBase + "academia/funcoes", {id: heroi}, function (data) {
        $('#Academia .funcoes').html(data);
    });

    $.post(urlBase + "academia/sidebar", {id: heroi}, function (data) {
        $('#Academia .sidebar').html(data);
    });
};

/*Função ao clicar em um heroi*/
$(document).on('click', '#Academia .lista .herois.links li', function () {
    openHeroi(this);
});

/*Função ao clicar em uma função*/
$(document).on('click', '#Academia .funcoes .herois.links li', function () {
    var funcao = $(this).attr('data-funcao');

    $.post(urlBase + "academia/funcao", {id: funcao}, function (data) {
        $('#Academia .sidebar').html(data);
    });
});

//Atualização de recursos
Now = new Date();
app.tempo = Math.round(Now.getTime()/1000);

/*sidebar*/
setInterval(function () {
    var tempoAttr = $('.tempo');

    $.each(tempoAttr, function(index, el){

        var timer = $(el);

        timer.attr('data-tempo', parseInt(timer.attr('data-tempo')) - 1);

        var restante = timer.attr('data-tempo');

        var min = parseInt(restante / 60);
        var hor = parseInt(min / 60);
        var seg = restante % 60;

        var min = min % 60;

        if (hor < 10) {
            hor = '0' + hor;
            hor = hor.substr(0, 2);
        }

        if (min < 10) {
            min = '0' + min;
            min = min.substr(0, 2);
        }
        if (seg < 10) {
            seg = '0' + seg;
        }

        /* atividades */
        if(timer.hasClass("atividade_tempo")) {
            var porcentagem = 100 - (restante * 100) / timer.attr("data-duracao");

            if(timer.hasClass("volta"))
                porcentagem = 100 - porcentagem;

            if(porcentagem >= 0 && porcentagem < 100) {
                timer.siblings(".ponteiro").css({
                    left: porcentagem + "%",
                    display: ""
                });
            }
        }

        if(restante >= 0) {
            timer.html(hor + ':' + min + ':' + seg);
        } else {
            timer.html('Concluído.');

            setTimeout(function () {
                if(timer.hasClass("atividade_tempo")) {
                    timer.removeClass("atividade_tempo");

                    app.atividades();
                } else {
                    if (timer.closest('ul').length == 1) {
                        timer.closest('ul').prev("h3").remove();
                        timer.closest('ul').remove();
                    } else {
                        timer.parent().remove();
                    }
                }
            }, 2000);
        }
    });

    app.recursos('dinheiro', '#1FEB0D', '#005206');
    app.recursos('alimento', '#B40000', '#4D0000');
    app.recursos('metal', '#52B3AE', '#4D7E8D');
    app.recursos('petroleo', '#464646', '#141414');
    app.recursos('ouro', '#F5FF00', '#614A00');
    app.recursos('energia', '#FFA300', '#611300');
    app.recursos('populacao', '#00B8FF', '#003661');
}, 1000);

/*Expedição*/
$(document).on("change", "#expedicao [name='missao']", function () {
    var missaoTexto = $(this).find("option[value='" + $(this).val() + "']").text();

    $(".missao_info h2").html(missaoTexto);
});

/*Defesa*/
$(document).on("click", ".regressar_defesa", function () {
    var defesa = $(this).data('defesa'),
        $this = $(this);

    $.getJSON(urlBase + 'defesa/regressar/' + defesa, function (response) {
        if(response.error == 0) {
            $this.closest('tr').remove();
        }

        app.atividades();

        game.mensagem(response.mensagem, null, 3000);
    });
});

//inicia as notificações
app.notificacao.init();