<?php

/**
 * Key: arquivo em assets
 * Value: arquivo compilado | Array: index 0: arquivo, index 1: comentario, index 2: autor
 */

return [
    'css/padrao.css' => ['css/padrao.min.css', 'Todos os direitos reservados.', 'Jonas Tortato da Rosa'],
    'js/app.js' => 'js/app.min.js'
];