<?php

namespace app\middlewares;

use app\models\Funcoes;

class Login {

    public function run() {
        $usuario = Funcoes::getUsuario();

        if($usuario)
            return true;

        redirect('/');

        return false;
    }

}