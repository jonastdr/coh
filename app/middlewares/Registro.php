<?php

namespace app\middlewares;

class Registro extends \projectws\libs\Validator {

    /**
     * regras de validação
     * @return array
     */
    public function rules() {
        return [
            'email' => 'email|required|unique:usuario.email',
            'nome' => 'required|unique:usuario.nome',
            'senha' => 'required'
        ];
    }

    /**
     * mensagens de cada campo
     * @return array
     */
    public function messages() {
        return [
            'nome' => 'O campo nome é obrigatório'
        ];
    }

    /**
     * Executa se o usuario não for autorizado
     */
    public function unauthorized() {
        redirect()->route('registrar');
    }

}