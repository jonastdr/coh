<?php

namespace app\middlewares;

class Validacao extends \projectws\libs\Validator {

    /**
     * regras de validação
     * @return array
     */
    public function rules() {
        return [
            'nome' => 'required'
        ];
    }

    /**
     * mensagens de cada campo
     * @return array
     */
    public function messages() {
        return [
            'nome' => 'O campo nome é obrigatório'
        ];
    }

}