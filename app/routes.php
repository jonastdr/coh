<?php

/**
 * Definição de todas as rotas do sistema
 */
Router::get('teste2', ['action' => 'IndexController/teste']);
Router::get('map_gen', ['action' => 'MapaController/generate']);
//Tela Inicial
Router::get('/', ['alias' => 'index', 'action' => 'IndexController/index']);
Router::post('/', ['alias' => 'logar', 'action' => 'IndexController/index']);

//Registro de novo usuário
Router::get('registrar', ['action' => 'IndexController/index']);

Router::post('registrar', [
	'alias' => 'registrar_novo_user',
	'action' => 'RegistroController/registrar',
	//'middleware' => 'app\middlewares\Registro'
]);

//Rotas que podem ser acessadas somente se estiver logado
Router::group('/', ['middleware' => 'app\middlewares\Login'], function () {
	
	//Deslogar
	Router::get('logout', ['action' => 'IndexController/logout']);
	
	Router::get('mapa', ['action' => 'MapaController/index']);
	
	Router::post('mapa/push', ['action' => 'MapaController/push']);
	Router::get('mapa/push_initial', ['action' => 'MapaController/pushInitial']);
	
    //Game
    Router::group('game', function () {
        Router::get('/', ['action' => 'GameController/index']);
        Router::get('init', ['action' => 'GameController/init']);
	    
        Router::get('recursos', ['action' => 'GameController/recursos']);
        Router::get('recursos/data', ['action' => 'GameController/recursosData']);
	    
        Router::get('menu', ['action' => 'GameController/menu']);

        Router::get('notificacoes', ['action' => 'GameController/notificacoes']);
        Router::get('notificacoes/data', ['action' => 'GameController/notificacoesData']);
        Router::post('notificacoes/remover', ['action' => 'GameController/removerNotificacao']);

	    Router::get('cidades', ['action' => 'GameController/cidades']);
	    Router::get('cidades/data', ['action' => 'GameController/cidadesData']);
	    Router::post('alterar_nome_cidade', ['action' => 'GameController/alterarNomeCidade']);
	    
        Router::post('cidade/selecionar', ['action' => 'GameController/cidadeAlterar']);
    });

	//Sidebar
    Router::get('sidebar', ['action' => 'SidebarController/index']);
    Router::get('sidebar/data', ['action' => 'SidebarController/data']);

    //Laboratorio
    Router::group('laboratorio', function () {
	    Router::get('/', ['action' => 'LaboratorioController/index']);
	    Router::get('get', ['action' => 'LaboratorioController/getAll']);
	    Router::get('get/{id_pesquisa}', ['action' => 'LaboratorioController/get']);
	    Router::post('pesquisar', ['action' => 'LaboratorioController/pesquisar']);
    });

    //Barraca
	Router::group('barraca', function () {
		Router::get('/', ['action' => 'BarracaController/index']);
		Router::get('get/{id_tipo}', ['action' => 'BarracaController/getAll']);
		Router::get('get_unidade/{id_unidade}', ['action' => 'BarracaController/getUnidade']);
		Router::get('recrutamento', ['action' => 'BarracaController/recrutamento']);
		Router::post('recrutar', ['action' => 'BarracaController/recrutar']);
	});
	
	//Defesa
	Router::group('defesa', function () {
		Router::get('cidade', ['action' => 'DefesaController/cidade']);
		Router::get('expedicao', ['action' => 'DefesaController/expedicao']);
		
		Router::get('get', ['alias' => 'defesa_cidade', 'action' => 'DefesaController/getAll']);
		
		Router::get('cidade_data', ['alias' => 'defesa_cidade', 'action' => 'DefesaController/cidadeData']);
		Router::get('expedicao_data', ['alias' => 'defesa_expedicao', 'action' => 'DefesaController/expedicaoData']);
		Router::get('regressar/{id_defesa}', ['alias' => 'defesa_regressar', 'action' => 'DefesaController/regressar']);
	});
	
	//Mensagem
	Router::group('mensagem', function () {
		Router::get('/', ['action' => 'MensagensController/index']);
		
		Router::get('get', ['action' => 'MensagensController/getAll']);
		
		/**
		 * ID
		 * 1 = Mensagens entre usuários
		 * 2 = Mensagens de Relatórios
		 * 3 = Mensagens entre usuários da aliança
		 */
		Router::get('listar/{tipo}/{pag?}/{qt?}', ['action' => 'MensagensController/listar']);
		Router::any('visualizar/{id}', ['action' => 'MensagensController/visualizar']);
		
		Router::post('responder/{id}', ['action' => 'MensagensController/responder']);
		
		Router::post('nova', ['action' => 'MensagensController/nova']);
		
		Router::post('excluir', ['action' => 'MensagensController/excluir']);
	});
	
	//Relatorios
	Router::group('relatorio', function () {
		Router::get('/', ['action' => 'RelatorioController/index']);
		
		Router::get('get/{codigo}', ['action' => 'RelatorioController/getAll']);
	});
	
	//Aliança
	Router::group('alianca', function () {
		Router::get('/', ['action' => 'AliancaController/index']);
		Router::get('get', ['action' => 'AliancaController/getAll']);
		
		Router::get('search', ['action' => 'AliancaController/busca']);
		Router::post('search', ['action' => 'AliancaController/buscaAliancas']);
		
		Router::post('nova', ['action' => 'AliancaController/nova']);
		
		Router::post('propriedades', ['action' => 'AliancaController/propriedades']);
		
		Router::post('alistar', ['action' => 'AliancaController/alistar']);
		Router::get('recrutamento_aceitar/{id_recrutamento}', ['action' => 'AliancaController/aceitarRecrutamento']);
		Router::get('recrutamento_rejeitar/{id_recrutamento}', ['action' => 'AliancaController/rejeitarRecrutamento']);
        
        Router::get('nivel', ['action' => 'AliancaController/nivel']);
        Router::get('nivel/data', ['action' => 'AliancaController/nivelData']);
        Router::post('nivel/novo', ['action' => 'AliancaController/nivelNovo']);
        Router::post('nivel/editar/{id_nivel}', ['action' => 'AliancaController/nivelEditar']);
        Router::get('nivel/remover/{id_nivel}', ['action' => 'AliancaController/nivelRemover']);
        
        Router::post('membro_nivel/{id_membro}', ['action' => 'AliancaController/membroNivelAlterar']);
        
        Router::get('abandonar', ['action' => 'AliancaController/abandonar']);
        
        Router::get('excluir', ['action' => 'AliancaController/excluir']);
	});
	
	//Estatisticas
	Router::group('estatisticas', function () {
		Router::get('/', ['action' => 'EstatisticasController/index']);
		
		Router::get('get', ['action' => 'EstatisticasController/getAll']);
	});
	
	//Expedicao
	Router::group('expedicao', function () {
		Router::get('/', ['action' => 'ExpedicaoController/index']);
		
		Router::get('missao/{cod_ilha}/{cod_cidade}', ['action' => 'ExpedicaoController/missao']);
		
		Router::post('enviar', ['middleware' => 'app\middlewares\Missao', 'action' => 'ExpedicaoController/enviar']);
	});
    
    //Espionagem
    Router::group('espionagem', function () {
        Router::get('/', ['action' => 'EspionagemController/index']);
        Router::get('relatorio', ['action' => 'EspionagemController/relatorio']);
        Router::get('relatorio/{codigo}', ['action' => 'EspionagemController/getAll']);
        Router::get('cidade', ['action' => 'EspionagemController/cidade']);
        Router::get('armazem', ['action' => 'EspionagemController/armazem']);
        Router::post('armazenar', ['action' => 'EspionagemController/armazenar']);
        Router::post('enviar', ['action' => 'EspionagemController/enviar']);
        Router::get('missao/{cod_ilha}/{cod_cidade}', ['action' => 'EspionagemController/missao']);
    });
	
	Router::post('expedicao/atividade', ['alias' => 'ativadades', 'action' => 'ExpedicaoController/atividadeAction', 'ajax' => true]);
	
	//Overview
	Router::group('overview', function () {
		Router::get('/', ['action' => 'OverviewController/index']);
		Router::get('get', ['action' => 'OverviewController/getAll']);
		Router::get('insere/{id}/{slot}', ['action' => 'OverviewController/insere']);
	});
	
	//Construcao
	Router::group('construcao', function () {
		Router::get('/', ['action' => 'ConstrucaoController/index']);
		Router::get('get/{id}', ['action' => 'ConstrucaoController/getAll']);
		
		Router::post('melhorar', ['action' => 'ConstrucaoController/melhorar']);
		Router::post('cancelar', ['action' => 'ConstrucaoController/cancelar']);
		Router::post('destruir', ['action' => 'ConstrucaoController/destruir']);
	});
	
	//Academia
	Router::group('academia', function () {
		Router::get('/', ['action' => 'AcademiaController/index']);
		Router::get('get', ['action' => 'AcademiaController/getAll']);
		
		Router::get('get_heroi/{id_heroi}', ['action' => 'AcademiaController/getHeroi']);
		Router::get('get_funcao/{id_funcao}', ['action' => 'AcademiaController/getFuncao']);
		
		//Ativar heroi | funcao de um heroi
		Router::group('ativar', function () {
			Router::post('heroi', ['action' => 'AcademiaController/ativarHeroi']);
			Router::post('funcao', ['action' => 'AcademiaController/ativarFuncao']);
		});
	});
	
	Router::group('usuario', function () {
		Router::get('/', ['action' => 'UsuarioController/index']);
		
		Router::get('get/{id_usuario}', ['action' => 'UsuarioController/getAll']);
		
		Router::post('alterar', ['action' => 'UsuarioController/alterar']);
		Router::post('alterar_senha', ['action' => 'UsuarioController/alterarSenha']);
		
		Router::get('abandonar', ['action' => 'UsuarioController/abandonar']);
		Router::get('ferias', ['action' => 'UsuarioController/ferias']);
	});
	
	//Ajax Controller
	Router::get('buscar_usuarios/{nome}', ['action' => 'AjaxController/ListarUsuarios']);
	Router::get('buscar_aliancas/{nome}', ['action' => 'AjaxController/ListarAliancas']);
	
	

    //Configurações
    Router::get('configuracoes', ['action' => 'ConfiguracoesController/indexAction', 'ajax' => true]);

    //Administrar
    Router::get('administrador', ['action' => 'AdministradorController/indexAction', 'ajax' => true]);
    Router::get('administrador/chat', ['action' => 'AdministradorController/chatAction', 'ajax' => true]);
    Router::get('administrador/unidades', ['action' => 'AdministradorController/unidadesAction', 'ajax' => true]);
    Router::get('administrador/global', ['action' => 'AdministradorController/globalAction', 'ajax' => true]);

    //Ajax Controller
    Router::getJSON('ajax/listarusuarios/{nome}', ['action' => 'AjaxController/ListarUsuarios', 'ajax' => true]);
    Router::getJSON('ajax/listaraliancas/{nome}', ['action' => 'AjaxController/ListarAliancas', 'ajax' => true]);

    //Chat
    Router::post('chat/enviar', ['action' => 'ChatController/indexAction', 'ajax' => true]);
	
    //Radar
    Router::group('radar', ['ajax', true], function () {
        Router::get('/', ['alias' => 'defesa_index', 'action' => 'DefesaController/index']);
        Router::get('cidade', ['alias' => 'defesa_cidade', 'action' => 'DefesaController/cidade']);
        Router::get('expedicao', ['alias' => 'defesa_expedicao', 'action' => 'DefesaController/expedicao']);
        Router::getJSON('regressar/{id_defesa}', ['alias' => 'defesa_regressar', 'action' => 'DefesaController/regressar']);
    });
});

//Rotas de Teste
Router::group('grupo', ['middleware1' => 'app\middlewares\Validacao'], function () {
    Router::get('teste', ['alias' => 'teste/index', 'action' => 'TesteController/indexAction']);
});