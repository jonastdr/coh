<div class="lista">
    <ul class="pesquisas links">
        <li *ngFor="let p of pesquisas" (click)="openPesquisa(p)"
            [style.backgroundImage]="pesquisaImage(p)"
            [ngClass]="pesquisa.id == p.id ? 'selec' : ''">
            <h3>{{p.nome}}</h3>
            <i class="levelup fa fa-arrow-circle-o-up"></i>
            <span>{{p.level}}</span>
        </li>
    </ul>
</div>

<div class="requisitos">
    <h3>Requisitos:</h3>
    <ul class="pesquisas">
        <li *ngFor="let requisito of pesquisa.construcao_requisitos" [style.backgroundImage]="construcaoImage(requisito)">
            <h3>{{requisito.nome}}</h3>
            <span>{{requisito.level}}</span>
        </li>

        <li *ngFor="let requisito of pesquisa.pesquisa_requisitos" [style.backgroundImage]="pesquisaImage(requisito)">
            <h3>{{requisito.nome}}</h3>
            <span>{{requisito.level}}</span>
        </li>

        <li *ngIf="pesquisa.construcao_requisitos.length == 0 && pesquisa.pesquisa_requisitos.length == 0">
            <h3>Nenhum</h3>
            <span>0</span>
        </li>
    </ul>
</div>

<div class="sidebar">
    <div class="detalhes" myscroll>
        <img src="imagens/pesquisas/{{pesquisa.id}}.png" width="80" height="80">
        <h3>{{pesquisa.nome}} <i class="fa fa-arrow-circle-o-up"></i>{{pesquisa.level+1}}</h3>
        <p>{{pesquisa.descricao}}</p>
    </div>
    <div class="recursos">
        <h4>Recursos:</h4>
        <ul class="recursos">
            <li class="dinheiro" *ngIf="pesquisa.dinheiro">{{pesquisa.dinheiro}}</li>
            <li class="alimento" *ngIf="pesquisa.alimento">{{pesquisa.alimento}}</li>
            <li class="metal" *ngIf="pesquisa.metal">{{pesquisa.metal}}</li>
            <li class="petroleo" *ngIf="pesquisa.petroleo">{{pesquisa.petroleo}}</li>
            <li class="populacao" *ngIf="pesquisa.populacao">{{pesquisa.populacao}}</li>
            <li class="relogio" *ngIf="pesquisa.tempo">{{pesquisa.tempo | recursoTempo}}</li>
        </ul>
        <p class="pesquisar">
            <button class="btn" (click)="pesquisar(pesquisa)" *ngIf="tempo == 0">{{acao}}</button>
            <button class="btn" disabled *ngIf="tempo > 0" [coh-timer]="tempo" (callback)="getAll()"></button>
        </p>
    </div>
</div>