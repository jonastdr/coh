<div *ngIf="construcao">
    <h3>Construindo</h3>
    <ul class="construcoes">
        <li [style.backgroundImage]="construcaoImage(construcao)">
            <h3>{{construcao.nome}}</h3>
            <p class="tempo" [coh-timer]="construcao.restante" (callback)="termino()"></p>
            <span>{{construcao.level}}</span>
        </li>
    </ul>
</div>
<div *ngIf="pesquisa">
    <h3>Pesquisando</h3>
    <ul class="pesquisas">
        <li [style.backgroundImage]="pesquisaImage(pesquisa)">
            <h3>{{pesquisa.nome}}</h3>
            <p class="tempo" [coh-timer]="pesquisa.restante" (callback)="termino()"></p>
            <span>{{pesquisa.level}}</span>
        </li>
    </ul>
</div>

<div *ngIf="unidade">
    <h3>Recrutamento</h3>
    <ul class="unidades">
        <li [style.backgroundImage]="unidadeImage(unidade)">
            <h3>{{unidade.nome}}</h3>
            <p class="tempo" [coh-timer]="unidade.restante" (callback)="termino()"></p>
            <span>{{unidade.quantidade}}</span>
        </li>
    </ul>
</div>

<div *ngIf="!construcao && !pesquisa && !unidade">
    <h3>Nada a construir</h3>
</div>