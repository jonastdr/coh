<ul>
    <li *ngFor="let n of notificacoes" [coh-title]="n.titulo">
        <a (click)="abrir(n)">
            <i class="fa fa-comment" *ngIf="n.tipo == 1"></i>
            <i class="fa fa-flag" *ngIf="n.tipo == 2"></i>
            <i class="fa fa-warning" *ngIf="n.tipo == 3" style="color: #b80000;"></i>
            <i class="fa fa-warning" *ngIf="n.tipo == 4"></i>
            <i class="fa fa-warning" *ngIf="n.tipo == 5"></i>
        </a>
    </li>
</ul>