<div title="Dinheiro" class="recursos_painel" [coh-recurso]="{valor: incremento.dinheiro, max: armazenamento.dinheiro}">
	<div class="dinheiro_img"></div>
	<canvas id="dinheiro" class="recursos_valor" width="42" height="12"></canvas>
</div>
<div title="Alimento" class="recursos_painel" [coh-recurso]="{valor: incremento.alimento, max: armazenamento.alimento}">
	<div class="alimento_img"></div>
	<canvas id="alimento" class="recursos_valor" width="42" height="12"></canvas>
</div>
<div title="Metal" class="recursos_painel" [coh-recurso]="{valor: incremento.metal, max: armazenamento.metal}">
	<div class="metal_img"></div>
	<canvas id="metal" class="recursos_valor" width="42" height="12"></canvas>
</div>
<div title="Petróleo" class="recursos_painel" [coh-recurso]="{valor: incremento.petroleo, max: armazenamento.petroleo}">
	<div class="petroleo_img"></div>
	<canvas id="petroleo" class="recursos_valor" width="42" height="12"></canvas>
</div>
<div title="Ouro" class="recursos_painel" [coh-recurso]>
	<div class="ouro_img"></div>
	<canvas id="ouro" class="recursos_valor" width="42" height="12"></canvas>
</div>
<div title="Energia" class="recursos_painel" [coh-recurso]>
	<div class="energia_img"></div>
	<canvas id="energia" class="recursos_valor" width="42" height="12"></canvas>
</div>
<div title="População" class="recursos_painel" [coh-recurso]>
	<div class="populacao_img"></div>
	<canvas id="populacao" class="recursos_valor" width="42" height="12"></canvas>
</div>

<div title="Honra" class="recursos_painel" [coh-recurso]="{valor: incremento.honra, max: armazenamento.honra}">
	<canvas id="honra" class="recursos_valor recursos_honra" width="48" height="48"></canvas>
</div>