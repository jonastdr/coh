<div id="cidade-preview">
    <div class="icon"></div>
    <div id="cidade-atual">
        <span (click)="mostrarCidades($event)" *ngIf="!cidadeEdit">{{usuario.cidade_nome}}</span>

        <span *ngIf="cidadeEdit">
            <input type="text" [(ngModel)]="cidadeNome" (keydown)="cidadeEditPress($event)">
        </span>

        <button class="btn btn-rename" (click)="renomearCidade()" title="Renomear">
            <i class="fa fa-pencil"></i>
        </button>
    </div>
    <div id="cidade-atividade" [class]="atividades.length ? 'atividade' : ''" (click)="mostrarAtividades($event)">
        <i class="fa fa-warning"></i>
    </div>
</div>
<div id="menu_cidades" (coh-clickout)="hideMenuCidades()">
    <ul>
        <li *ngFor="let cidade of cidades" (click)="selecionarCidade(cidade)">
            <div class="icon"></div>
            <p>{{cidade.nome}}</p>
        </li>
    </ul>
</div>
<div id="menu_atividade" (coh-clickout)="hideAtividades()">
    <ul>
        <li *ngFor="let atividade of atividades">
            <div class="ponto_a"><i class="fa fa-truck"></i></div>
            <div class="trajeto">
                <i class="fa fa-arrow-{{atividade.volta ? 'left' : 'right'}} ponteiro {{atividade.missao}}" [coh-exp-pos]="atividade"></i>
                <div class="atividade_tempo alerta-anim1x tempo {{atividade.volta}}" [coh-timer]="atividade.tempo" (callback)="terminou()"></div>
            </div>
            <div class="ponto_b"><i class="fa fa-truck"></i></div>
        </li>
    </ul>
</div>