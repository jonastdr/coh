<script src="<?= url() ?>public/js/md5.js"></script>
<script>
var timeChat,
    lastDate = null;

var Global = function (callback) {
    var now = new Date();
    var nomearquivo = 'global-' + now.getDate() + '-' + (now.getMonth()+1) + '-' + now.getFullYear();

    var arquivo = '<?= url() ?>public/chat/'+ nomearquivo +'.txt';

    var geraMensagem = [];

    $.get(arquivo, {"_": $.now()}, function(data) {
        var linha = data.split('^%/%'),
            last = linha.length-2;

        $.each(linha, function(key, value) {
            var msg = value.split('^%|%');

            if (msg[0] != '' && msg[3]>=<?php echo $horaatualizacao; ?> && (lastDate == null || lastDate < msg[3])) {
                geraMensagem.push({
                    datahora: msg[0],
                    nome: msg[1],
                    mensagem: msg[2]
                });
            }

            if(key == last)
                lastDate = msg[3];
        });

        if(geraMensagem.length > 0) {
            var msgslist = $('#chat_msgs .content');

            $.each(geraMensagem.reverse(), function (key, value) {
                var html = $('<div class="chatlist">' +
                '   <span class="text_azul">[' + value.datahora + ']</span>' +
                '   <span class="text_verde">' + value.nome + ':</span>' +
                value.mensagem +
                '</div>');

                msgslist.append(html);
            });

            var position = scrollChat.getPosition();

            if (typeof callback == "function")
                callback();
            else
                if(position == 100)
                    scrollChat.setPosition(100);
                else
                    scrollChat.refresh();
        }

        timeChat = setTimeout('Global();', 1000);
    });
};

/*evento ao enviar a mensagem*/
$('#chat').submit(function (e) {
    e.preventDefault();

    var self = this,
        texto = self.chat_input.value;

    if(texto == '\\opencity') {
        var link = '/frame/overview';

        $( '#mapa_construtor' ).load(link, function(){
            $('.mapa_construtor').parent().toggle();

            $('.minimap').toggle();
        });

        $(self.chat_input).val('');

        return false;
    }
    if(texto == '\\cls') {
        $("#chat_msgs .content").empty();

        $(self.chat_input).val('');

        return false;
    }

    $.post("<?= url() ?>chat/enviar", {chat_input: texto}, function (data) {
        var input = $(self.chat_input);

        clearTimeout(timeChat);

        Global(function () {
            scrollChat.setPosition(100);
        });

        input.val('').focus();
    });
});

var minimize = false;

var scrollChat = $("#chat_msgs").myscroll(100);

Global(function () {
    scrollChat.setPosition(100);
});

$('.minimizar').click(function() {
    if (!minimize) {
        $("#chat_botao").text('+');

        $("#chat_msgs, .chat_entrada").hide();

        $(".chat").height(16);

        minimize = true;
    } else {
        $('#chat_botao').text('-');

        $("#chat_msgs, .chat_entrada").css("display", "");

        $(".chat").height(200);

        minimize = false;
    }
});

$(".chat").draggable({
containment: "window",
    handle: "#chat_header",
    cursor: "move"
});
</script>
