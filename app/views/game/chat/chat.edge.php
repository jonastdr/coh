<div class="chat">
    <div id="chat_header">
        <div id="title_chat">Chat - Geral</div>
        <button id="chat_botao" class="minimizar">-</button>
    </div>
	<div id="chat_msgs"></div>

    <form id="chat" action="chat/enviar" method="post">
        <div class="chat_entrada">
            <input class="chat_texto" type="text" name="chat_input" /><input type="submit" class="chat_botao" name="enviar" value="Enviar">
        </div>
    </form>
    <div id="chat_update"></div>
</div>