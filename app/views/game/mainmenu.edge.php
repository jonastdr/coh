<div id="menu_acao" (click)="toggleMenu()"></div>

<div title="Cidade" [hidden]="view == 'overview'" [coh-title]>
    <a (click)="openOverview()">
        <div class="menu_icon city_icon"></div>
    </a>
</div>

<div title="Mapa" [hidden]="view == 'mapa'" [coh-title]>
    <a (click)="openMapa()">
        <div class="menu_icon map_icon"></div>
    </a>
</div>

<div title="Mensagens" [coh-title]>
    <a (click)="openModal('mensagem')">
        <div class="menu_icon mensagens_icon"></div>
    </a>
</div>

<!--<div title="Negociações">
    <div class="menu_icon negoc_icon"></div>
</div>-->

<div title="Aliança" [coh-title]>
    <a (click)="openModal('alianca')">
       <div class="menu_icon alianca_icon"></div>
    </a>
</div>

<div title="Estatísticas" [coh-title]>
    <a (click)="openModal('estatisticas')">
        <div class="menu_icon estatistica_icon"></div>
    </a>
</div>

<div title="Configurações" [coh-title]>
    <a (click)="openConfig({% $usuario->id %})">
        <div class="menu_icon config_icon"></div>
    </a>
</div>

@if($usuario->nivel == 3 && 1==2)
<div title="Administrar">
    <a class="abrir_dialog" dialog-title="Administrador" href="<?= url() ?>administrador">
        <div class="menu_icon adm_icon"></div>
    </a>
</div>
@endif

<div title="Sair" [coh-title]>
    <a (click)="logout()">
        <div class="menu_icon exit_icon"></div>
    </a>
</div>