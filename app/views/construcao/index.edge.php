<ul class="dialog-tabs">
	<li><a (click)="alternarAba('construcao')" [class]="abaAtiva('construcao')">Construção</a></li>
	<li *ngFor="let component of construcao.components">
		<a (click)="alternarAba(component.component, component.params)" [class]="abaAtiva(component.component)">
			{{component.nome}}
		</a>
	</li>
</ul>

<div class="construcoes_info" *ngIf="aba == 'construcao'">
	<div>
		<div class="construcao_img" [style.backgroundImage]="construcaoImage(construcao)"></div>

		<div class="construcao_opcoes">
			<div class="construcao_titulo">Recursos</div>
			<div *ngIf="construcao.level >= construcao.max">Nível Máximo Atingido!</div>
			<ul class="recursos">
				<li class="dinheiro" *ngIf="construcao.dinheiro">{{construcao.dinheiro}}</li>
				<li class="alimento" *ngIf="construcao.alimento">{{construcao.alimento}}</li>
				<li class="metal" *ngIf="construcao.metal">{{construcao.metal}}</li>
				<li class="petroleo" *ngIf="construcao.petroleo">{{construcao.petroleo}}</li>
				<li class="energia" *ngIf="construcao.energia">{{construcao.energia}}</li>
				<li class="populacao" *ngIf="construcao.populacao">{{construcao.populacao}}</li>
				<li class="relogio" *ngIf="construcao.tempo">{{construcao.tempo}}</li>
            </ul>
			<div class="construcao_titulo">Propriedades</div>
			<div class="construcao_prp_tudo">
				<div class="construcao_propriedades construcao_prp_lvl">
					<div class="construcao_prop_level">Construção Atual</div>

					<div *ngIf="construcao.level > 0">Level: {{construcao.level}}</div>

					<div id="tempo_construcao"
						 *ngIf="tempoGerado > 0 && (construindo && construindo[2] == 0)"
						 style="color: green; font-size: 16px; font-weight: 900;"
						 [coh-timer]="tempoGerado" (callback)="get()">
					</div>

					<button class="btn destruir" *ngIf="construindo && construindo[1] == construcao.id"
							(click)="cancelar(construcao)">
						<i></i>
					</button>

					<button *ngIf="construcao.level < construcao.max && construindo == 0 && construcao.level > 0" class="btn destruir"
							(click)="destruir(construcao)">
						<i></i>
					</button>

					<div *ngIf="construcao.level == 0 && tempoGerado == 0">Para construir deve arrastar a construção na lateral para o campo.</div>
				</div>

				<div class="construcao_propriedades construcao_prp_lvl">
					<div *ngIf="construcao.level > 0" class="construcao_prop_level" style="color: #0039B3;">Melhoria</div>
					<div *ngIf="construcao.level == 0" class="construcao_prop_level" style="color: #0039B3;">Construir</div>
					<div *ngIf="construcao.level >= construcao.max">Nível Máximo Atingido!</div>

					<div *ngIf="construcao.level < construcao.max">Level: {{construcao.level+1}}</div>

					<div id="tempo_construcao"
						 *ngIf="tempoGerado > 0 && (construindo && construindo[2] == 1)"
						 style="color: green; font-size: 16px; font-weight: 900;"
						 [coh-timer]="tempoGerado" (callback)="get()">
					</div>

					<button *ngIf="construcao.level < construcao.max && construindo == 0 && construcao.level > 0" class="btn melhoria"
							(click)="melhorar(construcao)">
						<i></i>
					</button>
				</div>
			</div>
		</div>
	</div>

	<div class="construcao_descricao container">
		{{construcao.descricao}}
	</div>

	<div class="container">
		<h3>Requisitos:</h3>
		<ul class="pesquisas">
			<li *ngFor="let requisito of construcao.construcao_requisitos" [style.backgroundImage]="construcaoImage(requisito)">
				<h3>{{requisito.nome}}</h3>
				<span>{{requisito.level}}</span>
			</li>

			<li *ngFor="let requisito of construcao.pesquisa_requisitos" [style.backgroundImage]="pesquisaImage(requisito)">
				<h3>{{requisito.nome}}</h3>
				<span>{{requisito.level}}</span>
			</li>

			<li *ngIf="construcao.construcao_requisitos.length == 0 && construcao.pesquisa_requisitos.length == 0">
				<h3>Nenhum</h3>
				<span>0</span>
			</li>
		</ul>
	</div>
</div>

<div #components></div>