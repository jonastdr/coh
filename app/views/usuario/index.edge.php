<ul class="dialog-tabs">
    <li>
        <a (click)="alternarAba('geral')" [class]="aba == 'geral' ? 'ativo' : ''">Geral</a>
    </li>
    <li>
        <a (click)="alternarAba('cidades')" [class]="aba == 'cidades' ? 'ativo' : ''">Cidades</a>
    </li>
    <li *ngIf="usuario.id == usuarioLogado.id">
        <a (click)="alternarAba('config')" [class]="aba == 'config' ? 'ativo' : ''">Configurações</a>
    </li>
</ul>

<div *ngIf="aba == 'geral'">
    <div class="avatar">
        <img src="imagens/unidades/1.png" width="100" height="100" />
    </div>
    <div class="detalhes">
        <h3>Detalhes</h3>
        <p>
            <span>Nome:</span>
            <span class="usuario"><i class="fa fa-user"></i> {{usuario.nome}}</span>
        </p>
        <p>Aliança: <span class="alianca"><i class="fa fa-users"></i> {{usuario.alianca}}</span></p>
        <p>Cidades: <span class="cidades"><i class="fa fa-flag"></i> {{cidades.length}}</span></p>
        <p>Pontos: <span><i class="fa fa-star-o"></i> {{usuario.pontos}}</span></p>
        <p>Rank: <span><i class="fa fa-trophy"></i> {{usuario.rank}}</span></p>
    </div>

    <div class="descricao">
        <h3>Descrição</h3>
        Estou fazendo um teste da descrição
    </div>
</div>

<div *ngIf="aba == 'cidades'">
    <h3>Cidades de {{usuario.nome}}</h3>
    <div class="cidades-todas">
        <div class="cidades">
            <p *ngFor="let cidade of cidades">
                <a>
                    <i class="fa fa-flag"></i>
                    {{cidade.nome}}
                    <br />
                    <span><i class="fa fa-star-o"></i> {{cidade.pontos}}</span>
                </a>
            </p>
        </div>
    </div>
</div>

<div *ngIf="aba == 'config'">
    <div class="avatar">
        <img src="imagens/unidades/1.png" width="100" height="100" />
        <input name="avatar" type="file" />
    </div>
    <div class="detalhes">
        <h3>Configurações</h3>
        <form (submit)="alterarConfig(form, fConfig.valid)" autocomplete="off" #fConfig="ngForm">
            <p layout-flex="row" class="input-container">
                <i class="fa fa-user"></i>
                <label>Nome:</label>
                <input type="text" name="nome" [(ngModel)]="form.usuario" placeholder="{{usuario.nome}}" flex required />
            </p>
            <p layout-flex="row" class="input-container">
                <i class="fa fa-inbox"></i>
                <label>Email:</label>
                <input type="text" name="email" [(ngModel)]="form.email" placeholder="{{usuario.email}}" flex required />
            </p>

            <p layout-flex="row" class="input-container">
                <i class="fa fa-lock"></i>
                <label>Senha:</label>
                <input type="password" name="senha" [(ngModel)]="form.senha" placeholder="******" flex required />
            </p>

            <p class="text-center">
                <button class="btn" type="submit">Salvar</button>
            </p>
        </form>
        <form (submit)="alterarSenha(formSenha, fSenha.valid)" autocomplete="off" #fSenha="ngForm">
            <p layout-flex="row" class="input-container">
                <i class="fa fa-lock"></i>
                <label>Senha Anterior:</label>
                <input type="password" name="senha_anterior" [(ngModel)]="formSenha.senha_anterior"
                       placeholder="******" flex required #senha_anterior="ngModel" />
                <small [hidden]="senha_anterior.valid || (senha_anterior.pristine && !fSenha.submitted)">
                    Este campo é obrigatório
                </small>
            </p>

            <p layout-flex="row" class="input-container">
                <i class="fa fa-lock"></i>
                <label>Senha:</label>
                <input type="password" name="senha" validateEqual="senha_confirma" reverse="true" [(ngModel)]="formSenha.senha"
                       placeholder="******" flex required #senha="ngModel" />
                <small [hidden]="senha.valid || (senha.pristine && !fSenha.submitted)">
                    A senha não confere
                </small>
            </p>

            <p layout-flex="row" class="input-container">
                <i class="fa fa-lock"></i>
                <label>Repetir:</label>
                <input type="password" name="senha_confirma" validateEqual="senha" [(ngModel)]="formSenha.senha_confirma"
                       placeholder="******" flex required #senha_confirma="ngModel" />
                <small [hidden]="senha_confirma.valid || (senha_confirma.pristine && !fSenha.submitted)">
                    A senha não confere
                </small>
            </p>

            <p class="text-center">
                <button class="btn" type="submit">Salvar</button>
            </p>
        </form>
        <h3>Opções:</h3>
        <ul class="opcoes">
            <li>
                <a class="deletar" (click)="abandonar()">
                    <i class="fa fa-times-circle"></i>
                    Abandonar
                </a>
            </li>
            <li>
                <a class="ferias" (click)="ferias()">
                    <i class="fa fa-ship"></i>
                    Férias
                </a>
            </li>
        </ul>
    </div>
</div>