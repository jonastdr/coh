<div class="lista">
    <p>Heroi na cidade: {{heroi_cidade_desc}}</p>

    <ul class="herois links">
        <li *ngFor="let h of herois" [style.backgroundImage]="heroiImage(h)"
            (click)="openHeroi(h)"
            [ngClass]="heroi && heroi.id == h.id ? 'selec' : ''">
            <h3>{{h.nome}}</h3>
        </li>
    </ul>
</div>

<div class="funcoes">
    <h3>Funções:</h3>
    <ul class="herois links">
        <li *ngFor="let f of funcoes" [style.backgroundImage]="funcaoImage(f)"
            (click)="openFuncao(f)"
            [ngClass]="funcao && funcao.id == f.id ? 'selec' : ''">
            <h3>{{f.nome}}</h3>
            <span>{{f.honra}}</span>
        </li>
    </ul>
</div>

<div layout-flex="column" class="sidebar" *ngIf="heroi">
    <h3>{{heroi.nome}}</h3>
    <div flex class="detalhes">
        <img [src]="heroiImageSrc(heroi)" width="60" height="60">
        <p>{{heroi.descricao}}</p>
    </div>
    <div flex="10" class="recursos">
        <h4>Recursos</h4>

        <p>Ao alterar o herói de uma cidade a honra do herói é zerado.</p>

        <p class="pesquisar">
                <button *ngIf="heroi_cidade && heroi.id == heroi_cidade.id"
                        class="btn" disabled>Ativado</button>

                <button *ngIf="heroi_cidade == null || heroi.id != heroi_cidade.id" (click)="ativarHeroi(heroi)"
                        class="btn">Ativar</button>
        </p>
    </div>
</div>

<div layout-flex="column" class="sidebar" *ngIf="funcao">
    <h3>{{funcao.nome}}</h3>
    <div flex class="detalhes">
        <img [src]="funcaoImageSrc(funcao)" width="60" height="60">
        <p>{{funcao.descricao}}</p>
    </div>
    <div flex="10" class="recursos">
        <h4>Recursos:</h4>
        <ul class="recursos">
            <li class="honra">{{funcao.honra}}</li>
            <li class="relogio">{{funcao.duracao | recursoTempo}}</li>
        </ul>
        <p class="pesquisar">
            <button class="btn" (click)="ativarFuncao(funcao)" *ngIf="!funcao.expiracao">Acionar</button>

            <button class="btn" *ngIf="funcao.expiracao" [coh-timer]="funcao.expiracao"></button>
        </p>
    </div>
</div>