<div id="overview_mapa">
    <div id="overview_content" style="left:-600px; position: relative;">
        <canvas id="overview_canvas" width="2560" height="1280" style=""></canvas>
        <div style="left: 185px; top: -135px; position: absolute;">

            <div id="blocos">
                <div class="blocos_ov">
                </div>
            </div>
        </div>
    </div>
    <div id="overview_menu">
        <div class="opcoes">
            <button class="update-map">Atualizar</button>
        </div>
    </div>
    <div id="overview_update">
    </div>
</div>
<script>
    var canvas = document.getElementById('overview_canvas');
    var c = canvas.getContext('2d');

    $('.minimap')
    
    $.getJSON("<?= url() ?>public/vista_geral/1.map", function(mapa){

        var grid = {
            width: mapa[0].length,
            height: mapa.length
        }

        // Funcao do mouse que substitui um tile por outro ao clicar

        canvas.addEventListener('mouseup', MouseEvento);

        draw();

        $('.update-map').click(function(){
            $.post('<?= url() ?>overvieweditor/update/', { mapa: mapa }, function(data){
                if(data == 1)
                    game.mensagem('Mapa Salvo!', false, 2000);
            });
        });


        function MouseEvento(e){

            var gridOffsetY = 0;
            var gridOffsetX = 0;

            var thisOffset = $(this).offset();
            var relX = e.pageX - thisOffset.left;
            var relY = e.pageY - thisOffset.top;

            if(!$('#overview_content').is('.ui-draggable-dragging')){
                gridOffsetX += (canvas.width / 2) - (telhas[0].width / 2);

                var x = (relY - gridOffsetY) * 2;
                x = ((gridOffsetX + x) - relX) / 2;

                var y = ((relX + x) - telhas[0].height) - gridOffsetX;

                x = Math.round(x / telhas[0].height);
                y = Math.round(y / telhas[0].height);

                // Verificando todos os limites do campo

                if (y >= 0 && x >= 0 && y <= grid.width && x <= grid.height) {

                    mapa[y] = (mapa[y] === undefined) ? [] : mapa[y];

                    if(mapa[x][y] != undefined && mapa[x][y]<6 && mapa[x][y]!=0) {
                        mapa[x][y]++;
                    } else {
                        if(mapa[x][y] === 6) {
                            mapa[x][y] = 0;
                        } else {
                            mapa[x][y] = 1;
                        }
                    }

                    draw();
                }
            }
        }

        function draw(){
            // Desenhando o mapa
            c.clearRect (0, 0, canvas.width, canvas.height);
            // Array responsavel pela construcao do mapa
            for (var x = 0; x < grid.height; x++) {
                for (var y = 0; y < grid.width; y++) {

                    var tilePositionX = (y - x) * telhas[0].height;

                    tilePositionX += (canvas.width / 2) - (telhas[0].width / 2);

                    var tilePositionY = (y + x) * (telhas[0].height / 2);

                    $.each(telhas, function(id, val) {
                        if(mapa[x] != null && id === mapa[x][y]) {
                            c.drawImage(telhas[id], Math.round(tilePositionX), Math.round(tilePositionY), telhas[id].width, telhas[id].height);
                        }
                    });

                }
            }
        }
    });

        $( '#overview_content' ).draggable(/*{ containment: [-200, -200, 0, 0] }*/);

        $( 'a.abrir_geral' ).click(function() {
            $( '#mapa_construtor' ).load( $( this ).attr('href') );

            return false;
        });
</script>
