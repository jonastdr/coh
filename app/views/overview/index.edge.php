<div id="overview_content" style="left: -600px; position: relative;">
    <canvas id="overview_canvas" width="2560" height="1280"></canvas>
    <div class="overview_mask">
        <div class="blocos_mask">
            <div *ngFor="let mask of masks"
                 [id]="'slot_' + mask.slot"
                 [style.top]="mask.top"
                 [style.left]="mask.left"
                 [class]="mask.classe ? mask.classe : 'ov_bloco_bloq'">
                <a class="ov_item" *ngIf="mask.construcao"
                    [title]="mask.construcao.nome" [coh-title]
                    [style.backgroundImage]="construcaoImage(mask.construcao)"
                    (click)="openConstrucao(mask.construcao)">
                    <div *ngIf="mask.construcao.tempo > 0">
                        <div class="timer" [coh-timer]="mask.construcao.tempo"
                             (callback)="terminaConstrucao()"></div>
                    </div>

                    <div class="title">{{mask.construcao.nome}} {{mask.construcao.level > 0 ? mask.construcao.level : ''}}</div>
                </a>
            </div>
        </div>
    </div>
</div>

<div id="overview_menu">
    <h3>Construções</h3>
    <div class="construcoes" [coh-scroll]>
        <a *ngFor="let construcao of construcoesPendentes" [title]="construcao.nome" [coh-title] class="ov_side_item"
             [style.backgroundImage]="construcaoImage(construcao)"
             (click)="openConstrucao(construcao)"
             [coh-drag-construcao]="construcao" (drag-stop)="insereConstrucao(construcao)"></a>
    </div>
</div>