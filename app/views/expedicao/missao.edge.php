<form (submit)="enviar(form)" #formMissao autocomplete="off">
	<div class="col-1">
		<div class="unidade" *ngFor="let unidade of unidades"
			 [style.backgroundImage]="unidadeImage(unidade)">
			<h3>{{unidade.nome}}</h3>
			<input type="text" name="unidade{{unidade.id}}" [(ngModel)]="form.unidades[unidade.id]" class="cor1"
				   [placeholder]="unidade.quantidade"
				   (keyup)="calculaUnidade(unidade.id)">
		</div>

		<div class="unidade" *ngIf="unidades.length == 0">
			<h1>Nenhum</h1>
			<img src="imagens/unidades/1.png" alt="imagem">
		</div>
	</div>

	<div class="missao_info">
		<h2>{{info.missao}}</h2>
		<h1>{{cidade.nome}}</h1>
		<div class="tempo_restante">{{info.duracao}}</div>
		<div class="tempo_chegada">{{info.chegada}}</div>
		<div class="flex center">
			<button type="submit" class="btn">Enviar</button>
		</div>
	</div>

	<div class="col-2">
		<div class="recursos">
			<h4>Missão:</h4>
			<select class="select" name="missao" [(ngModel)]="form.missao" (ngModelChange)="changeMissao(form.missao)">
				<option [value]="missao.id" *ngFor="let missao of missoes">{{missao.nome}}</option>
			</select>

			<h4>Recursos:</h4>
			<ul class="recursos">
				<li class="dinheiro">
					<input type="text" name="dinheiro" [(ngModel)]="form.dinheiro" (keyup)="calculaRecurso('dinheiro')" placeholder="0">
				</li>
				<li class="alimento">
					<input type="text" name="alimento" [(ngModel)]="form.alimento" (keyup)="calculaRecurso('alimento')" placeholder="0">
				</li>
				<li class="metal">
					<input type="text" name="metal" [(ngModel)]="form.metal" (keyup)="calculaRecurso('metal')" placeholder="0">
				</li>
				<li class="petroleo">
					<input type="text" name="petroleo" [(ngModel)]="form.petroleo" (keyup)="calculaRecurso('petroleo')" placeholder="0">
				</li>
			</ul>

			<h4>Informações</h4>
			<ul class="info">
				<li>
					Moral: <span class="moral">100%</span>
				</li>
				<li>
					Capacidade Total: <span class="capacidade">{{info.capacidade}}</span>
				</li>
				<li>
					Capacidade restante:
					<span [class]="'capacidade_restante' + (this.info.capacidade_restante < 0 ? 'red' : '')">
						{{this.info.capacidade_restante}}
					</span>
				</li>
				<li>
					Unidades: <span class="unidades_quantidade">{{info.quantidade}}</span>
				</li>
                <li>
                    Velocidade Máxima: <span class="velocidade_maxima">{{info.velocidade}}</span>
                </li>
			</ul>
		</div>
	</div>
</form>