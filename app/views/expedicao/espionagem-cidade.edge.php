<div class="construcoes_info" *ngIf="quantidade_maxima != null">
    <div>
        <div class="construcao_img" [style.backgroundImage]="imagem"></div>

        <div class="construcao_opcoes">
            <p>Nível da pesquisa: {{pesquisa_nivel}}</p>
            <p>Armazenado: {{armazenado}}</p>
            <p>Limite: {{armazenado + quantidade_maxima}}</p>

            <br />

            <form (submit)="armazenar(form)" #formEspionagem autocomplete="off">
                Qual a quantia deseja armazenar?

                <div class="input-container">
                    <coh-slider name="quantidade" (change)="quantidade($event)" [max]="quantidade_maxima"></coh-slider>
                </div>
                <div class="flex center">
                    <button class="btn" type="submit">Armazenar</button>
                </div>
            </form>
        </div>
    </div>

    <div class="construcao_descricao container">
        Quanto mais recurso enviar para espionagem mais chance têm de ter sucesso nas missões de espionagem.
        <br />
        A cada nível que avança na pesquisa acrescenta 10000 no limite de armazenamento.
    </div>
</div>