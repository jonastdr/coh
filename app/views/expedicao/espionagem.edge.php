<form (submit)="enviar(form)" #formMissao autocomplete="off">
    <div class="missao_info">
        <h2>Nível da Tecnologia:</h2>
        <h1 class="text-right">{{pesquisa_nivel}}</h1>

        <div class="input-container input-investimento" *ngIf="form.cod_ilha">
            <label>Investimento</label>
            <coh-slider (change)="quantidade($event)" [max]="quantidade_maxima"></coh-slider>
        </div>

        <div class="tempo_restante">{{info.duracao}}</div>
        <div class="tempo_chegada">{{info.chegada}}</div>
        <div class="flex center">
            <button class="btn" type="submit">Espionar</button>
        </div>
    </div>
</form>