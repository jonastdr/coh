<div class="relatorio">
    <div class="titulo">
        <h3>
            Relatório de espionagem da cidade
            <a (click)="openCidade(cidade.id_usuario)" class="cidade-atacante">
                <i class="fa fa-flag"></i>
                {{cidade.nome}}
            </a>
        </h3>
        <span class="horario" *ngIf="data">{{data | date:'dd/MM/yyyy HH:mm'}}</span>
    </div>
    <div class="visualizacao">
        <div class="atacante">
            <div style="clear: both;" *ngIf="construcoes.length">
                <h4>Construções</h4>
                <ul class="unidades">
                    <li *ngFor="let c of construcoes" [style.backgroundImage]="construcaoImage(c)">
                        <h3>{{c.nome}}</h3>
                        <span>{{c.level}}</span>
                    </li>
                </ul>
            </div>

            <div style="clear: both;" *ngIf="pesquisas.length">
                <h4>Pesquisas</h4>
                <ul class="unidades">
                    <li *ngFor="let p of pesquisas" [style.backgroundImage]="pesquisaImage(p)">
                        <h3>{{p.nome}}</h3>
                        <span>{{p.level}}</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="propriedades">
            <div class="recursos">
                <h3>Recursos:</h3>
                <p *ngIf="cidade.dinheiro > 0">Dinheiro: <span>{{cidade.dinheiro | int}}</span></p>
                <p *ngIf="cidade.alimento > 0">Alimento: <span>{{cidade.alimento | int}}</span></p>
                <p *ngIf="cidade.metal > 0">Metal: <span>{{cidade.metal | int}}</span></p>
                <p *ngIf="cidade.petroleo > 0">Petróleo: <span>{{cidade.petroleo | int}}</span></p>

                <br/>

                <p>Investimento: <span>{{investimento}}</span></p>
            </div>

            <div>
                <p *ngIf="!exito" class="text-center red">A missão falhou</p>
            </div>
        </div>
        <div class="defensor">
            <div style="clear: both;" *ngIf="unidades.length">
                <h4>Unidades</h4>
                <ul class="unidades">
                    <li *ngFor="let u of unidades" [style.backgroundImage]="unidadeImage(u)">
                        <h3>{{u.nome}}</h3>
                        <span>{{u.quantidade}}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
