<div class="relatorio">
    <div class="titulo">
        <h3>
            <a (click)="openUsuario(detalhes.id_atacante)" class="atacante">
                <i class="fa fa-user"></i>
                {{detalhes.atacante}}
            </a>
            da cidade
            <a (click)="openCidade(detalhes.id_atacante)" class="cidade-atacante">
                <i class="fa fa-flag"></i>
                {{detalhes.atacante_cidade}}
            </a>
            está atacando a cidade
            <a (click)="openCidade(detalhes.id_defensor)" class="cidade-defensor">
                <i class="fa fa-flag"></i>
                {{detalhes.defensor_cidade}}
            </a>
            de
            <a (click)="openUsuario(detalhes.id_defensor)" class="defensor">
                <i class="fa fa-user"></i>
                {{detalhes.defensor}}
            </a>
        </h3>
        <span class="horario" *ngIf="data">{{data | date:'dd/MM/yyyy HH:mm'}}</span>
    </div>
    <div class="visualizacao">
        <div class="atacante">
            <div class="unidade"
                 [style.backgroundImage]="unidadeImage(unidade)"
                 *ngFor="let unidade of unidades.ataque.primeiro; let i = index">
                <h3>{{unidade.nome}}</h3>
                <span class="quantidade">{{unidade.quantidade}}</span>
                <span class="perdas" [innerHTML]="calcPerdas(unidades.ataque.ultimo[i], unidade)"></span>
            </div>
        </div>
        <div class="propriedades">
            <p class="moral">
                <i class="fa fa-smile-o"></i>
                Moral: <span [class]="detalhes.moral ? 'positivo' : 'negativo'">{{detalhes.moral}}%</span><br />
            </p>
            <p class="sorte">
                <i class="fa fa-pagelines"></i>
                Sorte: <span [class]="detalhes.moral ? 'positivo' : 'negativo'">{{detalhes.sorte}}%</span><br />
            </p>

            <p class="pontos" *ngIf="pontos">
                <i class="fa fa-star-o"></i>
                Você recebeu um total de <span>{{pontos}}</span> pontos.
            </p>

            <p class="status">
                {{status}}
            </p>

            <div class="recursos" *ngIf="recursos.dinheiro > 0 || recursos.alimento > 0 || recursos.metal > 0 || recursos.petroleo > 0">
                <h3>Recursos roubados:</h3>
                <p *ngIf="recursos.dinheiro > 0">Dinheiro: <span>{{recursos.dinheiro | number}}</span></p>
                <p *ngIf="recursos.alimento > 0">Alimento: <span>{{recursos.alimento | number}}</span></p>
                <p *ngIf="recursos.metal > 0">Metal: <span>{{recursos.metal | number}}</span></p>
                <p *ngIf="recursos.petroleo > 0">Petróleo: <span>{{recursos.petroleo | number}}</span></p>
            </div>
        </div>
        <div class="defensor">
            <div class="unidade"
                 [style.backgroundImage]="unidadeImage(unidade)"
                 *ngFor="let unidade of unidades.defesa.primeiro; let i = index">
                <h3>{{unidade.nome}}</h3>
                <span class="quantidade">{{unidade.quantidade}}</span>
                <span class="perdas" [innerHTML]="calcPerdas(unidades.defesa.ultimo[i], unidade)"></span>
            </div>
        </div>
    </div>
</div>
