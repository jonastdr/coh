<form action="" (ngSubmit)="novoNivel(form)" autocomplete="off">
    <div class="input-container">
        <label>Nova</label>
        <input type="text" name="nome" [(ngModel)]="form.nome"
               minlength="3" placeholder="Qual a descrição?">
    </div>

    <div class="flex center">
        <button class="btn" type="submit">Salvar</button>
    </div>
</form>

<div class="listar" coh-scroll>
    <table class="membros">
        <thead>
        <tr>
            <th class="text-left">Patente</th>
            <th class="text-left">Permissões</th>
            <th class="text-right">Opções</th>
        </tr>
        </thead>
        <tbody>
        <tr *ngFor="let n of niveis">
            <td class="input-container">
                <i class="fa fa-pencil"></i>
                <input type="text" name="nivel{{n.nome}}" [(ngModel)]="n.nome" (ngModelChange)="editaNivel(n)">
            </td>
            <td>
                <label *ngFor="let p of n.permissoes" style="display: block;">
                    <input type="checkbox" name="nivel_permis{{n.id}}" [(ngModel)]="p.checked" (ngModelChange)="marcarPermissao(n)">
                    {{p.nome}}
                </label>
            </td>
            <td class="text-center">
                <button class="btn btn-sm" (click)="removerNivel(n)" [coh-title]="'Remover'">
                    <i class="fa fa-close"></i>
                </button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
