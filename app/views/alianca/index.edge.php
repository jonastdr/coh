<ul class="dialog-tabs">
    <li><a (click)="alternarAba('inicial')" [class]="abaAtiva('inicial')">Inicial</a></li>
    <li><a (click)="alternarAba('membros')" [class]="abaAtiva('membros')">Membros</a></li>
    @if(in_array('2', $permissoes))
        <li><a (click)="alternarAba('recrutamento')" [class]="abaAtiva('recrutamento')">Recrutamento</a></li>
    @endif
    <li><a (click)="alternarAba('propriedades')" [class]="abaAtiva('propriedades')">Propriedades</a></li>
</ul>

<div *ngIf="aba == 'inicial'">
    <div class="titulo">
        <label>Aliança: </label><span>{{alianca.nome}}</span>
    </div>

    <div class="descricao float-left">
        {{alianca.descricao}}
    </div>

    <div class="rank float-left">
        <table>
            <thead>
            <tr>
                <th>Rank</th>
                <th>Nome</th>
            </tr>
            </thead>
            <tbody>
            <tr *ngFor="let membro of membros; let i = index">
                <td>{{i+1}}</td>
                <td>{{membro.nome}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div *ngIf="aba == 'membros'">
    <div class="listar" [coh-scroll]>
        <table class="membros">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Patente</th>
                <th>Status</th>
                <th width="20%" class="text-right">Opções</th>
            </tr>
            </thead>
            <tbody>
            <tr [class]="membro.bloqueio ? 'bloqueado' : ''" *ngFor="let membro of membros; let i = index">
                <td>{{membro.nome}}</td>
                <td>
                    <select name="patente{{membro.id}}" [(ngModel)]="membro.id_nivel"
                            (ngModelChange)="alterarNivel(membro)" *ngIf="permissoes.indexOf('2') > -1">
                        <option [value]="0">
                            Novato
                        </option>
                        <option [value]="n.id" *ngFor="let n of niveis"
                                [selected]="membro.id_nivel == n.id">
                            {{n.nome}}
                        </option>
                    </select>

                    <span *ngIf="permissoes.indexOf('2') == -1">{{membro.patente}}</span>
                </td>
                <td>{{membro.ultimo_acesso}}</td>
                <td width="20%" class="text-right">
                    @if(in_array('6', $permissoes))
                        <button class="btn-table" title="Mensagem" [coh-title] (click)="abrirMensagem(membro)">
                            <i class="fa fa-comment"></i>
                        </button>
                    @endif
                    @if(in_array('7', $permissoes))
                        <button class="btn-table" title="Bloquear" [coh-title] (click)="bloquearMembro(membro)">
                            <i class="fa fa-close"></i>
                        </button>
                    @endif
                    <button class="btn-table" title="Estatísticas" [coh-title] (click)="abrirEstatisticas(membro)">
                        <i class="fa fa-leanpub"></i>
                    </button>
                    @if(in_array('7', $permissoes))
                        <button class="btn-table" title="Expulsar" [coh-title] (click)="expulsarMembro(membro)">
                            <i class="fa fa-user-times"></i>
                        </button>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="opcoes">
        @if(in_array('2', $permissoes))
            <button class="btn btn-sm" (click)="abrirNiveis()">
                <i class="fa fa-key"></i>
                Níveis
            </button>
        @endif
    </div>
</div>

<div *ngIf="aba == 'recrutamento'">
    <div class="listar" [coh-scroll]>
        <table class="membros">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" (click)="checkAll('recrutamento', $event)">
                </th>
                <th>Nome</th>
                <th>Mensagem</th>
                <th width="82" class="text-right">Opções</th>
            </tr>
            </thead>
            <tbody>
            <tr *ngFor="let usuario of usuarios">
                <td width="30">
                    <input type="checkbox" [(ngModel)]="usuario.checked" />
                </td>
                <td>{{usuario.nome}}</td>
                <td>{{usuario.mensagem}}</td>
                <td class="text-right">
                    <button class="btn-table" (click)="aceitarRecrutamento(usuario)">
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <button class="btn-table" (click)="rejeitarRecrutamento(usuario)">
                        <i class="fa fa-user-times"></i>
                    </button>
                </td>
            </tr>
            <tr *ngIf="usuarios.length == 0">
                <td colspan="4">Nenhum recrutamento pendente!</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="opcoes">
        <button class="btn btn-sm" (click)="aceitarRecrutamento()">
            <i class="fa fa-user-plus"></i> Aceitar
        </button>
        <button class="btn btn-sm" (click)="aceitarRecrutamento()">
            <i class="fa fa-user-times"></i> Recusar
        </button>
    </div>
</div>

<div *ngIf="aba == 'propriedades'" [coh-scroll]>
    @if(in_array('1', $permissoes))
    <div class="titulo flex">
        <label for="nome">Aliança: </label>
        <input type="text" name="nome" [(ngModel)]="alianca.nome" placeholder="alterar nome">
        <button class="btn btn-sm btn-black" (click)="alterarPropriedade('nome', alianca.nome)">
            <i class="fa fa-check"></i>
        </button>
    </div>
    @endif

    <p>Opções da aliança:</p>

    <div class="opcoes_propriedades">
        <div class="propriedades">
            @if(in_array('2', $permissoes))
                <div class="recrutamento">
                    <label class="label">Recrutamento:</label>

                    <label class="choice">
                        <input type="radio" name="recrutamento" [(ngModel)]="alianca.recrutamento" [value]="true"
                               (change)="alterarPropriedade('recrutamento', true)" />
                        SIM
                    </label>

                    <label class="choice">
                        <input type="radio" name="recrutamento" [(ngModel)]="alianca.recrutamento" [value]="false"
                               (change)="alterarPropriedade('recrutamento', false)" />
                        NÃO
                    </label>
                </div>
            @endif

            @if($alianca->fundador !== $logado->id)
                <div class="abandonar">
                    <label>Abandonar?</label>
                    <button (click)="abandonar()">SIM</button>
                </div>
            @endif

            @if((in_array('5', $permissoes) || $alianca->fundador == $logado->id) && 1==2)
                <div class="transferir">
                    <label>Transferir?</label>
                    <span> para </span>
                    <select class="select">
                        <option>Eu</option>
                    </select>
                </div>
            @endif
            @if($alianca->fundador == $logado->id)
                <div class="excluir">
                    <label>Excluir?</label>
                    <button class="btn btn-sm btn-black" (click)="excluir()">SIM</button>
                </div>
            @endif
        </div>

        @if(in_array('3', $permissoes))
            <p>Descrição da aliança:</p>

            <div class="propriedades">
                <textarea name="descricao" [(ngModel)]="alianca.descricao" placeholder="Digite aqui a descrição da aliança">{{alianca.descricao}}</textarea>
            </div>
            <div class="flex center">
                <button class="btn" (click)="alterarPropriedade('descricao', alianca.descricao)">Salvar</button>
            </div>
        @endif

        @if(in_array('4', $permissoes))
            <p>Descrição interna:</p>

            <div class="propriedades" *ngIf="permissoes.indexOf('4')">
                <textarea name="descricao_interna" [(ngModel)]="alianca.descricao_interna" placeholder="Digite aqui a descrição interna da aliança">{{alianca.descricao_interna}}</textarea>
            </div>

            <div class="flex center" *ngIf="permissoes.indexOf('4')">
                <button class="btn" (click)="alterarPropriedade('descricao_interna', alianca.descricao_interna)">Salvar</button>
            </div>
        @endif
    </div>
</div>