<div class="listar">
    <table>
        <thead>
            <tr>
                <th>Assunto</th>
                <th>Mensagem</th>
                <th>#</th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($topicos as $index => $topico): ?>
                <tr>
                    <td><?= $topico->assunto ?></td>
                    <td><?= substr($topico->mensagem, 0, 30) ?></td>
                    <td>0</td>
                    <td><?= $topico->dataenvio ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<div class="paginacao">
    <?php if (isset($paginacao)) : ?>
        <?php foreach ($paginacao as $id => $pagina): ?>
            <a <?php if ($pagina->ativo) : ?>class="ativo"<?php endif ?> href="<?= $pagina->link ?>"><?= $pagina->numero ?></a>
        <?php endforeach ?>
        <div class="select">
            <select>
                <?php foreach ($options as $index => $option): ?>
                    <option value="<?= $option->link ?>"<?php if ($option->selected) : ?> selected="selected"<?php endif ?>>Mostrar <?= $option->text ?></option>
                <?php endforeach ?>
            </select>
        </div>
    <?php endif ?>
</div>


<script>
    $('#Forum_Alianca .listar').myscroll();
</script>
