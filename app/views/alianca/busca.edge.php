<form autocomplete="off">
	<div class="input-container titulo">
		<label>Aliança:</label>
		<input type="text" placeholder="Nome" name="busca" [(ngModel)]="busca" (keyup)="buscarAlianca($event)">
	</div>

	<div class="text-center">
		<button class="btn" (click)="buscarAlianca()">Buscar</button>

		<button class="btn" (click)="criarAlianca()">Criar</button>
	</div>
</form>

<div class="listar aliancas" [coh-scroll]>
	<table>
		<thead>
		<tr>
			<th>Rank</th>
			<th>Aliança</th>
			<th>Membros</th>
			<th class="text-right">Pontos</th>
			<th class="text-right" width="100">por membro</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<tr *ngFor="let alianca of aliancas">
			<td>{{alianca.rank}}</td>
			<td>{{alianca.nome}}</td>
			<td>{{alianca.membros}}</td>
			<td class="text-right">{{alianca.pontos}}</td>
			<td class="text-right">{{alianca.pontos_membro}}</td>
			<td class="text-right">
				<button class="btn btn-sm" (click)="alistar(alianca)">
					<i class="fa fa-users"></i>
					Alistar
				</button>
			</td>
		</tr>
		</tbody>
	</table>
</div>