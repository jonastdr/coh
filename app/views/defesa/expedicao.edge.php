<table>
    <thead>
    <tr>
        <th>Na Cidade</th>
        <th>Unidades</th>
        <th>Desde</th>
        <th>Opções</th>
    </tr>
    </thead>
    <tbody>
    <tr *ngFor="let defesa of expedicoes">
        <td>{{defesa.cidade}}</td>
        <td>
            <ul class="unidades links">
                <li *ngFor="let u of defesa.unidades"
                    [style.backgroundImage]="unidadeImage(u)">
                    <h3>{{u.nome}}</h3>
                    <span>{{u.quantidade}}</span>
                </li>
            </ul>
        </td>
        <td>{{defesa.data | date:'dd/MM/yyyy HH:mm'}}</td>
        <td>
            <button class="btn btn-sm" (click)="regressar(defesa)">Regressar</button>
        </td>
    </tr>
    </tbody>
</table>