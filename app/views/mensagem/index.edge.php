<ul class="dialog-tabs">
	<li><a (click)="alternarAba('usuarios')" [class]="abaAtiva('usuarios')">Usuarios</a></li>
	<li><a (click)="alternarAba('relatorios')" [class]="abaAtiva('relatorios')">Relatórios</a></li>
	<li><a (click)="alternarAba('aliancas')" [class]="abaAtiva('aliancas')">Alianças</a></li>
	<li><a (click)="alternarAba('nova')" [class]="abaAtiva('nova')">Nova Mensagem</a></li>
</ul>

<div *ngIf="aba == 'usuarios'">
	<div class="listar" coh-scroll>
		<table>
			<thead>
			<tr>
                <th width="30">
                    <input type="checkbox" (click)="checkAll(1, $event)">
                </th>
				<th>Assunto</th>
				<th>Mensagem</th>
				<th [coh-title]="'Respostas'">#</th>
				<th>Data</th>
			</tr>
			</thead>
			<tbody>
			<tr class="clicked" *ngFor="let mensagem of usuarios">
                <td class="text-center">
                    <input type="checkbox" [(ngModel)]="mensagem.checked">
                </td>
                <td class="clicked" (click)="visualizarMensagem(mensagem)">
                    <i class="fa fa-comments"></i>
                    {{mensagem.assunto}}
                </td>
                <td class="clicked" (click)="visualizarMensagem(mensagem)">{{mensagem.mensagem}}</td>
                <td class="clicked" (click)="visualizarMensagem(mensagem)">{{mensagem.replicas}}</td>
                <td class="clicked" (click)="visualizarMensagem(mensagem)">{{mensagem.data_criacao | date:'dd/MM/yyyy HH:mm'}}</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="table-footer">
        <div class="actions">
            <button class="btn btn-sm" (click)="excluir(1, usuarios)">
                <i class="fa fa-close"></i> Excluir
            </button>
        </div>

		<ul class="pagination">
			<li>
				<a class="btn btn-sm" (click)="pageChange(1, usuariosPag.startPage)"><<</a>
			</li>
			<li *ngFor="let p of usuariosPag.pages">
				<a class="btn btn-sm" (click)="pageChange(1, p)" [ngClass]="p == usuariosPag.currentPage ? 'active' : ''">{{p}}</a>
			</li>
			<li>
				<a class="btn btn-sm" (click)="pageChange(1, usuariosPag.endPage)">>></a>
			</li>
		</ul>

		<select class="select float-right" [(ngModel)]="usuariosMostrar" (ngModelChange)="pageChange(1, usuariosPag.currentPage)">
			<option value="5">Mostrar 5</option>
			<option value="10" selected>Mostrar 10</option>
			<option value="15">Mostrar 15</option>
		</select>
	</div>
</div>

<div *ngIf="aba == 'relatorios'">
	<div class="listar" coh-scroll>
		<table>
			<thead>
			<tr>
				<th width="30">
					<input type="checkbox" (click)="checkAll(2, $event)">
				</th>
				<th>Tipo</th>
				<th>Relatório</th>
				<th>Data</th>
			</tr>
			</thead>
			<tbody>
			<tr class="clicked" *ngFor="let mensagem of relatorios">
				<td class="text-center">
					<input type="checkbox" [(ngModel)]="mensagem.checked">
				</td>
				<td class="clicked" (click)="abrirRelatorio(mensagem)">
					<i class="fa fa-comments"></i>
					{{mensagem.assunto}}
				</td>
				<td class="clicked" (click)="abrirRelatorio(mensagem)" *ngIf="mensagem.tipo == 2">
					Há um confronto na cidade
					<span class="cidade-defensor">
						<i class="fa fa-flag"></i>{{ mensagem.mensagem }}.
					</span>
				</td>
				<td class="clicked" (click)="abrirRelatorio(mensagem)" *ngIf="mensagem.tipo == 4">
					<i class="fa fa-eye"></i> {{ mensagem.mensagem }}.
				</td>
				<td class="clicked" (click)="abrirRelatorio(mensagem)">{{mensagem.data_criacao | date:'dd/MM/yyyy HH:mm'}}</td>
			</tr>
			</tbody>
		</table>
	</div>

    <div class="table-footer">
        <div class="actions">
            <button class="btn btn-sm" (click)="excluir(2, relatorios)">
                <i class="fa fa-close"></i> Excluir
            </button>
        </div>

        <ul class="pagination">
            <li>
                <a class="btn btn-sm" (click)="pageChange(2, relatoriosPag.startPage)"><<</a>
            </li>
            <li *ngFor="let p of relatoriosPag.pages">
                <a class="btn btn-sm" (click)="pageChange(2, p)" [ngClass]="p == relatoriosPag.currentPage ? 'active' : ''">{{p}}</a>
            </li>
            <li>
                <a class="btn btn-sm" (click)="pageChange(2, relatoriosPag.endPage)">>></a>
            </li>
        </ul>

        <select class="select float-right" [(ngModel)]="relatoriosMostrar" (ngModelChange)="pageChange(2, relatoriosPag.currentPage)">
            <option value="5">Mostrar 5</option>
            <option value="10" selected>Mostrar 10</option>
            <option value="15">Mostrar 15</option>
        </select>
    </div>
</div>

<div *ngIf="aba == 'aliancas'">
	<div class="listar" coh-scroll>
		<table>
			<thead>
			<tr>
                <th width="30">
                    <input type="checkbox" (click)="checkAll(3, $event)">
                </th>
				<th>Assunto</th>
				<th>Mensagem</th>
				<th>#</th>
				<th>Data</th>
			</tr>
			</thead>
			<tbody>
			<tr *ngFor="let mensagem of aliancas">
                <td class="text-center">
                    <input type="checkbox" [(ngModel)]="mensagem.checked">
                </td>
				<td class="clicked" (click)="visualizarMensagem(mensagem)">
					<i class="fa fa-comments"></i>
					{{mensagem.assunto}}
				</td>
				<td class="clicked" (click)="visualizarMensagem(mensagem)">{{mensagem.mensagem}}</td>
				<td class="clicked" (click)="visualizarMensagem(mensagem)">{{mensagem.replicas}}</td>
				<td class="clicked" (click)="visualizarMensagem(mensagem)">{{mensagem.data_criacao | date:'dd/MM/yyyy HH:mm'}}</td>
			</tr>
			</tbody>
		</table>
	</div>

    <div class="table-footer">
        <div class="actions">
            <button class="btn btn-sm" (click)="excluir(3, aliancas)">
                <i class="fa fa-close"></i> Excluir
            </button>
        </div>

        <ul class="pagination">
            <li>
                <a class="btn btn-sm" (click)="pageChange(3, aliancasPag.startPage)"><<</a>
            </li>
            <li *ngFor="let p of aliancasPag.pages">
                <a class="btn btn-sm" (click)="pageChange(3, p)" [ngClass]="p == aliancasPag.currentPage ? 'active' : ''">{{p}}</a>
            </li>
            <li>
                <a class="btn btn-sm" (click)="pageChange(3, aliancasPag.endPage)">>></a>
            </li>
        </ul>

        <select class="select float-right" [(ngModel)]="aliancasMostrar" (ngModelChange)="pageChange(3, aliancasPag.currentPage)">
            <option value="5">Mostrar 5</option>
            <option value="10" selected>Mostrar 10</option>
            <option value="15">Mostrar 15</option>
        </select>
    </div>
</div>

<div *ngIf="aba == 'nova'">
	<form (ngSubmit)="nova(form)" #formNova="ngForm" autocomplete="off">
		<div class="novausuario">
			<label>Usuario:</label>
			<input-tag name="usuarios" [(ngModel)]="form.usuarios" [url]="'buscar_usuarios'" [list-key]="'usuarios'"></input-tag>
		</div>
		<div class="novaassunto">
			<label>Assunto:</label>
			<input type="text" name="assunto" [(ngModel)]="form.assunto" placeholder="Escreva" required minlength="3">
		</div>
		<div class="mensagem-editor">
			<div class="novamensagem">
				<textarea name="mensagem" [(ngModel)]="form.mensagem" placeholder="Digite aqui sua mensagem"
						  rows="20" required minlength="3"></textarea>
			</div>
		</div>

		<div class="text-center">
			<button class="btn" type="submit" [disabled]="!formNova.form.valid">Enviar</button>
		</div>
	</form>
</div>

<div *ngIf="aba == 'visualizar'">
	<div class="titulo">Conversa: {{visualizar.nomes_destino}}</div>
	<div class="replicas">
		<div class="dados" *ngFor="let texto of visualizar.mensagens">
			<div class="titulo">
				<span class="usuario">{{texto.nome_remetente}}</span> enviou às <span class="horario">
                {{texto.data_envio | date:'dd/MM/yyyy HH:mm'}}</span>
			</div>
			<div class="texto">
				{{texto.mensagem}}
			</div>
		</div>
	</div>

	<hr />

	<form (ngSubmit)="responder(visualizar.id, form)" #formResponder="ngForm">
		<div class="titulo">Escrever uma nova mensagem:</div>
		<div class="novaassunto">
			<label for="assunto">Assunto:</label>
			<input type="text" id="assunto" name="assunto" [(ngModel)]="form.assunto" [placeholder]="visualizar.assunto">
		</div>
		<div class="novamensagem">
			<textarea name="mensagem" [(ngModel)]="form.mensagem" placeholder="Digite aqui sua mensagem"
					  required minlength="3"></textarea>
		</div>
		<div class="text-center">
			<button class="btn" type="submit" [disabled]="!formResponder.form.valid">Enviar</button>
		</div>
	</form>
</div>