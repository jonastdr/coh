<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Command of Heroes</title>
        <?php echo $this->assets->outputCss(); ?>
        <?php echo $this->assets->outputJs(); ?>
    </head>
    <body>
        @include(header)
        @include(index/login)
    </body>
</html>
