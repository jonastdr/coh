<html>
<head>
	<title>Command of Heroes</title>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="../node_modules/jquery.scrollbar/jquery.scrollbar.css">
	<link rel="stylesheet" href="css/padrao.min.css">
</head>
<!-- 3. Display the application -->
<body>
<coh-game>
    <div class="loading-initial">
        <span>
            <img src="{% url('imagens/coh.png') %}" alt="Carregando...">
            <br />
            Carregando objetos, texturas e algo mais...
        </span>
    </div>
</coh-game>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="../node_modules/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="vendor.js"></script>
<script src="app.js"></script>
</body>
</html>