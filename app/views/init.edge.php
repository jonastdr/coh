<coh-mapa *ngIf="view == 'mapa'"></coh-mapa>

<coh-overview *ngIf="view == 'overview'"></coh-overview>

<coh-cidades></coh-cidades>

<coh-notificacao></coh-notificacao>

<coh-recursos></coh-recursos>

<coh-menu></coh-menu>

<coh-sidebar></coh-sidebar>

<div #dialog></div>