<div class="coordinates">
	<div class="positions">
		X <input type="text" [(ngModel)]="posx" (keyup)="changeCoords($event)">
	</div>
	<div class="positions">
		Y <input type="text" [(ngModel)]="posy" (keyup)="changeCoords($event)">
	</div>

	<div class="to_home">
		<button (click)="getPosition()" title="Ir para cidade" [coh-title]></button>
	</div>
</div>

<div id="mapa_fundo" style="left:0; top:0;" (click)="hideOptions($event)">
	<div class="map">
        <div id="cities" style="position:absolute;">
	        <div [id]="ilha.id" class="ilha" *ngFor="let ilha of ilhas"
	             [style.left]="ilha.x"
	             [style.top]="ilha.y"
	             [style.backgroundImage]="imageIsland(ilha)">
		        <div class="cidade" *ngFor="let cidade of ilha.cidades"
		             [style.left]="cidade.x"
		             [style.top]="cidade.y">
			        <div class="bandeira_cor" *ngIf="cidade.id"
			             [style.backgroundColor]="'rgb(255, 255, 255)'"></div>
			        <div class="bandeira" *ngIf="cidade.id"
			             [style.backgroundImage]="'url(map/flag1.png)'"></div>
			        <div class="rank" *ngIf="cidade.id"
			             [style.backgroundImage]="'url(map/rank.png)'"></div>
					<div class="home" *ngIf="cidade.id == cidadeAtual.id"></div>
			        <a [id]="cidade.id" (click)="showOptions(cidade)">
				        <div title="Cidade Slot" class="modelo0" *ngIf="!cidade.id" [coh-cidade]></div>
				        <div title="Cidade" class="modelo1" *ngIf="cidade.id" [coh-cidade]="cidade"></div>
			        </a>
			
			        <div class="menucity" *ngIf="menuId == cidade.cod_ilha + '' + cidade.cod_cidade">
				        <a (click)="openExpedicao(cidade)">
					        <img src="map/opt1.png" alt="missão" [coh-title]="'Missão'">
				        </a>
				        <a (click)="openUsuario(cidade)">
					        <img src="map/opt2.png" alt="perfil" [coh-title]="'Perfil'">
				        </a>
				        <a (click)="openEspionagem(cidade)">
					        <img src="map/opt3.png" alt="espionar" [coh-title]="'Espionar'">
				        </a>
			        </div>
		        </div>
	        </div>
        </div>
    </div>
</div>