<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Command of Heroes</title>
    <link rel="stylesheet" href="css/padrao.min.css">
</head>
<body>

<nav>
    <h1 class="text-center">Command of Heroes</h1>
</nav>

<div layout-flex="row">
    <div flex>

    </div>

    <div>
        {% Form::open(route('registrar_novo_user')) %}
            <div class="input-container">
                {% Form::label('email', 'Email') %}
                {% Form::text('email', Request::getPost('email'), ['placeholder' => 'Digite seu email']) %}
            </div>
            <div>
                {% Form::label('nome', 'Nome') %}
                {% Form::text('nome', null, ['placeholder' => 'Digite seu nome']) %}
            </div>
            <div>
                {% Form::label('senha', 'Senha') %}
                {% Form::password('senha') %}
            </div>
            <div>
                {% Form::submit('Registrar', ['class' => 'btn']) %}
            </div>
        {% Form::close() %}
    </div>
</div>
</body>
</html>