<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Projeto TCC</title>
          <?php echo $this->assets->outputCss(); ?>
          <?php echo $this->assets->outputJs(); ?>
    </head>
    <body>
        <?php echo $this->render('header'); ?>

        <?php $campos = array('Login' => array('login', 'text'), 'Senha' => array('senha1', 'password'), 'Repita a Senha' => array('senha2', 'password'), 'E-mail' => array('email', 'text')); ?>

        <div id="registro">
            <?php echo $this->tag->form(array('registro', 'method' => 'post')); ?>

            <table>
                <?php foreach ($campos as $titulo => $name) { ?>
                    <?php if ($name[1] == 'password') { ?>
                        <tr><td><label><?php echo $titulo; ?>:</label></td><td><?php echo $this->tag->passwordField(array($name[0], 'id' => false, 'style' => 'width: 155px;')); ?></td></tr>
                    <?php } else { ?>
                        <tr><td><label><?php echo $titulo; ?>:</label></td><td><?php echo $this->tag->textField(array($name[0], 'id' => false, 'style' => 'width: 155px;')); ?></td></tr>
                    <?php } ?>
                <?php } ?>
                <tr><td colspan="2"><?php echo $this->tag->submitButton(array('Registrar', 'class' => 'submit')); ?></td></tr>
            </table>
            </form>
            <?php echo $this->tag->linkTo(array('', 'Login', 'class' => 'link')); ?>
        </div>
		<?php echo $enviaMensagem; ?>
    </body>
</html>
