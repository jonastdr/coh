<ul class="dialog-tabs">
	<li><a (click)="alternarAba('usuarios')" [class]="abaAtiva('usuarios')">Usuários</a></li>
	<li><a (click)="alternarAba('aliancas')" [class]="abaAtiva('aliancas')">Alianças</a></li>
	<li><a (click)="alternarAba('ataque')" [class]="abaAtiva('ataque')">Ataque</a></li>
	<li><a (click)="alternarAba('defesa')" [class]="abaAtiva('defesa')">Defesa</a></li>
</ul>

<div *ngIf="aba == 'usuarios'">
	<div class="listar" [coh-scroll]>
		<table>
			<thead>
			<tr>
				<th>Rank</th>
				<th>Usuario</th>
				<th>Aliança</th>
				<th class="text-right">Pontos</th>
			</tr>
			</thead>
			<tbody>
			<tr *ngFor="let usuario of usuarios">
				<td>{{usuario.rank}}</td>
				<td>{{usuario.nome}}</td>
				<td>{{usuario.alianca}}</td>
				<td class="text-right">{{usuario.pontos_construcoes}}</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="paginacao">

	</div>
</div>

<div *ngIf="aba == 'aliancas'">
	<div class="listar">
		<table>
			<thead>
			<tr>
				<th>Rank</th>
				<th>Aliança</th>
				<th>Membros</th>
				<th class="text-right">Pontos</th>
				<th class="text-right" width="100">por membro</th>
			</tr>
			</thead>
			<tbody>
			<tr *ngFor="let alianca of aliancas; let i = index">
				<td>{{i+1}}</td>
				<td>{{alianca.nome}}</td>
				<td>{{alianca.membros}}</td>
				<td class="text-right">{{alianca.pontos}}</td>
				<td class="text-right">{{alianca.pontos_membro}}</td>
			</tr>
			</tbody>
		</table>
	</div>
	
	<div class="paginacao">
		
	</div>
</div>

<div *ngIf="aba == 'ataque'">
	<div class="listar" [coh-scroll]>
		<table>
			<thead>
			<tr>
				<th>Rank</th>
				<th>Usuario</th>
				<th>Aliança</th>
				<th class="text-right">Pontos</th>
			</tr>
			</thead>
			<tbody>
			<tr *ngFor="let usuario of ataques; let i = index">
				<td>{{i+1}}</td>
				<td>{{usuario.nome}}</td>
				<td>{{usuario.alianca}}</td>
				<td class="text-right">{{usuario.pontos_ataque}}</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="paginacao">

	</div>
</div>

<div *ngIf="aba == 'defesa'">
	<div class="listar" [coh-scroll]>
		<table>
			<thead>
			<tr>
				<th>Rank</th>
				<th>Usuario</th>
				<th>Aliança</th>
				<th class="text-right">Pontos</th>
			</tr>
			</thead>
			<tbody>
			<tr *ngFor="let usuario of defesas; let i = index">
				<td>{{i+1}}</td>
				<td>{{usuario.nome}}</td>
				<td>{{usuario.alianca}}</td>
				<td class="text-right">{{usuario.pontos_defesa}}</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="paginacao">

	</div>
</div>