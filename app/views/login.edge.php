<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Command of Heroes</title>
    <link rel="stylesheet" href="css/padrao.min.css">
</head>
<body class="login">

<nav>
    <h1 class="coh-title text-center">Command of Heroes</h1>
</nav>

<div layout-flex="row">
    <div flex>
        <div class="coh-detalhes">
            <h3>Sobre o jogo</h3>

            <p>
                Jogo online de estratégia em tempo real que permite que os jogadores batalhem entre si,
                conquistando cidades ou colonizando cidades em espaços disponíveis para colonização.
                Consta com 7 tipos de recursos: dinheiro, alimento, metal, petróleo, energia, população e ouro.
            </p>
        </div>

        <div layout-flex="row">
            <div class="coh-detalhes" flex="70">
                <h3>Imagens</h3>

                <div layout-flex="row" layout-wrap flex-center>
                    <div class="img-thumb">
                        <img src="<?= url('imagens/home/img1_thumb.jpg') ?>" alt="Inicial">
                    </div>

                    <div class="img-thumb">
                        <img src="<?= url('imagens/home/img1_thumb.jpg') ?>" alt="Inicial">
                    </div>

                    <div class="img-thumb">
                        <img src="<?= url('imagens/home/img1_thumb.jpg') ?>" alt="Inicial">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        {% Form::open(route('logar'), 'POST', ['class' => 'login', 'autocomplete' => 'off']) %}
            <h3>Logar</h3>

            <div class="input-container">
                {% Form::label('nome', 'Nome') %}
                {% Form::text('nome', Request::getPost('nome'), ['placeholder' => 'Nome']) %}
            </div>

            <div class="input-container">
                {% Form::label('senha', 'Senha') %}
                {% Form::password('senha', null, ['placeholder' => '****']) %}
            </div>

            @if($errorsLogin)
            <div>
                @foreach($errorsLogin as $error)
                <p class="valid-errors"><?= $error ?></p>
                @endforeach
            </div>
            @endif

            <div class="text-center">
                {% Form::submit('Logar', ['class' => 'btn btn-sm']) %}
            </div>
        {% Form::close() %}

        {% Form::open(route('registrar_novo_user'), 'POST', ['class' => 'login', 'autocomplete' => 'off']) %}
            <h3>Registre-se</h3>

            <div class="input-container">
                {% Form::label('email', 'Email') %}
                {% Form::text('email', Request::getPost('email'), ['placeholder' => 'Digite seu email']) %}
            </div>
            <div class="input-container">
                {% Form::label('nome_registro', 'Nome') %}
                {% Form::text('nome_registro', Request::getPost('nome_registro'), ['placeholder' => 'Digite seu nome']) %}
            </div>
            <div class="input-container">
                {% Form::label('senha_registro', 'Senha') %}
                {% Form::password('senha_registro', null, ['placeholder' => '****']) %}
            </div>
            <div class="input-container">
                {% Form::label('senha_confirma', 'Confirmar Senha') %}
                {% Form::password('senha_confirma', null, ['placeholder' => '****']) %}
            </div>

            @if($errorsRegister)
            <div>
                @foreach($errorsRegister as $error)
                <p class="valid-errors"><?= $error ?></p>
                @endforeach
            </div>
            @endif

            <div class="text-center">
                {% Form::submit('Registrar', ['class' => 'btn btn-sm']) %}
            </div>
        {% Form::close() %}
    </div>
</div>
</body>
</html>