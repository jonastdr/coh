<div class="lista">
    <ul class="unidades links">
        <li *ngFor="let u of unidades"
            [style.backgroundImage]="unidadeImage(u)" (click)="openUnidade(u)"
            [ngClass]="unidade.id == u.id ? 'selec' : ''">
            <h3>{{u.nome}}</h3>
            <span>{{u.quantidade}}</span>
        </li>
    </ul>
</div>

<div class="requisitos">
    <h3>Requisitos:</h3>
    <ul class="pesquisas">
        <li *ngFor="let requisito of unidade.construcao_requisitos" [style.backgroundImage]="construcaoImage(requisito)">
            <h3>{{requisito.nome}}</h3>
            <span>{{requisito.level}}</span>
        </li>

        <li *ngFor="let requisito of unidade.pesquisa_requisitos" [style.backgroundImage]="pesquisaImage(requisito)">
            <h3>{{requisito.nome}}</h3>
            <span>{{requisito.level}}</span>
        </li>

        <li *ngIf="unidade.construcao_requisitos.length == 0 && unidade.pesquisa_requisitos.length == 0">
            <h3>Nenhum</h3>
            <span>0</span>
        </li>
    </ul>
</div>

<div class="sidebar">
    <div class="detalhes" myscroll>
        <img src="imagens/unidades/{{unidade.id}}.png" width="60" height="60">
        <h3>{{unidade.nome}}</h3>
        <p>
            {{unidade.descricao}}
            <br />
            <br />
            Classe: {{unidade.classe}} - {{unidade.tipo}} <i class="fa fa-cogs"></i>
            <br />
            Ataque terrestre: {{unidade.ataque_terrestre}} <i class="fa fa-bomb"></i>
            <br />
            Ataque aéreo: {{unidade.ataque_aereo}} <i class="fa fa-bomb"></i>
            <br />
            Ataque naval: {{unidade.ataque_naval}} <i class="fa fa-bomb"></i>
            <br />
            Capacidade de carga: {{unidade.capacidade}} <i class="fa fa-truck"></i>
            <br />
            Integridade: {{unidade.integridade}} <i class="fa fa-shield"></i>
        </p>
    </div>
    <div class="recursos">
        <h4>Recursos:</h4>
        <ul class="recursos">
            <li class="dinheiro" *ngIf="unidade.dinheiro">{{unidade.dinheiro * quantidade}}</li>
            <li class="alimento" *ngIf="unidade.alimento">{{unidade.alimento * quantidade}}</li>
            <li class="metal" *ngIf="unidade.metal">{{unidade.metal * quantidade}}</li>
            <li class="petroleo" *ngIf="unidade.petroleo">{{unidade.petroleo * quantidade}}</li>
            <li class="populacao" *ngIf="unidade.populacao">{{unidade.populacao * quantidade}}</li>
            <li class="relogio" *ngIf="unidade.tempo">{{unidade.tempo * quantidade | recursoTempo}}</li>
        </ul>

        <div class="acoes">
            <div>
                <button class="btn menos" (click)="menos()">-</button>
                <input class="quantidade" readonly="true" [(ngModel)]="quantidade" name="quantidade" />
                <button class="btn mais" (click)="mais()">+</button>

                <div class="separator"></div>

                <button class="btn recrutar" (click)="recrutar(unidade)">Recrutar</button>
            </div>
        </div>
    </div>
</div>