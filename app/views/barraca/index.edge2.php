<div flex="80" layout-flex="column">
    <div flex class="sobre_unidade">
        <h1>{{unidade.nome}}</h1>
        <div class="imagem">
            <img src="imagens/unidades/{{unidade.id}}.png" width="120" />
        </div>
        <p>
            Classe: {{unidade.classe}} - {{unidade.tipo}}
            <br />
            {{unidade.descricao}}
        </p>
        <div class="recursos">
            <h4>Recursos:</h4>
            <ul class="recursos">
                <li class="dinheiro" *ngIf="unidade.dinheiro">{{unidade.dinheiro}}</li>
                <li class="alimento" *ngIf="unidade.alimento">{{unidade.alimento}}</li>
                <li class="metal" *ngIf="unidade.metal">{{unidade.metal}}</li>
                <li class="petroleo" *ngIf="unidade.petroleo">{{unidade.petroleo}}</li>
                <li class="populacao" *ngIf="unidade.populacao">{{unidade.populacao}}</li>
                <li class="relogio" *ngIf="unidade.tempo">{{unidade.tempo | recursoTempo}}</li>
            </ul>
        </div>
        <div>
            <h3>Forças:</h3>
            <ul class="forcas">
                <li *ngIf="unidade.ataque_terrestre"><b>Terrestre:</b> {{unidade.ataque_terrestre}}</li>
                <li *ngIf="unidade.ataque_aereo"><b>Aéreo:</b> {{unidade.ataque_aereo}}</li>
                <li *ngIf="unidade.ataque_naval"><b>Naval:</b> {{unidade.ataque_naval}}</li>
                <li *ngIf="unidade.ataque_defesa"><b>Defesa:</b> {{unidade.ataque_defesa}}</li>
                <li *ngIf="unidade.integridade"><b>Integridade:</b> {{unidade.integridade}}</li>
                <li *ngIf="unidade.capacidade"><b>Capacidade:</b> {{unidade.capacidade}}</li>
            </ul>
        </div>
        <div class="acoes">
            <div>
                <button class="btn menos" (click)="menos()">-</button>
                <input class="quantidade" readonly="true" [(ngModel)]="quantidade" name="quantidade" />
                <button class="btn mais" (click)="mais()">+</button>
            </div>
            <button class="btn recrutar" (click)="recrutar(unidade)">Recrutar</button>
        </div>
    </div>

    <div flex class="lista">
        <h3>Lista de unidades</h3>
        <ul class="unidades links">
            <li *ngFor="let unidade of unidades"
                [style.backgroundImage]="unidadeImage(unidade)" (click)="openUnidade(unidade)">
                <h3>{{unidade.nome}}</h3>
                <span>{{unidade.quantidade}}</span>
            </li>
        </ul>
    </div>
</div>

<div flex="20">
    <div class="recrutamento">
        <h3>Recrutamento:</h3>
        <ul class="unidades unidades_recrutar">
            <li class="unidades_recrutamento" *ngFor="let unidade of recrutados"
                [style.backgroundImage]="unidadeRecrutamentoImage(unidade)">
                <h3>{{unidade.nome}}</h3>
                <p class="tempo" [coh-timer]="unidade.restante" (callback)="termino($event)"></p>
                <span>{{unidade.quantidade}}</span>
            </li>
            <li class="unidades_recrutamento" *ngIf="recrutados == 0"
                [style.backgroundImage]="unidadeEmpty()">
                <h3>Vazio</h3>
                <span>0</span>
            </li>
        </ul>
    </div>
</div>