<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class ConstrucaoPesquisaRequisito
 * @property string id_construcao
 * @property string id_pesquisa_req
 * @property string level
 * @package app\models
 */
class ConstrucaoPesquisaRequisito extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_construcao',
        'id_pesquisa_req',
        'level'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_construcao', 'id_pesquisa_req'];

}