<?php

namespace app\models;

use projectws\libs\orm\Join;
use projectws\mvc\Model;

class Cidade extends Model {

    protected $fields = [
        'id',
        'nome',
        'id_usuario',
        'dinheiro',
        'alimento',
        'construcao',
        'pesquisa',
        'metal',
        'petroleo',
        'ov_layout',
	    'cod_ilha',
	    'cod_cidade',
        'espionagem_recurso',
        'atualizacao'
    ];

    protected $pk = 'id';

    /**
     * Cria uma nova cidade para um usuario
     * @param Usuario $usuario
     * @param bool $responseId
     * @return bool
     */
    public static function nova(Usuario $usuario, $responseId = false) {
        $last = Cidade::findFirst(Cidade::select("max(id) as id")->findFirst()->id);

        //define um limite de tentativas
        $limit = 0;
        $limitMax = 1000;

        while(true) {
	        $lastIlha = $last->cod_ilha;

	        $codIlha = $lastIlha + rand(-20, 20);
	        $codCidade = rand(1, 18);

            //Falta definir o limite maior
            if($codIlha > 0 && $codCidade > 0) {
                $novaCidade = Cidade::findFirst([
                    'cod_ilha' => $codIlha,
	                'cod_cidade' => $codCidade
                ]);

                if($novaCidade->count() == 0) {
                    //nova cidade para o usuario
                    $novaCidade->nome = "Nova Cidade";
                    $novaCidade->id_usuario = $usuario->id;
	                $novaCidade->cod_ilha = $codIlha;
	                $novaCidade->cod_cidade = $codCidade;
                    $id = $novaCidade->save();//id da nova cidade

                    //alterar a cidade ativa do usuario
                    $usuario->cidade = $id;
                    
	                if($usuario->save()) {
		                if ($responseId)
			                return true;
		
		                return (Object)[
			                'success' => true,
			                'id_cidade' => $id
		                ];
	                } else {
	                	return (Object) [
	                		'success' => false
                        ];
	                }
                }
            }

            if($limit > $limitMax) {
            	if($responseId)
	                return (Object) [
	                	'success' => false
	                ];
	            
	            return (Object) [
	            	'success' => false
	            ];
            }

            $limit++;
        }
    }
	
	/**
	 * Cria uma nova cidade apartir de uma posição
	 * @param Usuario $usuario
	 * @param $cod_ilha
	 * @param $cod_cidade
	 * @return object
	 */
    public static function novaComPosicao(Usuario $usuario, $cod_ilha, $cod_cidade) {
    	$temCidade = Cidade
		    ::where('cod_ilha', '=', $cod_cidade)
		    ->where('cod_ilha', '=', $cod_cidade)
	        ->row();
	    
	    if($temCidade->count()) {
	    	return (Object) [
	    		'success' => false,
			    'msg' => 'Cidade já colonizada.'
		    ];
	    }
    	
	    $novaCidade = new Cidade();
    	
	    //nova cidade para o usuario
	    $novaCidade->nome = "Nova Cidade";
	    $novaCidade->id_usuario = $usuario->id;
	    $novaCidade->cod_ilha = $cod_ilha;
	    $novaCidade->cod_cidade = $cod_cidade;
	    $id = $novaCidade->save();//id da nova cidade
	
	    //alterar a cidade ativa do usuario
	    $usuario->cidade = $id;
	    $usuario->save();
	
	    return (Object) [
		    'success' => true,
		    'id_cidade' => $id
	    ];
    }
	
	/**
	 * Retorna os recursos de uma determinada cidade ou de todas
	 * @param null $id_cidade
	 * @return mixed
	 */
    public static function recursos($id_cidade = null) {
        if(is_integer($id_cidade))
            $filtro = "WHERE c.id = $id_cidade";
        else
            $filtro = "";

        if(is_null($id_cidade))
            $row = false;
        else
            $row = true;

        $query = Cidade::query("
            SELECT
			  c.id,
			  c.id as _id_cidade,
			  c.dinheiro,
			  c.alimento,
			  c.metal,
			  c.petroleo,
			  u.ouro,
			  COALESCE(cc.energia, 50) - COALESCE(cc.energia_utilizada, 0) AS energia,
			  COALESCE(cc.populacao, 0)
			  -
			  COALESCE(cc.populacao_utilizada, 0)
			  -
			  COALESCE(un.populacao_utilizada, 0)
			  -
			  COALESCE(un2.populacao_utilizada, 0)
			  -
			  COALESCE(un3.populacao_utilizada, 0) AS populacao,
			  c.construcao,
			  c.pesquisa,
			  c.atualizacao,
			  coalesce(cc.banco, 0) banco,
			  coalesce(cc.fazenda, 0) fazenda,
			  coalesce(cc.metalurgica, 0) metalurgica,
			  coalesce(cc.torre, 0) torre,
			  cfg.dinheiro_fator,
			  cfg.alimento_fator,
			  cfg.metal_fator,
			  cfg.petroleo_fator,
			  cfg.dinheiro_armazenamento + cfg.dinheiro_armazenamento * CASE WHEN cc.banco > 1 THEN cc.banco * cfg.dinheiro_armazenamento_fator ELSE 0 END AS dinheiro_armazenamento,
			  cfg.dinheiro_armazenamento + cfg.alimento_armazenamento * CASE WHEN cc.fazenda > 1 THEN cc.fazenda * cfg.alimento_armazenamento_fator ELSE 0 END AS alimento_armazenamento,
			  cfg.dinheiro_armazenamento + cfg.metal_armazenamento * CASE WHEN cc.metalurgica > 1 THEN cc.metalurgica * cfg.metal_armazenamento_fator ELSE 0 END AS metal_armazenamento,
			  cfg.dinheiro_armazenamento + cfg.petroleo_armazenamento * CASE WHEN cc.torre > 1 THEN cc.torre * cfg.petroleo_armazenamento_fator ELSE 0 END AS petroleo_armazenamento
			FROM cidade c
			  INNER JOIN usuario u ON u.id = c.id_usuario
			  LEFT JOIN (
			              SELECT
			                cc.id_cidade,
			                50 + COALESCE(SUM(
			                                   CASE WHEN cc.id_construcao = 15 THEN cc.level * (c.fator * 50) END
			                               ), 0) AS energia,
			                COALESCE(SUM(
			                             CASE WHEN cc.id_construcao = 1 THEN cc.level * (c.fator * 300) END
			                         ), 0) AS populacao,
			                MAX(CASE WHEN cc.id_construcao = 2 THEN cc.level END) AS banco,
			                MAX(CASE WHEN cc.id_construcao = 3 THEN cc.level END) AS fazenda,
			                MAX(CASE WHEN cc.id_construcao = 4 THEN cc.level END) AS metalurgica,
			                MAX(CASE WHEN cc.id_construcao = 5 THEN cc.level END) AS torre,
			                MAX(CASE WHEN cc.id_construcao = 2 THEN c.fator END) AS banco_fator,
			                MAX(CASE WHEN cc.id_construcao = 3 THEN c.fator END) AS fazenda_fator,
			                MAX(CASE WHEN cc.id_construcao = 4 THEN c.fator END) AS metalurgica_fator,
			                MAX(CASE WHEN cc.id_construcao = 5 THEN c.fator END) AS torre_fator,
			                SUM(
			                  CASE WHEN (cc.level) > 1
			                    THEN
			                      c.energia + FLOOR(c.energia * POWER(c.fator, cc.level - 1))
			                    ELSE
			                      c.energia
			                  END
			                ) AS energia_utilizada,
			                SUM(
			                  CASE WHEN (cc.level) > 1
			                    THEN
			                      c.populacao + FLOOR(c.populacao * POWER(c.fator, cc.level - 1))
			                    ELSE
			                      c.populacao
			                  END
			                ) AS populacao_utilizada
			              FROM cidade_construcao cc
			                INNER JOIN construcao c ON c.id = cc.id_construcao
			              GROUP BY cc.id_cidade
			            ) cc ON cc.id_cidade = c.id
			  LEFT JOIN (
			    SELECT
			      cu.id_cidade,
			      SUM(
			        cu.quantidade * u.populacao
			      ) AS populacao_utilizada
			    FROM cidade_unidade cu
			      INNER JOIN unidade u ON u.id = cu.id_unidade
			    GROUP BY cu.id_cidade
			  ) AS un ON un.id_cidade = c.id
			  LEFT JOIN (
			              SELECT
			                e.id_cidade_atk as id_cidade,
			                SUM(
			                    eu.quantidade * u.populacao
			                ) AS populacao_utilizada
			              FROM expedicao e
			                INNER JOIN expedicao_unidade eu ON eu.id_expedicao = e.id
			                INNER JOIN unidade u ON u.id = eu.id_unidade
			              GROUP BY e.id_cidade_atk
			            ) AS un2 ON un2.id_cidade = c.id
			  LEFT JOIN (
			              SELECT
			                cd.id_cidade,
			                SUM(
			                    cdu.quantidade * u.populacao
			                ) AS populacao_utilizada
			              FROM cidade_defesa cd
			                INNER JOIN cidade_defesa_unidade cdu ON cdu.id_defesa = cd.id
			                INNER JOIN unidade u ON u.id = cdu.id_unidade
			              GROUP BY cd.id_cidade
			            ) AS un3 ON un3.id_cidade = c.id
			  CROSS JOIN (
			               SELECT
			                 MAX(CASE WHEN cfg.config = 'dinheiro_fator' THEN cfg.valor END) AS dinheiro_fator,
			                 MAX(CASE WHEN cfg.config = 'alimento_fator' THEN cfg.valor END) AS alimento_fator,
			                 MAX(CASE WHEN cfg.config = 'metal_fator' THEN cfg.valor END) AS metal_fator,
			                 MAX(CASE WHEN cfg.config = 'petroleo_fator' THEN cfg.valor END) AS petroleo_fator,
			                 MAX(CASE WHEN cfg.config = 'dinheiro_armazenamento' THEN CAST(cfg.valor AS FLOAT) END) AS dinheiro_armazenamento,
			                 MAX(CASE WHEN cfg.config = 'alimento_armazenamento' THEN CAST(cfg.valor AS FLOAT) END) AS alimento_armazenamento,
			                 MAX(CASE WHEN cfg.config = 'metal_armazenamento' THEN CAST(cfg.valor AS FLOAT) END) AS metal_armazenamento,
			                 MAX(CASE WHEN cfg.config = 'petroleo_armazenamento' THEN CAST(cfg.valor AS FLOAT) END) AS petroleo_armazenamento,
			                 MAX(CASE WHEN cfg.config = 'dinheiro_armazenamento_fator' THEN CAST(cfg.valor AS FLOAT) END) AS dinheiro_armazenamento_fator,
			                 MAX(CASE WHEN cfg.config = 'alimento_armazenamento_fator' THEN CAST(cfg.valor AS FLOAT) END) AS alimento_armazenamento_fator,
			                 MAX(CASE WHEN cfg.config = 'metal_armazenamento_fator' THEN CAST(cfg.valor AS FLOAT) END) AS metal_armazenamento_fator,
			                 MAX(CASE WHEN cfg.config = 'petroleo_armazenamento_fator' THEN CAST(cfg.valor AS FLOAT) END) AS petroleo_armazenamento_fator
			               FROM config cfg
			               WHERE cfg.config LIKE '%_fator' OR cfg.config LIKE '%_armazenamento'
			             ) cfg
            $filtro
        ", null, $row);

        return $query;
    }
	
	/**
	 * Retorna as construções que tem na cidade
	 * @return \projectws\libs\orm\Builder
	 */
    public function construcoes() {
    	return $this->hasMany('CidadeConstrucao');
    }
	
	/**
	 * Retorna as pesquisas que tem na cidade
	 * @return \projectws\libs\orm\Builder
	 */
    public function pesquisas() {
    	return $this->hasMany('CidadePesquisa');
    }
	
	/**
	 * Retorna as unidades que tem na cidade
	 * @return Model
	 */
    public function unidades() {
    	return $this
		    ->belongsToMany('Unidade', 'cidade_unidade')
		    ->select('
    	    a.*,
    	    cu.quantidade
    	')
		    ->join('cidade_unidade cu', function (Join $j) {
		    	$cidade = Funcoes::getCidade();
		    	
		    	$j->on('cu.id_unidade', '=', 'a.id');
			    
			    $j->where('cu.id_cidade', '=', $cidade->id);
		    })
		    ->orderBy('a.nome');
    }
	
	/**
	 * Retorna o usuário a qual a cidade pertence
	 * @return \projectws\libs\orm\Builder
	 */
    public function usuario() {
        return $this->belongsTo('Usuario');
    }
}
