<?php

namespace app\models;

use projectws\mvc\Model;

class Heroi extends Model {

    protected $update = true;

    protected $fields = [
        'id' => ['int'],
        'nome' => ['varchar', 50],
        'id_classe'=> ['int'],
        'descricao' => ['TEXT']
    ];

    protected $indices = ['id'];

    public function listarTodos() {
        $funcoes = new Funcoes();

        $cidade = $funcoes->getCidade();

        $query = $this
            ->from('heroi h')
            ->join('cidade_heroi ch', 'ch.id_heroi ', '=', 'h.id')
            ->where('ch.id_cidade', '=', $cidade->id);

        return $query->rows();
    }
	
	/**
	 * Retorna o id atualmente selecionado na cidade
	 * @param $id_cidade
	 * @return $this|array|null|\projectws\libs\orm\ModelCollection|\projectws\libs\orm\ORM
	 */
    public static function getHeroiCidade($id_cidade) {
        $query = static
	        ::from('heroi')
	        ->alias('h')
            ->join('cidade_heroi ch', 'ch.id_heroi ', '=', 'h.id')
            ->where('ch.id_cidade', '=', $id_cidade);

        return $query->row();
    }

    public function funcoes() {
        return $this->hasMany('HeroiFuncao')
	        ->orderBy('id');
    }
}