<?php

namespace app\models;

use projectws\libs\orm\Join;
use projectws\mvc\Model;

class Alianca extends Model {

    protected $fields = [
        'id',
        'nome',
        'descricao',
        'descricao_interna',
        'recrutamento',
        'fundador'
    ];
    
    protected $pk = 'id';
    
    protected $indices = ['id'];

    public static function membros($id_alianca) {
        $membros = AliancaMembro
                ::select(
                    'am.id',
                    'am.id_usuario',
                    'u.nome',
                    'am.bloqueio',
                    'COALESCE(an.nome, \'Novato\') as patente',
                    'am.id_nivel',
                    'u.ultimo_acesso'
                )
                ->from('alianca_membro am')
                ->join('alianca a', 'a.id', '=', 'am.id_alianca')
                ->leftJoin('alianca_nivel an', function (Join $j) {
                	$j->on('an.id_alianca', '=' ,'a.id');
	                
	                $j->where('an.id', '=', 'am.id_nivel');
                })
                ->join('usuario u', 'u.id', '=', 'am.id_usuario')
                ->where('am.id_alianca', '=', $id_alianca)
                ->rows();
    
        foreach ($membros as $membro) {
            $membro->ultimo_acesso = time() - strtotime($membro->ultimo_acesso);
            
            if($membro->ultimo_acesso <= 300) {
                $membro->ultimo_acesso = 'Online';
            } else {
                if($membro->ultimo_acesso > 108000)
                    $membro->ultimo_acesso = gmdate('n', $membro->ultimo_acesso) . ' mês(es) inativo';
                elseif($membro->ultimo_acesso > 3600)
                    $membro->ultimo_acesso = gmdate('j', $membro->ultimo_acesso) . ' dia(s) inativo';
                else
                    $membro->ultimo_acesso = gmdate('G', $membro->ultimo_acesso) . ' hora(s) inativo';
                    
            }
        }
        
        return $membros;
    }
	
	/**
	 * Retorna os dados do membro pelo id do usuario
	 * @param $id_usuario
	 * @return $this|array|null|\projectws\libs\orm\ModelCollection|\projectws\libs\orm\ORM
	 */
    public static function membro($id_usuario) {
        $membro = AliancaMembro
                ::select('am.*, an.permissoes')
                ->from('alianca_membro am')
                ->leftJoin('alianca_nivel an', 'an.id', '=', 'am.id_nivel')
                ->where('am.id_usuario', '=', $id_usuario)
                ->row();
        
        return $membro;
    }
	
	/**
	 * Retorna os dados de recrutamento
	 * @param $id_alianca
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
    public static function recrutamento($id_alianca) {
        $recrutamento = Alianca
                    ::select('a.*, b.nome')
                    ->from('alianca_recrutamento a')
                    ->join('usuario b', 'b.id', '=', 'a.id_usuario')
                    ->where('id_alianca', '=', $id_alianca)
                    ->rows();
                    
        return $recrutamento;
    }
}
