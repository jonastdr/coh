<?php

namespace app\models;

use projectws\libs\orm\Join;
use projectws\mvc\Model;

class Pesquisa extends Model {

    protected $fields = [
        'id',
        'nome',
        'dinheiro',
        'alimento',
        'metal',
        'petroleo',
        'ouro',
        'energia',
        'populacao',
        'tempo',
        'fator',
        'max',
        'descricao'
    ];

    /**
     * Função usada para retornar pesquisa no sidebar
     * @param $id_cidade
     * @param $id_pesquisa
     * @return \projectws\mvc\classe
     */
    public static function getForSide($id_cidade, $id_pesquisa) {
        $query = Pesquisa::query("
            SELECT p.*, coalesce(cp.level, 0)+1 as level
            FROM pesquisa p
            LEFT JOIN cidade_pesquisa cp ON cp.id_pesquisa = p.id AND cp.id_cidade = :id_cidade
            WHERE p.id = :id_pesquisa LIMIT 1", [
                'id_cidade' => $id_cidade,
                'id_pesquisa' => $id_pesquisa
        ], true);

        return $query;
    }

    /**
     * Retorna todas as pesquisas
     * @param $id_cidade
     * @return type
     */
    public static function listarTodas($id_cidade) {
	    $pesquisas = Pesquisa
		    ::select(
	    	    'p.id',
			    'p.nome',
			    'COALESCE(cp.level, 0) as level'
	        )
		    ->alias('p')
		    ->leftJoin('cidade_pesquisa cp', function (Join $j) use ($id_cidade) {
		        $j->on('cp.id_pesquisa', '=', 'p.id');
                
                $j->where('cp.id_cidade', '=', $id_cidade);
            })
		    ->orderBy('p.id')
	    ;
	    
	    return $pesquisas->rows();
    }
    
    public static function getPesquisa($id_pesquisa, $id_cidade) {
        return Pesquisa::query("
            SELECT p.*, coalesce(cp.level, 0) as level
            FROM pesquisa p
            LEFT JOIN cidade_pesquisa cp ON cp.id_pesquisa = p.id AND cp.id_cidade = :id_cidade
            WHERE p.id = :id_pesquisa
            ", ['id_cidade' => $id_cidade, 'id_pesquisa' => $id_pesquisa], true);
    }
	
	/**
	 * Construção Requisitos
	 * @param $id_pesquisa
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
	public static function construcaoRequisito($id_pesquisa) {
		return PesquisaConstrucaoRequisito
			::select(
				'c.id',
				'c.nome',
				'pcr.level'
			)
			->alias('pcr')
			->join('construcao c', 'c.id', '=', 'pcr.id_construcao_req')
			->where('pcr.id_pesquisa', '=', $id_pesquisa)
			->rows();
	}
	
	/**
	 * Pesquisa Requisitos
	 * @param $id_pesquisa
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
	public static function pesquisaRequisito($id_pesquisa) {
		return PesquisaPesquisaRequisito
			::select(
				'p.id',
				'p.nome',
				'ppr.level'
			)
			->alias('ppr')
			->join('pesquisa p', 'p.id', '=', 'ppr.id_pesquisa_req')
			->where('ppr.id_pesquisa', '=', $id_pesquisa)
			->rows();
	}
	
	/**
	 * Verifica se tem todos os requisitos de construções e pesquisas
	 * @param $id_pesquisa
	 * @return bool
	 */
	public static function temTodosRequisitos($id_pesquisa) {
		$construcoes = Funcoes::getCidade()->construcoes;
		$construcoesReq = Pesquisa::construcaoRequisito($id_pesquisa);
		
		$pesquisas = Funcoes::getCidade()->pesquisas;
		$pesquisasReq = Pesquisa::pesquisaRequisito($id_pesquisa);
		
		$reqCout = $construcoesReq->count() + $pesquisasReq->count();
		$reqPass = 0;
		
		foreach ($construcoesReq as $construcaoReq) {
			foreach ($construcoes as $construcao) {
				if($construcao->id_construcao == $construcaoReq->id && $construcao->level >= $construcaoReq->level) {
					$reqPass++;
				}
			}
		}
		
		foreach ($pesquisasReq as $pesquisaReq) {
			foreach ($pesquisas as $pesquisa) {
				if($pesquisa->id_pesquisa == $pesquisaReq->id && $pesquisa->level >= $pesquisaReq->level) {
					$reqPass++;
				}
			}
		}
		
		/**
		 * Indica que tem todas os requisitos
		 */
		if($reqCout == $reqPass)
			return true;
		
		return false;
	}
}