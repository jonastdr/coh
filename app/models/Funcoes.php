<?php

namespace app\models;

use projectws\libs\Cookie;
use projectws\libs\orm\Join;
use projectws\libs\Session;

class Funcoes {
    static private $velocidadeJogo;
    
    public static function calculoFator($recurso, $level, $fator, $max) {
        if($level>=$max){
            $calculo = 0;
        }
        elseif($level>0) {
            $level++;
            $calculo = $recurso + floor($recurso * pow($fator, $level));
        } else {
            $calculo = $recurso;
        }

        return $calculo;
    }

    public static function calculoTempo($tempo, $level, $fator, $max) {
        if($level>=$max){
            $calculo = 0;
        }
        elseif($level>0){
            $level++;
            $calculo = round($tempo + (floor($tempo * pow($fator, $level)) * $fator),0);
        } else {
            $calculo = $tempo;
        }

        return $calculo;
    }

    public static function getCidade() {
	    $usuario = static::getUsuario();
    	
        $cidadeId = $usuario->cidade;

        $cidade = Cidade
	        ::select(
	        	'c.*',
	            'uh.id_heroi',
		        'uh.honra',
		        'ic.x + i.x AS x',
		        'ic.y + i.y AS y',
		        'c.cod_ilha',
		        'c.cod_cidade'
	        )
	        ->alias('c')
	        ->leftJoin('cidade_heroi ch', 'ch.id_cidade', '=', 'c.id')
	        ->leftJoin('usuario_heroi uh', 'uh.id_heroi', '=', 'ch.id_heroi AND uh.id_usuario = c.id_usuario')
	        ->join('ilha_cidade ic', function (Join $j) {
	        	$j->on('ic.id_ilha', '=', 'c.cod_ilha');
		        
		        $j->where('ic.cod_cidade', '=', 'c.cod_cidade');
	        })
	        ->join('ilha i', 'i.id', '=', 'c.cod_ilha')
	        ->findFirst([
        	'c.id' => $cidadeId
        ]);

        return $cidade;
    }

	/**
	 * Retorna o usuário que está logado
	 * @return null|\projectws\libs\orm\ORM
	 */
    public static function getUsuario() {
        $session = Session::get('application');

        if(!$session)
            return null;

        return Usuario::select('usuario.*', 'c.nome as cidade_nome', 'am.id_alianca')
            ->join('cidade c', 'c.id', '=', 'usuario.cidade')
	        ->leftJoin('alianca_membro am', 'am.id_usuario', '=', 'usuario.id')
            ->findFirst([
            'usuario.id' => $session->id,
            'usuario.nome' => $session->nome
        ]);
    }

	/**
	 * Verifica se há recursos o suficiente para a ação
	 * @param $obj
	 * @return bool
	 */
    public static function tenhoRecursos($obj) {
        $usuario = static::getUsuario();

        $cidade = Cidade::recursos($usuario->cidade);

        if($obj->dinheiro > $cidade->dinheiro)
	        return false;
	    
        if($obj->alimento > $cidade->alimento)
	        return false;
	    
        if($obj->metal > $cidade->metal)
	        return false;
	    
        if($obj->petroleo > $cidade->petroleo)
	        return false;
	    
        if($obj->populacao > $cidade->populacao)
            return false;
	    
        if($obj->energia > $cidade->energia && $obj->energia != 0)
            return false;
	    
	    return true;
    }
	
	/**
	 * Calculo da distancia entre cidades
	 * @param $logado
	 * @param $target
	 * @return float
	 */
    public static function getDistancia($logado, $target) {
        $distancia = sqrt(
        	pow(($target->x - $logado->x), 2)
	        +
	        pow(($target->y - $logado->y), 2)
        );

        return $distancia;
    }

    /**
     * Calcula a duração de uma missão
     * @param $velocidade
     * @param $distancia
     * @return float|int
     */
    public static function getDuracao($velocidade, $distancia) {
        $velocidadeJogo = Funcoes::getVelocidadeJogo();
        
        /**
         * Calculo de distancia em horas
         */
        $horas = $distancia / $velocidade;
    
        /**
         * Horas para segundos
         */
        return round($horas * 3600) / $velocidadeJogo;
    }
	
    /**
     * Retorna a velocidade de aceleração do jogo
     * @return mixed
     */
    public static function getVelocidadeJogo() {
        if(is_null(Funcoes::$velocidadeJogo)) {
            Funcoes::$velocidadeJogo = Config::where('config', '=', 'velocidade')->row()->valor;
        }
        
        return Funcoes::$velocidadeJogo;
    }
	
    public function GlobalChat() {
        $nomearquivo = 'chat/global-'.date('j-n-Y', time()).'.txt';
        $DiaSeguinte = 'chat/global-'.date('j-n-Y', time()+3600*24).'.txt';

        if(!file_exists($nomearquivo)){
            fopen($nomearquivo, 'w+');
        }

        if(!file_exists($DiaSeguinte)){
            fopen($DiaSeguinte, 'w+');
        }

        return $nomearquivo;
    }
    
    public function msgChat($mensagem) {
        $spam = Spam::find();

        $tabela = array();
        foreach ($spam as $chave => $valor) {
            $tabela[0][$chave] = $valor->spam;
            $tabela[1][$chave] = $this->geraAsterisco(strlen($valor->spam));
        }

        $novamensagem = str_ireplace($tabela[0], $tabela[1], $mensagem);

        return $novamensagem;
    }
    
    private function geraAsterisco($contagem) {
        $novospam = '';
        for($n=1;$n<=$contagem;$n++){
            $novospam .= '*';
        }

        return $novospam;
    }
}