<?php

namespace app\models;

use projectws\mvc\Model;

class CidadeDefesa extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id',
        'id_cidade',
        'id_cidade_def',
        'data'
    ];

    //primary key da tabela
    protected $pk = 'id';

    //chaves estrangeiras
    protected $fk = [
        'id_cidade' => ['cidade', 'id'],
        'id_cidade_def' => ['cidade', 'id']
    ];

    //indices usado como padrão em métodos where
    protected $indices = ['id'];

    /**
     * Retorna as unidades que pertencem a defesa
     * @return mixed
     */
    public function unidades() {
        return $this->hasMany('CidadeDefesaUnidade', 'id_defesa', 'id')
	        ->select('unidade.*', 'cidade_defesa_unidade.quantidade')
	        ->join('unidade');
    }
}