<?php

namespace app\models;

use projectws\libs\Session;
use projectws\mvc\Model;
use projectws\libs\Cookie;

class Usuario extends Model {

    protected $update = true;

	protected $soft_delete = true;

    protected $fields = [
        'id' => ['int'],
        'nome' => ['varchar', 50],
        'senha' => ['varchar', 50],
        'email' => ['varchar', 50],
        'cidade' => ['int'],
        'nivel' => ['int'],
        'ouro' => ['int'],
        'pontos_ataque' => ['int'],
        'pontos_defesa' => ['int'],
        'ultimo_acesso' => ['timestamp'],
	    'data_removido' => ['timestamp'],
	    'data_criacao' => ['timestamp'],
	    'data_modificacao' => ['timestamp'],
	    'data_ferias' => ['timestamp']
    ];
	
	protected $hidden = ['senha', 'data_removido'];

    protected $indices = ['id'];
	
	protected $pk = 'id';

    protected $timestamp = true;

    public static function logout() {
        Session::destroy('application');
    }
    
	/**
	 * Traz os dados de honra
	 * @param Cidade $cidade
	 * @return mixed
	 */
    public static function heroisHonra($cidade = null) {
	    if($cidade) {
		    $filtro = "WHERE uh.id_usuario = :id_usuario AND uh.id_heroi = :id_heroi";
		    
		    $params = [
		    	'id_usuario' => $cidade->id_usuario,
			    'id_heroi' => $cidade->id_heroi
		    ];
		    
		    $row = true;
	    } else {
		    $filtro = "";
		    
		    $params = [];
		
		    $row = false;
	    }
    	
    	return UsuarioHeroi::query("
    	    SELECT
			  uh.id_usuario,
			  uh.id_heroi,
			  uh.honra,
			  SUM(cc.level) as level,
			  cfg.honra_fator,
			  cfg.honra_armazenamento,
			  uh.atualizacao
			FROM cidade c
			  INNER JOIN usuario_heroi uh ON uh.id_usuario = c.id_usuario
			  INNER JOIN cidade_heroi ch ON ch.id_cidade = c.id AND ch.id_heroi = uh.id_heroi
			  INNER JOIN cidade_construcao cc ON cc.id_cidade = ch.id_cidade AND cc.id_construcao = 12
			  CROSS JOIN (
			               SELECT
			                 MAX(CASE WHEN cfg.config = 'honra_fator' THEN cfg.valor END) AS honra_fator,
			                 MAX(CASE WHEN cfg.config = 'honra_armazenamento' THEN CAST(cfg.valor AS FLOAT) END) AS honra_armazenamento
			               FROM config cfg
			               WHERE cfg.config LIKE '%_fator' OR cfg.config LIKE '%_armazenamento'
			             ) cfg
         	$filtro
			GROUP BY
			  uh.id_usuario,
			  uh.id_heroi,
			  uh.honra,
			  cfg.honra_fator,
			  cfg.honra_armazenamento,
			  uh.atualizacao
    	", $params, $row);
    }

    /**
     * Usudo para fazer o login
     */
    public function logar() {
        $usuario = (Object)[
            'id' => $this->id,
            'nome' => $this->nome
        ];

        Session::set('application', $usuario);
    }

    public function cidades() {
        return $this->hasMany('Cidade', 'id_usuario', 'id');
    }
	
    public function herois() {
        return $this->belongsToMany('Heroi');
    }
}
