<?php

namespace app\models;

use projectws\mvc\Model;

class CidadeUnidade extends Model
{
    protected $fields = [
        'id_cidade',
        'id_unidade',
        'quantidade'
    ];

    protected $indices = ['id_cidade', 'id_unidade'];
	
	public static function getUnidades($id_cidade) {
		if(empty($id_cidade))
			$id_cidade = null;
		
		$query = CidadeUnidade::alias('cu')
			->select(
				'u.*',
				'cu.quantidade',
                'c.id_tipo as id_tipo_classe'
			)
			->join('unidade u', 'u.id ', '=', 'cu.id_unidade')
            ->join('classe c', 'c.id', '=', 'u.id_classe')
			->where('cu.id_cidade', "=", $id_cidade)
			->orderBy('u.id');
		
		return $query->rows();
	}
}
