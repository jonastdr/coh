<?php

namespace app\models;

use projectws\mvc\Model;

class Notificacao extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id',
        'id_usuario',
        'titulo',
        'component',
        'params',
        'id_referencia',
        'tipo',
        'datahora'
    ];

    //primary key da tabela
    protected $pk = 'id';

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id'];
    
    /**
     * Adiciona uma notificação
     * @param $id_usuario
     * @param $titulo
     * @param $component
     * @param $params
     * @param $tipo
     * @param null $id_ref
     * @return int
     */
    public static function add($id_usuario, $titulo, $component = null, $params = null, $tipo, $id_ref = null) {
        $notificacao = new static();
        $notificacao->id_usuario = $id_usuario;
        $notificacao->titulo = $titulo;
        $notificacao->component = $component;
        $notificacao->params = $params ? json_encode($params) : null;
        $notificacao->id_referencia = $id_ref;
        $notificacao->tipo = $tipo;

        return $notificacao->save();
    }
}