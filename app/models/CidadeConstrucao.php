<?php

namespace app\models;

use projectws\mvc\Model;

class CidadeConstrucao extends Model {
    protected $fields = [
        'id_cidade',
        'id_construcao',
        'level',
        'slot'
    ];

    protected $indices = ['id_cidade'];
}
