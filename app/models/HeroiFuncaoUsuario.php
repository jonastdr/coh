<?php

namespace app\models;

use projectws\mvc\Model;

class HeroiFuncaoUsuario extends Model {
    
    protected $fields = [
        'id_funcao',
        'id_usuario',
        'expiracao'
    ];

    protected $indices = ['id_funcao', 'id_usuario'];
}