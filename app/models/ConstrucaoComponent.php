<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class ConstrucaoComponent
 * @property int id_construcao
 * @property string nome
 * @property string component
 * @property int params
 * @package app\models
 */
class ConstrucaoComponent extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_construcao',
        'nome',
        'component',
	    'params'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_construcao', 'component'];

}