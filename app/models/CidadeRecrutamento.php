<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class CidadeRecrutamento
 * @property int id
 * @property string id_cidade
 * @property string id_unidade
 * @property string quantidade
 * @property string horario
 * @property string duracao
 * @property string tempo_gasto
 * @package app\models
 */
class CidadeRecrutamento extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id',
        'id_cidade',
        'id_unidade',
        'quantidade',
        'horario',
	    'duracao',
	    'tempo_gasto'
    ];

    //primary key da tabela
    protected $pk = 'id';

    //chaves estrangeiras
    protected $fk = [
    	'cidade' => 'id_cidade',
	    'unidade' => 'id_unidade'
    ];

    //indices usado como padrão em métodos where
    protected $indices = ['id'];

}