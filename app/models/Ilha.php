<?php

namespace app\models;

use projectws\libs\orm\Join;
use projectws\mvc\Model;

/**
 * Class Ilha
 * @property int id
 * @property string x
 * @property string y
 * @property string tipo
 * @property string img
 * @package app\models
 */
class Ilha extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id',
        'x',
        'y',
        'tipo',
        'img'
    ];

    //primary key da tabela
    protected $pk = 'id';

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id'];
	
	/**
	 *
	 * @return \projectws\libs\orm\Builder
	 */
	public function cidades() {
		return $this->hasMany('IlhaCidade')
			->select(
				'ilha_cidade.*',
				'ilha_cidade.id_ilha as cod_ilha',
				'c.id',
				'c.nome as cidade',
				'u.id as id_usuario',
				'u.nome as usuario',
                'a.nome as alianca',
				'coalesce(e.pontos_construcoes, 0) as pontos',
				"'7, 0, 167' as bandeira"
				)
			->leftJoin('cidade c', function (Join $j) {
				$j->on('c.cod_cidade', '=', 'ilha_cidade.cod_cidade');
				
				$j->where('c.cod_ilha', '=', 'ilha_cidade.id_ilha');
			})
			->leftJoin('usuario u', 'u.id', '=', 'c.id_usuario')
            ->leftJoin('alianca_membro am', 'am.id_usuario', '=', 'u.id')
            ->leftJoin('alianca a', 'a.id', '=', 'am.id_alianca')
			->leftJoin('estatisticas e', 'e.id_cidade', '=', 'c.id');
	}
	
}