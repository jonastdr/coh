<?php

namespace app\models;

use projectws\mvc\Model;

class Construcao extends Model
{
    protected $fields = [
        'id',
        'nome',
        'dinheiro',
        'alimento',
        'metal',
        'petroleo',
        'ouro',
        'energia',
        'populacao',
        'tempo',
        'fator',
        'max',
        'descricao',
    ];

    protected $indices = ['id'];

    /**
     * @param $id_cidade
     * @param $id_construcao
     * @return \projectws\mvc\classe
     */
    public static function getForSide($id_cidade, $id_construcao) {
        $query = Construcao::query("
            SELECT c.*, coalesce(cc.level, 0)+1 as level
            FROM construcao c
            LEFT JOIN cidade_construcao cc ON cc.id_construcao = c.id AND cc.id_cidade = :id_cidade
            WHERE c.id = :id_construcao LIMIT 1", [
                'id_cidade' => $id_cidade,
                'id_construcao' => $id_construcao
        ], true);

        return $query;
    }

    public static function getConstrucao($id_construcao, $id_cidade) {
        $query = Construcao::query("
            SELECT *, coalesce(cc.level, 0) as level
            FROM construcao c
            LEFT JOIN cidade_construcao cc ON cc.id_construcao = c.id AND cc.id_cidade = :id_cidade
            WHERE c.id = :id_construcao", [
            	'id_cidade' => $id_cidade,
            	'id_construcao' => $id_construcao
        ], true);

        return $query;
    }
	
	/**
	 * Traz os components relacionados a construção
	 * @return Model
	 */
    public function components() {
    	return $this->hasMany('ConstrucaoComponent')
		    ->orderBy('nome');
    }
	
	/**
	 * Construção Requisitos
	 * @param $id_construcao
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
    public static function construcaoRequisito($id_construcao) {
    	return ConstrucaoConstrucaoRequisito
		    ::select(
		    	'c.id',
		    	'c.nome',
			    'ccr.level'
		    )
		    ->alias('ccr')
		    ->join('construcao c', 'c.id', '=', 'ccr.id_construcao_req')
		    ->where('ccr.id_construcao', '=', $id_construcao)
		    ->rows();
    }
	
	/**
	 * Pesquisa Requisitos
	 * @param $id_construcao
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
    public static function pesquisaRequisito($id_construcao) {
    	return ConstrucaoPesquisaRequisito
		    ::select(
		    	'p.id',
		    	'p.nome',
			    'cpr.level'
		    )
		    ->alias('cpr')
		    ->join('pesquisa p', 'p.id', '=', 'cpr.id_pesquisa_req')
		    ->where('cpr.id_construcao', '=', $id_construcao)
		    ->rows();
    }
	
	/**
	 * Verifica se tem todos os requisitos de construções e pesquisas
	 * @param $id_construcao
	 * @return bool
	 */
    public static function temTodosRequisitos($id_construcao) {
    	$construcoes = Funcoes::getCidade()->construcoes;
	    $construcoesReq = Construcao::construcaoRequisito($id_construcao);
	    
    	$pesquisas = Funcoes::getCidade()->pesquisas;
	    $pesquisasReq = Construcao::pesquisaRequisito($id_construcao);
	
	    $reqCout = $construcoesReq->count() + $pesquisasReq->count();
	    $reqPass = 0;

	    foreach ($construcoesReq as $construcaoReq) {
		    foreach ($construcoes as $construcao) {
			    if($construcao->id_construcao == $construcaoReq->id && $construcao->level >= $construcaoReq->level) {
			    	$reqPass++;
			    }
		    }
	    }
	
	    foreach ($pesquisasReq as $pesquisaReq) {
		    foreach ($pesquisas as $pesquisa) {
			    if($pesquisa->id_pesquisa == $pesquisaReq->id && $pesquisa->level >= $pesquisaReq->level) {
			    	$reqPass++;
			    }
		    }
	    }
	
	    /**
	     * Indica que tem todas os requisitos
	     */
	    if($reqCout == $reqPass)
	        return true;
	    
	    return false;
    }
    
}
