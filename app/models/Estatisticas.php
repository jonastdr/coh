<?php

namespace app\models;

use projectws\mvc\Model;

class Estatisticas extends Model
{
    protected $fields = [
        'rank',
        'id_cidade',
        'id_usuario',
        'pontos_construcoes',
        'atualizacao'
    ];
	
	public static function usuario() {
		$query = Estatisticas
			::select('rank() OVER (ORDER BY sum(e.pontos_construcoes) DESC) as rank, e.id_usuario, u.nome, a.nome as alianca, sum(e.pontos_construcoes) as pontos_construcoes')
			->from('estatisticas e')
			->join('usuario u', 'u.id ', '=', 'e.id_usuario')
			->leftJoin('alianca_membro am', 'am.id_usuario ', '=', 'e.id_usuario')
			->leftJoin('alianca a', 'a.id ', '=', 'am.id_alianca')
			->groupBy('e.id_usuario, u.nome, a.nome')
			->orderBy('sum(e.pontos_construcoes) DESC');
		
		return $query->rows();
	}
	
	public static function alianca() {
		$query = Estatisticas
			::select('
	                a.nome,
	                count(e.id_usuario) as membros,
	                round(sum(e.pontos_construcoes) / count(e.id_usuario)) as pontos_membro,
	                sum(e.pontos_construcoes) as pontos')
			->from('estatisticas e')
			->join('usuario u', 'u.id ', '=', 'e.id_usuario')
			->leftJoin('alianca_membro am', 'am.id_usuario ', '=', 'e.id_usuario')
			->leftJoin('alianca a', 'a.id ', '=', 'am.id_alianca')
			->groupBy('a.id')
			->orderBy('sum(e.pontos_construcoes) DESC');
		
		return $query->rows();
    }
	
	public static function ataque() {
		$query = Estatisticas
			::select('e.id_usuario, u.nome, a.nome as alianca, max(e.pontos_ataque) as pontos_ataque')
			->from('estatisticas e')
			->join('usuario u', 'u.id ', '=', 'e.id_usuario')
			->leftJoin('alianca_membro am', 'am.id_usuario ', '=', 'e.id_usuario')
			->leftJoin('alianca a', 'a.id ', '=', 'am.id_alianca')
			->groupBy('e.id_usuario, u.nome, a.nome')
			->orderBy('max(e.pontos_ataque) DESC');
		
		return $query->rows();
	}
	
	public static function defesa() {
		$query = Estatisticas
			::select('e.id_usuario, u.nome, a.nome as alianca, max(e.pontos_defesa) as pontos_defesa')
			->from('estatisticas e')
			->join('usuario u', 'u.id ', '=', 'e.id_usuario')
			->leftJoin('alianca_membro am', 'am.id_usuario ', '=', 'e.id_usuario')
			->leftJoin('alianca a', 'a.id ', '=', 'am.id_alianca')
			->groupBy('e.id_usuario, u.nome, a.nome')
			->orderBy('max(e.pontos_defesa) DESC');
		
		return $query->rows();
	}
}
