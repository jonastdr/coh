<?php

namespace app\models;

use projectws\mvc\Model;

class AliancaForum extends Model {

    protected $fields = [
        'id',
        'id_alianca',
        'id_tabela',
        'agrupamento',
        'id_remetente',
        'assunto',
        'mensagem',
        'dataenvio'
    ];

    protected $indices = ['id', 'id_alianca', 'id_tabela'];
    
    public function getEscopo() {
        return $this
                ->select('count(*) as topicos')
                ->from('(SELECT 1 FROM alianca_forum GROUP BY agrupamento) as grupos')
                ->row();
    }
    
    public function getMembro($id_usuario) {
        return $this
                ->select('a.*, b.permissoes')
                ->from('alianca_membro a')
                ->join('alianca_nivel b', 'b.id ', '=', 'a.nivel')
                ->where('id_usuario', '=', $id_usuario)
                ->row();
    }
    
    public function getTopicos($id_alianca, $id_tabela = 0, $pag = 0, $quantidade = 10) {
        $topicos =  $this
                ->from('alianca_forum')
                ->where('id_alianca', '=', $id_alianca)
                ->where('AND id_tabela', '=', $id_tabela)
                ->limit($quantidade)
                ->offset($pag)
                ->rows();
        
        if($topicos)
            return $topicos;
        
        return [];
    }
    
    public function getIdAlianca(){
        return $this->id_alianca;
    }

}
