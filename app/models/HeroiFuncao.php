<?php

namespace app\models;

use projectws\mvc\Model;

class HeroiFuncao extends Model {

    protected $update = true;

    protected $fields = [
        'id' => ['serial'],
        'id_heroi' => ['int'],
        'nome' => ['varchar', 100],
        'descricao' => ['text'],
        'honra' => ['int'],
        'duracao' => ['int']
    ];

    protected $pk = 'id';

    protected $fk = [
        'id_heroi' => ['heroi']
    ];

    protected $indices = ['id'];
}