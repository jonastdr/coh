<?php

namespace app\models;

use projectws\mvc\Model;

class Overview extends Model {
    
    protected $fields = [
        'id',
        'slot',
        'left',
        'top'
    ];
}
