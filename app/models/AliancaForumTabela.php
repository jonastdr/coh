<?php

namespace app\models;

use projectws\mvc\Model;

class AliancaForumTabela extends Model
{
	protected $fields = [
	    'id',
	    'id_alianca',
	    'nome'
	];
}
