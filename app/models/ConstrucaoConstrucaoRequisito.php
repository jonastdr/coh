<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class ConstrucaoConstrucaoRequisito
 * @property string id_construcao
 * @property string id_construcao_req
 * @property string level
 * @package app\models
 */
class ConstrucaoConstrucaoRequisito extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_construcao',
        'id_construcao_req',
        'level'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_construcao', 'id_construcao_req'];

}