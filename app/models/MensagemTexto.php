<?php

namespace app\models;

use projectws\mvc\Model;

class MensagemTexto extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id',
        'id_mensagem',
        'id_remetente',
        'mensagem',
        'link',
        'data_envio'
    ];

    //primary key da tabela
    protected $pk = 'id';

    //chaves estrangeiras
    protected $fk = [
        'id_mensagem' => ['mensagem', 'id'],
        'id_remetente' => ['usuario', 'id']
    ];

    //indices usado como padrão em métodos where
    protected $indices = ['id'];

}