<?php

namespace app\models;

use projectws\mvc\Model;

class Spam extends Model
{
    protected $update = true;

    protected $fields = [
        'id' => ['int'],
        'spam' => ['varchar', 100]
    ];
}
