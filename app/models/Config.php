<?php

namespace app\models;

use projectws\mvc\Model;

class Config extends Model
{
    protected $fields = [
        'config',
        'valor'
    ];
}
