<?php

namespace app\models;

use projectws\mvc\Model;

class AliancaMembro extends Model
{
    protected $fields = [
       'id',
       'id_alianca',
       'id_usuario',
       'id_nivel',
       'bloqueio'
    ];
	
	protected $pk = 'id';
	
}
