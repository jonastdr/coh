<?php

namespace app\models;

use projectws\mvc\Model;

class ExpedicaoUnidade extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_expedicao',
        'id_unidade',
        'quantidade'
    ];

    //chaves estrangeiras
    protected $fk = [
        'id_expedicao' => ['expedicao' => 'id'],
        'id_unidade' => ['unidade', 'id']
    ];

    //indices usado como padrão em métodos where
    protected $indices = ['id_expedicao'];

}