<?php

namespace app\models;

use projectws\mvc\Model;

class AliancaNivel extends Model {
    
    protected $fields = [
       'id',
       'nome',
       'permissoes',
       'id_alianca'
    ];
    
    protected $pk = 'id';
    
}
