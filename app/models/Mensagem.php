<?php

namespace app\models;

use projectws\libs\Database;
use projectws\mvc\Model;

class Mensagem extends Model {

    protected $update = true;

    protected $fields = [
        'id' => ['int'],
        'id_usuario' => ['int'],
        'tipo' => ['int'],
        'assunto' => ['varchar', 100],
        'data_criacao' => ['timestamp']
    ];

    protected $indices = ['id'];
	
	protected $pk = 'id';

    protected $fk = [
        'id_usuario' => ['usuario', 'id']
    ];

    public function mensagens() {
        return $this->hasMany('MensagemTexto')
            ->select(
            	'mensagem_texto.*',
	            'usuario.nome as nome_remetente',
	            'mensagem_texto_lido.data_lido'
            )
            ->join('usuario', 'usuario.id', '=', 'mensagem_texto.id_remetente')
            ->leftJoin('mensagem_texto_lido', 'id_mensagem_texto', '=', Funcoes::getUsuario()->id)
            ->orderBy('mensagem_texto.data_envio DESC');
    }
}
