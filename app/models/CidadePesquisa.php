<?php

namespace app\models;

use projectws\mvc\Model;

class CidadePesquisa extends Model {

	protected $fields = [
		'id_cidade',
		'id_pesquisa',
		'level'
	];

}