<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class IlhaCidade
 * @property string id_ilha
 * @property string cod_cidade
 * @property string x
 * @property string y
 * @package app\models
 */
class IlhaCidade extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_ilha',
	    'cod_cidade',
        'x',
        'y'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_cidade', 'x', 'y'];

}