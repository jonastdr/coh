<?php

namespace app\models;

use projectws\libs\orm\Join;
use projectws\mvc\Model;

class Expedicao extends Model
{
    const ATACAR = 1;

    const TRANSPORTE = 2;

    const DEFENDER = 3;

	const COLONIZACAO = 4;
    
	const ESPIONAGEM = 5;
	
    protected $fields = [
        'id',
        'id_cidade_atk',
        'id_cidade_def',
        'missao',
        'duracao',
        'horario',
        'dinheiro',
        'alimento',
        'metal',
        'petroleo',
        'volta',
	    'cod_ilha',
	    'cod_cidade'
    ];

	protected $pk = 'id';
	
	protected $indices = ['id'];
	
	/**
	 * Retorna as missões em andamento do usuário logado
	 * @return \projectws\libs\orm\ModelCollection
	 */
    public static function getExpedicoes() {
        $usuario = Funcoes::getUsuario();

        $expedicao = Expedicao::select('expedicao.*, u_atk.nome as atacante, u_def.nome as defensor')
            ->join('cidade c_atk', 'c_atk.id ', '=', 'expedicao.id_cidade_atk')
            ->join('usuario u_atk', 'u_atk.id ', '=', 'c_atk.id_usuario')
            ->leftJoin('cidade c_def', function (Join $j) {
            	$j->on('c_def.cod_ilha', '=', 'expedicao.cod_ilha');
	            
	            $j->where('c_def.cod_cidade ', '=', 'expedicao.cod_cidade');
            })
            ->leftJoin('usuario u_def', 'u_def.id ', '=', 'c_def.id_usuario')
	        ->where(function ($q) use ($usuario) {
		        $q->where('c_atk.id', '=', $usuario->cidade);
		        
		        $q->orWhere(function ($q) use ($usuario) {
		        	$q->where('c_def.id', '=', $usuario->cidade);
			        $q->whereFalse('expedicao.volta');
		        });
	        })
            ->orderBy('expedicao.horario + duracao')
            ->rows();
	    
	    return $expedicao;
    }
	
	/**
	 * Retorna as missões em andamento somente do que foi enviado da cidade atual
	 * @return \projectws\libs\orm\ModelCollection
	 */
    public static function getExpedicoesEnvio() {
        $usuario = Funcoes::getUsuario();

        $expedicao = Expedicao::select('expedicao.*, u_atk.nome as atacante, u_def.nome as defensor')
            ->join('cidade c_atk', 'c_atk.id ', '=', 'expedicao.id_cidade_atk')
            ->join('usuario u_atk', 'u_atk.id ', '=', 'c_atk.id_usuario')
            ->leftJoin('cidade c_def', function (Join $j) {
            	$j->on('c_def.cod_ilha', '=', 'expedicao.cod_ilha');
	            
	            $j->where('c_def.cod_cidade ', '=', 'expedicao.cod_cidade');
            })
            ->leftJoin('usuario u_def', 'u_def.id ', '=', 'c_def.id_usuario')
	        ->where(function ($q) use ($usuario) {
		        $q->where('c_atk.id', '=', $usuario->cidade);
	        })
            ->orderBy('expedicao.horario + duracao')
            ->rows();
	    
	    return $expedicao;
    }

    public function unidades() {
        return $this->hasMany('ExpedicaoUnidade')
	        ->select('expedicao_unidade.*', 'unidade.*', 'c.id_tipo')
	        ->join('unidade')
	        ->join('classe c', 'c.id', '=', 'unidade.id_classe');
    }
}
