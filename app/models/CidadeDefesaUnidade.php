<?php

namespace app\models;

use projectws\mvc\Model;

class CidadeDefesaUnidade extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_defesa',
        'id_unidade',
        'quantidade'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_defesa', 'id_unidade'];

}