<?php

namespace app\models;

use projectws\mvc\Model;

class Relatorio extends Model {

    protected $fields = [
        'id',
        'link',
        'dados',
        'data',
        'tipo'
    ];
    
    protected $pk = 'id';

}