<?php

namespace app\models;

use projectws\mvc\Model;

class CidadeHeroi extends Model {

    protected $update = true;

    protected $fields = [
        'id_cidade',
        'id_heroi'
    ];

    protected $fk = [
        'id_cidade' => ['cidade'],
        'id_heroi' => ['heroi']
    ];

    protected $indices = ['id_cidade'];
}