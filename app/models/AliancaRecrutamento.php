<?php

namespace app\models;

use projectws\mvc\Model;

class AliancaRecrutamento extends Model
{
    protected $fields = [
       'id',
       'id_usuario',
       'id_alianca',
       'mensagem',
       'datahora'
    ];
    
    protected $pk = 'id';
}
