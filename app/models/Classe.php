<?php

namespace app\models;

use projectws\mvc\Model;

class Classe extends Model
{
    const TERRESTRE = 1;
    const AEREO = 2;
    const NAVAL = 3;
    const DEFESA = 4;

    protected $fields = [
        'id',
        'nome',
        'tipo',
        'classe'
    ];
}