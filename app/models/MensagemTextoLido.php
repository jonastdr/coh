<?php

namespace app\models;

use projectws\mvc\Model;

class MensagemTextoLido extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_mensagem_texto',
        'id_usuario',
        'data_lido'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_mensagem_texto', 'id_usuario'];

}