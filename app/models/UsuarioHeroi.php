<?php

namespace app\models;

use projectws\mvc\Model;

class UsuarioHeroi extends Model {

    protected $update = true;

    protected $fields = [
        'id_usuario' => ['int'],
        'id_heroi' => ['int'],
        'honra' => ['int'],
        'atualizacao' => ['int']
    ];

    protected $indices = ['id_usuario', 'id_heroi'];

    public function usuario() {
        return $this->hasOne('Usuario', 'id_usuario');
    }

    public function heroi() {
        return $this->hasOne('Heroi', 'id_heroi');
    }
}