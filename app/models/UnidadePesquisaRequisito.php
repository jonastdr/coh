<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class UnidadePesquisaRequisito
 * @property string id_unidade
 * @property string id_pesquisa_req
 * @property string level
 * @package app\models
 */
class UnidadePesquisaRequisito extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_unidade',
        'id_pesquisa_req',
        'level'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_unidade', 'id_pesquisa_req'];

}