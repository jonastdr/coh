<?php

namespace app\models;

use projectws\mvc\Model;

/**
 * Class Unidade
 * @property int id
 * @property string nome
 * @property string decricao
 * @property int id_classe
 * @property double dinheiro
 * @property double alimento
 * @property double metal
 * @property double petroleo
 * @property double ouro
 * @property int energia
 * @property int populacao
 * @property int tempo
 * @property int velocidade
 * @property int ataque_terrestre
 * @property int ataque_aereo
 * @property int ataque_naval
 * @property int integridade
 * @property int capacidade
 * @package app\models
 */
class Unidade extends Model {

    protected $update = true;

    protected $fields = [
        'id' => ['int'],
        'nome' => ['varchar', 50],
        'descricao' => ['text'],
        'id_classe' => ['int'],
        'dinheiro' => ['float'],
        'alimento' => ['float'],
        'metal' => ['float'],
        'petroleo' => ['float'],
        'ouro' => ['int'],
        'energia' => ['int'],
        'populacao' => ['int'],
        'tempo' => ['int'],
        'velocidade' => ['int'],
        'ataque_terrestre' => ['int'],
        'ataque_aereo' => ['int'],
        'ataque_naval' => ['int'],
        'integridade' => ['int'],
        'capacidade' => ['int']
    ];

    protected $indices = ['id'];
	
	/**
	 * Retorna com os dados necessários
	 * @param $id_unidade
	 * @return static
	 */
	public static function get($id_unidade) {
		$unidade = Unidade
			::select(
				'u.*',
				'c.nome as tipo',
				'c.classe'
			)
			->alias('u')
			->join('classe c', 'c.id', '=', 'u.id_classe')
			->findFirst($id_unidade);
		
		return $unidade;
	}
	
	/**
	 * Construção Requisitos
	 * @param $id_unidade
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
	public static function construcaoRequisito($id_unidade) {
		return UnidadeConstrucaoRequisito
			::select(
				'c.id',
				'c.nome',
				'ucr.level'
			)
			->alias('ucr')
			->join('construcao c', 'c.id', '=', 'ucr.id_construcao_req')
			->where('ucr.id_unidade', '=', $id_unidade)
			->rows();
	}
	
	/**
	 * Pesquisa Requisitos
	 * @param $id_unidade
	 * @return null|\projectws\libs\orm\ModelCollection
	 */
	public static function pesquisaRequisito($id_unidade) {
		return UnidadePesquisaRequisito
			::select(
				'p.id',
				'p.nome',
				'upr.level'
			)
			->alias('upr')
			->join('pesquisa p', 'p.id', '=', 'upr.id_pesquisa_req')
			->where('upr.id_unidade', '=', $id_unidade)
			->rows();
	}
	
	/**
	 * Verifica se tem todos os requisitos de construções e pesquisas
	 * @param $id_unidade
	 * @return bool
	 */
	public static function temTodosRequisitos($id_unidade) {
		$construcoes = Funcoes::getCidade()->construcoes;
		$construcoesReq = Unidade::construcaoRequisito($id_unidade);
		
		$pesquisas = Funcoes::getCidade()->pesquisas;
		$pesquisasReq = Unidade::pesquisaRequisito($id_unidade);
		
		$reqCout = $construcoesReq->count() + $pesquisasReq->count();
		$reqPass = 0;
		
		foreach ($construcoesReq as $construcaoReq) {
			foreach ($construcoes as $construcao) {
				if($construcao->id_construcao == $construcaoReq->id && $construcao->level >= $construcaoReq->level) {
					$reqPass++;
				}
			}
		}
		
		foreach ($pesquisasReq as $pesquisaReq) {
			foreach ($pesquisas as $pesquisa) {
				if($pesquisa->id_pesquisa == $pesquisaReq->id && $pesquisa->level >= $pesquisaReq->level) {
					$reqPass++;
				}
			}
		}
		
		/**
		 * Indica que tem todas os requisitos
		 */
		if($reqCout == $reqPass)
			return true;
		
		return false;
	}
}
