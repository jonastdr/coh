<?php

namespace app\models;

use projectws\mvc\Model;

class MensagemTextoUsuario extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
        'id_mensagem',
        'id_usuario'
    ];

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id_mensagem', 'id_usuario'];

}