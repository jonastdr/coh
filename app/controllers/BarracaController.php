<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadeRecrutamento;
use app\models\Funcoes;
use app\models\Unidade;
use projectws\libs\orm\Join;
use projectws\mvc\Controller;

class BarracaController extends Controller {

    public function index() {
        return $this->view->render('barraca/index.edge');
    }
	
	/**
	 * Retorna os dados da tela
	 * @return array
	 */
    public function getAll($id_tipo = 0) {
    	$unidades = Unidade
		    ::select(
		    	'u.*',
			    'coalesce(cu.quantidade, 0) as quantidade'
		    )
		    ->alias('u')
		    ->leftJoin('cidade_unidade cu', function (Join $j) {
		    	$cidade = Funcoes::getCidade();
		    	
		    	$j->on('cu.id_unidade', '=', 'u.id');
			    
			    $j->where('cu.id_cidade', '=', $cidade->id);
		    })
		    ->join('classe c', 'c.id', '=', 'u.id_classe')
		    ->join('classe_tipo ct', 'ct.id', '=', 'c.id_tipo')
		    ->where('ct.id', '=', $id_tipo)
		    ->orderBy('u.id')
		    ->rows();
	
	    $unidade = null;
	    
	    if($unidades->count() > 0) {
		    $id_unidade = $unidades[0]->id;
		
		    $unidade = Unidade::get($id_unidade)->toArray();
		
		    $unidade['construcao_requisitos'] = Unidade::construcaoRequisito($id_unidade)->toArray();
		    $unidade['pesquisa_requisitos'] = unidade::pesquisaRequisito($id_unidade)->toArray();
	    } else {
	    	$id_unidade = 0;
	    }
	    
    	return [
    		'success' => true,
		    'recrutados' => $this->recrutamento()['recrutados'],
		    'unidades' => $unidades->toArray(),
		    'unidade' => $unidade,
		    'temRequisito' => Unidade::temTodosRequisitos($id_unidade)
	    ];
    }
	
	/**
	 * Retorna os dados de uma unidade
	 * @param int $id_unidade
	 * @return array
	 */
    public function getUnidade($id_unidade = 0) {
	    $unidade = Unidade::get($id_unidade)->toArray();
	
	    $unidade['construcao_requisitos'] = Unidade::construcaoRequisito($id_unidade)->toArray();
	    $unidade['pesquisa_requisitos'] = unidade::pesquisaRequisito($id_unidade)->toArray();

        return [
        	'success' => true,
	        'unidade' => $unidade,
	        'temRequisito' => Unidade::temTodosRequisitos($id_unidade)
        ];
    }

    public function recrutamento() {
        $cidade = Funcoes::getCidade();

        $unidades = CidadeRecrutamento::where('id_cidade', '=', $cidade->id)->rows()->toArray();
	
	    foreach ($unidades as &$unidade) {
		    $unidade['restante'] = ($unidade['horario'] + $unidade['duracao']) - time();
	    }
	    
	    return [
	    	'success' => true,
		    'recrutados' => $unidades
	    ];
    }
	
	/**
	 * Recruta uma unidade na cidade
	 * @return array
	 */
    public function recrutar()
    {
	    $id_unidade = \Request::getPost("id");
	    $quantidade = \Request::getPost("quantidade");
	
	    $usuario = Funcoes::getUsuario();
	
	    $cidade = Cidade::findFirst($usuario->cidade);
	
	    $unidade = Unidade::get($id_unidade);
	
	    if (!is_numeric($quantidade)) {
		    return [
			    'success' => false,
			    'msg' => 'Quantidade inválida.'
		    ];
	    }
	
	    /**
	     * Verifica se tem todos os requisitos para a construção
	     */
	    if (Unidade::temTodosRequisitos($id_unidade) == false) {
		    return [
			    'success' => false,
			    'msg' => 'Verifique os requisitos necessários.'
		    ];
	    }
	    
	    /**
	     * Deve multiplicar pela quantidade que está querendo
	     */
	    $unidade->dinheiro = $unidade->dinheiro * $quantidade;
	    $unidade->alimento = $unidade->alimento * $quantidade;
	    $unidade->metal = $unidade->metal * $quantidade;
	    $unidade->petroleo = $unidade->petroleo * $quantidade;
	
	    /**
	     * Se existir os recursos necessários faz o recrutamento
	     */
	    if (Funcoes::tenhoRecursos($unidade)) {
		    $cidade->dinheiro = $cidade->dinheiro - $unidade->dinheiro;
		    $cidade->alimento = $cidade->alimento - $unidade->alimento;
		    $cidade->metal = $cidade->metal - $unidade->metal;
		    $cidade->petroleo = $cidade->petroleo - $unidade->petroleo;
		
		    $usuario->ouro = $usuario->ouro - $unidade->ouro;
		
		    $cidade->energia = $cidade->energia - $unidade->energia;
		    $cidade->populacao = $cidade->populacao - $unidade->populacao;
		    
		    $cidade->save();
		
		    $recrutamento = new CidadeRecrutamento();
		    $recrutamento->id_cidade = $cidade->id;
		    $recrutamento->id_unidade = $unidade->id;
		    $recrutamento->quantidade = $quantidade;
		    $recrutamento->horario = time();
		    $recrutamento->duracao = $unidade->tempo * $quantidade;
		    $recrutamento->tempo_gasto = $unidade->tempo;
		
		    if ($recrutamento->save()) {
			    return [
				    'success' => true,
				    'msg' => $quantidade . ' unidade(s) à recrutar',
				    'recrutados' => $this->recrutamento()['recrutados']
			    ];
		    }
		
		    return [
			    'success' => false,
			    'msg' => 'Não foi possível fazer o recrutamento.'
		    ];
	    }
	
	    return [
		    'success' => false,
		    'msg' => 'Você não tem recursos o suficiente.'
	    ];
    }

}