<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadeConstrucao;
use app\models\Construcao;
use app\models\Funcoes;
use projectws\mvc\Controller;

class ConstrucoesController extends Controller {

    public function IndexAction($id, $modo = '') {
        $funcoes = new Funcoes();
        $construcoes = new Construcao();
        $CidadeConstrucao = new CidadeConstrucao();

        $usuario = $funcoes->getUsuario();

        $construcaoAtual = $construcoes->findFirst("id=$id");

        $CidadeConstrucaoAtual = $CidadeConstrucao->findFirst("id_cidade=" . $usuario->cidade . " AND id_construcao=$id");

        if ($construcaoAtual) {
            $this->view->id = $id;

            $this->view->descricao = $construcaoAtual->descricao;

            if ($CidadeConstrucaoAtual) {
                $level = $CidadeConstrucaoAtual->level;
            } else {
                $level = 0;
            }

            //Faz calculo de todo o custo da pesquisa
            $construcaoAtual->dinheiro = $funcoes->CalculoFator($construcaoAtual->dinheiro, $level, $construcaoAtual->fator, $construcaoAtual->max);
            $construcaoAtual->alimento = $funcoes->CalculoFator($construcaoAtual->alimento, $level, $construcaoAtual->fator, $construcaoAtual->max);
            $construcaoAtual->metal = $funcoes->CalculoFator($construcaoAtual->metal, $level, $construcaoAtual->fator, $construcaoAtual->max);
            $construcaoAtual->petroleo = $funcoes->CalculoFator($construcaoAtual->petroleo, $level, $construcaoAtual->fator, $construcaoAtual->max);
            $construcaoAtual->ouro = $funcoes->CalculoFator($construcaoAtual->ouro, $level, $construcaoAtual->fator, $construcaoAtual->max);
            $construcaoAtual->energia = $funcoes->CalculoFator($construcaoAtual->energia, $level, $construcaoAtual->fator, $construcaoAtual->max);
            $construcaoAtual->populacao = $funcoes->CalculoFator($construcaoAtual->populacao, $level, $construcaoAtual->fator, $construcaoAtual->max);

            $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);

            //Calculo do tempo para cada level
            $CalcTempo = $funcoes->CalculoTempo($construcaoAtual->tempo, $level, $construcaoAtual->fator, $construcaoAtual->max);

            $construcao = $CidadeAtual->construcao;

            $this->view->construindo = $construcao;

            if ($construcao != 0) {
                $construcao = explode(',', $construcao);
            }

            if ($funcoes->tenhoRecursos($construcaoAtual) && $CidadeConstrucaoAtual && $construcao == 0) {
                $permitido = 1;
            } elseif ($CidadeAtual->construcao != 0 && $construcao[1] == $id) {
                $permitido = 2;
            } else {
                $permitido = 0;
            }

            if ($modo == 'melhorar' && $permitido == 1) {
                $this->Melhoria($id, $CalcTempo);
                $permitido = 2;
                $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);
            } elseif ($modo == 'cancelar' && $permitido == 2) {
                $this->CancelarMelhoria($id, $CalcTempo);
                $permitido = 1;
                $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);
            } elseif ($modo == 'destruir' && $permitido == 1) {
                $this->Destruir($id, $CalcTempo);
                $permitido = 2;
                $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);
            }

            if (isset($construcao[1]) && $construcao[1] == $id) {
                $this->view->tempoGerado = $construcao[0] - time();
            } else {
                $this->view->tempoGerado = 0;
            }

            $this->view->nome = $construcaoAtual->nome;

            $this->view->level = $level;

            $this->view->max = $construcaoAtual->max;

            $this->view->permitido = $permitido;

            if ($CalcTempo >= 3600 * 24) {
                $dias = round($CalcTempo / 3600, 0);

                $this->view->tempo = $dias . 'd ' . gmdate("H:i:s", $CalcTempo);
            } else {
                $this->view->tempo = gmdate("H:i:s", $CalcTempo);
            }

            $this->view->construcao = $construcaoAtual;
        } else {
            $this->view->disable();

            echo 'Está pagina não existe';
        }
    }

    private function CancelarMelhoria($id, $tempo) {
        $funcoes = new Funcoes();
        $usuario = $funcoes->getUsuario();

        //Atualiza o tempo para a melhoria
        $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);

        $construcao = explode(',', $CidadeAtual->construcao);

        if ($construcao[2] == 0) {
            $CidadeAtual->dinheiro = $CidadeAtual->dinheiro - ($this->recurso[0] / 2);
            $CidadeAtual->alimento = $CidadeAtual->alimento - ($this->recurso[1] / 2);
            $CidadeAtual->metal = $CidadeAtual->metal - ($this->recurso[2] / 2);
            $CidadeAtual->petroleo = $CidadeAtual->petroleo - ($this->recurso[3] / 2);
            $usuario->ouro = $usuario->ouro - ($this->recurso[4] / 2);
            $CidadeAtual->energia = $CidadeAtual->energia - ($this->recurso[5] / 2);
            $CidadeAtual->populacao = $CidadeAtual->populacao - ($this->recurso[6] / 2);
        } else {
            $CidadeAtual->dinheiro = $CidadeAtual->dinheiro + $this->recurso[0];
            $CidadeAtual->alimento = $CidadeAtual->alimento + $this->recurso[1];
            $CidadeAtual->metal = $CidadeAtual->metal + $this->recurso[2];
            $CidadeAtual->petroleo = $CidadeAtual->petroleo + $this->recurso[3];
            $usuario->ouro = $usuario->ouro + $this->recurso[4];
            $CidadeAtual->energia = $CidadeAtual->energia + $this->recurso[5];
            $CidadeAtual->populacao = $CidadeAtual->populacao + $this->recurso[6];
        }

        $CidadeAtual->construcao = 0;
        $CidadeAtual->save();
    }

    private function Destruir($id, $tempo) {
        $funcoes = new Funcoes();
        $usuario = $funcoes->getUsuario();

        //Atualiza o tempo para a melhoria
        $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);

        if ($funcoes->tenhoRecursos($this->recurso)) {
            $CidadeAtual->dinheiro = $CidadeAtual->dinheiro + ($this->recurso[0] / 2);
            $CidadeAtual->alimento = $CidadeAtual->alimento + ($this->recurso[1] / 2);
            $CidadeAtual->metal = $CidadeAtual->metal + ($this->recurso[2] / 2);
            $CidadeAtual->petroleo = $CidadeAtual->petroleo + ($this->recurso[3] / 2);
            $usuario->ouro = $usuario->ouro + ($this->recurso[4] / 2);
            $CidadeAtual->energia = $CidadeAtual->energia + ($this->recurso[5] / 2);
            $CidadeAtual->populacao = $CidadeAtual->populacao + ($this->recurso[6] / 2);

            $CidadeAtual->construcao = time() + $tempo . ',' . $id . ',' . 0;
            $CidadeAtual->save();
        }
    }

    private function Melhoria($id, $tempo) {
        $funcoes = new Funcoes();
        $usuario = $funcoes->getUsuario();

        //Atualiza o tempo para a melhoria
        $CidadeAtual = Cidade::findFirst("id=" . $usuario->cidade);

        if ($funcoes->tenhoRecursos($this->recurso)) {

            $CidadeAtual->dinheiro = $CidadeAtual->dinheiro - $this->recurso[0];
            $CidadeAtual->alimento = $CidadeAtual->alimento - $this->recurso[1];
            $CidadeAtual->metal = $CidadeAtual->metal - $this->recurso[2];
            $CidadeAtual->petroleo = $CidadeAtual->petroleo - $this->recurso[3];
            $usuario->ouro = $usuario->ouro - $this->recurso[4];
            $CidadeAtual->energia = $CidadeAtual->energia - $this->recurso[5];
            $CidadeAtual->populacao = $CidadeAtual->populacao - $this->recurso[6];

            $CidadeAtual->construcao = time() + $tempo . ',' . $id . ',' . 1;
            //print_r($CidadeAtual);
            $CidadeAtual->save();
        }
    }

}
