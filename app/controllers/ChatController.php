<?php

namespace app\controllers;

use app\models\Funcoes;
use projectws\mvc\Controller;

class ChatController extends Controller {

    public function IndexAction() {
        $funcoes = new Funcoes();

        $usuario = $funcoes->getUsuario()->nome;

        $nomearquivo = 'global-' . date('j-n-Y', time()) . '.txt';

        $arquivo = fopen('chat/' . $nomearquivo, 'a+');

        $msg_input = htmlentities(\Request::getPost('chat_input'));

        $msg_input =$this->estilo($msg_input);

        if ($arquivo && $msg_input != '') {
            $hora = date("H:i:s", time());

            $troll = array('^%|%', '^%/%');

            $mensagem = str_ireplace($troll, '', $msg_input);

            $mensagem = $funcoes->msgChat($mensagem);
            $escreve = $hora . '^%|%' . $usuario . '^%|%' . $mensagem . '^%|%' . time() . '^%/%';

            fwrite($arquivo, $escreve);

            fclose($arquivo);
        }
    }

    /**
     * @param $texto Ex: \i:texto\i = italico
     * @return mixed
     */
    private function estilo($texto) {
        $texto = preg_replace("/^\\\(.*?):(.*?)$/", "<$1>$2</$1>", $texto);

        return $texto;
    }
}
