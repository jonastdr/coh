<?php

namespace app\controllers;

use projectws\mvc\Controller;

class BatalhaController extends Controller {

    private $cidade;

    private function ListagemConstrucoes($overview = false) {
        $Construcoes = Construcoes::find();
        $CidadesConstrucoes = CidadesConstrucoes::find("id_cidade=" . $this->cidade->id);

        $listaSidebar = array();

        if ($overview) {
            for ($i = 0; $i <= 99; $i++) {
                $listaSidebar[$i] = '';
            }
        }

        foreach ($CidadesConstrucoes as $id => $coluna) {
            if ($overview) {
                $construcaoNome = Construcoes::findFirst("id=" . $coluna->construcao)->nome;

                $listaSidebar[$coluna->slot] = '<div class="ov_item" style="background-image: url(public/imagens/construcoes/' . $coluna->construcao . '.png);" onclick="abrirJanela(\'' . ($construcaoNome != null ? $construcaoNome : 'Construção Não Existe') . '\',\'construcoes?id=' . $coluna->construcao . '\');"></div>';
            } else {
                $itens[$id] = $coluna->construcao;
            }
        }

        if (!$overview) {
            foreach ($Construcoes as $id => $coluna) {

                if (!in_array($coluna->id, $itens)) {
                    $listaSidebar[$coluna->id][0] = $coluna->id;

                    $listaSidebar[$coluna->id][1] = ($coluna->nome != null ? $coluna->nome : 'Construção');
                }
            }
        }
        return $listaSidebar;
    }

    private function Arquivo() {
        $nomearquivo = $this->cidade->ov_layout . '.map';

        $arquivo = fopen('vista_geral/' . $nomearquivo, 'a+');

        $retorno = '';
        while (!feof($arquivo)) {
            $retorno .= fgets($arquivo);
        }

        fclose($arquivo);

        return $retorno;
    }

    public function IndexAction() {
        $usuario = Funcoes::getUsuario();

        $this->cidade = Cidades::findFirst("id=" . $usuario->cidade . "");

        $this->view->tabela = $this->Arquivo();

        $this->view->list = $this->ListagemConstrucoes();
    }

}
