<?php

namespace app\controllers;

use app\models\Alianca;
use app\models\Funcoes;
use app\models\Usuario;
use projectws\libs\bridges\DB;
use projectws\mvc\Controller;

class AjaxController extends Controller {
	
	/**
	 * Busca por usuários pelo nome
	 * @param string $nome
	 * @return array
	 */
    public function listarUsuarios($nome = '') {
    	$usuarioLogado = Funcoes::getUsuario();
    	
        $usuarios = Usuario
	        ::where('nome', 'ilike', "%$nome%")
	        ->where('id', '<>', $usuarioLogado->id)
	        ->limit(5)->rows();

        $nomes = [];

        foreach ($usuarios as $usuario) {
            $nomes[] = [
            	'id' => $usuario->id,
	            'label' => $usuario->nome
            ];
        }

        return [
        	'success' => true,
	        'usuarios' => $nomes
        ];
    }
    
    /**
     * Lista as alianças para um busca
     * @param $nome
     * @return array
     */
    public function listarAliancas($nome) {
        $query =
            DB::from(
                Alianca
                    ::select('
                    rank() OVER (ORDER BY sum(e.pontos_construcoes) ASC) as rank,
                    a.id,
                    a.nome,
                    count(am.id_usuario) as membros,
                    COALESCE(round(SUM(e.pontos_construcoes) / COUNT(e.id_usuario)), 0) as pontos_membro,
                    COALESCE(SUM(e.pontos_construcoes), 0) as pontos'
                    )
                    ->from('alianca')
                    ->alias('a')
                    ->leftJoin('alianca_membro am', 'am.id_alianca', '=', 'a.id')
                    ->leftJoin('estatisticas e', 'e.id_usuario', '=', 'am.id_usuario')
                    ->groupBy('a.id')
            )
                ->where('nome', 'ilike', "%$nome%")
                ->orderBy('pontos DESC')
                ->rows();

        return [
            'success' => true,
            'aliancas' => $query->toArray()
        ];
    }

}
