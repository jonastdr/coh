<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadeDefesa;
use app\models\Spam;
use app\models\Usuario;
use app\models\UsuarioHeroi;
use projectws\libs\bridges\DB;
use projectws\libs\Exception;
use projectws\libs\orm\Join;
use projectws\mvc\Controller;

class TesteController extends Controller {

    public function indexAction($teste = '') {
        //echo encrypt(12345);

        $this->view->teste = 123;
        $this->view->teste2 = $teste;

        //EXCLUSÃO LÓGICA
        //Usuario::findFirst(7)->trash();
        //Usuario::findFirst(7)->restore();

        //RAW
        /*$users = Usuario::join(Cidade::raw(), 'cidade.id_usuario', '=', 'usuario.id')
            ->whereIn('usuario.id', Usuario::select('id')->where('nome', 'like', '%%'))
            ->rows();

        pd($users);*/

        /*pd(Usuario::find()->toJson());

        DB::saveOrFail(function ($fail) {
            $user = new Spam();
            $user->spam = 'teste';
            $user->save();

            if(1==2)
                $fail->addMessage('Falhou');

            if(1==2)
                $fail->addMessage('Falho de novo');

        }, function () {
            echo 'deu certo';

            pd(Spam::find()->last());
        }, function ($e) {
            pd($e);
        });*/

        //HasOne
        $obj = UsuarioHeroi::findFirst(1);
        pd($obj->heroi);

        //HasMany
        $obj = Usuario::findFirst(7);
        pd($obj->cidades);

        //BelongsTo
        $obj = Cidade::findFirst(1);
        pd($obj->usuario);

        //BelongsToMany
        $obj = Usuario::findFirst(7);
        pd($obj->herois);

        //return view('teste/index.edge', ['teste2' => $this->view->teste2]);
    }

    public function teste() {
        Usuario::select('cidade.*')->join('cidade', function (Join $j) {
        	$j->on('cidade.id', '=', 'usuario.cidade');
	        $j->where('cidade.id', '=', 1);
	        $j->where('cidade.id', '=', 2);
        })->preview();
    }

}