<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\Usuario;
use projectws\libs\Encrypt;
use projectws\mvc\Controller;
use projectws\libs\Validator;
use projectws\libs\Cookie;

class IndexController extends Controller {

    public function Logout() {
        Usuario::logout();

        return [
        	'success' => true,
	        'msg' => 'Até breve.',
	        'redirect' => url()
        ];
    }

    public function index($middlewareValid = null) {
	    $this->view->errorsLogin = [];
	    $this->view->errorsRegister = [];
    	
	    if($middlewareValid) {
		    $this->view->errorsRegister = $middlewareValid;
	    }
	    
        if (\Request::getPost()) {
            $validacao = Validator::run([
                'rules' => [
                    'nome' => 'required',
                    'senha' => 'required'
                ],
                'messages' => [
                    'nome.required' => 'Nome é obrigatório.',
                    'senha.required' => 'Senha é obrigatório.',
                ]
            ]);

            if($validacao->fail()) {
                $this->view->errorsLogin = $validacao->showMessagesArr();
            } else {
                $usuario = Usuario::findFirst([
                    "nome" => \Request::getPost("nome"),
                    "senha" => Encrypt::make(\Request::getPost("senha"))
                ]);

                if ($usuario->count() == 1) {
                    $usuario->logar();

                    \Response::redirect()->route('game');
                } else {
                	$this->view->errorsLogin = [
                		'Nome e/ou senha inválido.'
	                ];
                }
            }
        }

        return $this->view->render('login.edge');
    }

}
