<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\Funcoes;
use app\models\Usuario;
use projectws\libs\Cookie;
use projectws\libs\Encrypt;
use projectws\libs\Request;
use projectws\libs\Validator;
use projectws\mvc\Controller;

class UsuarioController extends Controller {
	
	/**
	 * Cria a view do usuário
	 * @return null|string
	 */
	public function index() {
		return $this->view->render('usuario/index.edge');
	}
	
	/**
	 * Retorna os dados necessários para a tela
	 * @param int $id_usuario
	 * @return array
	 */
    public function getAll($id_usuario = 0) {
        $usuarioLogado = Funcoes::getUsuario();

        $usuario = Usuario
	        ::select(
	        	'u.id',
		        'u.nome',
	            'u.email',
		        'a.nome as alianca',
		        'e.rank',
		        'SUM(e.pontos_construcoes) as pontos'
	        )
            ->alias('u')
            ->leftJoin('estatisticas e', 'e.id_usuario', '=', 'u.id')
            ->leftJoin('alianca_membro am', 'am.id_usuario', '=', 'u.id')
            ->leftJoin('alianca a', 'a.id', '=', 'am.id_alianca')
            ->where('u.id', '=', $id_usuario)
            ->groupBy(
	            'u.id',
	            'u.nome',
	            'u.email',
	            'a.nome',
	            'e.rank'
            )
            ->row();

	    $cidades = Cidade
		    ::select(
		    	'c.*',
			    'COALESCE(e.pontos_construcoes, 0) as pontos'
		    )
		    ->alias('c')
		    ->leftJoin('estatisticas e', 'e.id_cidade', '=', 'c.id')
		    ->where('c.id_usuario', '=', $id_usuario)
		    ->rows();
	    
        return [
        	'success' => true,
	        'usuario' => $usuario->toArray(),
	        'cidades' => $cidades->toArray(),
	        'usuarioLogado' => $usuarioLogado->toArray()
        ];
    }
	
	/**
	 * Altera os dados do usuário
	 * @return array
	 */
	public function alterar() {
		$usuario = Funcoes::getUsuario();
		
		$senha = Encrypt::make(Request::getPost('senha'));
		
		$validSenha = Usuario
			::where('senha', '=', $senha)
			->where('id', '=', $usuario->id)
			->row()
			->count();
		
		/**
		 * Verifica se a senha é valida
		 */
		if(!$validSenha) {
			return [
				'success' => false,
				'msg' => 'A senha não confere com a antiga.'
			];
		}
		
		$usuario->nome = Request::getPost('nome');
		$usuario->email = Request::getPost('email');
		
		$validacao = Validator::run([
			'rules' => [
				'nome' => 'required',
				'email' => 'required|email'
			],
			'messages' => [
				'nome.required' => 'O campo %s é obrigatório.',
				'email.email' => 'Deve informar um e-mail válido.'
			]
		]);
		
		if($validacao->fail()) {
			return [
				'success' => false,
				'msg' => $validacao->showMessages()
			];
		}
        
        $usuarioExiste = Usuario
            ::where('nome', '=', $usuario->nome)
            ->where('id', '<>', $usuario->id)
            ->row()
            ->count();
		
		if($usuarioExiste > 0) {
		    return [
		        'success' => false,
                'msg' => 'Nome já cadastrado por outro usuário.'
            ];
        }
        
        $emailExiste = Usuario
            ::where('email', '=', $usuario->email)
            ->where('id', '<>', $usuario->id)
            ->row()
            ->count();
		
		if($emailExiste > 0) {
		    return [
		        'success' => false,
                'msg' => 'Email já cadastrado.'
            ];
        }
		
		if($usuario->save()) {
            Cookie::destroy('application');
		    
			return [
				'success' => true,
				'msg' => 'Dados alteradas.'
			];
		}
		
		return [
			'success' => false,
			'msg' => 'A senha não confere com a antiga.'
		];
	}
	
	/**
	 * Altera a senha do usuário
	 * @return array
	 */
    public function alterarSenha() {
    	$usuario = Funcoes::getUsuario();
	    
	    $senhaAnterior = Encrypt::make(Request::getPost('senha_anterior'));
	    
	    $valid = Usuario
	        ::where('id', '=', $usuario->id)
		    ->where('senha', '=', $senhaAnterior)
	        ->row()
	        ->count();
	
	    /**
	     * Se a senha conferir altera a senha
	     */
	    if($valid) {
		    $usuario->senha = Encrypt::make(Request::getPost('senha'));
		
		    if($usuario->save()) {
                Cookie::destroy('application');
		        
		    	return [
		    		'success' => true,
				    'msg' => 'Senha alterada.'
			    ];
		    }
	    }
	    
	    return [
	    	'success' => false,
		    'msg' => 'A senha não confere com a antiga.'
	    ];
	}
	
	/**
	 * Inativa um usuário
	 * @return array
	 */
	public function abandonar() {
		$usuario = Funcoes::getUsuario();
		
		if($usuario->count()) {
			if($usuario->trash()) {
				Cookie::destroy('application');
				
				return [
					'success' => true,
					'msg' => 'Usuário desativado.',
					'redirect' => url()
				];
			}
		}
		
		return [
			'success' => false,
			'msg' => 'Não foi possível desativar usuário.'
		];
	}
	
	/**
	 * Ativa o modo de férias para o jogador logado
	 * @return array
	 */
	public function ferias() {
		$usuario = Funcoes::getUsuario();
		
		if($usuario->count()) {
			$usuario->data_ferias = date('Y-m-d H:i:s');
			
			if($usuario->save()) {
				Cookie::destroy('application');
				
				return [
					'success' => true,
					'msg' => 'Usuário desativado.',
					'redirect' => url()
				];
			}
		}
		
		return [
			'success' => false,
			'msg' => 'Não foi possível desativar usuário.'
		];
	}

}
