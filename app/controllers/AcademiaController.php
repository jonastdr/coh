<?php

namespace app\controllers;

use app\models\CidadeHeroi;
use app\models\Funcoes;
use app\models\Heroi;
use app\models\HeroiFuncao;
use app\models\HeroiFuncaoUsuario;
use app\models\UsuarioHeroi;
use projectws\mvc\Controller;

class AcademiaController extends Controller {

    public function index() {
        return $this->view->render('academia/index.edge');
    }
	
	/**
	 * Traz os dados de um heroi
	 * @param int $id_heroi
	 * @return array
	 */
    public function getHeroi($id_heroi = 0) {
        $heroi = Heroi::findFirst($id_heroi);
    
        $funcoes = $heroi->funcoes;
        
	    if($heroi->count()) {
	    	return [
	    		'success' => true,
			    'heroi' => $heroi->toArray(),
                'funcoes' => $funcoes->toArray()
		    ];
	    }
	    
        return [
        	'success' => false,
	        'msg' => 'Função não existente.'
        ];
    }
	
	/**
	 * Traz os dados de uma função
	 * @param int $id_funcao
	 * @return array
	 */
    public function getFuncao($id_funcao = 0) {
        $funcao = HeroiFuncao
	        ::select(
	        	'hf.*',
		        'hfu.expiracao - EXTRACT(EPOCH FROM NOW())::INT AS expiracao'
	        )
	        ->alias('hf')
	        ->leftJoin('heroi_funcao_usuario hfu', 'hfu.id_funcao', '=', 'hf.id')
	        ->findFirst($id_funcao);

	    if($funcao->count()) {
	    	return [
	    		'success' => true,
			    'funcao' => $funcao->toArray()
		    ];
	    }
	    
        return [
        	'success' => false,
	        'msg' => 'Função não existente.'
        ];
    }
	
	/**
	 * Ativa uma função
	 * @return array
	 */
    public function ativarFuncao() {
        $id_funcao = \Request::getPost('id');

        $usuario = Funcoes::getUsuario();

        if(is_numeric($id_funcao)) {
            $funcao = HeroiFuncao::findFirst($id_funcao);
            
            $usuarioHeroi = UsuarioHeroi::findFirst([
                'id_usuario' => $usuario->id,
                'id_heroi' => $funcao->id_heroi
            ]);

            if($funcao->count() == 0 OR $usuarioHeroi->count() == 0) {
                return [
                	'success' => false,
	                'msg' => 'Função não disponível.'
                ];
            }/*
            
            $funcaoNaCidade = CidadeHeroi
                ::alias('ch')
                ->join('heroi_funcao hf', 'hf.id_heroi', '=', 'ch.id_heroi')
                ->where('hf.id', '=', $funcao->id)
                ->where('ch.id_cidade', '=', $usuario->cidade)
                ->row();
            
            if($funcaoNaCidade->count() == 0) {
                return [
                    'success' => false,
                    'msg' => 'Não disponível nesta cidade.'
                ];
            }*/

            //usado para verificação
            $heroiFuncaoUsuario = HeroiFuncaoUsuario::findFirst([
                'id_funcao' => $id_funcao,
                'id_usuario' => $usuario->id
            ]);

            if($heroiFuncaoUsuario->count() > 0) {
                return [
                	'success' => 1,
	                'msg' => $funcao->nome . ' já está ativo.'
                ];
            }

            if($usuarioHeroi->honra < $funcao->honra) {
                return [
                	'success' => false,
	                'msg' => 'Você não tem recursos o suficiente.'
                ];
            }

            //Atualizacao da função do heroi para o usuario
            $funcaoUsuario = new HeroiFuncaoUsuario();

            $funcaoUsuario->id_funcao = $id_funcao;
            $funcaoUsuario->id_usuario = $usuario->id;
            $funcaoUsuario->expiracao = time() + $funcao->duracao;
            $funcaoUsuario->save();

            //Atualização da honra do usuario
            $usuarioHeroi->honra = $usuarioHeroi->honra - $funcao->honra;
            $usuarioHeroi->save();

            $mensagem = $funcao->nome . ' ativado(a) até ' . timesToView(time() + $funcao->duracao, true) . '!';

            $getAll = $this->getAll();
	        
	        $getAll['msg'] = $mensagem;
	        
	        return $getAll;
        }

        return [
        	'success' => false,
	        'msg' => 'Não foi possível ativar.'
        ];

        return $id_funcao;
    }
	
	/**
	 * Retorna os dados principais para a tela
	 * @return array
	 */
    public function getAll() {
    	$usuario = Funcoes::getUsuario();
    	
	    $herois = Heroi::find();
    	
	    $heroi = $herois[0];
	    
	    $funcoes = $heroi->funcoes;
	    
	    $funcao = $funcoes[0];
	    
        $heroiCidade = Heroi
            ::alias('h')
            ->join('cidade_heroi ch', 'ch.id_heroi', '=', 'h.id')
            ->where('ch.id_cidade', '=', $usuario->cidade)
            ->row();
        
    	return [
    		'success' => true,
		    'herois' => $herois->toArray(),
		    'heroi' => $heroi->toArray(),
		    'funcoes' => $funcoes->toArray(),
		    'funcao' => $funcao->toArray(),
		    'heroi_cidade' => Heroi::getHeroiCidade($usuario->cidade)->toArray(),
            'heroi_cidade_desc' => $heroiCidade->nome
	    ];
    }
	
	/**
	 * Altera o heroi de uma cidade
	 * @return array
	 */
    public function ativarHeroi() {
        $id_heroi = \Request::getPost('id');

        $heroi = Heroi::findFirst($id_heroi);

        if($heroi->count() == 0) {
            return [
            	'success' => false,
	            'msg' => 'Heroi não disponível.'
            ];
        } else {
            $usuario = Funcoes::getUsuario();

            //fazer join para saber o usuario da cidade
            $cidadeHeroi = CidadeHeroi::join('cidade c', 'c.id', '=', 'cidade_heroi.id_cidade')
                ->where('id_cidade', '=', $usuario->cidade)->row();

            if($cidadeHeroi->id_cidade == $usuario->cidade && $cidadeHeroi->id_heroi == $id_heroi) {
                return [
                	'success' => false,
	                'msg' => 'Este heroi já está ativo nesta cidade.'
                ];
            }

            if ($cidadeHeroi->count() > 0) {
                $usuarioHeroi = UsuarioHeroi::findFirst([
                    'id_heroi' => $id_heroi,
                    'id_usuario' => $usuario->id
                ]);
	
                if($usuarioHeroi->count() == 0) {
                    $usuarioHeroi = new UsuarioHeroi();
                }
                
	            $usuarioHeroi->id_usuario = $usuario->id;
	            $usuarioHeroi->id_heroi = $id_heroi;
                $usuarioHeroi->honra = 0;
                $usuarioHeroi->atualizacao = time();
                $usuarioHeroi->save();
            }

            $cidadeHeroi->id_cidade = $usuario->cidade;
            $cidadeHeroi->id_heroi = $id_heroi;
            $cidadeHeroi->save();
	
	        $getAll = $this->getAll();
	
	        $getAll['msg'] = "$heroi->nome ativado.";
	
	        return $getAll;
        }

        return [
        	'success' => false,
	        'mensagem' => 'Não foi possível ativar heroi.'
        ];
    }
    
}