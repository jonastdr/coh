<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\Config;
use app\models\Expedicao;
use app\models\Funcoes;
use app\models\Notificacao;
use app\models\Usuario;
use projectws\mvc\Controller;

class GameController extends Controller {
	
	public function index() {
		return $this->view->render('game.edge');
	}
	
	public function init() {
		$usuario = Funcoes::getUsuario();
		
		$this->view->usuario = $usuario;
		
		return $this->view->render('init.edge');
	}
	
	/**
	 * Carrega a view de cidades
	 * @return null|string
	 */
	public function cidades() {
		return $this->view->render('game/maincidade.edge');
	}
	
	/**
	 * Carrega a view de notificações
	 * @return null|string
	 */
	public function notificacoes() {
		return $this->view->render('game/notificacoes.edge');
	}
	
	/**
	 * Retorna as cidades do usuário
	 * @return array
	 */
	public function cidadesData() {
		$usuario = Funcoes::getUsuario();
		
		$cidades = Cidade
			::select('cidade.id', 'cidade.nome')
			->orderBy('cidade.nome')
			->find([
			'id_usuario' => $usuario->id
		]);
		
		$missoes = [
			Expedicao::ATACAR => 'ataque',
			Expedicao::TRANSPORTE => 'transporte',
			Expedicao::DEFENDER => 'defender',
			Expedicao::COLONIZACAO => 'colonizacao',
            Expedicao::ESPIONAGEM => 'Espionagem'
		];
		
		$expedicoes = Expedicao::getExpedicoes();
		
		/**
		 * Cria as atividades que estão acontecendo na cidades
		 */
		$atividades = [];
		foreach($expedicoes as $expedicao) {
			$atividade = [];
			
			$atividade['id'] = $expedicao->id;
			$atividade['atancente'] = $expedicao->atacante;
			$atividade['defensor'] = $expedicao->defensor;
			$atividade['missao'] = $missoes[$expedicao->missao];
			$atividade['duracao'] = $expedicao->duracao;
			$atividade['volta'] = $expedicao->volta;
			
			if($expedicao->volta == 0)
				$atividade['tempo'] = $expedicao->duracao - (time() - $expedicao->horario);
			else
				$atividade['tempo'] = ($expedicao->duracao * 2) - (time() - $expedicao->horario);
			
			$atividade['restante'] = gmdate('H:i:s', $atividade['tempo']);
			
			$atividade['tempo_corrido'] = time() - $expedicao->horario;
			
			$atividades[] = $atividade;
		}
		
		return [
			'success' => true,
			'usuario' => $usuario->toArray(),
			'cidades' => $cidades->toArray(),
			'atividades' => $atividades
		];
	}
	
	/**
	 * Carrega os menus
	 * @return null|string
	 */
    public function menu() {
    	$this->view->usuario = Funcoes::getUsuario();
    	
    	return $this->view->render('game/mainmenu.edge');
    }
	
	/**
	 * Carrega a tela de recursos
	 * @return null|string
	 */
    public function recursos() {
    	return $this->view->render('game/recursos.edge');
    }
	
	/**
	 * Carrega todos os dados de recursos
	 * @return array
	 */
    public function recursosData() {
        $cidade = Funcoes::getCidade();

        $cidadeAtual = Cidade::recursos($cidade->id);
	    
	    $honra = Usuario::heroisHonra($cidade);
	    
        return [
        	'success' => true,
        	'recursos' => [
	            'dinheiro' => $cidadeAtual->dinheiro,
		        'alimento' => $cidadeAtual->alimento,
		        'metal' => $cidadeAtual->metal,
		        'petroleo' => $cidadeAtual->petroleo,
		        'ouro' => $cidadeAtual->ouro,
		        'energia' => (int) $cidadeAtual->energia,
		        'populacao' => $cidadeAtual->populacao,
		        'honra' => ($honra->count() ? $honra->honra : 0)
	        ],
	        'armazenamento' => [
	        	'dinheiro' => $cidadeAtual->dinheiro_armazenamento,
		        'alimento' => $cidadeAtual->alimento_armazenamento,
		        'metal' => $cidadeAtual->metal_armazenamento,
		        'petroleo' => $cidadeAtual->petroleo_armazenamento,
		        'honra' => ($honra->count() ? $honra->honra_armazenamento : 0)
	        ],
	        'tempo' => $cidadeAtual->atualizacao,
	        'incremento' => [
	        	'dinheiro' => ($cidadeAtual->banco + 1) / $cidadeAtual->dinheiro_fator,
		        'alimento' => ($cidadeAtual->fazenda + 1) / $cidadeAtual->alimento_fator,
		        'metal' => ($cidadeAtual->metalurgica + 1) / $cidadeAtual->metal_fator,
		        'petroleo' => $cidadeAtual->poco_petroleo / $cidadeAtual->petroleo_fator,
		        'honra' => $honra->level / ($honra->count() ? $honra->honra_fator : 1)
	        ]
        ];
    }
	
	/**
	 * Atualiza a cidade atual selecionada
	 * @return array
	 */
    public function cidadeAlterar() {
        $id_cidade = \Request::getPost('id');

        $usuario = Funcoes::getUsuario();
        
        $cidade = Funcoes::getCidade();
	
	    /**
	     * Somente altera se a cidade for do usuário logado
	     */
        if($cidade->id_usuario == $usuario->id) {
            $usuario->cidade = $id_cidade;
	        
            if($usuario->save()) {
	            $cidade = Funcoes::getCidade();
            	
	            return [
		            'success' => true,
		            'mensagem' => 'Cidade alterada.',
		            'cidade' => [
			            'id' => $cidade->id,
			            'x' => $cidade->x,
			            'y' => $cidade->y
		            ]
	            ];
            }
        }

        return [
            'success' => false,
            'mensagem' => 'Falha ao alterar cidade.'
        ];
    }
	
	/**
	 * Altera o nome da cidade
	 * @return array
	 */
    public function alterarNomeCidade() {
    	$nome = \Request::getPost('nome');
	    
	    if(strlen($nome) < 3) {
	    	return [
	    		'success' => false,
			    'msg' => 'Deve informar um nome com pelo menos 3 caracteres.'
		    ];
	    }
	    
	    $cidade = Funcoes::getCidade();
	    
	    $upd = Cidade::update([
	    	'nome' => $nome
	    ], [
	    	'id' => $cidade->id
	    ]);
	    
	    if($upd) {
		    return [
			    'success' => true,
			    'msg' => 'Nome alterado.'
		    ];
	    }
	    
	    return [
	    	'success' => false,
		    'msg' => 'Não foi possível alterar o nome da cidade.'
	    ];
    }
    
    /**
     * Traz todas as notificações para o jogo
     * @return array
     */
    public function notificacoesData() {
        $usuario = Funcoes::getUsuario();
        
        $notificacoes = $this->notificacoesUsuario($usuario);
    
        foreach ($notificacoes as $notificacao) {
            $notificacao->params = json_decode($notificacao->params);
        }
        
        return [
            'success' => true,
            'notificacoes' => $notificacoes->toArray()
        ];
    }
    
    /**
     * Retorna as 5 últimas notificações do usuário
     * @param Usuario $usuario
     * @return null|\projectws\libs\orm\ModelCollection
     */
    private function notificacoesUsuario(Usuario $usuario) {
        $notificacoes = Notificacao
            ::where('id_usuario', '=', $usuario->id)
            ->orderBy('datahora DESC')
            ->limit(5)
            ->rows();
        
        return $notificacoes;
    }

    /**
     * Método usado para excluir uma notificação do usuário logado
     */
    public function removerNotificacao() {
        $id_notificacao = \Request::getPost('id_notificacao');

        $usuario = Funcoes::getUsuario();

        $notificacao = Notificacao::findFirst($id_notificacao);

        if($notificacao->id_usuario == $usuario->id) {
            $notificacao->delete();
    
            $notificacoes = $this->notificacoesUsuario($usuario);
            
            return [
                'success' => true,
                'notificacoes' => $notificacoes->toArray()
            ];
        }
        
        return [
            'success' => false,
            'msg' => 'Não foi possível remover notificação.'
        ];
    }

}
