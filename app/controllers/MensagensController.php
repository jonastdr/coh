<?php

namespace app\controllers;

use app\models\Funcoes;
use app\models\Mensagem;
use app\models\MensagemTexto;
use app\models\MensagemTextoLido;
use app\models\MensagemTextoUsuario;
use app\models\Notificacao;
use app\models\Usuario;
use projectws\libs\bridges\DB;
use projectws\libs\Request;
use projectws\mvc\Controller;

/**
 * Tipos de Mensagens
 * 1 = Mensagens entre usuários
 * 2 = Mensagens de Relatórios
 * 3 = Mensagens entre usuários da aliança
 * 4 = Mensagens de Relatórios de Espionagem
 * Class MensagensController
 */
class MensagensController extends Controller {

	const USUARIO_TIPO = 1;
	const RELATORIO_TIPO = 2;
	const ALIANCA_TIPO = 3;
	const ESPIONAGEM_TIPO = 4;

    public function index() {
    	$this->view->usuario = Funcoes::getUsuario();
    	
        return $this->view->render('mensagem/index.edge');
    }
	
	/**
	 * Envia a resposta de uma mensagem
	 * @param int $id_mensagem
	 * @return array
	 */
    public function responder($id_mensagem = 0) {
	    $usuario = Funcoes::getUsuario();
    	
	    $mensagem = Mensagem::findFirst($id_mensagem);
	
	    $post = \Request::getPost();
	
	    if(!empty($post['assunto']) && $post['assunto'] != $mensagem->assunto) {
		    Mensagem::update([
			    'assunto' => \Request::getPost('assunto')
		    ], [
			    'id' => $id_mensagem
		    ]);
	    }
	
	    $resposta = new MensagemTexto();
	    $resposta->id_mensagem = $id_mensagem;
	    $resposta->id_remetente = $usuario->id;
	    $resposta->mensagem = $post['mensagem'];
	    
	    $respondido = $resposta->save();
        
        $mensagemTextUsuario = MensagemTextoUsuario
            ::where('id_mensagem', '=', $id_mensagem)
            ->where('id_usuario', '<>', $usuario->id)
            ->rows();
    
        foreach ($mensagemTextUsuario as $mtu) {
            Notificacao::add(
                $mtu->id_usuario,
                $mensagem->assunto,
                'mensagem',
                [
                    'aba' => 'visualizar',
                    'id' => $id_mensagem
                ],
                1,
                $id_mensagem
            );
        }
        
        $mensagemUsuarioEnvio = MensagemTexto
            ::select('DISTINCT mt.id_remetente as id_usuario')
            ->alias('mt')
            ->where('mt.id_mensagem', '=', $id_mensagem)
            ->whereNotIn('mt.id_remetente',
                MensagemTextoUsuario
                    ::select('id_usuario')
                    ->where('id_mensagem', '=', $id_mensagem)
            )
            ->rows();
        
        foreach($mensagemUsuarioEnvio as $u) {
            $mensagemUsuario = new MensagemTextoUsuario();
            $mensagemUsuario->id_mensagem = $id_mensagem;
            $mensagemUsuario->id_usuario = $u->id_usuario;
            $mensagemUsuario->save();
        }
	    
	    if($respondido) {
            $return = $this->getAll();
        
            $return['msg'] = 'Mensagem enviada.';
            $return['visualizar'] = $this->visualizar($id_mensagem)['visualizar'];
            
	    	return $return;
	    }
	    
	    return [
	    	'success' => false,
		    'msg' => 'não foi possível enviar mensagem. tente novamente mais tarde.'
	    ];
    }
	
	/**
	 * Retorna todos os tipos de mensagens
	 * @return array
	 */
    public function getAll() {
    	$usuarios = $this->listar(static::USUARIO_TIPO, 0);
    	$relatorios = $this->listar(static::RELATORIO_TIPO, 0);
    	$aliancas = $this->listar(static::ALIANCA_TIPO, 0);
	    
    	return [
    		'success' => true,
		    'usuarios' => $usuarios['mensagens'],
		    'usuariosCount' => $usuarios['count'],
		    'relatorios' => $relatorios['mensagens'],
		    'relatoriosCount' => $relatorios['count'],
		    'aliancas' => $aliancas['mensagens'],
		    'aliancasCount' => $aliancas['count']
	    ];
    }
	
	/**
	 * Faz a listagem das mensagens
	 * @param int $tipo
	 * @param int $pag
	 * @param int $quantidade
	 * @return array
	 */
    public function listar($tipo = 1, $pag = 0, $quantidade = 10) {
    	if($pag > 0)
		    $pag--;
    	
        if ($quantidade < 5) {
            $quantidade = 5;
        } elseif ($quantidade > 15) {
            $quantidade = 15;
        }

        $usuario = Funcoes::getUsuario();

        $tipo = [$tipo];
        
        if($tipo[0] == MensagensController::RELATORIO_TIPO) {
            $tipo[] = MensagensController::ESPIONAGEM_TIPO;
        }
        
	    $total = Mensagem
		    ::select('COUNT(DISTINCT mensagem.id) count')
		    ->leftJoin('mensagem_texto_usuario mtu', 'mtu.id_mensagem = mensagem.id')
		    ->whereIn('mensagem.tipo', $tipo)
		    ->where(function ($q) use ($usuario) {
			    $q->where('mtu.id_usuario', '=', $usuario->id);
		    })
	        ->row();
	    
        $mensagens = Mensagem::select(
            'mensagem.id',
            'mensagem.tipo',
            'mensagem.assunto',
            'mensagem.data_criacao',
            'string_agg(distinct destinos.nome, \', \') as nomes_destino',
            'count(DISTINCT mensagem_texto.*) -1 as replicas',
            'remetente.nome as nome_remetente',
            'ultimo_texto.mensagem',
            'ultimo_texto.link'
        )
            ->join('mensagem_texto', 'mensagem_texto.id_mensagem', '=', 'mensagem.id')
            ->join('mensagem_texto_usuario', 'mensagem_texto_usuario.id_mensagem', '=', 'mensagem.id')
            ->join('usuario destinos', 'destinos.id', '=', 'mensagem_texto_usuario.id_usuario')
            ->join(#JOIN PAR BUSCA ULTIMA MENSAGEM
                MensagemTexto::select(
                    'id_mensagem',
                    'max(ultima_msg.id) as ultimo_id'
                )
                    ->alias('ultima_msg')
                    ->groupBy('id_mensagem')
            , 'ultima_msg.id_mensagem', '=', 'mensagem.id')
            ->join('mensagem_texto as ultimo_texto', 'ultimo_texto.id', '=', 'ultima_msg.ultimo_id')
            ->leftJoin('usuario as remetente', 'remetente.id', '=', 'ultimo_texto.id_remetente')
	        ->leftJoin('mensagem_texto_usuario mtu', 'mtu.id_mensagem = mensagem.id')
            ->whereIn('mensagem.tipo', $tipo)
            ->where('mtu.id_usuario', '=', $usuario->id)
            ->groupBy(
                'mensagem.id',
                'mensagem.tipo',
                'mensagem.assunto',
                'mensagem.data_criacao',
                'ultimo_texto.data_envio',
                'remetente.nome',
                'ultimo_texto.mensagem',
                'ultimo_texto.link'
            )
            ->orderBy('ultimo_texto.data_envio DESC')
            ->limit($quantidade)
            ->offset($pag * $quantidade)
            ->rows();

        return [
        	'success' => true,
	        'mensagens' => $mensagens->toArray(),
	        'count' => $total->count
        ];
    }
	
	/**
	 * Retorna o grupo de mensagens
	 * @param int $id_mensagem
	 * @return array
	 */
    public function visualizar($id_mensagem = 0) {
        $usuario = Funcoes::getUsuario();
        
        $mensagemScope = Mensagem::select(
            'mensagem.id',
            'mensagem.tipo',
            'mensagem.assunto',
            'mensagem.data_criacao',
            'usuario.nome as nome_usuario',
            'string_agg(destino.nome, \', \') as nomes_destino'
        )
            ->leftJoin('usuario', 'usuario.id', '=', 'mensagem.id_usuario')
            ->join('mensagem_texto_usuario', 'mensagem_texto_usuario.id_mensagem', '=', 'mensagem.id')
            ->join('usuario destino', 'destino.id', '=', 'mensagem_texto_usuario.id_usuario')
            ->groupBy(
                'mensagem.id',
                'mensagem.tipo',
                'mensagem.assunto',
                'mensagem.data_criacao',
                'usuario.nome'
            )
            ->findFirst($id_mensagem);

        if(in_array($mensagemScope->tipo, [1, 3])) {
            $mensagens = $mensagemScope->mensagens;

            foreach ($mensagens as $mensagem) {
                if(is_null($mensagem->data_lido)) {
                    $mensagemLido = new MensagemTextoLido();
                    $mensagemLido->id_usuario = $usuario->id;
                    $mensagemLido->id_mensagem_texto = $mensagem->id_mensagem_texto;
                    $mensagemLido->save();
                }
            }
	
	        $mensagemArr = $mensagemScope->toArray();
	        $mensagemArr['mensagens'] = $mensagens->toArray();
    
            /**
             * Remove a notificação ao abrir
             */
            Notificacao
                ::where('id_referencia', '=', $id_mensagem)
                ->where('tipo', '=', 1)
                ->delete();
            
	        return [
		        'success' => true,
		        'visualizar' => $mensagemArr
	        ];
        } else {
	        return [
		        'success' => false,
		        'visualizar' => []
	        ];
        }
    }
	
	/**
	 * Faz a exclusão de mensagens
	 * @return array
	 */
    public function excluir() {
    	$usuario = Funcoes::getUsuario();
    	
    	$mensagens = Request::getPost('msg', true);
	    
	    $deleted = MensagemTextoUsuario
		    ::where('id_usuario', '=', $usuario->id)
	        ->whereIn('id_mensagem', $mensagens)
	        ->delete();
	    
	    if($deleted) {
	    	return [
	    		'success' => true,
			    'msg' => 'Mensagens deletadas com sucesso.'
		    ];
	    }
	    
	    return [
	    	'success' => false,
		    'msg' => 'Não foi possível deletar mensagens.'
	    ];
    }
	
	/**
	 * Insere uma nova mensagem
	 * @return null|string
	 */
    public function nova() {
        if (\Request::getPost()) {
            return DB::saveOrFail(function ($fail) {
	            $usuario = Funcoes::getUsuario();

                $mensagem = new Mensagem();
                $mensagem->id_usuario = $usuario->id;
                $mensagem->tipo = Request::getPost('alianca') ? 3 : 1;
                $mensagem->assunto = Request::getPost('assunto');
                $id_mensagem = $mensagem->save();

                $mensagemTexto = new MensagemTexto();
                $mensagemTexto->id_mensagem = $id_mensagem;
                $mensagemTexto->id_remetente = $usuario->id;
                $mensagemTexto->mensagem = Request::getPost('mensagem');
                $mensagemTexto->save();

                $usuarios = Request::getPost('usuario', true);

                $enviado = 0;
                //gera ids para destino
                foreach ($usuarios as $id_usuario) {
                	if($id_usuario == $usuario->id)
                		continue;
                    
                    $usuarioNovo = Usuario::findFirst($id_usuario);

                    if ($usuarioNovo) {
                        $mensagemUsuario = new MensagemTextoUsuario();
                        $mensagemUsuario->id_mensagem = $id_mensagem;
                        $mensagemUsuario->id_usuario = $usuarioNovo->id;
                        $mensagemUsuario->save();
    
                        Notificacao::add(
                            $id_usuario,
                            Request::getPost('assunto'),
                            'mensagem',
                            [
                                'aba' => 'visualizar',
                                'id' => $id_mensagem
                            ],
                            1,
                            $id_mensagem
                        );
                        
                        $enviado++;
                    }
                }
	
	            $mensagemUsuario = new MensagemTextoUsuario();
	            $mensagemUsuario->id_mensagem = $id_mensagem;
	            $mensagemUsuario->id_usuario = $usuario->id;
	            $mensagemUsuario->save();

                if($enviado == 0) {
                    $fail->addMessage('Usuário(s) não existe(m)!');
                }
            }, function () {
				return $this->getAll();
            }, function ($error) {
				return [
					'success' => false,
					'msg' => $error[0]
				];
            });
        }

        return [
        	'success' => false,
	        'msg' => 'Nada a fazer.'
        ];
    }

}
