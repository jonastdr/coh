<?php

namespace app\controllers;

use app\models\CidadeDefesa;
use app\models\CidadeDefesaUnidade;
use app\models\Expedicao;
use app\models\ExpedicaoUnidade;
use app\models\Funcoes;
use projectws\libs\bridges\DB;
use projectws\libs\orm\Join;
use projectws\mvc\Controller;

class DefesaController extends Controller {
	
	/**
	 * View da defesa
	 * @return null|string
	 */
    public function index() {
        return $this->view->render('defesa/index.edge');
    }
    
	/**
	 * View da defesa
	 * @return null|string
	 */
    public function cidade() {
        return $this->view->render('defesa/cidade.edge');
    }
    
	/**
	 * View da defesa
	 * @return null|string
	 */
    public function expedicao() {
        return $this->view->render('defesa/expedicao.edge');
    }
    
    public function getAll() {
	    $cidade = $this->cidadeData();
	    $expedicao = $this->expedicaoData();
    	
	    if($cidade['success'] && $expedicao['success']) {
		    return [
		    	'success' => true,
			    'defesas' => $cidade['defesas'],
			    'expedicoes' => $expedicao['expedicoes']
		    ];
	    }
	    
    	return [
    		'success' => false,
		    'msg' => 'não foi possível trazer os resultados'
	    ];
    }
	
	/**
	 * Traz os dados da aba de cidade
	 * @return array
	 */
    public function cidadeData() {
        $usuario = Funcoes::getUsuario();

        $defesas = CidadeDefesa::select(
            'c.nome as cidade_def',
            'cidade_defesa.*')
            ->join('cidade c', 'c.id', '=', 'cidade_defesa.id_cidade_def')
            ->where('id_cidade', '=', $usuario->cidade)
            ->rows();

        $cidadeLogado = Funcoes::getCidade();

        foreach($defesas as $defesa) {
            $unidades = $defesa->unidades;

            //Retorna a menor velocidade possivel
            $velocidadeMax = $this->getVelocidadeMax($unidades);

            $distancia = Funcoes::getDistancia($cidadeLogado, $defesa);

            $velocidadeGlobal = 10;
            $fator = 1;

            $duracao = Funcoes::getDuracao($velocidadeGlobal, $velocidadeMax, $distancia, $fator);

            $defesa->tempo = gmdate('H:i:s', $duracao);
    
            //seta as unidades na defesa
            $defesa->unidades = $unidades->toArray();
        }
    
        $defesasArr = [];
    
        foreach ($defesas as $defesa) {
            $defesaArr = $defesa->toArray();
            $defesaArr['unidades'] = $defesa->unidades;
        
            $defesasArr[] = $defesaArr;
        }

        return [
        	'success' => true,
	        'defesas' => $defesasArr
        ];
    }
	
    private function getVelocidadeMax($unidades) {
        $velocidadeMax = 1;

        foreach ($unidades as $unidade) {
            //Verifica a velocidade minima
            if(($unidade->quantidade > 0 && $unidade->velocidade < $velocidadeMax) OR $velocidadeMax == null)
                $velocidadeMax = $unidade->velocidade;
        }

        return $velocidadeMax;
    }
	
	/**
	 * Traz os dados de expedição
	 * @return array
	 */
    public function expedicaoData() {
        $usuario = Funcoes::getUsuario();

        $defesas = CidadeDefesa
            ::select(
                'c.nome as cidade',
                'cidade_defesa.*'
            )
            ->join('cidade c', 'c.id', '=', 'cidade_defesa.id_cidade_def')
            ->find(['id_cidade_def' => $usuario->cidade]);

        $cidadeLogado = Funcoes::getCidade();
        
        foreach($defesas as $defesa) {
            $unidades = $defesa->unidades;

            //Retorna a menor velocidade possivel
            $velocidadeMax = $this->getVelocidadeMax($unidades);

            $velocidadeGlobal = 10;
            $fator = 1;
    
            $distancia = Funcoes::getDistancia($cidadeLogado, $defesa);

            $duracao = Funcoes::getDuracao($velocidadeGlobal, $velocidadeMax, $distancia, $fator);

            $defesa->tempo = gmdate('H:i:s', $duracao);
	        
	        //seta as unidades na defesa
	        $defesa->unidades = $unidades->toArray();
        }
	    
        $defesasArr = [];
	
	    foreach ($defesas as $defesa) {
		    $defesaArr = $defesa->toArray();
		    $defesaArr['unidades'] = $defesa->unidades;
		    
		    $defesasArr[] = $defesaArr;
	    }
        
	    return [
	    	'success' => true,
		    'expedicoes' => $defesasArr
	    ];
    }

	/**
	 * Faz o regresso de uma defesa
	 * @param $id_defesa
	 * @return array
	 */
    public function regressar($id_defesa) {
        try{
            DB::begin();
            
            $defesa = CidadeDefesa
                ::select(
                    'cidade_defesa.*',
                    'ic.x + i.x AS x',
                    'ic.y + i.y AS y',
                    'c.cod_ilha',
                    'c.cod_cidade'
                )
                ->join('cidade c', 'c.id', '=', 'cidade_defesa.id_cidade')
                ->join('ilha_cidade ic', function (Join $j) {
                    $j->on('ic.id_ilha', '=', 'c.cod_ilha');
            
                    $j->where('ic.cod_cidade', '=', 'c.cod_cidade');
                })
                ->join('ilha i', 'i.id', '=', 'c.cod_ilha')
                ->findFirst($id_defesa);
    
            if($defesa->count() > 0) {
                $usuario = Funcoes::getUsuario();
        
                if (in_array($usuario->cidade, [$defesa->id_cidade, $defesa->id_cidade_def])) {
                    $expedicao = new Expedicao();
                    $expedicao->id_cidade_atk = $defesa->id_cidade_def;
                    $expedicao->id_cidade_def = $defesa->id_cidade;
                    $expedicao->cod_ilha = $defesa->cod_ilha;
                    $expedicao->cod_cidade = $defesa->cod_cidade;
                    $expedicao->id_cidade_def = $defesa->id_cidade;
                    $expedicao->missao = Expedicao::DEFENDER;
            
                    $cidadeLogado = Funcoes::getCidade();
            
                    $defesa->posicao = explode(';', $defesa->posicao);
            
                    /**
                     * @var CidadeDefesaUnidade $unidades
                     */
                    $unidades = $defesa->unidades;
            
                    //Retorna a menor velocidade possivel
                    $velocidadeMax = $this->getVelocidadeMax($unidades);
            
                    $distancia = Funcoes::getDistancia($cidadeLogado, $defesa);
            
                    $velocidadeGlobal = 10;
                    $fator = 1;
            
                    $duracao = Funcoes::getDuracao($velocidadeGlobal, $velocidadeMax, $distancia, $fator);
            
                    $expedicao->duracao = $duracao;
            
                    $expedicao->horario = time();
                    $expedicao->volta = true;
            
                    //Remove a defesa transferida para expedição
                    CidadeDefesaUnidade::where('id_defesa', '=', $id_defesa)->delete();
                    $defesa->delete();
            
                    //Salva o regresso da expedição
                    $id_expedicao = $expedicao->save();
            
                    foreach ($unidades as $unidade) {
                        $expedicaoUnidade = new ExpedicaoUnidade();
                        $expedicaoUnidade->id_expedicao = $id_expedicao;
                        $expedicaoUnidade->id_unidade = $unidade->id;
                        $expedicaoUnidade->quantidade = $unidade->quantidade;
                        $expedicaoUnidade->save();
                    }
    
                    DB::commit();
                    
                    return [
                        'success' => true,
                        'msg' => 'A defesa irá regressar ao seu destino.'
                    ];
                } else {
                    throw new \Exception('Falha no regresso da defesa.');
                }
            }
    
            throw new \Exception('Defesa não encontrada.');
        }catch (\Exception $e) {
            DB::rollback();
            
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }
        
    }
}