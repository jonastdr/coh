<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadeConstrucao;
use app\models\Construcao;
use app\models\Funcoes;
use projectws\mvc\Controller;

class OverviewController extends Controller {
	
	public function index() {
		return $this->view->render('overview/index.edge');
	}
	
	/**
	 * Traz todos os dados para tela
	 * @return array
	 */
	public function getAll() {
		$construcoes = $this->construcoes();
		
		$construcoesConstruir = $this->construcoesPendentes();
		
		if($construcoes['success'] && $construcoesConstruir['success']) {
			return [
				'success' => true,
				'construcoes' => $construcoes['construcoes'],
				'construcoesPendentes' => $construcoesConstruir['construcoesPendentes']
			];
		}
		
		return [
			'success' => false,
			'msg' => 'Não foi possível trazer os dados das construções.'
		];
	}
	
	/**
	 * Traz as construcoes
	 * @return array
	 */
    public function construcoes() {
        $usuario = Funcoes::getUsuario();

        $cidadeConstrucao = CidadeConstrucao::find($usuario->cidade);

        $cidade = Cidade::findFirst($usuario->cidade);

        $construcoes = [];

        foreach ($cidadeConstrucao as $id => $coluna) {
            $construcaoNome = Construcao::findFirst($coluna->id_construcao)->nome;

            $construcaoCidade = explode(',', $cidade->construcao);

            $construcao = [];

            $construcao['slot'] = $coluna->slot;
            $construcao['id'] = $coluna->id_construcao;
            $construcao['nome'] = $construcaoNome;
            $construcao['imagem'] = ($coluna->level == 0 ? 0 : $coluna->id_construcao);

	        $construcao['level'] = $coluna->level;
	        
            //Define o tempo para a conclusão da construção
            if (isset($construcaoCidade[1]) && $construcaoCidade[1] == $coluna->id_construcao) {
                $construcao['tempo'] = $construcaoCidade[0] - time();
            } else {
                $construcao['tempo'] = 0;
            }
	
	        $construcoes[] = $construcao;
        }

        return [
        	'success' => true,
	        'construcoes' => $construcoes
        ];
    }
	
	/**
	 * Traz todas as construcoes pendentes a construir
	 * @return array
	 */
    public function construcoesPendentes() {
	    $usuario = Funcoes::getUsuario();
	
	    $construcoes = Construcao
		    ::select('c.*', 'cc.id_cidade')
		    ->alias('c')
		    ->leftJoin('cidade_construcao cc', 'cc.id_construcao', '=', 'c.id AND cc.id_cidade = ' . $usuario->cidade)
		    ->whereNull('cc.id_cidade')
		    ->orderBy('c.id')
	        ->rows();
	
	    $construcoesArr = [];
	
	    foreach ($construcoes as $id => $coluna) {
		    $costrucao = [];
		
		    $costrucao['id'] = $coluna->id;
		    $costrucao['nome'] = $coluna->nome;
		
		    $construcoesArr[] = $costrucao;
	    }
	
	    return [
		    'success' => true,
		    'construcoesPendentes' => $construcoesArr
	    ];
    }
	
	/**
	 * Insere uma construção no mapa
	 * @param $id_construcao
	 * @param $slot
	 */
    public function insere($id_construcao, $slot) {
        $usuario = Funcoes::getUsuario();
	    
        $cidade = Cidade::findFirst($usuario->cidade);

        $construcaoAtual = Construcao::findFirst($id_construcao);
	
	    if($construcaoAtual->id == 1 && $construcaoAtual->level == 0) {
		    $construcaoAtual->dinheiro = 0;
		    $construcaoAtual->alimento = 0;
		    $construcaoAtual->metal = 0;
		    $construcaoAtual->petroleo = 0;
		    $construcaoAtual->ouro = 0;
		    $construcaoAtual->tempo = 0;
		    $construcaoAtual->populacao = 0;
	    }
	    
        //Recursos
        $CalcTempo = Funcoes::calculoFator($construcaoAtual->tempo, 0, $construcaoAtual->fator, $construcaoAtual->max);

        $construcaoAtual->dinheiro = Funcoes::calculoFator($construcaoAtual->dinheiro, 0, $construcaoAtual->fator, $construcaoAtual->max);
        $construcaoAtual->alimento = Funcoes::calculoFator($construcaoAtual->alimento, 0, $construcaoAtual->fator, $construcaoAtual->max);
        $construcaoAtual->metal = Funcoes::calculoFator($construcaoAtual->metal, 0, $construcaoAtual->fator, $construcaoAtual->max);
        $construcaoAtual->petroleo = Funcoes::calculoFator($construcaoAtual->petroleo, 0, $construcaoAtual->fator, $construcaoAtual->max);
        $construcaoAtual->ouro = Funcoes::calculoFator($construcaoAtual->ouro, 0, $construcaoAtual->fator, $construcaoAtual->max);
        $construcaoAtual->energia = Funcoes::calculoFator($construcaoAtual->energia, 0, $construcaoAtual->fator, $construcaoAtual->max);
        $construcaoAtual->populacao = Funcoes::calculoFator($construcaoAtual->populacao, 0, $construcaoAtual->fator, $construcaoAtual->max);
	
	    $construcao = CidadeConstrucao
			::where('id_cidade', '=', $cidade->id)
		    ->where(function ($q) use ($cidade, $id_construcao, $slot) {
			    $q->where('id_construcao', '=', $id_construcao);
			    $q->orWhere('slot', '=', $slot);
		    })
		    ->findFirst();
	
	    /**
	     * Já está construíndo
	     */
	    if($cidade->construcao != 0) {
	    	return [
	    		'success' => false,
			    'msg' => 'Os construtores estão ocupados atualmente.'
		    ];
	    }
	
	    /**
	     * Verifica se tem todos os requisitos para a construção
	     */
	    if (Construcao::temTodosRequisitos($id_construcao) == false) {
		    return [
			    'success' => false,
			    'msg' => 'Verifique os requisitos necessários.'
		    ];
	    }
	    
	    /**
	     * Sem recursos |
	     */
	    if(!($id_construcao == 1 && $construcao->count() == 0) && !Funcoes::tenhoRecursos($construcaoAtual)) {
		    return [
			    'success' => false,
			    'msg' => 'Não há recursos suficiente para esta construção.'
		    ];
	    }
	
	    /**
	     * Posição inválida ou já construído
	     */
	    if($construcao->count() > 0) {
	    	return [
	    		'success' => false,
			    'msg' => 'Não é possível construir.'
		    ];
	    }
	
	    $cidade->dinheiro = $cidade->dinheiro - $construcao->dinheiro;
	    $cidade->alimento = $cidade->alimento - $construcao->alimento;
	    $cidade->metal = $cidade->metal - $construcao->metal;
	    $cidade->petroleo = $cidade->petroleo - $construcao->petroleo;
	    $usuario->ouro = $usuario->ouro - $construcao->ouro;
	    $cidade->energia = $cidade->energia - $construcao->energia;
	    $cidade->populacao = $cidade->populacao - $construcao->populacao;

        $cidade->construcao = time() + $CalcTempo . ',' . $construcaoAtual->id . ',' . 1;

        $cidade->save();
        
        $cidadeConstrucao = new CidadeConstrucao();

        //Insere a na tabela CidadesConstrucoes
        $cidadeConstrucao->id_cidade = $cidade->id;
        $cidadeConstrucao->id_construcao = $construcaoAtual->id;
        $cidadeConstrucao->level = 0;
        $cidadeConstrucao->slot = $slot;

        $cidadeConstrucao->save();
        
        return $this->getAll();
    }
    
}
