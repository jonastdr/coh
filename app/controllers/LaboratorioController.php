<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadePesquisa;
use app\models\Funcoes;
use app\models\Pesquisa;
use projectws\mvc\Controller;

class LaboratorioController extends Controller {
	
	/**
	 * Retorna o layout da tela
	 * @return null|string
	 */
	public function index() {
		return $this->view->render('laboratorio/index.edge');
	}
	
	/**
	 * Retorna os dados necessários para a tela inicial
	 * @return array
	 */
    public function getAll() {
    	$usuario = Funcoes::getUsuario();
    	
	    $pesquisas = Pesquisa::listarTodas($usuario->cidade);
	
	    //pesquisa selecionada
	    $id_pesquisa = $pesquisas[0]->id;
	    
	    //dados da pesquisa
	    $pesquisa = $this->getPesquisa($id_pesquisa, $usuario);
	    $pesquisaArray = $pesquisa->toArray();
	    $pesquisaArray['construcao_requisitos'] = Pesquisa::construcaoRequisito($id_pesquisa)->toArray();
	    $pesquisaArray['pesquisa_requisitos'] = Pesquisa::pesquisaRequisito($id_pesquisa)->toArray();
	
	    $cidade = Funcoes::getCidade();
	    
	    $pesquisaExp = explode(',', $cidade->pesquisa);
	
	    if(isset($pesquisaExp[1]) && $pesquisaExp[1] == $id_pesquisa) {
		    $acao = 'Pesquisando';
		    
		    $tempo = $pesquisaExp[0] - time();
	    } else {
		    $acao = 'Pesquisar';
		    
		    $tempo = 0;
	    }
	    
	    return [
	    	'success' => true,
	    	'pesquisas' => $pesquisas->toArray(),
		    'pesquisa' => $pesquisaArray,
		    'tempo' => $tempo,
		    'acao' => $acao
	    ];
    }
	
	/**
	 * Retorna os dados da pesquisa selecionada
	 * @return array
	 */
    public function get($id_pesquisa = 0) {
        $usuario = Funcoes::getUsuario();
	
	    //dados da pesquisa
	    $pesquisa = $this->getPesquisa($id_pesquisa, $usuario);
	    $pesquisaArray = $pesquisa->toArray();
	
	    $pesquisaArray['construcao_requisitos'] = Pesquisa::construcaoRequisito($id_pesquisa)->toArray();
	    $pesquisaArray['pesquisa_requisitos'] = Pesquisa::pesquisaRequisito($id_pesquisa)->toArray();

        $cidade = Funcoes::getCidade();

        $pesquisaExp = explode(',', $cidade->pesquisa);

        if(isset($pesquisaExp[1]) && $pesquisaExp[1] == $id_pesquisa) {
            $acao = 'Pesquisando';
	
	        $tempo = $pesquisaExp[0] - time();
        } else {
            $acao = 'Pesquisar';
	        
	        $tempo = 0;
        }
	    
	    return [
	    	'success' => true,
		    'acao' => $acao,
		    'pesquisa' => $pesquisaArray,
		    'tempo' => $tempo
	    ];
    }
	
	/**
	 * Sobe o level da pesquisa ou adiciona ao usuário
	 * @return array
	 */
    public function pesquisar() {
        $usuario = Funcoes::getUsuario();

        $id_pesquisa = \Request::getPost('id');

        $cidade = Cidade::findFirst($usuario->cidade);

        $pesquisa = Pesquisa::findFirst($id_pesquisa);

        $cidadePesquisa = CidadePesquisa::findFirst([
        	'id_cidade' => $usuario->cidade,
	        'id_pesquisa' => $id_pesquisa
        ]);

        if ($cidadePesquisa) {
            $level = $cidadePesquisa->level;
        } else {
            $level = 0;
        }

        //Faz calculo do custo da pesquisa
        $pesquisa->dinheiro = Funcoes::CalculoFator($pesquisa->dinheiro, $level, $pesquisa->fator, $pesquisa->max);
        $pesquisa->alimento = Funcoes::CalculoFator($pesquisa->alimento, $level, $pesquisa->fator, $pesquisa->max);
        $pesquisa->metal = Funcoes::CalculoFator($pesquisa->metal, $level, $pesquisa->fator, $pesquisa->max);
        $pesquisa->petroleo = Funcoes::CalculoFator($pesquisa->petroleo, $level, $pesquisa->fator, $pesquisa->max);
        $pesquisa->ouro = Funcoes::CalculoFator($pesquisa->ouro, $level, $pesquisa->fator, $pesquisa->max);
        $pesquisa->energia = Funcoes::CalculoFator($pesquisa->energia, $level, $pesquisa->fator, $pesquisa->max);
        $pesquisa->populacao = Funcoes::CalculoFator($pesquisa->populacao, $level, $pesquisa->fator, $pesquisa->max);

        //Calculo do tempo para cada level
        $pesquisa->tempo = Funcoes::CalculoTempo($pesquisa->tempo, $level, $pesquisa->fator, $pesquisa->max);

        if($cidade->pesquisa != 0) {
	        return [
		        'success' => false,
		        'msg' => 'O Laboratório está ocupado.'
	        ];
        }
	
	    /**
	     * Verifica se tem todos os requisitos para a construção
	     */
	    if (Pesquisa::temTodosRequisitos($id_pesquisa) == false) {
		    return [
			    'success' => false,
			    'msg' => 'Verifique os requisitos necessários.'
		    ];
	    }
	
	    /**
	     * Ao atinger o nível máximo da pesquisa
	     */
	    if($cidadePesquisa->level >= $pesquisa->max) {
		    return [
			    'success' => false,
			    'msg' => 'Nível máximo da pesquisa atingido.'
		    ];
	    }

        if (Funcoes::tenhoRecursos($pesquisa)) {
            $cidade->dinheiro = $cidade->dinheiro - $pesquisa->dinheiro;
            $cidade->alimento = $cidade->alimento - $pesquisa->alimento;
            $cidade->metal = $cidade->metal - $pesquisa->metal;
            $cidade->petroleo = $cidade->petroleo - $pesquisa->petroleo;
            $usuario->ouro = $usuario->ouro - $pesquisa->ouro;
            $cidade->energia = $cidade->energia - $pesquisa->energia;
            $cidade->populacao = $cidade->populacao - $pesquisa->populacao;

            $cidade->pesquisa = time() + $pesquisa->tempo . ',' . $id_pesquisa . ',' . 1;

            $cidade->save();

            $mensagem = 'A pesquisa irá terminar às ' . date('H:i:s', time() + $pesquisa->tempo) . '!';
    
            $cidade = Funcoes::getCidade();
    
            $pesquisaExp = explode(',', $cidade->pesquisa);
    
            if(isset($pesquisaExp[1]) && $pesquisaExp[1] == $id_pesquisa) {
                $acao = 'Pesquisando';
        
                $tempo = $pesquisaExp[0] - time();
            } else {
                $acao = 'Pesquisar';
        
                $tempo = 0;
            }
            
            return [
            	'success' => true,
	            'msg' => $mensagem,
                'acao' => $acao,
                'tempo' => $tempo
            ];
        } else {
            return [
            	'success' => false,
	            'msg' => 'Você não tem recursos o suficiente.'
            ];
        }
    }
	
	/**
	 * Retorna os dados da pesquisa de acordo com o level atual na cidade
	 * @param $id_pesquisa
	 * @param $usuario
	 * @return mixed
	 */
    private function getPesquisa($id_pesquisa, $usuario) {
	    $cidadePesquisa = Pesquisa::getPesquisa($id_pesquisa, $usuario->cidade);
	
	    //Level atual da pesquisa
	    $level = $cidadePesquisa->level;
	
	    //Calculo do tempo para cada level
	    $cidadePesquisa->tempo = Funcoes::CalculoTempo($cidadePesquisa->tempo, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	
	    $cidadePesquisa->dinheiro = Funcoes::CalculoFator($cidadePesquisa->dinheiro, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    $cidadePesquisa->alimento = Funcoes::CalculoFator($cidadePesquisa->alimento, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    $cidadePesquisa->metal = Funcoes::CalculoFator($cidadePesquisa->metal, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    $cidadePesquisa->petroleo = Funcoes::CalculoFator($cidadePesquisa->petroleo, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    $cidadePesquisa->ouro = Funcoes::CalculoFator($cidadePesquisa->ouro, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    $cidadePesquisa->energia = Funcoes::CalculoFator($cidadePesquisa->energia, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    $cidadePesquisa->populacao = Funcoes::CalculoFator($cidadePesquisa->populacao, $level, $cidadePesquisa->fator, $cidadePesquisa->max);
	    
	    return $cidadePesquisa;
    }

}
