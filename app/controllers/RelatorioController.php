<?php

namespace app\controllers;

use app\models\Funcoes;
use app\models\Relatorio;
use projectws\mvc\Controller;

class RelatorioController extends Controller {

	public function index() {
		return $this->view->render('relatorio/index.edge');
	}
	
    public function getAll($codigo) {
        $relatorio = Relatorio::where('link', '=', $codigo)->row();
	    
	    if($relatorio->count() > 0) {
		    $dados = unserialize(base64_decode($relatorio->dados));
		
		    $detalhes = $dados['detalhes'];
		
		    $unidadesAtaque = [];
		
		    $unidadesAtaque['primeiro'] = [];
		    $unidadesAtaque['ultimo'] = [];
		
		    foreach ($dados[0]->atacante->unidades as $unidade) {
			    $unidadesAtaque['primeiro'][] = $unidade->toArray();
		    }
		
		    foreach ($dados[count($dados) - 3]->atacante->unidades as $unidade) {
			    $unidadesAtaque['ultimo'][] = $unidade->toArray();
		    }
		
		    //une todas as unidades defesa
		    $unidadesPrimeiro = [];
		
		    $unidadesDef = $dados[0]->defensor->unidades;
		    
		    if($unidadesDef) {
			    foreach ($dados[0]->defensor->unidades as $unidade) {
				    $unidadesPrimeiro[] = [
					    'id' => $unidade->id,
					    'nome' => $unidade->nome,
					    'quantidade' => $unidade->quantidade
				    ];
			    }
		    }
		
		    $unidadesDef = $dados[0]->defesas->unidades;
		    
		    if($unidadesDef) {
			    foreach ($dados[0]->defesas->unidades as $unidade) {
				    $unidadesPrimeiro[] = [
					    'id' => $unidade->id,
					    'nome' => $unidade->nome,
					    'quantidade' => $unidade->quantidade
				    ];
			    }
		    }
		
		    //unidades da ultima rodada
		    $unidadesUltimo = [];
		
		    $unidadesDef = $dados[count($dados) - 3]->defensor->unidades;
		    
		    if($unidadesDef) {
			    foreach ($unidadesDef as $unidade) {
				    $unidadesUltimo[] = [
					    'id' => $unidade->id,
					    'nome' => $unidade->nome,
					    'quantidade' => $unidade->quantidade
				    ];
			    }
		    }
		
		    $unidadesDef = $dados[count($dados) - 3]->defesas->unidades;
		    
		    if($unidadesDef) {
			    foreach ($dados[count($dados) - 3]->defesas->unidades as $unidade) {
				    $unidadesUltimo[] = [
					    'id' => $unidade->id,
					    'nome' => $unidade->nome,
					    'quantidade' => $unidade->quantidade
				    ];
			    }
		    }
		
		    $unidadesDefesa = [];
		
		    $unidadesDefesa['primeiro'] = $unidadesPrimeiro;
		    $unidadesDefesa['ultimo'] = $unidadesUltimo;
		
		    //Detalhes de pontuações
		    $usuario = Funcoes::getUsuario();
		
		    $this->view->pontos = false;
		
		    if ($usuario->id == $dados['detalhes']->id_atacante)
			    $pontos = $dados['detalhes']->pontos_atacante;
		    elseif (isset($dados['detalhes']->pontos_por_defensor[$usuario->id]))
			    $pontos = $dados['detalhes']->pontos_por_defensor[$usuario->id];
		    else
			    $pontos = 0;
		
		    if ($dados['detalhes']->vencedor == 1)
			    $status = 'O Atacante ganhou a batalha.';
		    elseif ($dados['detalhes']->vencedor == 2)
			    $status = 'O Defensor ganhou a batalha.';
		    else
			    $status = 'Houve um empate.';
		
		    //Recursos
		    $recursos = $dados['recursos'];
		
		    return [
			    'success' => true,
			    'recursos' => $recursos,
			    'status' => $status,
			    'pontos' => $pontos,
			    'data' => $relatorio->data,
			    'detalhes' => $detalhes,
			    'unidades' => [
				    'ataque' => $unidadesAtaque,
				    'defesa' => $unidadesDefesa
			    ]
		    ];
	    }
	    
	    return [
	        'success' => false,
		    'msg' => 'Relatório não encontrado.'
	    ];
    }

}
