<?php

namespace app\controllers;

use app\models\Funcoes;
use app\models\Ilha;
use app\models\IlhaCidade;
use projectws\libs\bridges\DB;
use projectws\libs\Request;
use projectws\mvc\Controller;

class MapaController extends Controller {

	private $ilhas = [];
	
    public function index() {
        return $this->view->render('mapa/index.edge');
    }
    
    public function pushInitial() {
	    $cidade = Funcoes::getCidade();
    	
	    return [
	    	'success' => true,
		    'cidadeAtual' => [
		    	'id' => $cidade->id,
			    'x' => $cidade->x,
			    'y' => $cidade->y,
			    'cod_ilha' => $cidade->cod_ilha,
			    'cod_cidade' => $cidade->cod_cidade
		    ]
	    ];
    }
    
    public function push() {
    	$x_start = Request::getPost('x_start');
    	$x_end = Request::getPost('x_end');
	    
    	$y_start = Request::getPost('y_start');
    	$y_end = Request::getPost('y_end');
	    
	    $cidade = Funcoes::getCidade();
	    
	    $ilhas = Ilha
		    ::whereBetween('x', $x_start, $x_end)
		    ->whereBetween('y', $y_start, $y_end)
		    ->rows();
	
	    $ilhasArr = [];
	    
	    foreach ($ilhas as $ilha) {
		    $ilhaArr = $ilha->toArray();
		    $ilhaArr['cidades'] = $ilha->cidades->toArray();
		    
		    $ilhasArr[] = $ilhaArr;
	    }
	    
	    return [
	    	'success' => true,
	    	'ilhas' => $ilhasArr,
		    'cidadeAtual' => [
		    	'id' => $cidade->id,
			    'x' => $cidade->x,
			    'y' => $cidade->y,
			    'cod_ilha' => $cidade->cod_ilha,
			    'cod_cidade' => $cidade->cod_cidade
		    ]
	    ];
    }
	
	/**
	 * Script para gerar o mapa
	 * @return string
	 */
    public function generate()
    {
	    $id = 1;
	    $island_left = [];
	    $island_top = [];
	    $sum_line1 = 0;//LEFT
	    $sum_line2 = 0;//TOP
	    $sum_line3 = 0;//TOP2
	    $end = 0;
	    $end2 = 0;
	    $line1 = 0;
	    $line2 = 0;
	    $getisland = 1;
	    $model = 0;
	
	    for ($x = 1; $x <= 100; $x++) {
		    for ($y = 1; $y <= 100; $y++) {
			    if ($getisland == 19) {
				    $islandimg = 'island' . $getisland;
				    $getisland = 1;
			    } else {
				    $islandimg = 'island' . $getisland;
				    $getisland++;
			    }
			
			    if ($id > 1) {
				    $end += 1;
			    }
			
			    for ($n = (1 + $end); $n <= (1 + $end); $n++) {
				    if ($n > (1 + $end) && $line1 == 0)
					    $sum_line1 += 1600;
				
				    //LEFT POSITION
				    if ($line2 == 0) {
					    $island_left[$n] = 223 + $sum_line1;
					    $line2++;
				    } else if ($line2 == 1) {
					    $island_left[$n] = 354 + $sum_line1;
					    $line2++;
				    } else if ($line2 == 2) {
					    $island_left[$n] = 333 + $sum_line1;
					    $line2++;
				    } else if ($line2 == 3) {
					    $island_left[$n] = 237 + $sum_line1;
					    $line2 = 0;
				    }
				
				    //TOP POSITION
				    if ($line1 == 0) {
					    $island_top[$n] = 234 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 1) {
					    $island_top[$n] = 250 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 2) {
					    $island_top[$n] = 256 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 3) {
					    $island_top[$n] = 223 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 4) {
					    $island_top[$n] = 250 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 5) {
					    $island_top[$n] = 257 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 6) {
					    $island_top[$n] = 233 + $sum_line2 + $sum_line3;
					    $line1++;
				    } else if ($line1 == 7) {
					    $island_top[$n] = 239 + $sum_line2 + $sum_line3;
					    $line1 = 0;
					    $sum_line2 = 0;
				    }
				    if ($n == (1 + $end)) {
					    $sum_line1 += 1000;
				    }
				    if ($n == (100 + $end2)) {
					    $end2 += 100;
					    $sum_line1 = 0;
					    $sum_line3 += 600;
				    }
			    }
			
			    $this->ilhas[] = (Object) [
				    'id' => $id,
				    'x' => $island_left[$id],
				    'y' => $island_top[$id],
				    'tipo' => $getisland,
				    'img' => $islandimg,
				    'cidades' => $this->cidades($this->cidadePosicoes($model))
			    ];
			
			    if ($model == 18) {
				    $model = 0;
			    } else {
				    $model += 1;
			    }
			
			    $id++;
		    }
	    }
	    
	    if(Ilha::findFirst()->count() == 0)
	        return $this->gerarIlhas();
	    
	    return 'Ilhas já foram geradas.';
    }
	
	/**
	 * Gera as ilhas
	 * @return string
	 */
    private function gerarIlhas() {
    	try {
		    set_time_limit(3600);
    		
    		DB::begin();
		    
		    DB::query('ALTER SEQUENCE ilha_id_seq RESTART WITH 1');
		    
		    foreach ($this->ilhas as $ilha) {
			    $novaIlha = new Ilha();
			
			    $novaIlha->x = $ilha->x;
			    $novaIlha->y = $ilha->y;
			    $novaIlha->tipo = $ilha->tipo;
			    $novaIlha->img = $ilha->img;
			
			    $id_ilha = $novaIlha->save();
			
			    foreach ($ilha->cidades as $cidade) {
				    $novaIlhaCidade = new IlhaCidade();
				
				    $novaIlhaCidade->id_ilha = $id_ilha;
				    $novaIlhaCidade->cod_cidade = $cidade->cod_cidade;
				    $novaIlhaCidade->x = $cidade->x;
				    $novaIlhaCidade->y = $cidade->y;
				
				    $novaIlhaCidade->save();
			    }
		    }
		    
		    DB::commit();
		    
		    return 'Ilhas geradas.';
    	}catch (\Exception $e){
    		DB::rollback();
    		
    	    return 'Não foi possível gerar ilhas.';
    	}
    }
	
	/**
	 * Cria as posições das cidades
	 * @param $posicoes
	 * @return array
	 */
    private function cidades($posicoes) {
    	$cidades = [];
	    
	    for ($i = 0; $i < count($posicoes[0]); $i++) {
		    $cidade = (Object) [
		    	'cod_cidade' => $i + 1,
		    	'x' => $posicoes[0][$i],
			    'y' => $posicoes[1][$i]
		    ];
		    
		    $cidades[] = $cidade;
    	}
	    
    	return $cidades;
    }
	
	/**
	 * Arrays com posições de cidades
	 * @param $i
	 * @return array
	 */
    private function cidadePosicoes($i) {
	    if($i == 0) {
		    return [
			    [384, 311, 224, 185, 147, 95, 205, 300, 385, 450, 527, 588, 650, 681, 669, 595, 543, 466],
			    [35, 61, 81, 135, 166, 237, 244, 228, 242, 226, 240, 236, 216, 164, 110, 76, 53, 38]
		    ];
	    }
	
	    if($i == 1) {
		    return [
			    [409, 345, 270, 189, 201, 126, 262, 193, 83, 327, 408, 478, 569, 635, 697, 663, 582, 492],
			    [79, 73, 78, 80, 146, 184, 234, 234, 238, 242, 236, 219, 233, 211, 157, 113, 77, 76]
		    ];
	    }
	
	    if($i == 2) {
		    return [
			    [384, 306, 221, 122, 143, 248, 329, 380, 417, 481, 556, 608, 634, 616, 603, 588, 539, 471],
			    [25, 30, 16, 26, 131, 129, 113, 181, 233, 277, 299, 266, 228, 185, 135, 82, 55, 28]
		    ];
	    }
	
	    if($i == 3) {
		    return [
			    [378, 287, 216, 160, 128, 121, 340, 181, 252, 410, 465, 507, 568, 618, 633, 603, 538, 457],
			    [87, 87, 80, 75, 120, 170, 268, 230, 256, 256, 255, 231, 229, 193, 138, 96, 74, 83]
		    ];
	    }
	
	    if($i == 4) {
		    return [
			    [374, 284, 207, 127, 125, 134, 141, 253, 364, 264, 467, 516, 622, 636, 629, 605, 545, 463],
			    [44, 46, 53, 68, 113, 170, 269, 278, 248, 143, 133, 215, 223, 179, 136, 93, 61, 54]
		    ];
	    }
	
	    if($i == 5) {
		    return [
			    [384, 311, 238, 239, 158, 85, 299, 248, 168, 380, 507, 670, 572, 478, 499, 588, 561, 492],
			    [37, 37, 46, 101, 95, 125, 222, 181, 164, 270, 276, 276, 225, 212, 144, 120, 79, 46]
		    ];
	    }
	
	    if($i == 6) {
		    return [
			    [380, 263, 106, 242, 162, 74, 244, 345, 412, 486, 443, 522, 589, 632, 644, 589, 512, 458],
			    [49, 49, 87, 104, 154, 227, 170, 130, 95, 147, 221, 268, 250, 201, 151, 114, 84, 74]
		    ];
	    }
	
	    if($i == 7) {
		    return [
			    [388, 293, 182, 201, 241, 160, 249, 105, 39, 136, 379, 355, 449, 596, 683, 713, 598, 489],
			    [50, 44, 23, 71, 118, 154, 271, 206, 253, 285, 259, 158, 167, 287, 248, 183, 105, 76]
		    ];
	    }
	
	    if($i == 8) {
		    return [
			    [373, 311, 224, 197, 256, 169, 125, 221, 188, 288, 350, 431, 502, 567, 643, 590, 485, 466],
			    [65, 61, 81, 112, 135, 173, 209, 169, 227, 231, 222, 218, 207, 170, 147, 113, 104, 60]
		    ];
	    }
	
	    if($i == 9) {
		    return [
			    [357, 272, 204, 152, 121, 95, 158, 231, 312, 388, 475, 565, 664, 683, 596, 509, 471, 347],
			    [93, 91, 86, 115, 160, 212, 223, 223, 226, 232, 236, 224, 210, 132, 104, 93, 143, 171]
		    ];
	    }
	
	    if($i == 10) {
		    return [
			    [384, 313, 250, 198, 139, 101, 153, 220, 246, 310, 388, 459, 538, 649, 591, 568, 526, 467],
			    [92, 100, 106, 119, 86, 99, 147, 156, 202, 210, 199, 175, 227, 244, 185, 142, 96, 87]
		    ];
	    }
	
	    if($i == 11) {
		    return [
			    [419, 347, 276, 210, 167, 117, 257, 190, 120, 313, 383, 453, 533, 609, 683, 636, 571, 508],
			    [92, 88, 94, 95, 115, 162, 230, 211, 217, 233, 222, 215, 229, 203, 186, 114, 80, 84]
		    ];
	    }
	
	    if($i == 12) {
		    return [
			    [376, 289, 208, 133, 69, 59, 119, 248, 334, 417, 514, 580, 677, 688, 700, 623, 543, 463],
			    [43, 30, 47, 71, 92, 153, 236, 247, 208, 172, 233, 296, 272, 185, 120, 80, 53, 55]
		    ];
	    }
	
	    if($i == 13) {
		    return [
			    [383, 318, 267, 209, 159, 138, 175, 231, 288, 337, 302, 427, 489, 528, 635, 607, 553, 483],
			    [92, 96, 91, 100, 108, 161, 209, 208, 199, 185, 142, 125, 143, 180, 178, 134, 107, 94]
		    ];
	    }
	
	    if($i == 14) {
		    return [
			    [408, 329, 254, 182, 126, 174, 218, 303, 385, 450, 529, 320, 575, 670, 602, 474, 407, 509],
			    [94, 102, 104, 115, 148, 184, 218, 205, 211, 224, 232, 156, 182, 154, 115, 178, 151, 90]
		    ];
	    }
	
	    if($i == 15) {
		    return [
			    [380, 300, 232, 174, 185, 124, 163, 240, 340, 438, 508, 567, 643, 653, 602, 510, 544, 438],
			    [97, 117, 136, 154, 100, 173, 218, 164, 202, 193, 187, 229, 196, 144, 115, 95, 147, 124]
		    ];
	    }
	
	    if($i == 16) {
		    return [
			    [389, 319, 256, 200, 171, 126, 198, 278, 353, 413, 469, 516, 575, 626, 674, 582, 526, 455],
			    [80, 82, 91, 114, 169, 216, 242, 223, 215, 193, 196, 175, 171, 161, 143, 99, 82, 78]
		    ];
	    }
	
	    if($i == 17) {
		    return [
			    [343, 279, 226, 176, 134, 129, 132, 199, 255, 296, 363, 429, 517, 585, 670, 603, 539, 440],
			    [94, 93, 79, 68, 97, 141, 194, 200, 220, 252, 245, 219, 197, 173, 132, 109, 90, 85]
		    ];
	    }
	
	    if($i == 18) {
		    return [
			    [350, 277, 220, 175, 383, 121, 471, 428, 255, 306, 384, 464, 548, 623, 635, 573, 523, 443],
			    [103, 113, 114, 122, 128, 147, 139, 159, 220, 170, 208, 204, 206, 185, 153, 127, 104, 99]
		    ];
	    }
    }
    
}
