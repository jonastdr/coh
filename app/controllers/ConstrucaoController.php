<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadeConstrucao;
use app\models\Construcao;
use app\models\Funcoes;
use app\models\HeroiFuncaoUsuario;
use projectws\mvc\Controller;

class ConstrucaoController extends Controller {
	
	/**
	 * Retorna a view de construção
	 * @return null|string
	 */
	public function index() {
		return $this->view->render('construcao/index.edge');
	}
	
	/**
	 * Cancela a construção e devolve a metade dos recursos
	 * @param $id
	 */
    public function cancelar() {
    	$id = \Request::getPost('id');
    	
        $usuario = Funcoes::getUsuario();

        //Atualiza o tempo para a melhoria
        $cidadeAtual = Cidade::findFirst($usuario->cidade);

        $construcaoStr = explode(',', $cidadeAtual->construcao);
	
	    $construcao = $this->getConstrucao($id);
	    
	    if($construcao->count() == 0) {
	    	return [
	    		'success' => false,
			    'msg' => 'Construção não localizada.'
		    ];
	    }
	
	
	    $construcaoCidade = CidadeConstrucao::findFirst([
		    'id_cidade' => $usuario->cidade,
		    'id_construcao' => $id
	    ]);
	
	    if($construcaoCidade->level == 0) {
	    	$construcaoCidade->delete();
	    }
	    
        if ($construcaoStr[2] != 0) {
	        $cidadeAtual->dinheiro = $cidadeAtual->dinheiro + ($construcao->dinheiro / 2);
	        $cidadeAtual->alimento = $cidadeAtual->alimento + ($construcao->alimento / 2);
	        $cidadeAtual->metal = $cidadeAtual->metal + ($construcao->metal / 2);
	        $cidadeAtual->petroleo = $cidadeAtual->petroleo + ($construcao->petroleo / 2);
	        $usuario->ouro = $usuario->ouro + ($construcao->ouro / 2);
	        $cidadeAtual->energia = $cidadeAtual->energia + $construcao->energia;
	        $cidadeAtual->populacao = $cidadeAtual->populacao + $construcao->populacao;
        } else {
	        $cidadeAtual->dinheiro = $cidadeAtual->dinheiro + $construcao->dinheiro;
	        $cidadeAtual->alimento = $cidadeAtual->alimento + $construcao->alimento;
	        $cidadeAtual->metal = $cidadeAtual->metal + $construcao->metal;
	        $cidadeAtual->petroleo = $cidadeAtual->petroleo + $construcao->petroleo;
	        $usuario->ouro = $usuario->ouro + $construcao->ouro;
	        $cidadeAtual->energia = $cidadeAtual->energia + $construcao->energia;
	        $cidadeAtual->populacao = $cidadeAtual->populacao + $construcao->populacao;
        }

        $cidadeAtual->construcao = 0;
        
	    if($cidadeAtual->save()) {
	    	return $this->getAll($id);
	    }
	    
	    return [
	    	'success' => false,
		    'msg' => 'Não foi possível cancelar.'
	    ];
    }
	
	/**
	 * Traz os dados da construção
	 * @param $id
	 * @return null
	 */
    private function getConstrucao($id) {
    	$cidade = Funcoes::getCidade();
    	
        $construcao = Construcao::getConstrucao($id, $cidade->id);

        if ($construcao) {
            $level = $construcao->level;

            //Faz calculo do custo da pesquisa
            $construcao->dinheiro = Funcoes::calculoFator($construcao->dinheiro, $level, $construcao->fator, $construcao->max);
            $construcao->alimento = Funcoes::calculoFator($construcao->alimento, $level, $construcao->fator, $construcao->max);
            $construcao->metal = Funcoes::calculoFator($construcao->metal, $level, $construcao->fator, $construcao->max);
            $construcao->petroleo = Funcoes::calculoFator($construcao->petroleo, $level, $construcao->fator, $construcao->max);
            $construcao->ouro = Funcoes::calculoFator($construcao->ouro, $level, $construcao->fator, $construcao->max);
            $construcao->energia = Funcoes::calculoFator($construcao->energia, $level, $construcao->fator, $construcao->max);
            $construcao->populacao = Funcoes::calculoFator($construcao->populacao, $level, $construcao->fator, $construcao->max);

            $construcao->tempo = Funcoes::calculoTempo($construcao->tempo, $level, $construcao->fator, $construcao->max);

            return $construcao;
        }

        return null;
    }
	
	/**
	 * Retorna os dados para a tela
	 * @param int $id
	 * @return array|null|string
	 */
    public function getAll($id = 0) {
        $usuario = Funcoes::getUsuario();

        $construcao = $this->getConstrucao($id);
        
        $heroiFuncao = HeroiFuncaoUsuario::findFirst([
            'id_usuario' => $usuario->id,
            'id_funcao' => 3
        ]);

        if ($construcao->count()) {
            $level = $construcao->level;
	
	        if($construcao->id == 1 && $level == 0) {
		        $construcao->dinheiro = 0;
		        $construcao->alimento = 0;
		        $construcao->metal = 0;
		        $construcao->petroleo = 0;
		        $construcao->ouro = 0;
		        $construcao->tempo = 0;
		        $construcao->populacao = 0;
	        }
	        
            $cidade = Cidade::findFirst($usuario->cidade);

	        $funcaoTempo = 1;
    
            /**
             * Quando ativa a função de tempo
             */
	        if($heroiFuncao->count())
	            $funcaoTempo = 0.95;
	        
            //Calculo do tempo para cada level
            $calcTempo = Funcoes::calculoTempo($construcao->tempo * $funcaoTempo, $level, $construcao->fator, $construcao->max);

            $cidade_construcao = $cidade->construcao;

            if ($cidade_construcao != 0) {
                $cidade_construcao = explode(',', $cidade_construcao);
            }

            if (isset($cidade_construcao[1]) && $cidade_construcao[1] == $id) {
                $tempoGerado = $cidade_construcao[0] - time();
            } else {
                $tempoGerado = 0;
            }

            if ($calcTempo >= 3600 * 24) {
                $dias = round($calcTempo / 3600, 0);

                $construcao->tempo = $dias . 'd ' . gmdate("H:i:s", $calcTempo);
            } else {
                $construcao->tempo = gmdate("H:i:s", $calcTempo);
            }
            
            $construcaoArr = $construcao->toArray();
	
	        $construcaoArr['construcao_requisitos'] = Construcao::construcaoRequisito($id)->toArray();
	        $construcaoArr['pesquisa_requisitos'] = Construcao::pesquisaRequisito($id)->toArray();
	        
	        if($construcao->level > 0)
	            $construcaoArr['components'] = $construcao->components->toArray();
	        else
	            $construcaoArr['components'] = [];
	
	        return [
		        'success' => true,
		        'construindo' => $cidade_construcao,
		        'tempoGerado' => $tempoGerado,
		        'construcao' => $construcaoArr,
		        'temRequisito' => Construcao::temTodosRequisitos($id)
	        ];
        } else {
            return [
            	'success' => false,
	            'msg' => 'Construção não encontrada.'
            ];
        }

        return $this->view->render('construcao/index');
    }
	
	/**
	 * Destroi uma construção
	 * @param $id
	 */
    public function destruir() {
	    $id = \Request::getPost('id');
    	
        $usuario = Funcoes::getUsuario();

        //Atualiza o tempo para a melhoria
        $cidadeAtual = Cidade::findFirst($usuario->cidade);

        $construcao = $this->getConstrucao($id);
	
	    if($construcao->count() == 0) {
		    return [
			    'success' => false,
			    'msg' => 'Construção não localizada.'
		    ];
	    }
	    
        if (Funcoes::tenhoRecursos($construcao)) {
            $cidadeAtual->dinheiro = $cidadeAtual->dinheiro + ($construcao->dinheiro / 2);
            $cidadeAtual->alimento = $cidadeAtual->alimento + ($construcao->alimento / 2);
            $cidadeAtual->metal = $cidadeAtual->metal + ($construcao->metal / 2);
            $cidadeAtual->petroleo = $cidadeAtual->petroleo + ($construcao->petroleo / 2);
            $usuario->ouro = $usuario->ouro + ($construcao->ouro / 2);
            $cidadeAtual->energia = $cidadeAtual->energia + ($construcao->energia / 2);
            $cidadeAtual->populacao = $cidadeAtual->populacao + ($construcao->populacao / 2);

	        $tempo = 0;
	        
            $cidadeAtual->construcao = time() + $tempo . ',' . $id . ',' . 0;
	        
	        if($cidadeAtual->save()) {
		        return $this->getAll($id);
	        }
	
	        return [
		        'success' => false,
		        'msg' => 'Não foi possível cancelar.'
	        ];
        }
        
        return [
        	'success' => false,
	        'msg' => 'Você não tem recursos o suficiente.'
        ];
    }
	
	/**
	 * Avança um level da construção
	 * @return array|object
	 */
    public function melhorar() {
        $id = \Request::getPost('id');

        $usuario = Funcoes::getUsuario();

        //Atualiza o tempo para a melhoria
        $cidadeAtual = Cidade::findFirst($usuario->cidade);

        $construcao = $this->getConstrucao($id);

        if($cidadeAtual->construcao != 0) {
	        return [
		        'success' => false,
		        'msg' => 'Os construtores já estão trabalhando!'
	        ];
        }
        
        if($construcao->level == 0) {
        	return [
        		'success' => false,
		        'msg' => 'Para construir deve arrastar a construção na lateral para o campo.'
	        ];
        }
	
	    /**
	     * Verifica se tem todos os requisitos para a construção
	     */
        if (Construcao::temTodosRequisitos($id) == false) {
        	return [
        		'success' => false,
		        'msg' => 'Verifique os requisitos necessários.'
	        ];
        }

        if (Funcoes::tenhoRecursos($construcao)) {
            $cidadeAtual->dinheiro = $cidadeAtual->dinheiro - $construcao->dinheiro;
            $cidadeAtual->alimento = $cidadeAtual->alimento - $construcao->alimento;
            $cidadeAtual->metal = $cidadeAtual->metal - $construcao->metal;
            $cidadeAtual->petroleo = $cidadeAtual->petroleo - $construcao->petroleo;
            $usuario->ouro = $usuario->ouro - $construcao->ouro;
            $cidadeAtual->energia = $cidadeAtual->energia - $construcao->energia;
            $cidadeAtual->populacao = $cidadeAtual->populacao - $construcao->populacao;
	        
	        $tempo = Funcoes::calculoTempo($construcao->tempo, $construcao->level, $construcao->fator, $construcao->max);

            $cidadeAtual->construcao = time() + $tempo . ',' . $id . ',' . 1;

            if($cidadeAtual->save()) {
                return $this->getAll($id);
            }

            return [
                'success' => false,
                'msg' => 'Não foi possível fazer melhoria.'
            ];
        }
	
	    return [
		    'success' => false,
		    'msg' => 'Você não tem recursos o suficiente.'
	    ];
    }

}
