<?php

namespace app\controllers;

use app\models\Classe;
use app\models\Config;
use app\models\Funcoes;
use app\models\Spam;
use app\models\Unidade;
use projectws\mvc\Controller;

class AdministradorController extends Controller {

    public function IndexAction() {
        return $this->view->render('administrador/index');
    }

    public function ChatAction() {
        $spam = new Spam();
        $config = new Config();

        $this->view->texto = '';

        if ($_POST) {
            $funcoes = new Funcoes();

            $consulta = \Request::getPost('spam');

            $unidade = \Request::getPost('unidade_nome');

            $exclusao = \Request::getPost('excluir_spam');

            $exclusaoUnidade = \Request::getPost('excluir_unidade');

            $alterarUnidade = \Request::getPost('alterar_unidade');

            $chat_limit = \Request::getPost('limit_chat');

            $verifica = $spam->findFirst("spam='$consulta'");

            if ($exclusaoUnidade) {
                $this->UnidadesExcluir();
            } elseif ($alterarUnidade) {
                $this->UnidadesAlterar();
            }

            if ($unidade) {
                if ($this->UnidadesCadastro()) {
                    $this->view->texto = $funcoes->mensagem(\Request::getPost('unidade_nome') . ' criado com sucesso!', 'alerta');
                } else {
                    foreach ($spam->getMessages() as $mensagem) {
                        $this->view->texto = $funcoes->mensagem($mensagem->getMessage());
                    }
                }
            }

            if ($chat_limit) {
                $updateChatLimit = $config->findFirst("config='chat_limit'");
                $updateChatLimit->valor = $chat_limit;
                $updateChatLimit->save();

                $this->view->texto = $funcoes->mensagem('Alterado o limite para ' . $chat_limit, 'alerta');
            }

            if ($exclusao) {
                $mensagem = '';
                foreach (\Request::getPost('excluir_spam') as $id => $valor) {
                    if ($valor) {
                        $delete = $spam->findFirst("id=$id");
                        $delete->delete();

                        $mensagem .= $delete->spam . ' ';
                    }
                }
                $this->view->texto = $funcoes->mensagem($mensagem . ' - deletado com sucesso!', 'alerta');
            }

            if ($consulta) {
                $spam->spam = \Request::getPost('spam');
                $spam->save();

                $this->view->texto = $funcoes->mensagem($consulta . ' - adicionado com sucesso!', 'alerta');
            }
            if ($verifica) {
                $this->view->texto = $funcoes->mensagem('Esta mensagem já existe!', 'alerta');
            }

            if (!$spam) {
                foreach ($spam->getMessages() as $mensagem) {
                    $this->view->texto = $funcoes->mensagem($mensagem->getMessage());
                }
            }
        }
        $this->view->chat_limit = $config->findFirst("config='chat_limit'")->valor;

        $this->view->tabela = $spam->orderBy("spam")->rows();

        return $this->view->render('administrador/chat');
    }

    public function GlobalAction() {
        return $this->view->render('administrador/global');
    }

    public function UnidadesAction() {
        $this->view->tabela = Unidade::find();

        $this->view->classes = Classe::find();

        return $this->view->render('administrador/unidades');
    }

    private function UnidadesCadastro() {
        $unidades = new Unidade();

        $nomeUnidade = \Request::getPost('unidade_nome');

        $unidades->nome = $nomeUnidade;
        $unidades->classe = \Request::getPost('classe');
        $unidades->ataque_terrestre = \Request::getPost('ataque_terrestre');
        $unidades->ataque_aereo = \Request::getPost('ataque_aereo');
        $unidades->ataque_naval = \Request::getPost('ataque_naval');
        $unidades->ataque_defesa = \Request::getPost('ataque_defesa');
        $unidades->save();

        $this->UploadImagem($nomeUnidade);

        return true;
    }

    private function UploadImagem($nome) {
        $uploaddir = '../public/imagens/unidades/';

        $nomeArquivo = $_FILES['unidade_img']['name'];

        $tamanhoArquivo = round($_FILES['unidade_img']['size'] / 1024);

        $extensao = strtolower(strrchr($nomeArquivo, "."));

        $Unidade = Unidade::findFirst("nome='$nome'");

        if ($tamanhoArquivo < 1024 && $extensao == '.png') {
            $uploadfile = $uploaddir . $Unidade->id . $extensao;

            if (move_uploaded_file($_FILES['unidade_img']['tmp_name'], $uploadfile)) {
                return true;
            }
        } else {
            $Unidade->delete();
        }
    }

    private function UnidadesExcluir() {
        $funcoes = new Funcoes;
        $unidades = new Unidade;

        $mensagem = '';
        foreach (\Request::getPost('excluir_unidade') as $id => $valor) {
            if ($valor == 'on') {
                $delete = $unidades->findFirst("id=$id");
                $delete->delete();

                $mensagem .= $delete->nome . ' ';
            }
        }
        $this->view->texto = $funcoes->mensagem($mensagem . ' - deletado com sucesso!', 'alerta');
    }

    private function UnidadesAlterar() {
        $funcoes = new Funcoes;
        $unidades = new Unidade;

        $mensagem = '';
        foreach (\Request::getPost('alterar_unidade') as $id => $valor) {
            if ($valor) {
                $unidade = $unidades->findFirst("id=$id");

                if ($valor['alt_classe'] != null && $valor['alt_classe'] >= 0) {
                    $unidade->classe = $valor['alt_classe'];
                }
                if ($valor['alt_atk_terrestre'] != null && $valor['alt_atk_terrestre'] >= 0) {
                    $unidade->ataque_terrestre = $valor['alt_atk_terrestre'];
                }
                if ($valor['alt_atk_aereo'] != null && $valor['alt_atk_aereo'] >= 0) {
                    $unidade->ataque_aereo = $valor['alt_atk_aereo'];
                }
                if ($valor['alt_atk_naval'] != null && $valor['alt_atk_naval'] >= 0) {
                    $unidade->ataque_naval = $valor['alt_atk_naval'];
                }
                if ($valor['alt_atk_defesa'] != null && $valor['alt_atk_defesa'] >= 0) {
                    $unidade->ataque_defesa = $valor['alt_atk_defesa'];
                }
                $unidade->save();

                $mensagem .= $unidade->nome . ' ';
            }
        }
        $this->view->texto = $funcoes->mensagem($mensagem . ' - alterado com sucesso!', 'alerta');
    }

}
