<?php

namespace app\controllers;

use app\models\Alianca;
use app\models\AliancaMembro;
use app\models\AliancaNivel;
use app\models\AliancaRecrutamento;
use app\models\Funcoes;
use app\models\Mensagem;
use app\models\MensagemTexto;
use app\models\MensagemTextoUsuario;
use projectws\libs\bridges\DB;
use projectws\libs\Request;
use projectws\mvc\Controller;

class AliancaController extends Controller {

    const PERMISSAO = [
        1 => 'Nome',
        2 => 'Recrutamento',
        3 => 'Descrição',
        4 => 'Descrição Interna',
        5 => 'Transferir',
        6 => 'Mensagem',
        7 => 'Expulsar'
    ];
    
    private $membro;
    private $usuarioLogado;
    private $alianca;
    
    public function index() {
        $this->verificaAlianca();
	
	    if($this->alianca) {
		    $this->view->alianca = $this->alianca;
		    $this->view->logado = $this->usuarioLogado;
		    $this->view->permissoes = explode(',', $this->membro->permissoes);
		
		    return $this->view->render('alianca/index.edge');
	    }
	    
	    return $this->view->render('alianca/busca.edge');
    }

    private function verificaAlianca() {
        $this->usuarioLogado = Funcoes::getUsuario();
        
        $this->membro = Alianca::membro($this->usuarioLogado->id);
        
        if ($this->membro->count()) {
            $this->alianca = Alianca::findFirst($this->membro->id_alianca);
        }
    }
	
    public function busca() {
        return $this->view->render('alianca/busca.edge');
    }

    public function nivel() {
        return $this->view->render('alianca/nivel.edge');
    }
    
    /**
     * Cadastrar um novo nível de permissões
     * @return array
     */
    public function nivelNovo() {
        $this->verificaAlianca();
        
        $nome = Request::getPost('nome');
        
        if(strlen($nome) < 3) {
            return [
                'success' => false,
                'msg' => 'O nome deve conter pelo menos 3 caracteres.'
            ];
        }
        
        $inserted = AliancaNivel::insert([
            'nome' => $nome,
            'id_alianca' => $this->alianca->id
        ]);
        
        if($inserted) {
            return $this->nivelData();
        }
        
        return [
            'success' => false,
            'msg' => 'Não foi possível cadastrar.'
        ];
    }
    
    public function nivelData() {
        $this->verificaAlianca();
        
        $niveis = AliancaNivel
            ::orderBy('nome')
            ->find([
            'id_alianca' => $this->alianca->id
        ])->toArray();
    
        foreach ($niveis as &$nivel) {
            $permis = explode(',', $nivel['permissoes']);
    
            $permissoes = [];
            foreach (AliancaController::PERMISSAO as $cod => $item) {
                $permissoes[] = [
                    'cod' => $cod,
                    'nome' => $item,
                    'checked' => !!in_array($cod, $permis)
                ];
            }
            
            $nivel['permissoes'] = $permissoes;
        }
        
        return [
            'success' => true,
            'niveis' => $niveis
        ];
    }
    
    /**
     * Edita um nível de permissões
     * @param int $id_nivel
     * @return array
     */
    public function nivelEditar($id_nivel = 0) {
        $this->verificaAlianca();
        
        if($id_nivel > 0) {
            $nivel = AliancaNivel::findFirst([
                'id' => $id_nivel,
                'id_alianca' => $this->alianca->id
            ]);
            
            if($nivel->count()) {
                $nome = trim(Request::getPost('nome'));
                $permissoes = (array) Request::getPost('permissoes', true);
                
                if($nivel->nome != $nome) {
                    $nivel->nome = $nome;
                }
    
                $nivel->permissoes = implode(',', $permissoes);
                
                $nivel->save();
    
                return $this->nivelData();
            }
            
            return [
                'success' => false,
                'msg' => 'Patente não encontrada.'
            ];
        }
    }
    
    /**
     * Remove um nível
     * @param int $id_nivel
     * @return array
     */
    public function nivelRemover($id_nivel = 0) {
        if($id_nivel) {
            $this->verificaAlianca();
            
            $nivel = AliancaNivel::findFirst([
                'id' => $id_nivel,
                'id_alianca' => $this->alianca->id
            ]);
    
            if($nivel->count()) {
                $membroNivel = AliancaMembro::find([
                    'id_alianca' => $this->alianca->id,
                    'id_nivel' => $nivel->id
                ])->count();
                
                if($membroNivel) {
                    return [
                        'success' => false,
                        'msg' => 'Deve alterar a patente de todos os jogadores antes de remover.'
                    ];
                }
                
                $nivel->delete();
                
                return $this->nivelData();
            }
        }
    
        return [
            'success' => false,
            'msg' => 'Patente não encontrada.'
        ];
    }
    
    /**
     * Altera o nível de um membro da aliança
     * @param int $id_membro
     * @return array
     */
    public function membroNivelAlterar($id_membro = 0) {
        if($id_membro) {
            $this->verificaAlianca();
            
            $membro = AliancaMembro::findFirst([
                'id' => $id_membro,
                'id_alianca' => $this->alianca->id
            ]);
    
            if($membro->id_usuario == $this->membro->id_usuario) {
                $return = $this->getAll();
                
                $return['success'] = false;
                $return['msg'] = 'Não é possível alterar patente de fundador.';
                
                return $return;
            }
            
            if($membro->count()) {
                $id_nivel = Request::getPost('id_nivel');
    
                /**
                 * Caso não informe nenhum nível é atualizado para nenhum
                 */
                if(!$id_nivel) {
                    $membro->id_nivel = null;
    
                    $membro->save();
    
                    return [
                        'success' => true,
                        'msg' => 'Patente alterada.'
                    ];
                }
                
                $nivel = AliancaNivel::findFirst([
                    'id' => $id_nivel,
                    'id_alianca' => $this->alianca->id
                ]);
                
                if($nivel->count()) {
                    $membro->id_nivel = $id_nivel;
                    
                    $membro->save();
                    
                    return [
                        'success' => true,
                        'msg' => 'Patente alterada.'
                    ];
                }
                
                return [
                    'success' => false,
                    'msg' => 'Nível não encontrado.'
                ];
            }
            
            return [
                'success' => false,
                'msg' => 'Membro não encontrado.'
            ];
        }
    }
    
    /**
     * Retorna todas as informações da aliança
     * @return array
     */
    public function getAll() {
	    $this->verificaAlianca();
	
	    if($this->alianca) {
	    	$alianca = $this->alianca;
	    	
		    $membros = Alianca::membros($this->membro->id_alianca);
		
		    $usuarios = Alianca::recrutamento($this->membro->id_alianca);
		    
            $niveis = $this->nivelData();
        
            return [
                'success' => true,
                'alianca' => $alianca->toArray(),
                'membros' => $membros->toArray(),
                'usuarios' => $usuarios->toArray(),
                'niveis' => $niveis['niveis'],
                'permissoes' => $this->membro->permissoes
                    ? explode(',', $this->membro->permissoes)
                    : []
            ];
	    }
	    
    	return [
    		'success' => true,
		    'alianca' => null,
		    'membros' => null,
		    'usuarios' => null,
		    'aliancas' => []
	    ];
    }

    public function propriedades() {
        $this->verificaAlianca();

        /*
          Permissoes
          1 = alterar nome,
          2 = recrutamento,
          3 = descricao,
          4 = descricao_interna,
          5 = transferir
          6 = enviar msg
          7 = bloquear / expulsar
         */

        if (\Request::getPost()) {
            $permissoes = explode(',', $this->membro->permissoes);

            switch (\Request::getPost('acao')) {
                case 'nome':
                    if (array_search(1, $permissoes) !== false) {
                        $existe = Alianca
                            ::where('nome', '=', \Request::getPost('value'))
                            ->where('id', '<>', $this->alianca->id)
                            ->row();

                        if ($existe->count() == 0) {
                            $Alianca = $this->alianca;

                            $Alianca->nome = \Request::getPost('value');
                            if($Alianca->save()) {
	                            return [
		                            'success' => true,
		                            'msg' => 'Nome da aliança alterado.'
	                            ];
                            }
                        }
                    } else {
                        return [
                            'success' => false,
                            'msg' => 'Você não tem permissão para esta ação.'
                        ];
                    }

                    break;

                case 'descricao':
                    if (array_search(3, $permissoes) !== false) {
                        $Alianca = $this->alianca;

                        $Alianca->descricao = \Request::getPost('value');
                        if($Alianca->save()) {
	                        return [
		                        'success' => true,
		                        'msg' => 'Descrição alterada.'
	                        ];
                        }
                    }
                    break;

                case 'descricao_interna':
                    if (array_search(3, $permissoes) !== false) {
                        $Alianca = $this->alianca;

                        $Alianca->descricao_interna = \Request::getPost('value');
                        
	                    if($Alianca->save()) {
	                    	return [
	                    		'success' => true,
			                    'msg' => 'Descrição interna alterada.'
		                    ];
	                    }
                    }

                    break;

                case 'recrutamento':
                    if (array_search(2, $permissoes) !== false) {
                        $Alianca = $this->alianca;

                        $Alianca->recrutamento = (\Request::getPost('value') ? true : false);
	                    
                        if($Alianca->save()) {
	                        return [
		                        'success' => true,
		                        'msg' => 'Recrutamento alterado.'
	                        ];
                        }
                    }

                    break;

                case 'bloquear':
                    if (array_search(7, $permissoes) !== false) {
	                    $usuario = Funcoes::getUsuario();
                    	
	                    $alianca = $this->alianca;
                    	
                        $id_usuario = \Request::getPost('value');
	
	                    if($id_usuario == $usuario->id_usuario) {
		                    return [
			                    'success' => false,
			                    'msg' => 'Não pode expulsar a sí próprio.'
		                    ];
	                    }
	                    
	                    $membro = AliancaMembro::findFirst([
	                    	'id_alianca' => $alianca->id,
		                    'id_usuario' => $id_usuario
	                    ]);
	                    pd($membro);
	                    $membro->bloqueio = date('Y-m-d H:i:s');
	                    
                        if($membro->save()) {
	                        return [
		                        'success' => true,
		                        'msg' => 'Membro expulso.'
	                        ];
                        }
                    }

                    break;

                case 'expulsar':
                    if (array_search(7, $permissoes) !== false) {
                    	$usuario = Funcoes::getUsuario();
                    	
	                    $alianca = $this->alianca;
                    	
                        $id_usuario = \Request::getPost('value');
	                    
	                    if($id_usuario == $usuario->id_usuario) {
	                    	return [
	                    		'success' => false,
			                    'msg' => 'Não pode expulsar a sí próprio.'
		                    ];
	                    }
	                    
	                    $membro = AliancaMembro::findFirst([
	                    	'id_alianca' => $alianca->id,
		                    'id_usuario' => $id_usuario
	                    ]);
	                    
                        if($membro->delete()) {
	                        return [
		                        'success' => true,
		                        'msg' => 'Membro expulso.'
	                        ];
                        }
                    } else {
                    	return [
                    		'success' => false,
		                    'msg' => 'Você não tem permissão para esta ação.'
	                    ];
                    }

                    break;
            }
	
	        return [
		        'success' => false,
		        'msg' => 'Não foi possível fazer alteração.'
	        ];
        }
    }
    
    public function recrutamento() {
        $this->verificaAlianca();

        if (Request::isMethod('POST')) {
            $this->view->disable();

            if (\Request::getPost('acao') == 'excluir') {
                $AliancaRecrutamento = AliancaRecrutamento::findFirst(Request::getPost('id'));

                $AliancaRecrutamento->delete();

                echo 1;
            } elseif (\Request::getPost('acao') == 'aceitar') {
                $AliancaRecrutamento = AliancaRecrutamento::findFirst(Request::getPost('id'));

                $AliancaMembros = new AliancaMembro();

                $AliancaMembros->id_alianca = $AliancaRecrutamento->id_alianca;
                $AliancaMembros->id_usuario = $AliancaRecrutamento->id_usuario;
                $AliancaMembros->nivel = 1;

                $AliancaMembros->save();

                $AliancaRecrutamento->delete();

                echo 1;
            }
        } else {
            $this->view->usuarios = $this->model->recrutamento($this->membro->id_alianca);
        }

        return $this->view->render('alianca/recrutamento');
    }
    
    /**
     * Faz o alistamento
     * @return array
     */
    public function alistar() {
        $id_alianca = Request::getPost('id_alianca');
        $mensagem = Request::getPost('mensagem');
        
        if(!$mensagem) {
            return [
                'success' => false,
                'msg' => 'Deve informar uma mensagem.'
            ];
        }
        
        $usuario = Funcoes::getUsuario();
        
        if($id_alianca > 0) {
            $alianca = Alianca::findFirst($id_alianca);
            
            if($alianca->count() == 0) {
                return [
                    'success' => false,
                    'msg' => 'Aliança não encontrada.'
                ];
            }
            
            if($alianca->recrutamento == false) {
                return [
                    'success' => false,
                    'msg' => 'Esta aliança não está recrutando no momento.'
                ];
            }
            
            $recrutamento = new AliancaRecrutamento();
            
            $recrutamento->id_usuario = $usuario->id;
            $recrutamento->id_alianca = $id_alianca;
            $recrutamento->mensagem = $mensagem;
            $recrutamento->save();
            
            return [
                'success' => true,
                'msg' => 'Alistamento realizado. Em breve receberá uma resposta.'
            ];
        }
        
        return [
            'success' => false,
            'msg' => 'Operação não permitida.'
        ];
    }
    
    /**
     * Aceita como novo membro
     * @param int $id_recrutamento
     * @return array
     */
    public function aceitarRecrutamento($id_recrutamento = 0) {
        try {
            DB::begin();
            
            $this->verificaAlianca();
    
            $alianca = $this->alianca;
            $membro = $this->membro;
    
            $permissoes = explode(',', $membro->permissoes);
    
            /**
             * Se tiver permissão executa
             * 2 = RECRUTAMENTO
             */
            if (in_array(2, $permissoes)) {
                $recrutamento = AliancaRecrutamento
                    ::where('id', '=', $id_recrutamento)
                    ->row();
        
                if ($recrutamento->id_alianca != $alianca->id) {
                    return [
                        'success' => false,
                        'msg' => 'Operação não permitida.'
                    ];
                }
                
                $mensagem = new Mensagem();
                $mensagem->id_usuario = $membro->id_usuario;
                $mensagem->tipo = 3;
                $mensagem->assunto = 'Recrutamento';
                $id_mensagem = $mensagem->save();
                
                if(!$id_mensagem) {
                    throw new \Exception('Parece que algo deu errado aqui.');
                }
                
                $mensagemTexto = new MensagemTexto();
                $mensagemTexto->id_mensagem = $id_mensagem;
                $mensagemTexto->id_remetente = $membro->id_usuario;
                $mensagemTexto->mensagem = 'Bem vindo a aliança ' . $alianca->nome . '!';
                $save = $mensagemTexto->save();
    
                if(!$save) {
                    throw new \Exception('Parece que algo deu errado aqui.');
                }
        
                $mensagemUsuario = new MensagemTextoUsuario();
                $mensagemUsuario->id_mensagem = $id_mensagem;
                $mensagemUsuario->id_usuario = $recrutamento->id_usuario;
                $save = $mensagemUsuario->save();
    
                if(!$save) {
                    throw new \Exception('Parece que algo deu errado aqui.');
                }
        
                $aliancaMembro = new AliancaMembro();
        
                $aliancaMembro->id_alianca = $alianca->id;
                $aliancaMembro->id_usuario = $recrutamento->id_usuario;
                $save = $aliancaMembro->save();
                
                if(!$save) {
                    throw new \Exception('Parece que algo deu errado aqui.');
                }
        
                $recrutamento->delete();
        
                $retorno = $this->getAll();
                $retorno['msg'] = 'Recrutamento aprovado.';
                
                DB::commit();
        
                return $retorno;
                
            }
            
            throw new \Exception('Você não têm permissão para recrutamentos.');
        } catch (\Exception $e) {
            DB::rollback();
            
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }
    }

    /**
     * Rejeita um pedido de recrutamento
     * @param int $id_recrutamento
     * @return array
     */
    public function rejeitarRecrutamento($id_recrutamento = 0) {
        $this->verificaAlianca();
        
        $membro = $this->membro;
    
        $permissoes = explode(',', $membro->permissoes);
    
        /**
         * Se tiver permissão executa
         * 2 = RECRUTAMENTO
         */
        if(in_array(2, $permissoes)) {
            $recrutamento = AliancaRecrutamento
                ::where('id', '=', $id_recrutamento)
                ->row();
    
            if($recrutamento->id_alianca != $this->alianca->id) {
                return [
                    'success' => false,
                    'msg' => 'Operação não permitida.'
                ];
            }
            
            $mensagem = new Mensagem();
            $mensagem->id_usuario = $membro->id_usuario;
            $mensagem->tipo = 3;
            $mensagem->assunto = 'Recrutamento';
            $id_mensagem = $mensagem->save();
    
            /**
             * Mensagem do solicitante
             */
            $mensagemTexto = new MensagemTexto();
            $mensagemTexto->id_mensagem = $id_mensagem;
            $mensagemTexto->id_remetente = $recrutamento->id_usuario;
            $mensagemTexto->mensagem = $recrutamento->mensagem;
            $mensagemTexto->data_envio = $recrutamento->datahora;
            $mensagemTexto->save();
    
            /**
             * Mensagem do usuário logado
             */
            $mensagemTexto = new MensagemTexto();
            $mensagemTexto->id_mensagem = $id_mensagem;
            $mensagemTexto->id_remetente = $membro->id_usuario;
            $mensagemTexto->mensagem = 'Sua solicitação de alistamento foi recusada.';
            $mensagemTexto->save();
    
            $mensagemUsuario = new MensagemTextoUsuario();
            $mensagemUsuario->id_mensagem = $id_mensagem;
            $mensagemUsuario->id_usuario = $recrutamento->id_usuario;
            $mensagemUsuario->save();
            
            $recrutamento->delete();
    
            $retorno = $this->getAll();
            $retorno['msg'] = 'Recrutamento rejeitado.';
    
            return $retorno;
        }
        
        return [
            'success' => false,
            'msg' => 'Você não têm permissão para recrutamentos.'
        ];
    }
    
    /**
     * Abandona a aliança
     * @return array
     */
    public function abandonar() {
        $this->verificaAlianca();
        
        if(AliancaMembro::where('id', '=', $this->membro->id)->delete()) {
            return [
                'success' => true,
                'msg' => 'Agora você não faz mais parte desta aliança.',
                'redirect' => url('game')
            ];
        }
        
        return [
            'success' => false,
            'msg' => 'Não foi possível realizar esta operação.'
        ];
    }
    
    public function nova() {
        $usuario = Funcoes::getUsuario();
        
        $aliancas = $this->buscaAliancas();
        
        if(count($aliancas) == 0) {
            $nome = trim(Request::getPost('texto'));
            
            if(strlen($nome) > 15) {
                return [
                    'success' => false,
                    'msg' => 'O nome escolhido é muito grande.'
                ];
            }
            
            DB::begin();
            
            $alianca = new Alianca();
            
            $alianca->nome = $nome;
            $alianca->fundador = $usuario->id;
            $id_alianca = $alianca->save();
            
            if($id_alianca) {
                $aliancaNivel = new AliancaNivel();
                
                $aliancaNivel->id_alianca = $id_alianca;
                $aliancaNivel->nome = 'Líder';
                $aliancaNivel->permissoes = implode(',', [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7
                ]);
    
                $id_nivel = $aliancaNivel->save();
                
                if($id_nivel) {
                    $aliancaMembro = new AliancaMembro();
    
                    $aliancaMembro->id_alianca = $id_alianca;
                    $aliancaMembro->id_usuario = $usuario->id;
                    $aliancaMembro->id_nivel = $id_nivel;
    
                    if ($aliancaMembro->save()) {
                        DB::commit();
        
                        return [
                            'success' => true,
                            'msg' => 'Aliança criada.',
                            'redirect' => url('game')
                        ];
                    }
                }
            }
            
            DB::rollback();
            
            return [
                'success' => false,
                'msg' => 'Não foi possível criar aliança.'
            ];
        }
        
        return [
            'success' => false,
            'msg' => 'Já existe uma aliança com este nome.'
        ];
    }

	/**
	 * Faz a query para buscar as alianças
	 * @return array
	 */
    public function buscaAliancas() {
    	$texto = trim(Request::getPost('texto'));
	    
	    return Alianca
		    ::select('
                a.nome,
                count(e.id_usuario) as membros,
                round(sum(e.pontos_construcoes) / count(e.id_usuario)) as pontos_membro,
                sum(e.pontos_construcoes) as pontos'
		    )
		    ->alias('a')
		    ->join('alianca_membro am', 'am.id_alianca', '=', 'a.id')
		    ->leftJoin('estatisticas e', 'e.id_usuario = am.id_usuario')
		    ->where('a.nome', 'ilike', "%$texto%")
		    ->whereBool('a.recrutamento')
		    ->groupBy('a.id')
		    ->orderBy('sum(e.pontos_construcoes) DESC')
		    ->rows()
		    ->toArray();
    }
    
    /**
     * Exclusão de aliança
     * @return array
     */
    public function excluir() {
        $this->verificaAlianca();
        
        if($this->alianca->fundador != $this->membro->id_usuario) {
            return [
                'success' => false,
                'msg' => 'Operação não permitida.'
            ];
        }
        
        DB::begin();
        
        $delete = AliancaMembro::delete([
            'id_alianca' => $this->alianca->id
        ]);
        
        if(!$delete) {
            DB::rollback();
            
            return [
                'success' => false,
                'msg' => 'Não foi possível realizar exclusão.'
            ];
        }
    
        $delete = AliancaNivel::delete([
            'id_alianca' => $this->alianca->id
        ]);
    
        if(!$delete) {
            DB::rollback();
        
            return [
                'success' => false,
                'msg' => 'Não foi possível realizar exclusão.'
            ];
        }
    
        $delete = Alianca::delete([
            'id' => $this->alianca->id
        ]);
    
        if(!$delete) {
            DB::rollback();
        
            return [
                'success' => false,
                'msg' => 'Não foi possível realizar exclusão.'
            ];
        }
        
        DB::commit();
        
        return [
            'success' => true,
            'msg' => 'Aliança excluída.',
            'redirect' => url('game')
        ];
    }
    
}
