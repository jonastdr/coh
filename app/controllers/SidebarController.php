<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadeRecrutamento;
use app\models\Construcao;
use app\models\Funcoes;
use app\models\Pesquisa;
use projectws\mvc\Controller;

class SidebarController extends Controller {

    public function index() {
        return $this->view->render('sidebar/index.edge');
    }
    
    public function data() {
    	$cidade = Funcoes::getCidade();
    	
	    $construcao = $cidade->construcao;
	
	    $pesquisa = $cidade->pesquisa;
	
	    if($construcao != 0) {
		    $construcao = explode(',', $construcao);
		
		    $construcaoItem = Construcao::getForSide($cidade->id, $construcao[1]);
		    
		    $construcaoItem = [
		    	'id' => $construcaoItem->id,
			    'nome' => $construcaoItem->nome,
			    'level' => $construcaoItem->level,
			    'restante' => $construcao[0] - time()
		    ];
	    } else {
		    $construcaoItem = null;
	    }
	
	    if($pesquisa != 0) {
		    $pesquisa = explode(',', $pesquisa);
		
		    $pesquisaItem = Pesquisa::getForSide($cidade->id, $pesquisa[1]);
		
		    $pesquisaItem = [
			    'id' => $pesquisaItem->id,
			    'nome' => $pesquisaItem->nome,
			    'level' => $pesquisaItem->level,
			    'restante' => $pesquisa[0] - time()
		    ];
	    } else {
		    $pesquisaItem = null;
	    }
	    
	    $unidadeRecrutamento = CidadeRecrutamento
		    ::select(
		    	'cr.*',
		        'unidade.nome'
		    )
		    ->alias('cr')
		    ->join('unidade')
		    ->limit(1)
		    ->orderBy('cr.horario ASC')
		    ->findFirst([
	    	'cr.id_cidade' => $cidade->id
	    ]);
	    
	    if($unidadeRecrutamento->count() > 0) {
	    	$unidadeItem = [
	    		'id' => $unidadeRecrutamento->id_unidade,
			    'nome' => $unidadeRecrutamento->nome,
			    'quantidade' => $unidadeRecrutamento->quantidade,
			    'restante' => $unidadeRecrutamento->horario + $unidadeRecrutamento->duracao - time()
		    ];
	    } else {
	    	$unidadeItem = null;
	    }
	    
	    return [
	    	'success' => true,
		    'construcao' => $construcaoItem,
		    'pesquisa' => $pesquisaItem,
		    'unidade' => $unidadeItem
	    ];
    }

}
