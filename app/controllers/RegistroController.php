<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\Usuario;
use projectws\libs\Encrypt;
use projectws\libs\Request;
use projectws\libs\Validator;
use projectws\mvc\Controller;

class RegistroController extends Controller {

    public function index() {
        return $this->view->render('registro/index.edge');
    }

    public function registrar() {
	    $this->view->errorsLogin = [];
	    $this->view->errorsRegister = [];
	
	    if (\Request::getPost()) {
	        Validator::custom('senha_confirma', function () {
	            return Request::getPost('senha_confirma') == Request::getPost('senha_registro');
            });
            
		    $validacao = Validator::run([
			    'rules' => [
				    'email' => 'required|unique:usuario.email',
				    'nome_registro' => 'required|unique:usuario.nome',
				    'senha_registro' => 'required',
                    'senha_confirma' => 'required|senha_confirma'
			    ],
			    'messages' => [
				    'email.required' => 'O email é obrigatório.',
				    'email.email' => 'Email inválido.',
				    'email.unique' => 'Email já cadastrado.',
				    'nome_registro.required' => 'O nome é obrigatório.',
				    'nome_registro.unique' => 'O nome é obrigatório.',
                    'senha_registro.required' => 'A Senha é obrigatório.',
                    'senha_confirma.required' => 'A confirmação de senha é obrigatório.',
                    'senha_confirma.senha_confirma' => 'As senhas não conferem.'
			    ]
		    ]);
		
		    if ($validacao->fail()) {
			    $this->view->errorsRegister = $validacao->showMessagesArr();
		    } else {
			    $novoUsuario = new Usuario();
			
			    $novoUsuario->nome = \Request::getPost('nome_registro');
			    $novoUsuario->senha = Encrypt::make(\Request::getPost('senha_registro'));
			    $novoUsuario->email = \Request::getPost('email');
			    $novo = $novoUsuario->save();
			
			    if ($novo) {
				    $usuario = Usuario::findFirst($novo);
				
				    $cidade = Cidade::nova($usuario);
				
				    if ($cidade) {
					    $usuario->logar();
					
					    \Response::redirect()->route('game');
				    }
			    }
		    }
	    }
	
	    return $this->view->render('login.edge');
    }

}
