<?php

namespace app\controllers;

use app\models\Cidade;
use app\models\CidadePesquisa;
use app\models\Expedicao;
use app\models\Funcoes;
use app\models\IlhaCidade;
use app\models\Relatorio;
use projectws\libs\Request;
use projectws\mvc\Controller;

class EspionagemController extends Controller {
    
    const VELOCIDADE_ESPIAO = 22000;
    
    /**
     * Visualização da tela
     * @return null|string
     */
    public function index() {
        return $this->view->render('expedicao/espionagem.edge');
    }
    
    /**
     * Visualização da tela de armazem
     * @return null|string
     */
    public function cidade() {
        return $this->view->render('expedicao/espionagem-cidade.edge');
    }
    
    /**
     * Visualização da tela de armazem
     * @return null|string
     */
    public function relatorio() {
        return $this->view->render('relatorio/espionagem.edge');
    }
    
    /**
     * Traz os dados da tela de espionagem
     * @param $codigo
     * @return array
     */
    public function getAll($codigo) {
        $relatorio = Relatorio::where('link', '=', $codigo)->row();
    
        if($relatorio->count() > 0) {
            $dados = unserialize(base64_decode($relatorio->dados));
            
            if($dados->exito) {
                return [
                    'success' => true,
                    'data' => $dados->data,
                    'investimento' => $dados->investimento,
                    'exito' => $dados->exito,
                    'cidade' => $dados->cidade->toArray(),
                    'unidades' => $dados->unidades->toArray(),
                    'pesquisas' => $dados->pesquisas->toArray(),
                    'construcoes' => $dados->construcoes->toArray()
                ];
            }
            
            return [
                'success' => true,
                'data' => $dados->data,
                'investimento' => $dados->investimento,
                'exito' => false,
                'cidade' => $dados->cidade->toArray(),
                'unidades' => [],
                'pesquisas' => [],
                'construcoes' => []
            ];
        }
        
        return [
            'success' => false,
            'msg' => 'Relatório não encontrado.'
        ];
    }
    
    /**
     * Armazena mais dinheiro
     * @return array
     */
    public function armazenar() {
        $cidade = Funcoes::getCidade();
        
        $quantidade = Request::getPost('quantidade');
    
        /**
         * Seleciona a pesquisa de espionagem
         */
        $pesquisa = CidadePesquisa::findFirst([
            'id_cidade' => $cidade->id,
            'id_pesquisa' => 6
        ]);
    
        if($pesquisa->count() > 0) {
            $quantidade_maxima = 10000 + ($pesquisa->level * 10000);
        } else {
            $quantidade_maxima = 10000;
        }
    
        /**
         * Diminui a quantidade que está disponível para o usuário na cidade atualmente selecionada
         */
        $quantidade_maxima -= $cidade->espionagem_recurso;
        
        if($quantidade > $quantidade_maxima) {
            return [
                'success' => false,
                'msg' => 'Limite de armazenamento atingido.'
            ];
        }
        
        if($quantidade <= $cidade->dinheiro) {
            $cidade = Cidade::findFirst($cidade->id);
            
            $cidade->espionagem_recurso += $quantidade;
            $cidade->dinheiro -= $quantidade;
            $salvo = $cidade->save();
            
            if($salvo) {
                $return = $this->armazem();
                
                $return['msg'] = 'Armazenamento concluído.';
                
                return $return;
            }
        }
        
        return [
            'success' => false,
            'msg' => 'Você não têm esta quantia de dinheiro.'
        ];
    }
    
    /**
     * Traz os dados de armazem
     * @return array
     */
    public function armazem() {
        $cidade = Funcoes::getCidade();
    
        /**
         * Seleciona a pesquisa de espionagem
         */
        $pesquisa = CidadePesquisa::findFirst([
            'id_cidade' => $cidade->id,
            'id_pesquisa' => 6
        ]);
        
        if($pesquisa->count() > 0) {
            $quantidade_maxima = 10000 + ($pesquisa->level * 10000);
            $pesquisa_nivel = $pesquisa->level;
        } else {
            $quantidade_maxima = 10000;
            $pesquisa_nivel = 0;
        }
    
        /**
         * Diminui a quantidade que está disponível para o usuário na cidade atualmente selecionada
         */
        $quantidade_maxima -= $cidade->espionagem_recurso;
        
        return [
            'success' => true,
            'quantidade_maxima' => $quantidade_maxima,
            'pesquisa_nivel' => $pesquisa_nivel,
            'armazenado' => $cidade->espionagem_recurso
        ];
    }
    
    public function missao($cod_ilha, $cod_cidade) {
        //Cidade local
        $cidade = Funcoes::getCidade();
        
        $ilhaCidade = IlhaCidade
            ::select(
                'ic.x + i.x AS x',
                'ic.y + i.y AS y'
            )
            ->alias('ic')
            ->join('ilha i', 'i.id', '=', 'ic.id_ilha')
            ->where('i.id', '=', $cod_ilha)
            ->where('ic.cod_cidade', '=', $cod_cidade)
            ->row();
    
        $cidadeExp = Cidade
            ::select(
                'c.*'
            )
            ->alias('c')
            ->findFirst([
                'c.cod_ilha' => $cod_ilha,
                'c.cod_cidade' => $cod_cidade
            ]);
    
        $distancia = Funcoes::getDistancia($cidade, $ilhaCidade);
        
        $duracao = Funcoes::getDuracao(EspionagemController::VELOCIDADE_ESPIAO, $distancia);
    
        /**
         * Seleciona a pesquisa de espionagem
         */
        $pesquisa = CidadePesquisa::findFirst([
            'id_cidade' => $cidade->id,
            'id_pesquisa' => 6
        ]);
    
        if($pesquisa->count() > 0) {
            $pesquisa_nivel = $pesquisa->level;
        } else {
            $pesquisa_nivel = 0;
        }
        
        return [
            'success' => true,
            'cod_ilha' => $cod_ilha,
            'cod_cidade' => $cod_cidade,
            'quantidade_maxima' => $cidade->espionagem_recurso,
            'pesquisa_nivel' => $pesquisa_nivel,
            'distancia' => $distancia,
            'duracao' => $duracao,
            'destino_posicao' => [
                'x' => $ilhaCidade->x,
                'y' => $ilhaCidade->y
            ],
            'cidade' => $cidadeExp->toArray(),
            'now' => time()
        ];
    }
    
    public function enviar() {
        //Cidade local
        $cidade = Funcoes::getCidade();
    
        //Dados da cidade adversaria
        $cod_ilha = \Request::getPost('cod_ilha');
        $cod_cidade = \Request::getPost('cod_cidade');
        
        $ilhaCidade = IlhaCidade
            ::select(
                'ic.x + i.x AS x',
                'ic.y + i.y AS y'
            )
            ->alias('ic')
            ->join('ilha i', 'i.id', '=', 'ic.id_ilha')
            ->where('i.id', '=', $cod_ilha)
            ->where('ic.cod_cidade', '=', $cod_cidade)
            ->row();
    
        /**
         * Cidade do adversário
         */
        $cidadeAdv = Cidade::findFirst([
            'cod_ilha' => $cod_ilha,
            'cod_cidade' => $cod_cidade,
        ]);
    
        $distancia = Funcoes::getDistancia($cidade, $ilhaCidade);
    
        $duracao = Funcoes::getDuracao(EspionagemController::VELOCIDADE_ESPIAO, $distancia);
        
        //Seleciona a tabela
        $expedicao = new Expedicao();
        $expedicao->id_cidade_atk = $cidade->id;
        $expedicao->id_cidade_def = $cidadeAdv->id;
        $expedicao->missao = Expedicao::ESPIONAGEM;
        $expedicao->duracao = $duracao;
        $expedicao->horario = time();
        $expedicao->dinheiro = (int)\Request::getPost('dinheiro');
        $expedicao->cod_ilha = $cod_ilha;
        $expedicao->cod_cidade = $cod_cidade;
        $id_expedicao = $expedicao->save();
        
        Cidade::update([
            'espionagem_recurso' => $cidade->espionagem_recurso - (int)\Request::getPost('dinheiro')
        ], [
            'id' => $cidade->id
        ]);
        
        if($id_expedicao) {
            return [
                'success' => true,
                'msg' => 'Seus espiões foram enviados.'
            ];
        }
        
        return [
            'success' => false,
            'msg' => 'Não foi possível enviar espiões.'
        ];
    }

}