<?php

namespace app\controllers;

use app\models\Estatisticas;
use projectws\mvc\Controller;

class EstatisticasController extends Controller {
	
	/**
	 * Carrega a view para a tela
	 * @return null|string
	 */
    public function index() {
        return $this->view->render('estatisticas/index.edge');
    }
	
	/**
	 * Retorna todos os dados para a tela
	 */
    public function getAll() {
    	$usuarios = Estatisticas::usuario();
    	$aliancas = Estatisticas::alianca();
    	$ataques = Estatisticas::ataque();
    	$defesas = Estatisticas::defesa();
    	
    	return [
    		'success' => true,
		    'usuarios' => $usuarios->toArray(),
		    'aliancas' => $aliancas->toArray(),
		    'ataques' => $ataques->toArray(),
		    'defesas' => $defesas->toArray()
	    ];
    }

}
