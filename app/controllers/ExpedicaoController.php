<?php

namespace app\controllers;

use app\models\CidadePesquisa;
use app\models\Expedicao;
use app\models\Cidade;
use app\models\CidadeUnidade;
use app\models\ExpedicaoUnidade;
use app\models\Funcoes;
use app\models\IlhaCidade;
use app\models\Notificacao;
use projectws\libs\bridges\DB;
use projectws\libs\orm\FailException;
use projectws\libs\orm\Join;
use projectws\mvc\Controller;

class ExpedicaoController extends Controller {

    public function index() {
	    return $this->view->render('expedicao/missao.edge');
    }

    public function missao($cod_ilha, $cod_cidade) {
    	$ilhaCidade = IlhaCidade
		    ::select(
			    'ic.x + i.x AS x',
			    'ic.y + i.y AS y'
		    )
		    ->alias('ic')
		    ->join('ilha i', 'i.id', '=', 'ic.id_ilha')
	        ->where('i.id', '=', $cod_ilha)
	        ->where('ic.cod_cidade', '=', $cod_cidade)
	        ->row();
    	
        $cidadeExp = Cidade
	        ::select(
	        	'c.*'
	        )
	        ->alias('c')
            ->findFirst([
		        'c.cod_ilha' => $cod_ilha,
		        'c.cod_cidade' => $cod_cidade
	        ]);

        //usuario logado
        $cidade = Funcoes::getCidade();
	    
	    if($cidadeExp->id == $cidade->id) {
	    	return [
	    		'success' => false,
			    'msg' => 'Não é possível enviar para a mesma cidade.'
		    ];
	    }

        $unidades = CidadeUnidade::getUnidades($cidade->id);

	    if($unidades->count() == 0) {
	    	return [
	    		'success' => false,
			    'msg' => 'Não há unidades para enviar.'
		    ];
	    }
	    
        //View
        $minha_posicao = [
        	'x' => $cidade->x,
	        'y' => $cidade->y
        ];

        $missoes = [
        	[
        		'id' => Expedicao::ATACAR,
		        'nome' => 'ataque'
	        ],
	        [
		        'id' => Expedicao::TRANSPORTE,
		        'nome' => 'transporte'
	        ],
	        [
		        'id' => Expedicao::DEFENDER,
		        'nome' => 'defender'
	        ]
        ];
	    
	    $missoes[] = [
	    	'id' => Expedicao::COLONIZACAO,
		    'nome' => 'colonização'
	    ];

        return [
        	'success' => true,
            'cod_ilha' => $cod_ilha,
            'cod_cidade' => $cod_cidade,
	        'missoes' => $missoes,
	        'unidades' => $unidades->toArray(),
	        'minha_posicao' => $minha_posicao,
	        'destino_posicao' => [
	        	'x' => $ilhaCidade->x,
		        'y' => $ilhaCidade->y
	        ],
	        'cidade' => $cidadeExp->toArray(),
	        'now' => time(),
            'jogoVelocidade' => 1
        ];
    }

    public function enviar() {
        return DB::saveOrFail(function (FailException $fail) {
	        //Cidade local
	        $cidade = Funcoes::getCidade();
	
	        //Dados da cidade adversaria
	        $cod_ilha = \Request::getPost('cod_ilha');
	        $cod_cidade = \Request::getPost('cod_cidade');
    
            /**
             * Missões em andamento
             */
            $expedicoes = Expedicao::getExpedicoesEnvio();
            
            $nivelPesquisa = CidadePesquisa
                ::where('id_cidade', '=', $cidade->id)
                ->where('id_pesquisa', '=', 1)
                ->row();
            
            if($nivelPesquisa->count() == 0) {
                $slot = 1;
            } else {
                $slot = $nivelPesquisa->level + 1;
            }
    
            /**
             * Somente é enviado se houver slot disponível para o envio
             * Pesquisa de expedição = ID 1
             */
            if($expedicoes->count() >= $slot) {
                return $fail->addMessage('Não há slot disponível para o envio.');
            }
	        
	        /**
	         * Posição da cidade
	         */
	        $ilhaCidade = IlhaCidade
		        ::select(
			        'ic.x + i.x AS x',
			        'ic.y + i.y AS y'
		        )
		        ->alias('ic')
		        ->join('ilha i', 'i.id', '=', 'ic.id_ilha')
		        ->where('i.id', '=', $cod_ilha)
		        ->where('ic.cod_cidade', '=', $cod_cidade)
		        ->row();
	        
	        /**
	         * Cidade do adversário
	         */
	        $cidadeAdv = Cidade::findFirst([
		        'cod_ilha' => $cod_ilha,
		        'cod_cidade' => $cod_cidade,
	        ]);
	
	        $missao = \Request::getPost('missao');
    
            if($cidade->id == $cidadeAdv->id && $missao == Expedicao::ATACAR) {
                return $fail->addMessage('Você não pode atacar sua própria cidade.');
            }
	        
	        if ($cidadeAdv->count() == 0 && $missao != Expedicao::COLONIZACAO) {
		        return $fail->addMessage('Não há cidade neste local.');
            }
            
            if($cidade->id_usuario == $cidadeAdv->id_usuario && in_array($missao, [Expedicao::ATACAR, Expedicao::COLONIZACAO])) {
            	return $fail->addMessage('Não pode atacar sua própria cidade.');
            }

            //Unidades
            $unidadesPost = \Request::getPost('unidades', true);

            if (count($unidadesPost) == 0) {
	            return $fail->addMessage('O que irá enviar para esta missão?');
            }

            //usado para fazer calculo de recursos possiveis de armazenamento
            $capacidade = 0;

            $velocidadeMax = null;
            $velocidadeMaxAereoNaval = null;
    
            /**
             * Indica que há unidade aérea ou naval
             */
            $unidadeAereaNaval = false;
            
            foreach ($unidadesPost as $id => $quantidade) {
            	if(!$quantidade)
            		continue;
            	
                $unidade = CidadeUnidade
                    ::select(
                        'cidade_unidade.*',
                        'u.capacidade',
                        'u.velocidade',
                        'c.id_tipo as id_tipo_classe'
                    )
                    ->join('unidade u', 'u.id', '=', 'cidade_unidade.id_unidade')
                    ->join('classe c', 'c.id', '=', 'u.id_classe')
                    ->findFirst([
                        'id_cidade' => $cidade->id,
                        'id_unidade' => $id
                    ]);
                
                if(in_array($unidade->id_tipo_classe, [2, 3]))
                    $unidadeAereaNaval = true;

                $unidadesPost[$id] = (Object)[
                    'quantidade' => $quantidade,
                    'detalhes' => $unidade
                ];

                $capacidade += $unidade->capacidade * $quantidade;
    
                /**
                 * Verifica a velocidade minima
                 */
                if (($quantidade > 0 && $unidade->velocidade < $velocidadeMax) OR $velocidadeMax == null)
                    $velocidadeMax = $unidade->velocidade;
    
                /**
                 * Verifica a velocidade minima
                 */
                if(in_array($unidade->id_tipo_classe, [2, 3])) {
                    if (($quantidade > 0 && $unidade->velocidade < $velocidadeMaxAereoNaval) OR $velocidadeMaxAereoNaval == null)
                        $velocidadeMaxAereoNaval = $unidade->velocidade;
                }

                //erro de quantidade
                if ($quantidade > $unidade->quantidade) {
	                return $fail->addMessage('Quantidade não permitida.');
                }
            }
    
            /**
             * Unidades terrestres não podem ser enviadas sem um transporte para outra ilha
             */
            if($cidade->cod_ilha != $cidadeAdv->cod_ilha && $unidadeAereaNaval == false) {
                return $fail->addMessage('Unidades terrestres necessitam ser transportadas.');
            }
    
            /**
             * A velocidade que prevalece é a de unidades aerea e naval
             */
            if($velocidadeMaxAereoNaval !== null) {
                $velocidadeMax = $velocidadeMaxAereoNaval;
            }

            //Velocidade 0 = parado || não é possivel
            if (empty($velocidadeMax))
                return $fail->addMessage('Velocidade não permitida.');

            $distancia = Funcoes::getDistancia($cidade, $ilhaCidade);

            $duracao = Funcoes::getDuracao($velocidadeMax, $distancia);

            $restante = $capacidade;
            $restante -= (int)\Request::getPost('dinheiro');
            $restante -= (int)\Request::getPost('alimento');
            $restante -= (int)\Request::getPost('metal');
            $restante -= (int)\Request::getPost('petroleo');

            //erro ao enviar
            if ($restante < 0) {
                return $fail->addMessage("Falha no envio.");
            }

            //Seleciona a tabela
            $expedicao = new Expedicao();
            $expedicao->id_cidade_atk = $cidade->id;
            $expedicao->id_cidade_def = $cidadeAdv->id;
            $expedicao->missao = $missao;
            $expedicao->duracao = $duracao;
            $expedicao->horario = time();
            $expedicao->dinheiro = (int)\Request::getPost('dinheiro');
            $expedicao->alimento = (int)\Request::getPost('alimento');
            $expedicao->metal = (int)\Request::getPost('metal');
            $expedicao->petroleo = (int)\Request::getPost('petroleo');
	        $expedicao->cod_ilha = $cod_ilha;
	        $expedicao->cod_cidade = $cod_cidade;
            $id_expedicao = $expedicao->save();
	
	        $cidadeAtual = Cidade::findFirst($cidade->id);
	        $cidadeAtual->dinheiro = $cidadeAtual->dinheiro - (int)\Request::getPost('dinheiro');
	        $cidadeAtual->alimento = $cidadeAtual->alimento - (int)\Request::getPost('alimento');
	        $cidadeAtual->metal = $cidadeAtual->metal - (int)\Request::getPost('metal');
	        $cidadeAtual->petroleo = $cidadeAtual->petroleo - (int)\Request::getPost('petroleo');
	        $cidadeAtual->save();
	        
            //Adiciona as unidades
            foreach ($unidadesPost as $id => $obj) {
                if ($obj->quantidade > 0) {
                    $obj->detalhes->quantidade = $obj->detalhes->quantidade - $obj->quantidade;

                    if ($obj->detalhes->quantidade === 0)
                        $obj->detalhes->delete();
                    else
                        $obj->detalhes->save();
                }
                if ($obj->quantidade > 0) {
                    $expedicaoUnidade = new ExpedicaoUnidade();
                    $expedicaoUnidade->id_expedicao = $id_expedicao;
                    $expedicaoUnidade->id_unidade = $id;
                    $expedicaoUnidade->quantidade = $obj->quantidade;
                    $expedicaoUnidade->save();
                }
            }

            switch ($missao) {
                case Expedicao::ATACAR:
                    $notificacaoMsg = 'Está vindo um ataque em sua direção!';
                    $notificacaoTipo = 3;
                    break;

                case Expedicao::TRANSPORTE:
                    $notificacaoMsg = 'Está vindo um frota amigável em sua direção!';
                    $notificacaoTipo = 4;

                    break;

                case Expedicao::DEFENDER:
                    $notificacaoMsg = 'Está vindo um frota amigável em sua direção!';
                    $notificacaoTipo = 5;

                    break;
            }

            if($missao != Expedicao::COLONIZACAO) {
                Notificacao::add(
                    $cidadeAdv->id_usuario,
                    $notificacaoMsg,
                    null,
                    null,
                    $notificacaoTipo,
                    $id_expedicao
                );
            }
        }, function () {//SUCESSO
            return [
            	'success' => true,
	            'msg' => 'Missão enviada.'
            ];
        }, function ($e) {//FALHA
            return [
            	'success' => false,
	            'msg' => implode("\n", $e)
            ];
        });
    }

    public function atividadeAction() {
        //Constroi a visualização das atividades da cidade
        $missoes = [
            1 => 'ataque',
            2 => 'transporte',
            3 => 'defender'
        ];

        $expedicoes = Expedicao::getExpedicoes();

        $atividades = [];
        foreach($expedicoes as $expedicao) {
            $atividade = new \StdClass();

            $atividade->id = $expedicao->id;
            $atividade->atancente = $expedicao->atacante;
            $atividade->defensor = $expedicao->defensor;
            $atividade->missao = $missoes[$expedicao->missao];
            $atividade->duracao = $expedicao->duracao;
            $atividade->volta = $expedicao->volta ? "volta" : "";

            if($expedicao->volta == 0)
                $atividade->tempo = $expedicao->duracao - (time() - $expedicao->horario);
            else
                $atividade->tempo = ($expedicao->duracao * 2) - (time() - $expedicao->horario);

            $atividade->restante = gmdate('H:i:s', $atividade->tempo);

            if($expedicao->volta == 0)
                $atividade->porcentagem = 100 - (($atividade->tempo * 100) / $expedicao->duracao);
            else
                $atividade->porcentagem = 100 - (100 - (($atividade->tempo * 100) / $expedicao->duracao));

            $atividade->direcao = ($expedicao->volta == 0) ? "right" : "left";

            $atividades[] = $atividade;
        }

        $this->view->atividades = $atividades;

        return $this->view->render("expedicao/atividade.edge");
    }

}
