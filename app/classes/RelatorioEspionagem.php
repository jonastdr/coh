<?php
/**
 * Created by PhpStorm.
 * User: jonas
 * Date: 04/11/16
 * Time: 22:16
 */

namespace app\classes;


class RelatorioEspionagem {
    
    public $exito = false;
    
    public $data;
    
    public $investimento = 0;
    
    public $cidade;
    
    public $construcoes;
    public $pesquisas;
    public $unidades;
    
    public function __construct() {
        $this->data = date('Y-m-d H:i:s');
    }
    
}