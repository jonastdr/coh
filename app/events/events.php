<?php

/**
 * A execução é feita pela ordem cadastrada
 */
return [
    'app\events\EstatisticasEvent',
    'app\events\RecursosEvent',
    'app\events\FuncaoEvent',
    'app\events\UnidadesEvent',
    'app\events\ExpedicaoEvent'
];