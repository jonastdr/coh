<?php

namespace app\events;

use app\models\HeroiFuncaoUsuario;
use projectws\libs\Events;
use projectws\mvc\DB;

class FuncaoEvent extends Events {

    protected $active = true;

	/**
	 * Executa o evento
	 */
    public function run() {
        $heroiFuncao = HeroiFuncaoUsuario::find('expiracao <= EXTRACT(EPOCH FROM NOW())');

        foreach ($heroiFuncao as $funcao) {
            $funcao->delete();
        }
    }

	/**
	 * Regras a ser executa para validar o Evento
	 */
    public function rules() {
        return true;
    }
}