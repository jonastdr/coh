<?php

namespace app\events;

use app\models\CidadeRecrutamento;
use app\models\CidadeUnidade;
use projectws\libs\Events;

class UnidadesEvent extends Events {

    protected $active = true;

	/**
	 * Executa o evento
	 */
    public function run() {
        $recrutamentos = CidadeRecrutamento::rows();

        foreach ($recrutamentos as $recrutamento) {
	        $duracao = time() - $recrutamento->horario;
	        
	        $tempoCadaUnidade = $recrutamento->tempo_gasto;
	
	        if($duracao < $recrutamento->duracao)
	            $diff = floor($duracao / $tempoCadaUnidade);
	        else
	        	$diff = $recrutamento->quantidade;
	        
	        //pd($diff, $duracao);
            //Atualização da quantidade de unidades disponíveis na cidade
            if ($diff > 0) {
	            //As unidades estão em uma tabela separada
	            $cidadeUnidade = CidadeUnidade::where('id_cidade', '=', $recrutamento->id_cidade)
		            ->where('id_unidade', '=', $recrutamento->id_unidade)->row();
	
	            if($cidadeUnidade->count() == 0) {
		            $cidadeUnidade->id_cidade = $recrutamento->id_cidade;
		            $cidadeUnidade->id_unidade = $recrutamento->id_unidade;
	            }
	            $cidadeUnidade->quantidade = $cidadeUnidade->quantidade + $diff;
	
	            $cidadeUnidade->save();
	
	            if ($recrutamento->quantidade == $diff) {
		            $recrutamento->delete();
                } else {
		            $recrutamento->quantidade = $recrutamento->quantidade - $diff;
		            
		            $recrutamento->save();
	            }
            }
        }
    }

	/**
	 * Regras a ser executa para validar o Evento
	 */
    public function rules() {
        return true;
    }
}