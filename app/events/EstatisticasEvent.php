<?php

namespace app\events;

use app\models\Estatisticas;
use app\models\Funcoes;
use projectws\libs\Database;
use projectws\libs\Events;

class EstatisticasEvent extends Events {

    protected $active = true;

	/**
	 * Executa o evento
	 */
    public function run() {
        $estatisticas = new Estatisticas();

        $estatisticasAtual = Estatisticas::findFirst();

        if($estatisticasAtual)
            $data = strtotime($estatisticasAtual->atualizacao);
        else
            $data = time();

        $query = (Database::getInstance()->getConfig('DRIVER') == 'mysql' ? $this->queryMysql() : $this->queryPgsql());

        if(time() >= $data+60*10){
            $estatisticas->query("DELETE FROM estatisticas")->execute();
            $estatisticas->query($query)->execute();
        }
        
        $this->atualizaUltimoAcesso();
    }
    
    private function queryMysql() {
        return "
            SET @rank = 0;
            SET @usuario = 0;

            INSERT INTO estatisticas

            SELECT rank, id_cidade, id_usuario, pontos_construcoes, pontos_ataque, pontos_defesa, atualizacao
            FROM(
            SELECT
            @rank := IF(@usuario <> u.id, @rank + 1, @rank) as rank,
            a.id_cidade,
            c.id_usuario,
            IF(a.level>1,
            sum(
                (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) +
                round(
                    (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) *
                    pow(b.fator, a.level)
                , 0)
            ), b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) as pontos_construcoes,
            u.pontos_ataque,
            u.pontos_defesa,
            now() as atualizacao,
            @usuario := u.id as usuarioAnterior

            FROM cidade_construcao a

            JOIN construcao b ON b.id = a.id_construcao
            JOIN cidade c ON c.id = a.id_cidade
            JOIN usuario u ON u.id = c.id_usuario

            GROUP BY a.id_cidade, c.id_usuario
            ORDER BY pontos_construcoes DESC
            ) as tab;
        ";
    }

    private function queryPgsql() {
        return '
            INSERT INTO estatisticas

            SELECT rank, id_cidade, id_usuario, pontos_construcoes, pontos_ataque, pontos_defesa, atualizacao
            FROM(
                SELECT
                    rank() OVER (ORDER BY SUM(
                    CASE
                        WHEN a.level>1 THEN
                            (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) +
                            (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) *
                            pow(b.fator, a.level)
                        ELSE
                            b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia
                    END
                    ) DESC),
                    a.id_cidade,
                    c.id_usuario,
                    sum(
                        CASE
                            WHEN a.level>1 THEN
                                (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) +
                                (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) *
                                pow(b.fator, a.level)
                            ELSE
                                b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia
                        END
                    ) as pontos_construcoes,
                    u.pontos_ataque,
                    u.pontos_defesa,
                    now() as atualizacao
                FROM cidade_construcao a

                JOIN construcao b ON b.id = a.id_construcao
                JOIN cidade c ON c.id = a.id_cidade
                JOIN usuario u ON u.id = c.id_usuario

                GROUP BY a.id_cidade, c.id_usuario, u.pontos_ataque, u.pontos_defesa
                ORDER BY pontos_construcoes DESC
            ) as tab;
        ';
    }

    private function atualizaUltimoAcesso() {
        $usuario = Funcoes::getUsuario();
        
        if($usuario) {
            $usuario->ultimo_acesso = date('Y-m-d H:i:s');
            
            $usuario->save();
        }
    }

	/**
	 * Regras a ser executa para validar o Evento
	 */
    public function rules() {
        return true;
    }
}