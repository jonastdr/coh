<?php

namespace app\events;

use app\models\Cidade;
use app\models\CidadeConstrucao;
use app\models\CidadePesquisa;
use app\models\Usuario;
use projectws\libs\Events;

class RecursosEvent extends Events {

    protected $active = true;

	/**
	 * Executa o evento
	 */
    public function run() {
        $this->honra();
	    $this->recursos();
    }
    
    private function recursos() {
	    $cidades = Cidade::recursos();
	
	    foreach ($cidades as $cidadeAtual) {
		    //Atualizacao dos recursos em tempo real
		    $timeAtual = time();
		    $tempoCalc = $timeAtual - $cidadeAtual->atualizacao;
		
		    //Atualiza construcao nivel
		    $construcao = explode(',', $cidadeAtual->construcao);
		
		    if($construcao[0] <= time() && $cidadeAtual->construcao > 0) {
			    $ConstrucaoAtualizar = CidadeConstrucao::where('id_cidade', '=', $cidadeAtual->id)
				    ->where('id_construcao', '=', $construcao[1])->row();
			
			    if($ConstrucaoAtualizar->level==1 && $construcao[2]==0) {
				    $ConstrucaoAtualizar->delete();
			    } else {
				    if($ConstrucaoAtualizar->count() == 0) {
					    $ConstrucaoAtualizar = new CidadeConstrucao();
					    $ConstrucaoAtualizar->id_cidade = $cidadeAtual->id;
					    $ConstrucaoAtualizar->id_construcao = $construcao[1];
				    }
				
				    if($construcao[2]==1){
					    $ConstrucaoAtualizar->level++;
				    } else {
					    $ConstrucaoAtualizar->level--;
				    }
				    $ConstrucaoAtualizar->save();
			    }
			
			    $cidadeAtual->construcao = 0;
		    }
		    //Final atualizacao construcao nivel
		
		    //Atualiza construcao nivel
		    $pesquisa = explode(',', $cidadeAtual->pesquisa);
		
		    if($pesquisa[0] <= time() && $cidadeAtual->pesquisa > 0) {
			    $pesquisaAtualizar = CidadePesquisa::where('id_cidade', '=', $cidadeAtual->id)
				    ->where('id_pesquisa', '=', $pesquisa[1])->row();
			
			    if($pesquisaAtualizar->count() && $pesquisaAtualizar->level==1 && $pesquisa[2]==0){
				    $pesquisaAtualizar->delete();
			    } else {
				    if($pesquisaAtualizar->count() == 0) {
					    $pesquisaAtualizar = new CidadePesquisa();
					    $pesquisaAtualizar->id_cidade = $cidadeAtual->id;
					    $pesquisaAtualizar->id_pesquisa = $pesquisa[1];
				    }
				
				    if($pesquisa[2]==1){
					    $pesquisaAtualizar->level++;
				    } else {
					    $pesquisaAtualizar->level--;
				    }
				
				    $pesquisaAtualizar->save();
			    }
			
			    $cidadeAtual->pesquisa = 0;
		    }
		    //Final atualizacao construcao nivel
		
		    //Dinheiro
		    $dinheiro = $cidadeAtual->dinheiro;
		    $banco = $cidadeAtual->banco + 1;
		    $fator = $cidadeAtual->dinheiro_fator;
		    $armazenamento = $cidadeAtual->dinheiro_armazenamento;
		    if($dinheiro < $armazenamento) {
			    $cidadeAtual->dinheiro = $dinheiro + (($tempoCalc/60) * ($banco / $fator));
		    } else {
			    $cidadeAtual->dinheiro = $armazenamento;
		    }
		    //Final Atualizacao Dinheiro
		
		    //Alimento
		    $alimento = $cidadeAtual->alimento;
		    $fazenda = $cidadeAtual->fazenda;
		    $alimento_fator = $cidadeAtual->alimento_fator;
		    $armazenamento = $cidadeAtual->alimento_armazenamento;
		    if($alimento < $armazenamento) {
			    $cidadeAtual->alimento = $alimento + (($tempoCalc/60) * ($fazenda / $alimento_fator));
		    } else {
			    $cidadeAtual->alimento = $armazenamento;
		    }
		    //Final Atualizacao Alimento
		
		    //Metal
		    $metal = $cidadeAtual->metal;
		    $metalurgica = $cidadeAtual->metalurgica + 1;
		    $metal_fator = $cidadeAtual->metal_fator;
		    $armazenamento = $cidadeAtual->metal_armazenamento;
		    if($metal < $armazenamento) {
			    $cidadeAtual->metal = $metal + (($tempoCalc/60) * ($metalurgica / $metal_fator));
		    } else {
			    $cidadeAtual->metal = $armazenamento;
		    }
		    //Final Atualizacao Metal
		
		    //Petroleo
		    $petroleo = $cidadeAtual->petroleo;
		    $poco = $cidadeAtual->poco_petroleo;
		    $petroleo_fator = $cidadeAtual->petroleo_fator;
		    $armazenamento = $cidadeAtual->petroleo_armazenamento;
		    if($petroleo < $armazenamento) {
			    $cidadeAtual->petroleo = $petroleo + (($tempoCalc/60) * ($poco / $petroleo_fator));
		    } else {
			    $cidadeAtual->petroleo = $armazenamento;
		    }
		    //Final Atualizacao Petroleo
		
		    //Atualizacao no banco de dados
		    $cidadeAtual->atualizacao = time();
		
		    $cidadeAtual->save();
	    }
    }
    
    private function honra() {
    	$recursos = Usuario::heroisHonra();
	
	    foreach ($recursos as $recurso) {
		    //Atualizacao dos recursos em tempo real
		    $timeAtual = time();
		    
		    $tempoCalc = $timeAtual - $recurso->atualizacao;
		
		    $honra = $recurso->honra;
		    $armazenamento = $recurso->honra_armazenamento;
		    
		    $level = $recurso->level;
		    $fator = $recurso->honra_fator;
		    
		    if($honra < $armazenamento) {
			    $recurso->honra = $honra + (($tempoCalc/60) * ($level / $fator));
		    } else {
			    $recurso->honra = $armazenamento;
		    }
		    
		    $recurso->atualizacao = time();
		
		    $recurso->save();
	    }
	}

	/**
	 * Regras a ser executa para validar o Evento
	 */
    public function rules() {
        return true;
    }
}