<?php

namespace app\events;

use app\classes\RelatorioEspionagem;
use app\models\Cidade;
use app\models\CidadeConstrucao;
use app\models\CidadeDefesa;
use app\models\CidadeDefesaUnidade;
use app\models\CidadePesquisa;
use app\models\CidadeUnidade;
use app\models\Classe;
use app\models\Expedicao;
use app\models\ExpedicaoUnidade;
use app\models\HeroiFuncaoUsuario;
use app\models\Mensagem;
use app\models\MensagemTexto;
use app\models\MensagemTextoUsuario;
use app\models\Notificacao;
use app\models\Relatorio;
use app\models\Usuario;
use projectws\libs\bridges\DB;
use projectws\libs\Events;
use projectws\libs\orm\FailException;
use projectws\libs\orm\Join;

class ExpedicaoEvent extends Events {

    protected $active = true;

	/**
	 * Executa o evento
	 */
    public function run() {
        $expedicoes = Expedicao::select(
            'expedicao.*',
            'cidade_def.nome as cidade_def',
            'cidade_atk.nome as cidade_atk',
            'usuario_atk.nome as usuario_atk')
	        ->join('cidade cidade_atk', 'cidade_atk.id', '=', 'expedicao.id_cidade_atk')
            ->leftJoin('cidade cidade_def', function (Join $j) {
	            $j->on('cidade_def.cod_ilha', '=', 'expedicao.cod_ilha');
	
	            $j->where('cidade_def.cod_cidade', '=', 'expedicao.cod_cidade');
            })
            ->leftJoin('usuario usuario_atk', 'usuario_atk.id', '=', 'cidade_atk.id_usuario')
            ->rows();

        if($expedicoes) {
            foreach ($expedicoes as $id => $expedicao){
                $chegada = $expedicao->duracao+$expedicao->horario;

                //inicio da missao
                if(time() >= $chegada){
                    if(!$expedicao->volta) {
	                    /**
	                     * Se existir uma cidade para a missão pega os dados da cidade
	                     */
                    	if($expedicao->id_cidade_def) {
                    		$cidade = Cidade::findFirst($expedicao->id_cidade_def);
                    		
		                    //Retorna as unidades da cidade a ser atacada
		                    $destino = CidadeUnidade::select(
			                    'b.id',
			                    'b.nome',
			                    'b.ataque_terrestre',
			                    'b.ataque_aereo',
			                    'b.ataque_naval',
			                    'b.integridade',
			                    'cidade_unidade.quantidade',
			                    'c.id_tipo')
			                    ->join('unidade b', 'b.id', '=', 'cidade_unidade.id_unidade')
			                    ->join('classe c', 'c.id', '=', 'b.id_classe')
			                    ->where('id_cidade', '=', $expedicao->id_cidade_def)
			                    ->rows();
		
		                    //Retorna as unidades defensivas da cidade atacada
		                    $defesas = CidadeDefesaUnidade::select(
			                    'b.id',
			                    'b.nome',
			                    'b.ataque_terrestre',
			                    'b.ataque_aereo',
			                    'b.ataque_naval',
			                    'b.integridade',
			                    'a.quantidade',
			                    'c.id_tipo',
			                    'cidade.id_usuario',
			                    'a.id_defesa')
			                    ->alias('a')
			                    ->join('cidade_defesa', 'cidade_defesa.id', '=', 'a.id_defesa')
			                    ->join('unidade b', 'b.id', '=', 'a.id_unidade')
			                    ->join('classe c', 'c.id', '=', 'b.id_classe')
			                    ->join('cidade', 'cidade.id', '=', 'cidade_defesa.id_cidade_def')
			                    ->where('cidade_defesa.id_cidade', '=', $expedicao->id_cidade_def)
			                    ->rows();
	                    } else {
                            $cidade = null;
	                    	$destino = null;
	                    	$defesas = null;
	                    }

                        switch ($expedicao->missao) {
                            case Expedicao::ATACAR:
                                $simulacao = $this->simularAtaque($expedicao, $destino, $defesas, $cidade);
                                
                                $this->geraRelatorioAtaque($simulacao);

                                $expedicao = $this->acaoAtaque($expedicao, $simulacao);

                                break;
	                        
	                        case Expedicao::COLONIZACAO:
		                        $simulacao = $this->simularAtaque($expedicao, $destino, $defesas, $cidade);
								
		                        /**
		                         * Executa a ação de colonização
		                         */
								$colonizacao = $this->acaoColonizacao($simulacao, $expedicao);
								
								$this->geraRelatorioColonizacao($simulacao, $colonizacao);
	                        	
	                        	break;

                            case Expedicao::TRANSPORTE:
                                $recursos = $this->acaoTransporte($expedicao);
                                $this->geraRelatorioTransporte($expedicao, $recursos);

                                break;

                            case Expedicao::DEFENDER:
                                $this->acaoDefesa($expedicao);
                                $this->geraRelatorioDefesa($expedicao);

                                break;
                            
                            case Expedicao::ESPIONAGEM:
                                $relatorio = $this->acaoEspionagem($expedicao);
                                $this->geraRelatorioEspionagem($relatorio, $expedicao);
                                
                                break;
                        }
                    }
                    if($expedicao)
                        $this->regressoMissao($expedicao);
                }
            }
        }
    }

    /**
     * Faz uma simulação de um ataque e retorna o resultado do ataque
     * @param Expedicao $expedicao
     * @param CidadeUnidade $destino
     * @param Cidade $cidade
     * @return mixed
     */
    public function simularAtaque($expedicao, $destino, $defesas, $cidade) {
        //Inicializa variaveis de combate
        $forcasAtaque = new \StdClass;

        $forcasAtaque->terrestre = 0;
        $forcasAtaque->aereo = 0;
        $forcasAtaque->naval = 0;
        $forcasAtaque->integridade = 0;

        $forcasDestino = new \StdClass;

        $forcasDestino->terrestre = 0;
        $forcasDestino->aereo = 0;
        $forcasDestino->naval = 0;
        $forcasDestino->integridade = 0;

        $forcasDefesa = new \StdClass;

        $forcasDefesa->terrestre = 0;
        $forcasDefesa->aereo = 0;
        $forcasDefesa->naval = 0;
        $forcasDefesa->integridade = 0;
        //fim

        $id_usuario_atk = Cidade::findFirst($expedicao->id_cidade_atk)->id_usuario;
        $id_usuario_def = Cidade::findFirst($expedicao->id_cidade_def)->id_usuario;

        $porcentagemJogadores = [];

        $escopo = DB::query("
        SELECT
            MAX(pontos_atacante) as pontos_atacante,
            MAX(pontos_defensor) as pontos_defensor,
            MAX(atacante) as atacante,
            MAX(defensor) as defensor,
            MAX(atacante_cidade) as atacante_cidade,
            MAX(defensor_cidade) as defensor_cidade,
            MAX(id_atacante) as id_atacante,
            MAX(id_defensor) as id_defensor
        FROM (
            SELECT
                CASE WHEN u.id = :id_usuario_atk THEN
                    SUM(CASE
			WHEN a.level>1 THEN
                            (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) +
                            
                            (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) *
                             pow(b.fator, a.level)
                        ELSE b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia
			END
			)
                ELSE 0 END as pontos_atacante,
                CASE WHEN u.id = :id_usuario_def THEN
                    SUM(CASE
			WHEN a.level>1 THEN
                            (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) +
                            
                            (b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia) *
                             pow(b.fator, a.level)
                        ELSE b.dinheiro + b.alimento + b.metal + b.petroleo + b.ouro + b.energia
			END
			)
                ELSE 0 END as pontos_defensor,
                CASE WHEN u.id = :id_usuario_atk THEN u.nome ELSE null END as atacante,
                CASE WHEN u.id = :id_usuario_def THEN u.nome ELSE null END as defensor,
                CASE WHEN u.id = :id_usuario_atk THEN c.nome ELSE null END as atacante_cidade,
                CASE WHEN u.id = :id_usuario_def THEN c.nome ELSE null END as defensor_cidade,
                CASE WHEN u.id = :id_usuario_atk THEN u.id ELSE null END as id_atacante,
                CASE WHEN u.id = :id_usuario_def THEN u.id ELSE null END as id_defensor,
                CASE WHEN u.id = :id_usuario_def THEN c.dinheiro ELSE null END as dinheiro,
                CASE WHEN u.id = :id_usuario_def THEN c.alimento ELSE null END as alimento,
                CASE WHEN u.id = :id_usuario_def THEN c.metal ELSE null END as metal,
                CASE WHEN u.id = :id_usuario_def THEN c.petroleo ELSE null END as petroleo
            FROM cidade c
              LEFT JOIN cidade_construcao a ON a.id_cidade = c.id
              LEFT JOIN construcao b ON b.id = a.id_construcao
              JOIN usuario u ON u.id = c.id_usuario

            WHERE u.id in (:id_usuario_atk, :id_usuario_def)

            GROUP BY u.id, c.id_usuario, c.nome, c.dinheiro, c.alimento, c.metal, c.petroleo
          ) tab
        ", ['id_usuario_atk' => $id_usuario_atk, 'id_usuario_def' => $id_usuario_def], true);

        //Pontuações gerais dos usuarios
        $ptsDefensor = $escopo->pontos_defensor;
        $ptsAtacante = $escopo->pontos_atacante;

        $moral = round($ptsDefensor / ($ptsAtacante * 0.7) * 100);

        //moral maxima do atancante
        if($moral > 100) $moral = 100;

        //moral minima do atacante
        if($moral < 20) $moral = 20;

        $sorte = rand(-20, 20);

        //Unidades de ataque
        $unidadesAtaque = [];

        $unidadesExpedicao = $expedicao->unidades;
    
        /**
         * Função de 10% de força de ataque
         */
        $funcaoAtaque = HeroiFuncaoUsuario::findFirst([
            'id_usuario' => $id_usuario_atk,
            'id_funcao' => 1
        ]);
        
        $funcaoDefesa = HeroiFuncaoUsuario::findFirst([
            'id_usuario' => $id_usuario_def,
            'id_funcao' => 2
        ]);
        
        //Unidades de ataque
        foreach ($unidadesExpedicao as $index => $unidade){
            $unidadesAtaque[$index] = $unidade;

            $unidadesAtaque[$index]->integridadeTotal = $unidadesAtaque[$index]->integridade * $unidadesAtaque[$index]->quantidade;

            $forcasAtaque->integridade += $unidadesAtaque[$index]->integridadeTotal;

            //soma de pontos total de combate do atacante
            $forcasAtaque->terrestre += $unidade->quantidade * $unidadesAtaque[$index]->ataque_terrestre;
            $forcasAtaque->terrestre *= rand(90, 110)/100;

            $forcasAtaque->aereo += $unidade->quantidade * $unidadesAtaque[$index]->ataque_aereo;
            $forcasAtaque->aereo *= rand(90, 110)/100;

            $forcasAtaque->naval += $unidade->quantidade * $unidadesAtaque[$index]->ataque_naval;
            $forcasAtaque->naval *= rand(90, 110)/100;
        }
    
        /**
         * Aumenta a força de ataque em 10%
         */
        if($funcaoAtaque->count()) {
            $forcasAtaque->terrestre *= 1.1;
            $forcasAtaque->aereo *= 1.1;
            $forcasAtaque->naval *= 1.1;
        }

        //Unidades de destino
        if($destino && $destino->count()) {
            foreach ($destino as $index => $unidade){
                //cria a integridade total
                $destino[$index]->integridadeTotal = $unidade->integridade * $unidade->quantidade;

                $forcasDestino->integridade += $destino[$index]->integridadeTotal;

                //Soma de toda a integridade do jogador
                if(!isset($porcentagemJogadores[$id_usuario_def]))
                    $porcentagemJogadores[$id_usuario_def] = 0;
                    
                $porcentagemJogadores[$id_usuario_def] += $forcasDestino->integridade;

                //soma de pontos total de combate do defensor
                $forcasDestino->terrestre += $unidade->quantidade * $unidade->ataque_terrestre;
                $forcasDestino->terrestre *= rand(90, 110)/100;

                $forcasDestino->aereo += $unidade->quantidade * $unidade->ataque_aereo;
                $forcasDestino->aereo *= rand(90, 110)/100;

                $forcasDestino->naval += $unidade->quantidade * $unidade->ataque_naval;
                $forcasDestino->naval *= rand(90, 110)/100;
            }
        }
    
        /**
         * Aumenta a força de defesa em 15%
         */
        if($funcaoDefesa->count()) {
            $forcasDestino->terrestre *= 1.15;
            $forcasDestino->aereo *= 1.15;
            $forcasDestino->naval *= 1.15;
        }

        //Unidades de defesa
        if($defesas && $defesas->count()) {
            foreach ($defesas as $index => $unidade){
                //cria a integridade total
                $unidade->integridadeTotal = $unidade->integridade * $unidade->quantidade;

                $forcasDefesa->integridade += $unidade->integridadeTotal;

                if($id_usuario_atk != $unidade->id_usuario) {
                    if(!isset($porcentagemJogadores[$unidade->id_usuario]))
                        $porcentagemJogadores[$id_usuario_def] = 0;
                    
                    $porcentagemJogadores[$unidade->id_usuario] += $forcasDefesa->integridade;
                }

                //soma de pontos total de combate do apoio
                $forcasDefesa->terrestre += $unidade->quantidade * $unidade->ataque_terrestre;
                $forcasDefesa->terrestre *= rand(90, 110)/100;

                $forcasDefesa->aereo += $unidade->quantidade * $unidade->ataque_aereo;
                $forcasDefesa->aereo *= rand(90, 110)/100;

                $forcasDefesa->naval += $unidade->quantidade * $unidade->ataque_naval;
                $forcasDefesa->naval *= rand(90, 110)/100;
            }
        }
    
        /**
         * Aumenta a força de defesa em 15%
         */
        if($funcaoDefesa->count()) {
            $forcasDefesa->terrestre *= 1.15;
            $forcasDefesa->aereo *= 1.15;
            $forcasDefesa->naval *= 1.15;
        }

        $integridadeTotalDefensores = array_sum($porcentagemJogadores);

        //Calculo para saber a porcentagem de pontos para cada defensor
        foreach($porcentagemJogadores as &$integridade) {
            $integridade = $integridade * 100 / ($integridadeTotalDefensores ? $integridadeTotalDefensores : 1);
        }

        //Calculo de Moral e Sorte
        $forcasAtaque->terrestre = $forcasAtaque->terrestre * ($moral / 100);
        $forcasAtaque->terrestre += $forcasAtaque->terrestre * ($sorte / 100);

        $forcasAtaque->aereo = $forcasAtaque->aereo * ($moral / 100);
        $forcasAtaque->aereo += $forcasAtaque->aereo * ($sorte / 100);

        $forcasAtaque->naval = $forcasAtaque->naval * ($moral / 100);
        $forcasAtaque->naval += $forcasAtaque->naval * ($sorte / 100);

        //armazena historico do round atual (ESCOPO)
        $detalhes = new \StdClass();

        $detalhes->moral = $moral;
        $detalhes->sorte = $sorte;
        $detalhes->atacante = $escopo->atacante;
        $detalhes->defensor = $escopo->defensor;
        $detalhes->id_atacante = $escopo->id_atacante;
        $detalhes->id_defensor = $escopo->id_defensor;

        $detalhes->id_cidade_atk = $expedicao->id_cidade_atk;
        $detalhes->id_cidade_def = $expedicao->id_cidade_def;

        $detalhes->atacante_cidade = $escopo->atacante_cidade;
        $detalhes->defensor_cidade = $escopo->defensor_cidade;

        // 0 = empate, 1 = atacante, 2 = defensor
        $detalhes->vencedor = 0;

        $detalhes->data = date('Y-m-d H:i:s', $expedicao->horario);

        //pontuacões
        $detalhes->pontos_atacante = 0;
            $detalhes->pontos_defensor = 0;

        $resultado['detalhes'] = $detalhes;

        //Recursos
        $recursos = new \StdClass();

        $recursos->dinheiro = 0;
        $recursos->alimento = 0;
        $recursos->metal = 0;
        $recursos->petroleo = 0;

        $resultado['recursos'] = $recursos;

        //Primeira rodada
        $resultado[0] = new \StdClass();

        //Ataque
        $atacante = new \StdClass();
        $atacante->unidades = $unidadesAtaque;
        $atacante->forcasTerrestre = $forcasAtaque->terrestre;
        $atacante->forcasAerea = $forcasAtaque->aereo;
        $atacante->forcasNaval = $forcasAtaque->naval;
        $atacante->integridadeTotal = $forcasAtaque->integridade;

        $resultado[0]->atacante = $atacante;

        //Cidade destino || cidade a ser atacada
        $defensor = new \StdClass();
        $defensor->unidades = $destino;
        $defensor->forcasTerrestre = $forcasDestino->terrestre;
        $defensor->forcasAerea = $forcasDestino->aereo;
        $defensor->forcasNaval = $forcasDestino->naval;
        $defensor->integridadeTotal = $forcasDestino->integridade;

        $resultado[0]->defensor = $defensor;

        //Defesa
        $apoio = new \StdClass();
        $apoio->unidades = $defesas;
        $apoio->forcasTerrestre = $forcasDefesa->terrestre;
        $apoio->forcasAerea = $forcasDefesa->aereo;
        $apoio->forcasNaval = $forcasDefesa->naval;
        $apoio->integridadeTotal = $forcasDefesa->integridade;

        $resultado[0]->defesas = $apoio;

        //Simulacao dos ataques
        //
        //IMPLEMENTAR PESQUISA DO ATACANTE DE DURACAO DE MAIS UMA RODADA
        //
        $rodadas = 2;

        for($rodada = 1; $rodada <= $rodadas; $rodada++) {
            $rodadaAnterior = serialize($resultado[$rodada-1]);

            $rodadaAnterior = unserialize($rodadaAnterior);

            $unidadeAtk = $rodadaAnterior->atacante->unidades;
            $unidadeDef = $rodadaAnterior->defensor->unidades;
            $unidadeApoio = $rodadaAnterior->defesas->unidades;

            $resultado[$rodada] = new \StdClass();

            //já define que o atacante é um vencedor
            if($unidadeDef && $unidadeDef->count() == 0 && $unidadeApoio->count() == 0) {
                $resultado['detalhes']->vencedor = 1;
            }
	
	        /**
	         * Deve verificar se existes unidades de defesa
	         */
            if($unidadeDef) {
	            /**
	             * ataque do Atacante NAS UNIDADES DA CIDADE
	             */
	            foreach ($unidadeDef as $i => $unidade) {
		            $subtracao = 0;
		
		            /**
		             * ataque terrestre
		             */
		            if ($unidade->id_tipo == Classe::TERRESTRE) {
			            /**
			             * a integridade não pode ficar menor que 0
			             */
			            if ($rodadaAnterior->atacante->forcasTerrestre > $unidade->integridadeTotal)
				            $subtracao = $unidade->integridadeTotal;
			            else
				            $subtracao = $rodadaAnterior->atacante->forcasTerrestre;
		            }
		
		            /**
		             * ataque aéreo
		             */
		            if ($unidade->id_tipo == Classe::AEREO) {
			            /**
			             * a integridade não pode ficar menor que 0
			             */
			            if ($rodadaAnterior->atacante->forcasAerea > $unidade->integridadeTotal)
				            $subtracao += $unidade->integridadeTotal;
			            else
				            $subtracao += $rodadaAnterior->atacante->forcasAerea;
		            }
		
		            /**
		             * ataque naval
		             */
		            if ($unidade->id_tipo == Classe::NAVAL) {
			            /**
			             * a integridade não pode ficar menor que 0
			             */
			            if ($rodadaAnterior->atacante->forcasNaval > $unidade->integridadeTotal)
				            $subtracao += $unidade->integridadeTotal;
			            else
				            $subtracao += $rodadaAnterior->atacante->forcasNaval;
		            }
		
		            $unidade->integridadeTotal -= $subtracao;
		
		            $rodadaAnterior->defensor->integridadeTotal -= $subtracao;
		
		            if ($rodadaAnterior->defensor->integridadeTotal <= 0) {
			            $resultado['detalhes']->vencedor = 1;
		            }
		
		            $quantidade = $unidade->quantidade;
		            $integridadeTotal = $unidade->integridadeTotal;
		
		            /**
		             * calculo de perdas
		             */
		            if ($unidade->integridade > 0)
			            $perdas = $integridadeTotal / $unidade->integridade;
		            else
			            $perdas = 0;
		
		            /**
		             * Se não existir unidades a serem atacadas o atacante não recebe pontos
		             */
		            if (count($unidadeDef) > 0)
			            $calcPerdasIntegridade = $unidade->integridade;
		            else
			            $calcPerdasIntegridade = 0;
		
		            /**
		             * Pontos do atacante
		             */
		            if ($detalhes->id_atacante != $detalhes->id_defensor) {
                        $pontuar = (round($quantidade - $perdas) * $unidade->integridade) / $calcPerdasIntegridade;
            
                        $detalhes->pontos_atacante += $pontuar * ($unidade->integridade / 100);
                    }
		
		            $unidade->quantidade = round($perdas < 0 ? 0 : $perdas);
	            }
            }
	
	        /**
	         * ataque do Atacante EM DEFESAS
	         */
	        if($unidadeApoio) {
		        foreach ($unidadeApoio as $i => $unidade) {
			        $subtracao = 0;
			
			        //ataque terrestre
			        if ($unidade->id_tipo == Classe::TERRESTRE) {
				        //a integridade não pode ficar menor que 0
				        if ($rodadaAnterior->atacante->forcasTerrestre > $unidade->integridadeTotal)
					        $subtracao = $unidade->integridadeTotal;
				        else
					        $subtracao = $rodadaAnterior->atacante->forcasTerrestre;
			        }
			
			        //ataque aéreo
			        if ($unidade->id_tipo == Classe::AEREO) {
				        //a integridade não pode ficar menor que 0
				        if ($rodadaAnterior->atacante->forcasAerea > $unidade->integridadeTotal)
					        $subtracao += $unidade->integridadeTotal;
				        else
					        $subtracao += $rodadaAnterior->atacante->forcasAerea;
			        }
			
			        //ataque naval
			        if ($unidade->id_tipo == Classe::NAVAL) {
				        //a integridade não pode ficar menor que 0
				        if ($rodadaAnterior->atacante->forcasNaval > $unidade->integridadeTotal)
					        $subtracao += $unidade->integridadeTotal;
				        else
					        $subtracao += $rodadaAnterior->atacante->forcasNaval;
			        }
			
			        $unidade->integridadeTotal -= $subtracao;
			
			        $rodadaAnterior->defesas->integridadeTotal -= $subtracao;
			
			        if ($rodadaAnterior->defesas->integridadeTotal <= 0) {
				        $resultado['detalhes']->vencedor = 1;
			        }
			
			        $quantidade = $unidade->quantidade;
			        $integridadeTotal = $unidade->integridadeTotal;
			
			        //calculo de perdas
			        if ($unidade->integridade > 0)
				        $perdas = $integridadeTotal / $unidade->integridade;
			        else
				        $perdas = 0;
			
			        //Se não existir unidades a serem atacadas o atacante não recebe pontos
			        if (count($unidadeApoio) > 0)
				        $calcPerdasIntegridade = $unidade->integridade / 2;
			        else
				        $calcPerdasIntegridade = 0;
			
			        //Pontos do atacante
			        if ($detalhes->id_atacante != $detalhes->id_defensor) {
                        $pontuar = (round($quantidade - $perdas) * $unidade->integridade) / $calcPerdasIntegridade;
                
                        $detalhes->pontos_atacante += $pontuar * ($unidade->integridade / 100);
                    }
			
			        $unidade->quantidade = round($perdas < 0 ? 0 : $perdas);
		        }
	        }

            //ataque do Defensor
            $capacidadeTotal = 0;

            foreach ($unidadeAtk as $i => $unidade) {
                $subtracao = 0;

                //ataque terrestre
                if($unidade->id_tipo == Classe::TERRESTRE) {
                    //a integridade não pode ficar menor que 0
                    if ($rodadaAnterior->defensor->forcasTerrestre > $unidade->integridadeTotal)
                        $subtracao = $unidade->integridadeTotal;
                    else
                        $subtracao = $rodadaAnterior->defensor->forcasTerrestre;
                }

                //ataque aéreo
                if($unidade->id_tipo == Classe::AEREO) {
                    //a integridade não pode ficar menor que 0
                    if ($rodadaAnterior->defensor->forcasAerea > $unidade->integridadeTotal)
                        $subtracao += $unidade->integridadeTotal;
                    else
                        $subtracao += $rodadaAnterior->defensor->forcasAerea;
                }
                
                //ataque naval
                if($unidade->id_tipo == Classe::NAVAL) {
                    //a integridade não pode ficar menor que 0
                    if ($rodadaAnterior->defensor->forcasNaval > $unidade->integridadeTotal)
                        $subtracao += $unidade->integridadeTotal;
                    else
                        $subtracao += $rodadaAnterior->defensor->forcasNaval;
                }

                $unidade->integridadeTotal -= $subtracao;

                $rodadaAnterior->atacante->integridadeTotal -= $subtracao;

                if($rodadaAnterior->atacante->integridadeTotal <= 0){
                    $resultado['detalhes']->vencedor = 2;
                }

                $quantidade = $unidade->quantidade;
                $integridadeTotal = $unidade->integridadeTotal;

                //calculo de perdas
                if($unidade->integridade > 0)
                    $perdas = $integridadeTotal / $unidade->integridade;
                else
                    $perdas = 0;

                //Se não existir unidades a serem atacadas o defensor não recebe pontos
                if(count($unidadeAtk) > 0)
                    $calcPerdasIntegridade = $unidade->integridade;
                else
                    $calcPerdasIntegridade = 0;
                
                //Pontos do Defensor
                if($detalhes->id_atacante != $detalhes->id_defensor && $calcPerdasIntegridade > 0) {
                    $pontuar = (round($quantidade - $perdas) * $unidade->integridade) / $calcPerdasIntegridade;
                    
                    $detalhes->pontos_defensor += $pontuar * ($unidade->integridade / 100);
                }

                $unidade->quantidade = round($perdas < 0 ? 0 : $perdas);

                //soma a capacidade total
                $capacidadeTotal += $unidade->quantidade * $unidade->capacidade;
            }

            //Ataque das unidades de apoio
            foreach ($unidadeAtk as $i => $unidade) {
                $subtracao = 0;

                //ataque terrestre
                if($unidade->id_tipo == Classe::TERRESTRE) {
                    //a integridade não pode ficar menor que 0
                    if ($rodadaAnterior->defesas->forcasTerrestre > $unidade->integridadeTotal)
                        $subtracao = $unidade->integridadeTotal;
                    else
                        $subtracao = $rodadaAnterior->defesas->forcasTerrestre;
                }

                //ataque aéreo
                if($unidade->id_tipo == Classe::AEREO) {
                    //a integridade não pode ficar menor que 0
                    if ($rodadaAnterior->defesas->forcasAerea > $unidade->integridadeTotal)
                        $subtracao += $unidade->integridadeTotal;
                    else
                        $subtracao += $rodadaAnterior->defesas->forcasAerea;
                }

                //ataque naval
                if($unidade->id_tipo == Classe::NAVAL) {
                    //a integridade não pode ficar menor que 0
                    if ($rodadaAnterior->defesas->forcasNaval > $unidade->integridadeTotal)
                        $subtracao += $unidade->integridadeTotal;
                    else
                        $subtracao += $rodadaAnterior->defesas->forcasNaval;
                }

                $unidade->integridadeTotal -= $subtracao;

                $rodadaAnterior->atacante->integridadeTotal -= $subtracao;

                if($rodadaAnterior->atacante->integridadeTotal <= 0){
                    $resultado['detalhes']->vencedor = 2;
                }

                $quantidade = $unidade->quantidade;
                $integridadeTotal = $unidade->integridadeTotal;

                //calculo de perdas
                if($unidade->integridade > 0)
                    $perdas = $integridadeTotal / $unidade->integridade;
                else
                    $perdas = 0;

                //Se não existir unidades a serem atacadas o defensor não recebe pontos
                if($unidadeApoio && $unidadeApoio->count() > 0)
                    $calcPerdasIntegridade = $unidade->integridade / 2;
                else
                    $calcPerdasIntegridade = 0;
                
                //Pontos para defensores
                if($detalhes->id_atacante != $unidade->id_usuario && $calcPerdasIntegridade > 0) {
                    $pontuar = (round($quantidade - $perdas) * $unidade->integridade) / $calcPerdasIntegridade;
    
                    $detalhes->pontos_defensor += $pontuar * ($unidade->integridade / 10);
                }
                
                $unidade->quantidade = round($perdas < 0 ? 0 : $perdas);

                //soma a capacidade total
                $capacidadeTotal += $unidade->quantidade * $unidade->capacidade;
            }

            //armazena historico do round atual

            //Ataque
            $atacante = new \StdClass();
            $atacante->unidades = $unidadeAtk;
            $atacante->forcasTerrestre = $rodadaAnterior->atacante->forcasTerrestre;
            $atacante->forcasAerea = $rodadaAnterior->atacante->forcasAerea;
            $atacante->forcasNaval = $rodadaAnterior->atacante->forcasNaval;
            $atacante->integridadeTotal = $rodadaAnterior->atacante->integridadeTotal;

            $resultado[$rodada]->atacante = $atacante;

            //Cidade defensora
            $defensor = new \StdClass();
            $defensor->unidades = $unidadeDef;
            $defensor->forcasTerrestre = $rodadaAnterior->defensor->forcasTerrestre;
            $defensor->forcasAerea = $rodadaAnterior->defensor->forcasAerea;
            $defensor->forcasNaval = $rodadaAnterior->defensor->forcasNaval;
            $defensor->integridadeTotal = $rodadaAnterior->defensor->integridadeTotal;

            $resultado[$rodada]->defensor = $defensor;

            //Defesas
            $apoio = new \StdClass();
            $apoio->unidades = $unidadeApoio;
            $apoio->forcasTerrestre = $rodadaAnterior->defesas->forcasTerrestre;
            $apoio->forcasAerea = $rodadaAnterior->defesas->forcasAerea;
            $apoio->forcasNaval = $rodadaAnterior->defesas->forcasNaval;
            $apoio->integridadeTotal = $rodadaAnterior->defesas->integridadeTotal;

            $resultado[$rodada]->defesas = $apoio;

            if($resultado['detalhes']->vencedor == 1) {
                //Calculo dos recursos

                //recursos já adicionados são subtraidos
                $capacidadeTotal -= $expedicao->dinheiro;
                $capacidadeTotal -= $expedicao->alimento;
                $capacidadeTotal -= $expedicao->metal;
                $capacidadeTotal -= $expedicao->petroleo;

                $recursos->petroleo = $cidade->petroleo;
                $recursos->metal = $cidade->metal;
                $recursos->alimento = $cidade->alimento;
                $recursos->dinheiro = $cidade->dinheiro;

                //quantidade de recursos subtraidos
                $resSub = 0;

                if($recursos->dinheiro > 0)
                    $resSub++;

                if($recursos->alimento > 0)
                    $resSub++;

                if($recursos->metal > 0)
                    $resSub++;

                if($recursos->petroleo > 0)
                    $resSub++;

                //Se não existir recursos a subtrair termina o calculo aqui
                if($resSub == 0)
                    break;

                if($recursos->petroleo > ($capacidadeTotal / $resSub)) {
                    $recursos->petroleo = $capacidadeTotal / $resSub;

                    $resSub--;
                }

                $capacidadeTotal -= $recursos->petroleo;

                if($recursos->metal > $capacidadeTotal / $resSub) {
                    $recursos->metal = $capacidadeTotal / $resSub;

                    $resSub--;
                }

                $capacidadeTotal -= $recursos->metal;

                if($recursos->alimento > 0 && $recursos->alimento > $capacidadeTotal / $resSub) {
                    $recursos->alimento = $capacidadeTotal / $resSub;
                }

                $capacidadeTotal -= $recursos->alimento;

                if($recursos->dinheiro > $capacidadeTotal) {
                    $recursos->dinheiro = $capacidadeTotal;
                }

                break;
            }
        }

        $resultado['detalhes']->pontos_por_defensor = [];

        foreach($porcentagemJogadores as $id_usuario => $porcentagem) {
            $resultado['detalhes']->pontos_por_defensor[$id_usuario] = $detalhes->pontos_defensor * ($porcentagem / 100);
        }

        return $resultado;
    }

    /**
     * Gera relatório de ataque
     * @param $simulacao
     */
    public function geraRelatorioAtaque($simulacao) {
        $base64 = base64_encode(serialize($simulacao));

        $relatorios = new Relatorio();

        $relatorios->link = md5($base64 . time());
        $relatorios->dados = $base64;
        $relatorios->data = $simulacao['detalhes']->data;

        $relatorios->save();

        $usuarios = [$simulacao['detalhes']->id_atacante, $simulacao['detalhes']->id_defensor];

        $defesas = CidadeDefesa::select('cidade.id_usuario')
            ->join('cidade', 'cidade.id', '=', 'cidade_defesa.id_cidade_def')
            ->where('id_cidade', '=', $simulacao['detalhes']->id_cidade_def)->rows();

        foreach ($defesas as $defesa)
            $usuarios[] = $defesa->id_usuario;

        $usuarios = array_unique($usuarios);

        $mensagem = new Mensagem();
        $mensagem->tipo = 2;
        $mensagem->assunto = 'Combate';
        $id_mensagem = $mensagem->save();

        $mensagemTexto = new MensagemTexto();
        $mensagemTexto->id_mensagem = $id_mensagem;
        $mensagemTexto->mensagem = $simulacao['detalhes']->defensor_cidade;
        $mensagemTexto->link = $relatorios->link;
        $mensagemTexto->save();

        foreach ($usuarios as $id_usuario) {
            $mensagemUsuario = new MensagemTextoUsuario();
            $mensagemUsuario->id_mensagem = $id_mensagem;
            $mensagemUsuario->id_usuario = $id_usuario;
            $mensagemUsuario->save();

            Notificacao::add(
                $id_usuario,
                'Relatório de Ataque',
                'relatorio',
                [
                    'link' => $relatorios->link
                ],
                2,
                $id_mensagem
            );
        }
    }

    /**
     * Ação ao chegar no destino com a missão de ataque
     * @param $expedicao Model Expedicao
     * @param $simulacao
     * @return bool
     */
    private function acaoAtaque($expedicao, $simulacao) {
        //Atualiza as pontuacoes

        //Usuario de ataque
        $detalhes = $simulacao['detalhes'];

        $usuarioAtaque = Usuario::findFirst($detalhes->id_atacante);

        $usuarioAtaque->pontos_ataque = $usuarioAtaque->pontos_ataque + $detalhes->pontos_atacante;

        $usuarioAtaque->save();

        //Usuarios de defesa
        foreach ($detalhes->pontos_por_defensor as $id_defensor => $pontos) {
            $usuarioDefesa = Usuario::findFirst($id_defensor);

            $usuarioDefesa->pontos_defesa = $usuarioDefesa->pontos_defesa + $pontos;

            $usuarioDefesa->save();
        }

        //Recursos roubados
        $recursos = $simulacao['recursos'];

        $cidade = Cidade::findFirst($expedicao->id_cidade_def);

        $cidade->dinheiro = $cidade->dinheiro - $recursos->dinheiro;
        $cidade->alimento = $cidade->alimento - $recursos->alimento;
        $cidade->metal = $cidade->metal - $recursos->metal;
        $cidade->petroleo = $cidade->petroleo - $recursos->petroleo;
        $cidade->atualizacao = time();

        $cidade->save();

        //Pega somente a ultima rodada
        $simulacao = $simulacao[count($simulacao)-3];

        //altera a quantidade de unidade que há na cidade do defensor
        if($simulacao->defensor->unidades) {
            foreach ($simulacao->defensor->unidades as $index => $unidade){
                $cidadesUnidades = CidadeUnidade::findFirst([
                    'id_unidade' => $unidade->id,
                    'id_cidade' => $expedicao->id_cidade_def
                ]);

                if($unidade->quantidade<=0){
                    $cidadesUnidades->delete();
                }
                elseif($cidadesUnidades->quantidade !== $unidade->quantidade){
                    $cidadesUnidades->quantidade = $unidade->quantidade;
                    $cidadesUnidades->save();
                }
            }
        }

        //altera a quantidade de unidades de apoio que há na cidade do defensor
        if($simulacao->defesas->unidades) {
            foreach ($simulacao->defesas->unidades as $index => $unidade){
                $cidadeDefesaUnidade = CidadeDefesaUnidade::findFirst([
                    'id_unidade' => $unidade->id,
                    'id_defesa' => $unidade->id_defesa
                ]);

                if($unidade->quantidade<=0){
                    $cidadeDefesaUnidade->delete();
                }
                elseif($cidadeDefesaUnidade->quantidade !== $unidade->quantidade){
                    $cidadeDefesaUnidade->quantidade = $unidade->quantidade;
                    $cidadeDefesaUnidade->save();
                }
            }
        }

        //altera a quantidade de unidades do atacante
        $unidades = $expedicao->unidades;

        $count = $unidades->count();

        foreach ($simulacao->atacante->unidades as $index => $unidade) {
            foreach ($unidades as $unidadeExp) {
                if($unidadeExp->id == $unidade->id) {
                    if($unidade->quantidade > 0) {
                        $unidadeExp->quantidade = $unidade->quantidade;
                        $unidadeExp->save();
                    } else {
                        $unidadeExp->delete();

                        $count--;
                    }
                }
            }
        }

        if($count == 0) {
            $expedicao->delete();

            return null;
        }

        //Recursos adicionados
        $expedicao->dinheiro += $recursos->dinheiro;
        $expedicao->alimento += $recursos->alimento;
        $expedicao->metal += $recursos->metal;
        $expedicao->petroleo += $recursos->petroleo;

        $expedicao->volta = true;

        $expedicao->save();

        return $expedicao;
    }

    private function acaoColonizacao($simulacao, $expedicao) {
	    
	    return DB::saveOrFail(function (FailException $f) use ($simulacao, $expedicao) {
		    /**
		     * Se o usuário vencer a missão
		     */
		    if($simulacao['detalhes']->vencedor == 1) {
			    /**
			     * Se a cidade era de algum usuário altera o usuário que à pertence
			     */
			    if($expedicao->id_cidade_def) {
				
			    } else {
			    	###########################
				    ############################
				    ######################## ADD VERIFICAÇÃO DE SLOTS DE CIDADE
				    ###############################
			    	
				    /**
				     * Pega os dados do usuário que está atacando
				     */
				    $usuario = Usuario::findFirst($simulacao['detalhes']->id_atacante);
				
				    /**
				     * Cria uma nova cidade para o usuário
				     */
				    $novaCidade = Cidade::novaComPosicao($usuario, $expedicao->cod_ilha, $expedicao->cod_cidade);
				
				    if($novaCidade->success) {
					    $cidade = Cidade::findFirst($novaCidade->id_cidade);
					
					    $cidade->dinheiro += $expedicao->dinheiro;
					    $cidade->alimento = $cidade->alimento + $expedicao->alimento;
					    $cidade->metal = $cidade->metal + $expedicao->metal;
					    $cidade->petroleo = $cidade->petroleo + $expedicao->petroleo;
					    $cidade->atualizacao = time();
					    $cidade->save();
					
					    $expedicao->dinheiro = 0;
					    $expedicao->alimento = 0;
					    $expedicao->metal = 0;
					    $expedicao->petroleo = 0;
					
					    $expedicao->volta = true;
					
					    $expedicao->save();
					    
					    return (Object) [
					    	'success' => true,
						    'msg' => 'Parabéns, você colonizou uma nova cidade.'
					    ];
				    } else {
				    	$f->addMessage('falha ao inserir a nova cidade.');
				    }
			    }
		    }
	    }, function ($success) {
	    	return $success;
	    }, function ($f) {
	    	return (Object) [
	    		'success' => false
		    ];
	    });
	    
    }
    
	/**
	 * Gera um relatório de colonização parecido com o de ataque
	 * @param $simulacao
	 * @param $colonizacao
	 */
    public function geraRelatorioColonizacao($simulacao, $colonizacao) {
    	if($colonizacao->success === true) {
		    $base64 = base64_encode(serialize($simulacao));
		
		    $relatorios = new Relatorio();
		
		    $relatorios->link = md5($base64 . time());
		    $relatorios->dados = $base64;
		    $relatorios->data = $simulacao['detalhes']->data;
		
		    $relatorios->save();
		
		    $usuarios = [$simulacao['detalhes']->id_atacante];
		
		    if($simulacao['detalhes']->id_cidade_def) {
		    	$usuarios[] = $simulacao['detalhes']->id_defensor;
		    	
			    $defesas = CidadeDefesa::select('cidade.id_usuario')
				    ->join('cidade', 'cidade.id', '=', 'cidade_defesa.id_cidade_def')
				    ->where('id_cidade', '=', $simulacao['detalhes']->id_cidade_def)->rows();
			
			    foreach ($defesas as $defesa)
				    $usuarios[] = $defesa->id_usuario;
		    }
		
		    $usuarios = array_unique($usuarios);
		
		    $mensagem = new Mensagem();
		    $mensagem->tipo = 2;
		    $mensagem->assunto = 'Colonização';
		    $id_mensagem = $mensagem->save();
		
		    $mensagemTexto = new MensagemTexto();
		    $mensagemTexto->id_mensagem = $id_mensagem;
		    $mensagemTexto->mensagem = $simulacao['detalhes']->defensor_cidade;
		    $mensagemTexto->link = $relatorios->link;
		    $mensagemTexto->save();
		
		    foreach ($usuarios as $id_usuario) {
			    $mensagemUsuario = new MensagemTextoUsuario();
			    $mensagemUsuario->id_mensagem = $id_mensagem;
			    $mensagemUsuario->id_usuario = $id_usuario;
			    $mensagemUsuario->save();
            
                Notificacao::add(
                    $usuarios->destino,
                    'Relatório de Colonização',
                    'relatorio',
                    [
                        'link' => $relatorios->link
                    ],
                    2,
                    $id_mensagem
                );
		    }
	    }
    }

    /**
     * Ação ao chegar no destino com a missão de transporte
     * @param Expedicao $expedicao
     * @return Expedicao
     */
    private function acaoTransporte($expedicao) {
        $recursos = (Object)[
            'dinheiro' => $expedicao->dinheiro,
            'alimento' => $expedicao->alimento,
            'metal' => $expedicao->metal,
            'petroleo' => $expedicao->petroleo,
        ];
        $recursos = $recursos;

        $cidade = Cidade::findFirst($expedicao->id_cidade_def);

        $cidade->dinheiro += $expedicao->dinheiro;
        $cidade->alimento = $cidade->alimento + $expedicao->alimento;
        $cidade->metal = $cidade->metal + $expedicao->metal;
        $cidade->petroleo = $cidade->petroleo + $expedicao->petroleo;
        $cidade->atualizacao = time();
        $cidade->save();

        $expedicao->dinheiro = 0;
        $expedicao->alimento = 0;
        $expedicao->metal = 0;
        $expedicao->petroleo = 0;
        $expedicao->save();

        return $recursos;
    }

    /**
     * Gera relatório de transporte
     * @param Expedicao $expedicao
     * @param \StdClass $recursos
     */
    public function geraRelatorioTransporte($expedicao, $recursos) {
        //usuarios
        $usuarios = Expedicao::query("
            SELECT
                us_atk.id as atacante,
                us_def.id as destino
            FROM expedicao exp
            JOIN cidade cid_atk ON cid_atk.id = exp.id_cidade_atk
            JOIN cidade cid_def ON cid_def.cod_ilha = exp.cod_ilha AND cid_def.cod_cidade = exp.cod_cidade
            JOIN usuario us_atk ON us_atk.id = cid_atk.id_usuario
            JOIN usuario us_def ON us_def.id = cid_def.id_usuario
            WHERE exp.id = :id_expedicao
        ", ['id_expedicao' => $expedicao->id], true);

        //mensagem para o destino
        $mensagem = new Mensagem();
	    $mensagem->id_usuario = $usuarios->destino;
	    $mensagem->tipo = 2;
	    $mensagem->assunto = "Transporte";
	    $id_mensagem = $mensagem->save();
	
	    $mensagemTexto = new MensagemTexto();
	    $mensagemTexto->id_mensagem = $id_mensagem;
	    $mensagemTexto->mensagem = view('mensagem/textos/transporte_destino.edge', ['expedicao' => $expedicao, 'recursos' => $recursos]);
	    $mensagemTexto->data_envio = date('Y-m-d H:i:s', $expedicao->horario + $expedicao->duracao);
	    $mensagemTexto->save();
    
        Notificacao::add(
            $usuarios->destino,
            'Relatório de Transporte',
            'mensagem',
            [
                'aba' => 'visualizar',
                'id' => $id_mensagem
            ],
            1,
            $id_mensagem
        );

        //mensagem para remetente
        $mensagem = new Mensagem();
        $mensagem->id_usuario = $usuarios->atacante;
        $mensagem->tipo = 2;
        $mensagem->assunto = "Transporte";
	    $id_mensagem = $mensagem->save();
	
	    $mensagemTexto = new MensagemTexto();
	    $mensagemTexto->id_mensagem = $id_mensagem;
	    $mensagemTexto->mensagem = view('mensagem/textos/transporte_remetente.edge', ['expedicao' => $expedicao, 'recursos' => $recursos]);
	    $mensagemTexto->data_envio = date('Y-m-d H:i:s', $expedicao->horario + $expedicao->duracao);
	    $mensagemTexto->save();
    
        Notificacao::add(
            $usuarios->atacante,
            'Relatório de Transporte',
            'mensagem',
            [
                'aba' => 'visualizar',
                'id' => $id_mensagem
            ],
            1,
            $id_mensagem
        );

        $expedicao->volta = true;
        $expedicao->save();
    }

    /**
     *
     * @param Expedicao $expedicao
     */
    private function acaoDefesa($expedicao) {
        $unidades = $expedicao->unidades;
        
        //Insere uma nova defesa na cidade
        $cidadeDefesa = CidadeDefesa::insert([
            'id_cidade' => $expedicao->id_cidade_def,
            'id_cidade_def' => $expedicao->id_cidade_atk
        ]);

        //Armazena as unidades na cidade
        foreach ($unidades as $id => $unidade) {
            $atual = new CidadeDefesaUnidade();
            $atual->id_defesa = $cidadeDefesa;
            $atual->id_unidade = $unidade->id_unidade;
            $atual->quantidade = $unidade->quantidade;

            $atual->save();
        }

        //armazena os recursos
        $cidade = Cidade::findFirst($expedicao->id_cidade_atk);
        $cidade->dinheiro += $expedicao->dinheiro;
        $cidade->alimento += $expedicao->alimento;
        $cidade->metal += $expedicao->metal;
        $cidade->petroleo += $expedicao->petroleo;
        $cidade->save();
    
        /**
         * Destroi as unidades da expedição
         * pois as mesmas já foram adicionadas a defesa da cidade
         */
        $unidades->delete();
    
        /**
         * Destroi a missão de expedição
         */
        $expedicao->delete();
    }

    /**
     * Gera relatório de defesa
     * @param Expedicao $expedicao
     */
    public function geraRelatorioDefesa($expedicao) {
        try {
            DB::begin();
            
            //Usuario da missão
            $usuarioAtaque = Cidade::findFirst($expedicao->id_cidade_atk);
            
            //Usuário da cidade
            $usuarioCidade = Cidade::findFirst([
                'cod_ilha' => $expedicao->cod_ilha,
                'cod_cidade' => $expedicao->cod_cidade
            ]);
    
            //mensagem para o destino
            $mensagem = new Mensagem();
            $mensagem->id_destino = $usuarioCidade->id_usuario;
            $mensagem->tipo = 2;
            $mensagem->assunto = "Defesa";
            $id_mensagem = $mensagem->save();
    
            $mensagemTexto = new MensagemTexto();
            $mensagemTexto->id_mensagem = $id_mensagem;
            $mensagemTexto->mensagem = '';
            $mensagemTexto->data_envio = date('Y-m-d H:i:s', $expedicao->horario + $expedicao->duracao);
            $mensagemTexto->save();
    
            Notificacao::add(
                $usuarioCidade->id_usuario,
                'Relatório de Defesa',
                'mensagem',
                [
                    'aba' => 'visualizar',
                    'id' => $id_mensagem
                ],
                1,
                $id_mensagem
            );
    
            //mensagem para remetente
            $mensagem = new Mensagem();
            $mensagem->id_destino = $usuarioAtaque->id_usuario;
            $mensagem->tipo = 2;
            $mensagem->assunto = "Defesa";
            $mensagem->mensagem = '';
            $id_mensagem = $mensagem->save();
    
            $mensagemTexto = new MensagemTexto();
            $mensagemTexto->id_mensagem = $id_mensagem;
            $mensagemTexto->mensagem = '';
            $mensagemTexto->data_envio = date('Y-m-d H:i:s', $expedicao->horario + $expedicao->duracao);
            $mensagemTexto->save();
    
            Notificacao::add(
                $usuarioAtaque->id_usuario,
                'Relatório de Defesa',
                'mensagem',
                [
                    'aba' => 'visualizar',
                    'id' => $id_mensagem
                ],
                1,
                $id_mensagem
            );
    
            $expedicao->volta = true;
            $expedicao->save();
            
            DB::rollback();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
    
    private function acaoEspionagem($expedicao) {
        $cidade = Cidade::findFirst([
            'cod_ilha' => $expedicao->cod_ilha,
            'cod_cidade' => $expedicao->cod_cidade
        ]);
    
        $relatorio = new RelatorioEspionagem();
    
        $relatorio->investimento = $expedicao->dinheiro;
    
        /**
         * Perdas da cidade
         */
        if($cidade->espionagem_recurso > $expedicao->dinheiro) {
            $calcPerdas = $cidade->espionagem_recurso - $expedicao->dinheiro;
        } else {
            $calcPerdas = 0;
        }
        
        Cidade::update([
            'espionagem_recurso' => $calcPerdas
        ], [
            'id' => $cidade->id
        ]);
        
        if($expedicao->dinheiro > $cidade->espionagem_recurso) {
            $relatorio->exito = true;
            
            $relatorio->cidade = $cidade;
    
            $construcoes = CidadeConstrucao
                ::alias('cc')
                ->select(
                    'c.*',
                    'cc.level'
                )
                ->join('construcao c', 'c.id', '=', 'cc.id_construcao')
                ->where('id_cidade', '=', $cidade->id)
                ->rows();
    
            $relatorio->construcoes = $construcoes;
    
            $pesquisas = CidadePesquisa
                ::alias('cp')
                ->select(
                    'p.*',
                    'cp.level'
                )
                ->join('pesquisa p', 'p.id', '=', 'cp.id_pesquisa')
                ->where('id_cidade', '=', $cidade->id)
                ->rows();
    
            $relatorio->pesquisas = $pesquisas;
    
            $unidades = CidadeUnidade
                ::alias('cu')
                ->select(
                    'u.*',
                    'cu.quantidade'
                )
                ->join('unidade u', 'u.id', '=', 'cu.id_unidade')
                ->where('id_cidade', '=', $cidade->id)
                ->rows();
    
            $relatorio->unidades = $unidades;
        } else {
            $relatorio->cidade = Cidade
                ::select('cidade.nome')
                ->findFirst([
                'cod_ilha' => $expedicao->cod_ilha,
                'cod_cidade' => $expedicao->cod_cidade
            ]);
        }
    
        return $relatorio;
    }
    
    private function geraRelatorioEspionagem($relatorio, $expedicao) {
        try {
            DB::begin();
            
            $base64 = base64_encode(serialize($relatorio));
    
            $relatorioNovo = new Relatorio();
    
            $relatorioNovo->link = md5($base64 . time());
            $relatorioNovo->dados = $base64;
            $relatorioNovo->data = $relatorio->data;
            $relatorioNovo->tipo = 2;
            
            if(!$relatorioNovo->save()) {
                throw new \Exception('Falha ao inserir relatório.');
            }
            
            $mensagem = new Mensagem();
            $mensagem->tipo = 4;
            $mensagem->assunto = 'Espionagem';
            $id_mensagem = $mensagem->save();
    
            if(!$id_mensagem) {
                throw new \Exception('Falha ao inserir mensagem.');
            }
    
            $mensagemTexto = new MensagemTexto();
            $mensagemTexto->id_mensagem = $id_mensagem;
            $mensagemTexto->mensagem = 'Relatório de Espionagem em ' . $relatorio->cidade->nome;
            $mensagemTexto->link = $relatorioNovo->link;
    
            if(!$mensagemTexto->save()) {
                throw new \Exception('Falha ao inserir mensagem de texto.');
        
                DB::rollback();
            }
    
            $cidadeEspionou = Cidade::findFirst($expedicao->id_cidade_atk);
    
            $mensagemUsuario = new MensagemTextoUsuario();
            $mensagemUsuario->id_mensagem = $id_mensagem;
            $mensagemUsuario->id_usuario = $cidadeEspionou->id_usuario;
    
            if(!$mensagemUsuario->save()) {
                throw new \Exception('Falha ao inserir relacionamento de usuário com mensagem de texto.');
            }
    
            Notificacao::add(
                $cidadeEspionou->id_usuario,
                'Relatório de Espionagem',
                'relatorio-espionagem',
                [
                    'link' => $relatorioNovo->link
                ],
                2,
                $id_mensagem
            );
    
            $expedicao->delete();
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
	
    /**
     * Realoca as unidades na cidade natal
     * @param Expedicao $expedicao
     */
    private function regressoMissao($expedicao) {
        $volta = ($expedicao->duracao*2)+$expedicao->horario;

        if(time() >= $volta){
            //regresso a cidade
            foreach ($expedicao->unidades as $id => $unidade) {
                $atual = CidadeUnidade::findFirst([
                    'id_cidade' => $expedicao->id_cidade_atk,
                    'id_unidade' => $unidade->id_unidade
                ]);

                //Se a unidade não existir adiciona
                if($atual->count() == 0) {
                    $atual = new CidadeUnidade();
                    $atual->id_cidade = $expedicao->id_cidade_atk;
                    $atual->id_unidade = $unidade->id_unidade;
                }

                $atual->quantidade = $atual->quantidade + $unidade->quantidade;

                $atual->save();
            }

            ExpedicaoUnidade::where('id_expedicao', '=', $expedicao->id)->delete();

            //armazena os recursos
            $cidade = Cidade::findFirst($expedicao->id_cidade_atk);
            $cidade->dinheiro += $expedicao->dinheiro;
            $cidade->alimento += $expedicao->alimento;
            $cidade->metal += $expedicao->metal;
            $cidade->petroleo += $expedicao->petroleo;
            $cidade->save();

            //destroi a missao
            $expedicao->delete();
        }
    }

	/**
	 * Regras a ser executa para validar o Evento
	 */
    public function rules() {
        return true;
    }
}