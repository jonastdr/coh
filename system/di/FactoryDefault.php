<?php

namespace projectws\di;

class FactoryDefault implements \ArrayAccess {

    private $conteudo = array();

    public function __construct() {

    }

    public function offsetSet($offset, $value = '')
    {
        if (is_null($offset) && !empty($value)) {
            $this->conteudo[] = $value();
        } else {
            $this->conteudo[$offset] = $value();
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->conteudo[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->conteudo[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->conteudo[$offset]) ? $this->conteudo[$offset] : null;
    }

    public function getContent()
    {
        return $this->conteudo;
    }
}