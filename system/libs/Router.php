<?php

namespace projectws\libs;

use projectws\Loader;

class Router {
    
    public static $routes = array();

    //usado para criação de grupo de rotas
    private static $group = [];

    //As propriedades da rota atual
    private static $options;

    //As propriedades da rota por grupo
    private static $optionsGroup = [];

    /**
     * Retorna todas as rotas
     * @return array
     */
    public static function getRoutes() {
        return self::$routes;
    }

    /**
     * Método usuado para construir as opções do router
     * @param string $route
     * @param array $options
     * @return array
     */
    private static function construct($route = '') {
        if($route == '/')
            $route = '';

        $options = self::$options;

        foreach (self::$optionsGroup as $chave => $valor)
            $options[$chave] = $valor;

        if(!isset($options['action'])) {
            new Exception(Loader::getLang('Router', 'action_not_informed', $route));

            return null;
        }

        return [
            'http' => self::httpRoute($route),
            'alias' => isset($options['alias']) ? $options['alias'] : self::httpRoute($route),
            'action' => $options['action'],
            'method' => $options['method'],
            'ajax' => isset($options['ajax']) ? $options['ajax'] : false,
            'middleware' => isset($options['middleware']) ? $options['middleware'] : true,
            'response_type' => isset($options['response_type']) ? $options['response_type'] : 'html'
        ];
    }

    /**
     * Usado para construir a url | action
     * @param $name
     * @return string
     */
    private static function httpRoute($name) {
        if(count(self::$group) > 0)
            $http =  implode('/', self::$group) . ($name == '' ? $name : '/' . $name);
        else
            $http = $name;

        if(empty($http))
            $http = '/';

        return $http;
    }

    /**
     * Se $options for uma função adiciona à propriedade action da variavel $options
     * @param $options
     * @return array
     */
    private static function optionsToAction($options) {
        //A variavel options pode ser tmbm uma função que será executada como o action
        if($options instanceof \Closure) {
            $options = [
                'action' => $options
            ];
        }

        return $options;
    }

    /**
     * Constrói um rota com método GET
     * @param string $route
     * @param array $options
     */
    public static function get($route = '', $options = array()) {
        $options = self::optionsToAction($options);

        $options['method'] = 'GET';
        self::$options = $options;

        self::$routes[] = self::construct($route);

        //Reseta as propriedades
        self::$options = null;
    }

    /**
     * Constrói um rota com método POST
     * @param string $route
     * @param array $options
     */
    public static function post($route = '', Array $options = array()) {
        $options = self::optionsToAction($options);

        $options['method'] = 'POST';
        self::$options = $options;

        self::$routes[] = self::construct($route);

        //Reseta as propriedades
        self::$options = null;
    }

    /**
     * Constrói um rota com método DELETE
     * @param string $route
     * @param array $options
     */
    public static function delete($route = '', Array $options = array()) {
        $options = self::optionsToAction($options);

        $options['method'] = 'DELETE';
        self::$options = $options;

        self::$routes[] = self::construct($route);

        //Reseta as propriedades
        self::$options = null;
    }

    /**
     * Constrói um rota com método PUT
     * @param string $route
     * @param array $options
     */
    public static function put($route = '', Array $options = array()) {
        $options = self::optionsToAction($options);

        $options['method'] = 'PUT';
        self::$options = $options;

        self::$routes[] = self::construct($route);

        //Reseta as propriedades
        self::$options = null;
    }

    /**
     * Constrói um rota com método GET|POST
     * @param string $route
     * @param array $options
     */
    public static function any($route = '', Array $options = array()) {
        $methods = ['get', 'post'];

        foreach ($methods as $method)
            call_user_func_array("Router::$method", [$route, $options]);
    }

    /**
     * Constrói um rota com método GET com retorno JSON
     * @param string $route
     * @param array $options
     */
    public static function getJSON($route = '', Array $options = array()) {
        $options = self::optionsToAction($options);

        $options['method'] = 'GET';
        $options['response_type'] = 'json';
        self::$options = $options;

        self::$routes[] = self::construct($route);

        //Reseta as propriedades
        self::$options = null;
    }

    /**
     * Constrói um rota com método POST com retorno JSON
     * @param string $route
     * @param array $options
     */
    public static function postJSON($route = '', Array $options = array()) {
        $options = self::optionsToAction($options);

        $options['method'] = 'POST';
        $options['response_type'] = 'json';
        self::$options = $options;

        self::$routes[] = self::construct($route);

        //Reseta as propriedades
        self::$options = null;
    }

    private static function setOptionsGroup($options) {
        foreach ($options as $index => $valor) {
            if($index == 'ajax' || $index == 'middleware')
                self::$optionsGroup[$index] = $valor;
        }
    }

    private static function unsetOptionsGroup($options) {
        foreach ($options as $index => $valor) {
            if($index == 'ajax' || $index == 'middleware')
                unset(self::$optionsGroup[$index]);
        }
    }

    /**
     * Define um grupo de rotas
     * @param string $name
     * @throws \Exception
     */
    public static function group($name = '') {
        $name = preg_replace('/^(\/|\s)$/', '', $name);

        if($name != '') {
            self::$group[] = $name;
            $unset = true;
        } else {
            $unset = false;
        }

        $argsGroup = func_get_args();

        //Define os argumentos para todas as funções do grupo
        if(func_num_args() > 2) {
            $options = $argsGroup[1];

            if (is_array($options))
                self::setOptionsGroup($options);
        } else {
            $options = [];
        }

        //As funções são sempre o ultimo parametro
        $functions = end($argsGroup);

        //executa as funçoes determinadas pelo usuario
        if($functions instanceof \Closure)
            $functions();
        else
            new Exception(Loader::getLang('Loader', 'last_param_not_function', $name));

        //reseta as propriedades de grupo
        if($unset)
            array_pop(self::$group);

        self::unsetOptionsGroup($options);
    }

    /**
     * busca pelo alias da rota e retorna a url
     * @param $routeName
     * @return string
     */
    public static function alias() {
        $rotas = self::$routes;

        //parametros passados para a string
        $parametros = func_get_args();

        //define o nome da rota
        $routeName = $parametros[0];

        foreach ($rotas as $rota) {
            if($rota['alias'] === $routeName) {
                $routerString = preg_split('/\\\|\//', $rota['http']);

                //verifica se existe parametro da string e altera para o valor do proximo parametro
                foreach ($routerString as $i => $str) {
                    if(preg_match('/\{(.*?)\}/', $str)) {
                        $routerString[$i] = next($parametros);
                    }
                }

                return preg_replace('/\/{1,}$/', '', implode('/', $routerString));
            }
        }
    }

    /**
     * Lista os parametros
     * @param $valor
     * @return array
     */
    public static function params($valor) {
        preg_match_all('/\{(.*?)\}/', $valor, $matchs);

        return isset($matchs[1]) ? $matchs[1] : [];
    }

    /**
     * Lista as rotas
     * @return array
     */
    public static function listRoutes($search = null, $valor = null) {
        $rotas = [];

        foreach (self::$routes as $route) {
            if(($search == null && $valor == null) OR (isset($route[$search]) && $route[$search] == $valor))
                $rotas[] = "Rota ({$route['method']}" . ($route['ajax'] ? '/AJAX' : '') . ") --> " . $route['http'] . ($route['alias'] != $route['http'] ? ' alias: ' . $route['alias'] : '') . " .... Retorno: {$route['response_type']}";
        }

        return $rotas;
    }
}