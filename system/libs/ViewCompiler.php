<?php

namespace projectws\libs;

use projectws\Loader;

class ViewCompiler {

    private $conteudo = '';
    private $arquivo = '';
    private $arquivo_compilado = '';

    private $baseView = '';
	
	static $ECHO = ['{%', '%}'];

    public function __construct($arquivo) {
        $this->arquivo = $arquivo;

        $base = Loader::getBasePath();
        $app = Loader::getDir('APP');
        $dirView = Loader::getDir('VIEWS');

        //base de todas as views
        $this->baseView = $base . $app . $dirView;

        //arquivo original
        $arquivoOriginal = $this->baseView . $arquivo . '.php';

        if(!file_exists($arquivoOriginal)) {
            new Exception(Loader::getLang('View', 'include_not_found', $arquivo));

            return null;
        }

        //conteudo do template
        $this->conteudo = file_get_contents($arquivoOriginal);

        //arquivo compilado
        $this->arquivo_compilado = 'store/views/' . md5($this->arquivo) . '.php';
    }

    /**
     * Verifica a existência do arquivo compilado
     * @return bool
     */
    public function existCompile() {
        if(file_exists(Loader::getBasePath() . $this->arquivo_compilado))
            return true;

        return false;
    }

    /**
     * Cria a estrutura do arquivo compilado
     * @return mixed|string
     */
    public function make($conteudo = null) {
        $base = $this->baseView;

        if($conteudo == null) {
            $conteudo = $this->conteudo;

            $returnThis = true;
        } else {
            $returnThis = false;
        }

        //extends
        $conteudo = preg_replace_callback('/(@extends\()(.*?)(\))(.*?)(@stop)/s', function ($matchs) use ($base) {
            //remove anterior
            unset($matchs[0]);

            $arquivo = $matchs[2];

            //abre o arquivo extendido
            $extends = $this->make(file_get_contents($base . $arquivo . '.edge.php'));

            return preg_replace_callback('/(@content\()(.*?)(\))/', function ($matchsContent) use ($matchs) {
                $content = $matchsContent[2];

                $section = preg_replace_callback('/(@section\()(.*?)(\))(.*?)(@endsection)/s', function ($matchsSection) use ($content) {
                    $section = $matchsSection[2];

                    //verifica se é a sessao e retorno o conteudo
                    if($content == $section)
                        return trim($matchsSection[4]);
                }, $matchs[4]);

                //elimina os espaços
                return trim($section);
            },$extends);
        }, $conteudo);

        //include
        $conteudo = preg_replace_callback('/@include\((.*)\)/m', function ($matchs) use ($base) {
            $arquivo = $matchs[1];

            if(!file_exists($base . $arquivo . '.edge.php')) {
                new Exception(Loader::getLang('View', 'include_not_found', $arquivo));

                return null;
            }

            $contents = file_get_contents($base . $arquivo . '.edge.php');

            if(empty($contents))
                return null;

            return $this->make($contents);
        }, $conteudo);

        //comentário
        $conteudo = preg_replace('/{{--(.*?)--}}/', '<?php /*$1*/ ?>', $conteudo);

        //funcoes
        $conteudo = preg_replace('/@(elseif|if|foreach|for|while)\((.*)\)/m', '<?php $1 ($2): ?>', $conteudo);

        //else
        $conteudo = preg_replace('/@(else)/', '<?php $1: ?>', $conteudo);

        //final funcoes
        $conteudo = preg_replace('/@(endif|endforeach|endfor|endwhile|endloop)/', '<?php $1 ?>', $conteudo);

        //echo com verificação
        $conteudo = preg_replace('/{{{(.*?)}}}/', '<?php echo isset($1) ? $1 : "" ?>', $conteudo);

        //echo
        $conteudo = preg_replace('/' . ViewCompiler::$ECHO[0] . '(.*?)' . ViewCompiler::$ECHO[1] . '/', '<?php echo $1 ?>', $conteudo);

        //rules
        $conteudo = preg_replace('/{%(.*?)%}/', '<?php $1 ?>', $conteudo);

        if($returnThis) {
            $this->conteudo = $conteudo;

            return $this->conteudo;
        }

        return $conteudo;
    }

    /**
     * Verifica se o arquivo será compilado ou não
     * @param $reconstruido
     * @return bool
     */
    public function se_compilar($reconstruido) {
        $arquivo_compile = Loader::getBasePath() . $this->file();

        //se o arquivo não existir deve compilar
        if(!file_exists($arquivo_compile))
            return true;

        if(md5($reconstruido) == md5(file_get_contents($arquivo_compile)))
            return false;

        return true;
    }

    /**
     * Compila o arquivo
     */
    public function compilar() {
        //gera ou regrava o arquivo
        $this->arquivo = fopen(Loader::getBasePath() . $this->arquivo_compilado, 'w+');

        fwrite($this->arquivo, $this->conteudo);
        fclose($this->arquivo);
    }

    /**
     * Retorna o arquivo compilado
     * @return string
     */
    public function file() {
        return $this->arquivo_compilado;
    }
}