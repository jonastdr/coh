<?php

namespace projectws\libs;

use projectws\libs\Url;

class Response {

    /**
     * Método usado para redirecionar para uma página do projeto
     * @param null $path
     * @return Response|null
     */
    public static function redirect($path = null) {
        if(is_null($path))
            return new Response();

        $defDir = Url::getBaseUrl();

        $path = preg_replace('/^\/$/', '', $path);

        header("location: $defDir$path");

        return null;
    }

    /**
     * Usado para um redirecionamento por uma rota
     * @param $nome
     * @return null
     */
    public function route($nome) {
        if($nome == '')
            return null;

        $parametros = func_get_args();

        $route = call_user_func_array(['projectws\libs\Router', 'alias'], $parametros);

        static::redirect($route);
    }
}
