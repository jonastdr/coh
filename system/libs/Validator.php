<?php

namespace projectws\libs;

use projectws\libs\bridges\DB;
use projectws\libs\Request;
use projectws\Loader;

class Validator {

    private static $custom = [];
    private static $message = [];

    /*campo percorrido pelo laço de repetição*/
    private $rules = [];
    private $messages = [];
    private $campo;
    private $tipo;
    private $valor;

    /*public function __construct(array $dados = []) {
        $this->dados = $dados;

        $this->execValidation();
    }*/

	/**
	 * Método para entrada com acesso estático
	 * @param array $dados
	 * @return Validator
	 */
    public static function run(array $dados = []) {
        $class  = get_called_class();

        $novaInstancia = new $class();

        $novaInstancia->exec($dados);

        return $novaInstancia;
    }

    /**
     * @param $validator
     * @param $funcao
     */
    public static function custom($validator, $funcao) {
        //self::$custom[$validator] = $funcao;
        if($funcao())
            self::$custom[$validator] = false;
        else
            self::$custom[$validator] = true;
    }
	
    /**
     * Para verifição se existe erros de validação
     * @return bool
     */
    public static function fail() {
        if(count(self::$message) > 0) {
            $errors = [];
            foreach (self::$message as $message)
                $errors[] = $message;

            Messages::setErrors($errors);

            return true;
        }

        return false;
    }

    /**
     * Retorna as mensagens de erro
     * @return string
     */
    public function showMessages() {
        $messages = implode("\n", self::$message);

        return nl2br($messages);
    }

	/**
	 * Retorna as mensagens de erro
	 * @return array
	 */
    public function showMessagesArr() {
        return self::$message;
    }

    /* funções de validação */

    private function exec(array $dados = []) {
        if(count($dados) > 0) {
            $this->rules = $dados['rules'];
            $this->messages = $dados['messages'];
        }

        foreach ($this->rules() as $campo => $regras) {
            $this->campo = $campo;

            $this->valor = Request::getPost($campo) ? Request::getPost($campo) : Request::get($campo);

            $this->validators($regras);
        }
    }

    public function rules() {
        return $this->rules;
    }

    /**
     * @param $validators
     */
    private function validators($validators) {
        $validators = explode("|", $validators);

        //busca validator e executa a sua função
        foreach ($validators as $val) {
            $val = explode(":", $val);

            $this->tipo = $val[0];

            $method = '_' . $val[0];

            if(method_exists($this, $method)) {
                if(isset($val[1]))
                    $this->$method($val[1]);
                else
                    $this->$method();
            }

            elseif(isset(self::$custom[$val[0]])) {
                if(self::$custom[$val[0]]) {
                    $this->addMessage();
                }
            }

            else
                new Exception(Loader::getLang('Validator', 'function_not_exist', $val[0]));
        }

    }

    /**
     * Adiciona a mensagem de erro do formuário
     * @return string | Boolean
     */
    private function addMessage() {
        $campo = $this->campo;
        $tipo = $this->tipo;
        $valor = $this->valor;

        $lang = Loader::getLang('Validator', $this->tipo, $campo, $valor);

        $message = isset($this->messages()["$campo.$tipo"]) ? sprintf($this->messages()["$campo.$tipo"], $campo, $valor) : $lang;

        if(!isset(self::$message[$campo]))
            return self::$message[$campo] = $message;

        return false;
    }

    public function messages() {
        return $this->messages;
    }

    /**
     * not work
     * @return bool
     */
    private function _required() {
        if(!$this->valor)
            $this->addMessage();

        return false;
    }

    /**
     * @param $min
     * @return bool
     */
    private function _min($min) {
        if($this->valor < $min)
            $this->addMessage();

        return false;
    }

    /**
     * @param $max
     * @return bool
     */
    private function _max($max) {
        if($this->valor > $max)
            $this->addMessage();

        return false;
    }

    /**
     * @param $min
     * @return bool
     */
    private function _minstr($min) {
        if(strlen($this->valor) < $min)
            $this->addMessage();

        return false;
    }

    /**
     * @param $max
     * @return bool
     */
    private function _maxstr($max) {
        if(strlen($this->valor) > $max)
            $this->addMessage();

        return false;
    }

    /* fim de funções de validação */

    /**
     * @return bool
     */
    private function _email() {
        //verifica se e-mail esta no formato correto de escrita
        if (!preg_match('/^([a-zA-Z0-9.-])*([@])([a-z0-9]).([a-z]{2,3})/', $this->valor)){
            $this->addMessage();
        } else {
            //Valida o dominio
            $dominio = explode('@', $this->valor);

            if(!checkdnsrr($dominio[1],'A'))
                $this->addMessage();
        }

        return false;
    }

    /**
     * @return bool
     */
    private function _digit() {
        if (!preg_match('^\d+$', $this->valor))
            $this->addMessage();

        return false;
    }
	
    private function _ip() {
    if (!preg_match('\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b', $this->valor))
        $this->addMessage();

    return false;
}

    /**
     * Verifica se tem um campo único
     * Syntax: unique:tabela.campo
     * @param $tabela_campo
     * @return bool
     */
    private function _unique($tabela_campo) {
        $tabela_campo = explode(".", $tabela_campo);

        $tabela = $tabela_campo[0];

        //Verifica se o campo foi informado
        if(!isset($tabela_campo[1])) {
            new Exception(lang('Validator', 'no_informed_field'));

            return false;
        }

        $campo = $tabela_campo[1];

        if(DB::from($tabela)->where($campo, '=', $this->valor)->rows()->count() > 0)
            $this->addMessage();

        return false;
    }
}