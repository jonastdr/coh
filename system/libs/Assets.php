<?php

namespace projectws\libs;

use projectws\libs\Url;

class Assets {

    private $Css = [];
    private $Js = [];
    private static $instance;

    public function __construct() {
        self::$instance = $this;
    }
    
    public static function getInstance() {
        return self::$instance;
    }
    
    public function outputCss() {
        $retorno = implode("\n", $this->Css);
        return $retorno . "\n";
    }

    public function outputJs() {
        $retorno = implode("\n", $this->Js);
        return $retorno . "\n";
    }

    public function addCss($css) {
        $this->Css[] = '<link rel="stylesheet" type="text/css" href="' . Url::getBaseUrl() . $css . '" />';
        return $this;
    }

    public function addJs($js) {
        $this->Js[] = '<script type="text/javascript" src="' . Url::getBaseUrl() . $js . '"></script>';
        return $this;
    }

}
