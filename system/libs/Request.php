<?php

namespace projectws\libs;

class Request {

    /**
     * Retornar parametros GET
     * @param null $get
     * @return bool
     */
    public static function get($get = null, $sanitize = true) {
        $filter = $sanitize ? FILTER_SANITIZE_STRING : FILTER_DEFAULT;

        if (!$get) {
            return filter_input_array(INPUT_GET);
        }

        return filter_input(INPUT_GET, $get, $filter);
    }

    /**
     * Retornar parametros POST
     * @param null $post
     * @return bool
     */
    public static function getPost($post = null, $array = false, $sanitize = true) {
        $filter = $sanitize ? FILTER_SANITIZE_STRING : FILTER_DEFAULT;

        if (!$post) {
            return filter_input_array(INPUT_POST);
        }

        if($array === true)
            return filter_input(INPUT_POST, $post, $filter, FILTER_REQUIRE_ARRAY);

        return filter_input(INPUT_POST, $post, $filter);
    }

    /**
     * Retornar parametros FILE
     * @param null $post
     * @return bool
     */
    public static function getFile($file = null) {
        if (!$file) {
            return $_FILES;
        }

        if(isset($_FILES[$file]))
            return $_FILES[$file];

        return null;
    }

    /**
     * Usado para verificação de método de entrada
     * @param string $method
     * @return bool
     */
    public static function isMethod($method = 'GET') {
        if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') == $method) {
            return true;
        }

        return false;
    }

    /**
     * Usado para verificação se o cabeçalho é XHR
     * @return bool
     */
    public static function isAjax() {
        if(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest') {
            return true;
        }

        return false;
    }

}
