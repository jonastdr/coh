<?php

namespace projectws\libs;

use projectws\libs\Session;

class Messages {
    
    public static function setErrors($errors) {
        Session::set_flashdata('errors', $errors);
    }

    public static function getErrors() {
        return Session::get_flashdata('errors');
    }

    public static function setSuccess($errors) {
        Session::set_flashdata('success', $errors);
    }

    public static function getSuccess() {
        return Session::get_flashdata('success');
    }
}