<?php

namespace projectws\libs;

use projectws\Loader;

class Minification {

    private $config;

    public function __construct() {
        $this->config = require(Loader::getAppPath() . Loader::getDir('ASSETS') . 'min.php');
    }

    /**
     * Roda o script para construir o arquivo min
     */
    public function run() {
        foreach ($this->config as $arquivo => $params) {
            if(file_exists(Loader::getBasePath() . 'public/' . $params[0]) && Loader::getConfig('ENV') == 'master')
                return;

            $content = file_get_contents(Loader::getAppPath() . Loader::getDir('ASSETS') . $arquivo);

            //Define variavel de autor do arquivo
            $author = null;
            $comment = null;

            /**
             * Param 0 = Arquivo
             * Param 1 = Comentario OPT
             * Param 2 = Autor OPT
             */

            if(is_array($params)) {
                $arquivoMin = $params[0];
                $comment = isset($params[1]) ? $params[1] : "";
                $author = isset($params[2]) ? "\n * Author: " . $params[2] : "";
            } else {
                $arquivoMin = $params;
            }

            if (preg_match('/\.css$/', $arquivo))
            $content = $this->minCcs($content);

            if (preg_match('/\.js$/', $arquivo))
            $content = $this->minJs($content);

            if($author || $comment)
                $content = "/**\n * " . $comment . $author . "\n */\n" . $content;

            if($this->se_compilar($arquivoMin, $content))
                $this->compilar($arquivoMin, $content);
        }
    }

    /**
     * Regras para minificar um arquivo css
     * @param $content
     * @return mixed
     */
    private function minCcs($content) {
        /* comentario em bloco */
        $content = preg_replace('/\/\*(.*?)\*\//s', '', $content);

        return preg_replace('/(\\\*)(.*)(\*\/)|([\s]{2,})/', '', $content);
    }

    private function minJs($content) {
        //comentario em linha
        $content = preg_replace('/\/\/.*\n/', '', $content);

        /* comentario em bloco */
        $content = preg_replace('/\/\*(.*?)\*\//s', '', $content);

        return preg_replace('/\\\*.*\*\/|(\'.*\')|(\".*\")|(?:\s{2,})/', '$1 $2', $content);
    }

    private function se_compilar($atual, $reconstruido) {
        $arquivo_compile = Loader::getBasePath() . 'public/' . $atual;

        //se o arquivo não existir deve compilar
        if(!file_exists($arquivo_compile))
            return true;

        if(md5($reconstruido) == md5(file_get_contents($arquivo_compile)))
            return false;

        return true;
    }

    private function compilar($file, $content) {
        $file = Loader::getBasePath() . 'public/' . $file;

        $fo = fopen($file, 'w+');
        fwrite($fo, $content);
        fclose($fo);
    }

}