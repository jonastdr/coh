<?php

namespace projectws\libs;

use projectws\Loader;

/**
 * Class Database
 * @package projectws\libs
 */
class Database {

    /**
     * Instância
     * @var
     */
    private static $instance;

    /**
     * Configurações do banco de dados
     * @var
     */
    private $config;

    /**
     * Conexão com o banco de dados
     * @var
     */
    private static $connection;

    /**
     * Database constructor.
     * @param array $config
     */
    public function __construct(array $config) {
        self::$instance = $this;

        $this->config = $config;
    }

    /**
     * Retorna a instância
     * @return Database
     */
    public static function getInstance() {
        return self::$instance;
    }

    /**
     * Retorna uma configuração do banco de dados definido
     * @param $nome
     */
    public function getConfig($nome) {
        if(!isset($this->config[$nome])) {
            new Exception(Loader::getLang('Config', 'no_config', 'Database', $nome));

            return;
        }

        return $this->config[$nome];
    }

    /**
     * Define a configuração do banco de dados
     */
    public function connect() {
        $driver = $this->getConfig('DRIVER');
        $host = $this->getConfig('HOST');
        $banco = $this->getConfig('DBNAME');
        $usuario = $this->getConfig('USERNAME');
        $senha = $this->getConfig('PASSWORD');
        $charset = $this->getConfig('CHARSET');

        static::$connection = new \PDO("$driver:host=$host;dbname=$banco", $usuario, $senha,
            [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES " . $charset]);
    }

    /**
     * Retorna a conexao do banco de dados
     * @return mixed
     */
    public static function getConnection() {
        return static::$connection;
    }
}
