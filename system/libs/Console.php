<?php

namespace projectws\libs;

use projectws\Loader;
use projectws\mvc\Model;

class Console {

    private $args;
    private $command;
    private $mode;
    private $name;
    private $params;

    /**
     * [command]:[mode] [name]:[params...] [args > 2]
     * Console constructor.
     */
    public function __construct() {
        $this->args = $GLOBALS['argv'];

        if (count($this->args) > 0) {
            $commandMode = isset($this->args[1]) ? preg_split('/:/', $this->args[1], 2) : null;

            $this->command = isset($commandMode[0]) ? $commandMode[0] : null;

            $this->mode = isset($commandMode[1]) ? $commandMode[1] : null;

            $nameParams = isset($this->args[2]) ? preg_split('/:/', $this->args[2], 2) : null;

            $this->name = isset($nameParams[0]) ? $nameParams[0] : null;

            $this->params = isset($nameParams[1]) ? explode(",", str_replace('.', '\\', $nameParams[1])) : null;
        }
    }

    /**
     * Mostra a tela inicial do console
     * @return string
     */
    private function hello() {
        $msg = [];

        for($i=1;$i <= 14; $i++) {
            $msg[] = Loader::getLang('Console', 'hello' . $i);
        }

        return implode("\n", $msg);
    }

    /**
     * Verifica a existência do nome na linha de comando
     * php console command:mode name:params
     * @return bool
     */
    private function verifyName() {
        return $this->name === null ? false : true;
    }

    /**
     * Verifica a existencia de um arquivo,
     * caso o arquivo não exista mostra a mensagem do parametro $msgError
     * @param $arquivo
     * @param $msgError
     */
    private function verifyExist($arquivo, $msgError) {
        if(file_exists($arquivo . '.php'))
            $this->sendMsg($msgError);
    }

    /**
     * Abre um template de sistema
     * @param $template
     * @return string
     */
    private function getTemplate($template) {
        return file_get_contents(Loader::getBasePath() . "system/templates/$template.tpl");
    }

    /**
     * Cria um novo método para o controller
     * @param $method
     * @return string
     */
    private function createMethodController($method) {
        $method  = "    public function $method() {\n";
        $method .= "        //conteudo\n";
        $method .= "    }";

        return $method;
    }

    /**
     * Cria uma coluna para o novo model
     * @param $column
     * @return string|void
     */
    private function createColumnModel($column) {
        if(is_numeric(substr($column, 0, 1)))
            return $this->sendMsg(Loader::getLang('Console', 'column_incorrect'));

        return "        '$column'";
    }
    
    /**
     * Cria PHPDoc
     * @param $column
     * @return string|void
     */
    private function createColumnPropertyDocModel($column) {
        if(is_numeric(substr($column, 0, 1)))
            return $this->sendMsg(Loader::getLang('Console', 'column_incorrect'));
        
        $tipo = $column == 'id' ? 'int' : 'string';
        
        return " * @property $tipo $column";
    }
    
    /**
     * Cria e grava um arquivo
     * @param $file
     * @param $content
     */
    private function createFile($file, $content) {
        $arquivo = fopen($file . '.php', 'x+');
        fwrite($arquivo, $content);
        fclose($arquivo);
    }

    /**
     * Cria um controlador novo
     * Format Ex:
     * php console make:controller controller_nome:index,login,logout
     */
    private function makeController() {
        if($this->verifyName()) {
            $newController = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('CONTROLLERS');

            $this->verifyExist($destino.$newController, Loader::getLang('Console', 'controller_exist', $newController));

            //Seleciona o template de criação
            $content = $this->getTemplate('controller');

            //CONFIG NAMESPACE
            $APP_FOLDER = preg_replace('/\/|\\\/', '', Loader::getDir('APP'));
            $CONTROLLER_FOLDER = preg_replace('/\/|\\\/', '', Loader::getDir('CONTROLLERS'));

            //APP NAMESPACE
            $content = str_ireplace('{{APP_FOLDER}}', $APP_FOLDER, $content);

            //CONTROLLER NAMESPACE
            $content = str_ireplace('{{CONTROLLER_FOLDER}}', $CONTROLLER_FOLDER, $content);

            $content = str_ireplace('{{CLASSE}}', $newController, $content);

            //quantidade de metódos do controller
            $methods = [];

            if(count($this->params) > 0) {
                foreach ($this->params as $method) {
                    $methods[] = $this->createMethodController($method);
                }
            } else {
                $methods[] = $this->createMethodController('index');
            }

            $content = str_ireplace('{{METHODS}}', implode("\n\n", $methods), $content);

            //grava o arquivo
            $this->createFile($destino.$newController, $content);

            $this->sendMsg(Loader::getLang('Console', 'controller_create', $newController));
        } else {
            $this->sendMsg(Loader::getLang('Console', 'controller_help'));
        }
    }

    /**
     * Cria um modelo novo
     * Format Ex:
     * php console make:model modelo_nome:id,coluna1,coluna2
     */
    private function makeModel() {
        if($this->verifyName()) {
            $newModel = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('MODELS');

            $this->verifyExist($destino . $newModel, Loader::getLang('Console', 'model_exist', $newModel));

            //Seleciona o template de criação
            $content = $this->getTemplate('model');

            //CONFIG NAMESPACE
            $APP_FOLDER = preg_replace('/\/|\\\/', '', Loader::getDir('APP'));
            $MODEL_FOLDER = preg_replace('/\/|\\\/', '', Loader::getDir('MODELS'));

            //APP NAMESPACE
            $content = str_ireplace('{{APP_FOLDER}}', $APP_FOLDER, $content);

            //MODEL NAMESPACE
            $content = str_ireplace('{{MODEL_FOLDER}}', $MODEL_FOLDER, $content);

            $content = str_ireplace('{{MODEL_NAME}}', $newModel, $content);

            //Cria colunas do model -- OPCIONAL
            $columns = [];
            $columns_doc = [];

            if(count($this->params) > 0) {
                if(count(array_unique($this->params)) < count($this->params))
                    $this->sendMsg(Loader::getLang('Console', 'duplicate_column'));

                foreach ($this->params as $column) {
                    $columns[] = $this->createColumnModel($column);
                }
    
                foreach ($this->params as $column) {
                    $columns_doc[] = $this->createColumnPropertyDocModel($column);
                }
            }
    
            /**
             * Criação de PHPDoc
             */
            if(count($columns_doc))
                $content = str_ireplace('{{MODEL_COLUNAS_DOC}}', implode("\n", $columns_doc), $content);
            else
                $content = str_ireplace('{{MODEL_COLUNAS_DOC}}', "", $content);
        
            $content = str_ireplace('{{MODEL_COLUNAS}}', implode(",\n", $columns), $content);

            //grava o arquivo
            $this->createFile($destino.$newModel, $content);

            $this->sendMsg(Loader::getLang('Console', 'model_create', $newModel));
        } else {
            $this->sendMsg(Loader::getLang('Console', 'model_help'));
        }
    }

    /**
     * Cria uma view nova
     * Format Ex:
     * php console make:view index/inicio
     */
    private function makeView() {
        if($this->verifyName()) {
            $newView = $this->name;

            $destino = addslashes(Loader::getAppPath() . Loader::getDir('VIEWS'));

            $params = preg_split('/\\\|\//', $this->name);

            if(count($params) > 1){
                $dirs = $params;

                array_pop($dirs);

                $lastDir = "";

                foreach ($dirs as $dir) {
                    if(!file_exists($destino  . $lastDir . $dir))
                        mkdir($destino  . $lastDir . $dir);

                    $lastDir .= $dir . '/';
                }
            }

            //Verifica se o arquivo já não existe
            $this->verifyExist($destino.$newView, Loader::getLang('Console', 'view_exist', $newView));

            //Seleciona o template de criação
            $content = $this->getTemplate('view');

            //grava o arquivo
            $this->createFile($destino.$newView, $content);

            $this->sendMsg(Loader::getLang('Console', 'view_create', $newView));
        } else {
            $this->sendMsg(Loader::getLang('Console', 'view_help'));
        }
    }

    /**
     * Cria um evento novo
     * Format Ex:
     * php console make:event evento_nome
     */
    private function makeEvent() {
        if($this->verifyName()) {
            $newEvent = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('EVENTS');

            //Verifica a existencia do arquivo
            $this->verifyExist($destino . $newEvent, Loader::getLang('Console', 'event_exist', $newEvent));

            //Seleciona o template de criação
            $content = $this->getTemplate('event');

            $content = str_ireplace('{{CLASSE}}', $newEvent, $content);

            //grava o arquivo
            $this->createFile($destino . $newEvent, $content);

            $this->sendMsg(Loader::getLang('Console', 'event_create', $newEvent));
        } else {
            $this->sendMsg(Loader::getLang('Console', 'event_help'));
        }
    }

    /**
     * Cria um middleware novo
     * Format Ex:
     * php console make:middleware middleware_nome
     */
    private function makeMiddleware() {
        if($this->verifyName()) {
            $newEvent = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('MIDDLEWARES');

            //Verifica a existencia do arquivo
            $this->verifyExist($destino . $newEvent, Loader::getLang('Console', 'middleware_exist', $newEvent));

            //Seleciona o template de criação
            $content = $this->getTemplate('middleware');

            $content = str_ireplace('{{CLASSE}}', $newEvent, $content);

            //grava o arquivo
            $this->createFile($destino . $newEvent, $content);

            $this->sendMsg(Loader::getLang('Console', 'middleware_create', $newEvent));
        } else {
            $this->sendMsg(Loader::getLang('Console', 'middleware_help'));
        }
    }

    /**
     * Remove um controller
     * Format Ex:
     * php console remove:controller controller_nome
     */
    private function removeController() {
        if($this->verifyName()) {
            $controller = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('CONTROLLERS');

            $arquivo = $destino . $controller . '.php';

            if (!file_exists($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'controller_not_exist', $controller));
            }

            if (unlink($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'controller_remove', $controller));
            } else {
                $this->sendMsg(Loader::getLang('Console', 'controller_remove_error'));
            }
        } else {
            $this->sendMsg(Loader::getLang('Console', 'controller_remove_help'));
        }
    }

    /**
     * Remove um model
     * Format Ex:
     * php console remove:model model_nome
     */
    private function removeModel() {
        if($this->verifyName()) {
            $model = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('MODELS');

            $arquivo = $destino . $model . '.php';

            if (!file_exists($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'model_not_exist', $model));
            }

            if (unlink($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'model_remove', $model));
            } else {
                $this->sendMsg(Loader::getLang('Console', 'model_remove_error'));
            }
        } else {
            $this->sendMsg(Loader::getLang('Console', 'model_remove_help'));
        }
    }

    /**
     * Remove uma View
     * Format Ex:
     * php console remove:view view_nome
     */
    private function removeView() {
        if($this->verifyName()) {
            $view = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('VIEWS');

            $arquivo = $destino . $view . '.php';

            if (!file_exists($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'view_not_exist', $view));
            }

            if (unlink($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'view_remove', $view));
            } else {
                $this->sendMsg(Loader::getLang('Console', 'view_remove_error'));
            }
        } else {
            $this->sendMsg(Loader::getLang('Console', 'view_remove_help'));
        }
    }

    /**
     * Remove um evento
     * Format Ex:
     * php console remove:event event_nome
     */
    private function removeEvent() {
        if($this->verifyName()) {
            $event = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('EVENTS');

            $arquivo = $destino . $event . '.php';

            if (!file_exists($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'event_not_exist', $event));
            }

            if (unlink($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'event_remove', $event));
            } else {
                $this->sendMsg(Loader::getLang('Console', 'event_remove_error'));
            }
        } else {
            $this->sendMsg(Loader::getLang('Console', 'event_remove_help'));
        }
    }

    /**
     * Remove um middleware
     * Format Ex:
     * php console remove:middleware middleware_nome
     */
    private function removeMiddleware() {
        if($this->verifyName()) {
            $middleware = $this->name;

            $destino = Loader::getAppPath() . Loader::getDir('MIDDLEWARES');

            $arquivo = $destino . $middleware . '.php';

            if (!file_exists($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'middleware_not_exist', $middleware));
            }

            if (unlink($arquivo)) {
                $this->sendMsg(Loader::getLang('Console', 'middleware_remove', $middleware));
            } else {
                $this->sendMsg(Loader::getLang('Console', 'middleware_remove_error'));
            }
        } else {
            $this->sendMsg(Loader::getLang('Console', 'middleware_remove_help'));
        }
    }

    /**
     * Faz a leitura do arquivo de execução de rotas
     */
    private function execFileRoutes() {
        $arquivo = Loader::getAppPath() . 'routes.php';

        $this->verifyExist($arquivo, Loader::getLang('Console', 'routes_file_not_exist'));

        require($arquivo);
    }

    /**
     * Lista as rotas
     * Possíveis métodos: method, url, ajax, middleware
     */
    private function routes() {
        $this->execFileRoutes();

        $valor = $this->params[0] !== null ? $this->params[0] : null;

        $possible = [
            'method'     => 'method',
            'url'        => 'http',
            'ajax'       => 'ajax',
            'middleware' => 'middleware'
        ];
        $messages = [
            'console routes method:GET|POST',
            'console routes url:/',
            'console routes ajax',
            'console routes middleware:app.middleswares.Validator'
        ];

        if($this->name !== null) {
            if (!array_key_exists($this->name, $possible))
                $this->sendMsg(Loader::getLang('Console', 'whats') . "\n    " . implode("\n    ", $messages));

            if ($this->name == 'ajax')
                $valor = true;

            $msg = Loader::getLang('Console', 'route_list_param', $this->name, $valor) . "\n";
        } else {
            $msg = Loader::getLang('Console', 'route_list_all') . "\n";
        }

        $routes = Router::listRoutes($this->name, $valor);

        if(count($routes) > 0)
            $this->sendMsg($msg . implode("\n", $routes));
        else
            $this->sendMsg(Loader::getLang('Console', 'route_none'));
    }

    /**
     * Mostra o log de erros do sistema
     */
    private function log() {
        $log_file = Loader::getBasePath() . 'store/logs/';

        if ($this->verifyName())
            $log_file .= 'errors_' . $this->name . '.log';
        else
            $log_file .= 'errors_' . date('Y-m-Y') . '.log';

        //verifica a existencia do arquivo
        if(!file_exists($log_file))
            $this->sendMsg(Loader::getLang('Console', 'log_not_exist', $log_file));

        $log = file_get_contents($log_file);

        $this->sendMsg($log);
    }

    /**
     * Faz atualização de tabelas do banco de dados
     */
    private function dbUpdate() {
        Database::getInstance()->connect();

        $models = File::listModels();

        $atualizacoes = [];
        foreach ($models as $model) {
            $model = new $model();

            if($model instanceof Model)
                if($message = $model->updateTables())
                    $atualizacoes[] = $message;
        }

        if(count($atualizacoes))
            $this->sendMsg(implode("\n", $atualizacoes));
        else
            $this->sendMsg(Loader::getLang('Console', 'migrate_no_changes'));
    }

    /**
     * Para envio final de uma mensagem no terminal
     * @param $msg
     */
    private function sendMsg($msg) {
        die($msg . "\n");
    }

    /**
     * Comandos possivel para utilizar no terminal
     */
    private function possibleCommands() {
        $commands = [
            'make',
            'remove',
            'routes',
            'log',
            'db'
        ];

        if(!in_array($this->command, $commands))
            $this->sendMsg(Loader::getLang('Console', 'command_not_exist'));
    }

    /**
     * Faz a leitura de um comando no Console e verifica se é válido
     */
    private function readCommand() {
        $method = $this->command . ucwords($this->mode);

        if(method_exists($this, $method)) {
            $this->$method();
        } else {
            $this->sendMsg($method);
        }
    }

    /**
     * Executa a aplicação do Console
     */
    public function run() {
        if(count($this->args) == 1) {
            $this->sendMsg($this->hello());
        } else {
            $this->possibleCommands();

            $this->readCommand();
        }
    }
}