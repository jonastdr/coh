<?php

namespace projectws\libs;

class Collection implements \ArrayAccess, \Iterator {

    protected $items = [];

    /**
     * Retorna array de items
     * @return array
     */
    public function toArray() {
        return $this->items;
    }

    /**
     * Retorna um json de items
     * @return json
     */
    public function toJson() {
        return json_encode($this->items);
    }

    /**
     * Executa uma função para cada item
     * @param callable $callback
     * @return array
     */
    public function each(callable $callback) {
        $each = [];

        foreach ($this->items as $item) {
            $each[] = $callback($item);
        }

        return $each;
    }

    /**
     * Retorna o primeiro da lista
     * @return mixed
     */
    public function first() {
        return $this->items[0];
    }

    /**
     * Retorna o ultimo da lista
     * @return mixed
     */
    public function last() {
        return end($this->items);
    }

    /**
     * Quantidade de items
     * @return int
     */
    public function count() {
        return count($this->items);
    }

    /**
     * Seleciona um valor
     * @param mixed $offset
     * @return null
     */
    public function offsetGet($offset) {
        return isset($this->items[$offset]) ? $this->items[$offset] : null;
    }

    /**
     * Define um novo valor
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) {
        if(is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    /**
     * Destroi um valor
     * @param mixed $offset
     */
    public function offsetUnset($offset) {
        unset($this->items[$offset]);
    }

    /**
     * Verifica a existência do valor
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->items[$offset]);
    }

    /**
     * Retorna ao inicio
     */
    public function rewind()
    {
        reset($this->items);
    }

    /**
     * Retorna atual
     * @return mixed
     */
    public function current() {
        return current($this->items);
    }

    /**
     * Retorna chave
     * @return mixed
     */
    public function key() {
        return key($this->items);
    }

    /**
     * Retorna o proximo
     * @return mixed
     */
    public function next() {
        return next($this->items);
    }

    /**
     * Retorna todos os items
     * @return bool
     */
    public function valid() {
        $key = key($this->items);
        $items = ($key !== NULL && $key !== FALSE);

        return $items;
    }

}