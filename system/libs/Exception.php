<?php

namespace projectws\libs;

use projectws\Loader;

class Exception extends \Exception {

    private static $errors = [];
    private static $type;

    public function __construct($erro, $type = 404) {
        if(env('ENV') == 'system')
            $erro .= ' <span class="file">' . $this->getFile() . '</span>' . ' <small>linha ' . $this->getLine() . '</small>';

        self::$errors[] = $erro;

        self::$type = $type;
    }

    public static function error_handler($no, $msg, $file, $line) {
        self::$errors[] = $msg . ' <span class="file">' . $file . '</span>' . ' <small>linha ' . $line . '</small>';
    }

    public static function exception_handler($exception) {
	    $file = $exception->getFile();
	    $line = $exception->getLine();
    	
        self::$errors[] = $exception->getMessage() . ' <span class="file">' . $file . '</span>' . ' <small>linha ' . $line . '</small>';
    }

    public static function shutdown_function() {
        $error = error_get_last();

        if ($error)
            self::$errors[] = $error['message'] . ' <span class="file">' . $error['file'] . '</span>' . ' <small>linha ' . $error['line'] . '</small>';

        //registra logs caso esteja ativo
        if(env('LOG') === true && count(self::$errors) > 0)
            self::registerLog();

        if(env('ENV') == 'master')
            return Html::page(self::$type);

	    if(env('ENV') == 'production')
	    	return;
	    
        if(count(self::$errors) > 0)
            throw new Exception(Html::showErros(self::$errors));
    }

    public static function registerLog() {
        $base = Loader::getBasePath();
        $arquivoStr = $base . 'store/logs/errors_' . date('d-m-Y') . '.log';

        //verifica se o diretorio pode ser escrito
        if(!is_writable(dirname($base . 'store/logs'))) {
            new Exception(Loader::getLang('Exception', 'no_permission'));

            return null;
        }

        if($arquivo = fopen($arquivoStr, 'a+')) {
            $errors = [];
            foreach (self::$errors as $error) {
                $errors[] = date('d/m/Y H:i:s') . " " . strip_tags($error);
            }

            fwrite($arquivo, (filesize($arquivoStr) > 0 ? "\n" : "") . implode("\n", $errors));

            fclose($arquivo);
        } else {
            new Exception(Loader::getLang('Exception', 'log_not_exist'));
        }
    }
}