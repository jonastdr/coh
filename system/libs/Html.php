<?php

namespace projectws\libs;

use projectws\Loader;

class Html {
    /**
     * Mostra os erros
     * @param array $errors
     */
    public static function showErros($errors = []) {
        $arquivo = Loader::getBasePath() . 'system/html/errors.php';

        if(!file_exists($arquivo)) {
            echo Loader::getLang('Html', 'template_not_found', 'errors');

            return null;
        }

        require(Loader::getBasePath() . 'system/html/errors.php');
    }

    /**
     * Mostra um template de página
     * @param int $type
     */
    public static function page($type = 500) {
        $arquivo = Loader::getBasePath() . 'system/html/' . $type . '.php';

        if(!file_exists($arquivo)) {
            echo Loader::getLang('Html', 'template_not_found', $type);

            return null;
        }

        require(Loader::getBasePath() . 'system/html/' . $type . '.php');
    }
}