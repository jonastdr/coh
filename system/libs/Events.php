<?php

namespace projectws\libs;

class Events {

    protected $active = false;

    public function isActive() {
        return $this->active;
    }

    /**
     * Execução final da classe
     */
    public function run() { }

    /**
     * Regras para execução
     * @return array
     */
    public function rules() {
        return true;
    }
}