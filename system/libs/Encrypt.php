<?php

namespace projectws\libs;

use projectws\Loader;

class Encrypt {

    public static function make($pass)
    {
        $key = Loader::getConfig('KEY_ENCRYPT');
        $iv = "fYfhHeDm";// 8 bit IV
        $bit_check = 8;

        $novaSenha = array();

        for ($i = 0; $i < strlen(md5($pass)); $i++) {
            $novaSenha[] = md5($pass . substr($pass, 0, $i));
        }

        $novaSenha = implode(md5($key), $novaSenha);

        $novaSenha = md5($novaSenha);

        return self::encrypt($novaSenha,$key,$iv,$bit_check);
    }

    private static function encrypt($text,$key,$iv,$bit_check) {
        $text_num =str_split($text,$bit_check);
        $text_num = $bit_check-strlen($text_num[count($text_num)-1]);
        for ($i=0;$i<$text_num; $i++) {
            $text = $text . chr($text_num);
        }
        $cipher = mcrypt_module_open(MCRYPT_TRIPLEDES,'','cbc','');
        mcrypt_generic_init($cipher, $key, $iv);
        $decrypted = mcrypt_generic($cipher,$text);
        mcrypt_generic_deinit($cipher);
        return base64_encode($decrypted);
    }

}