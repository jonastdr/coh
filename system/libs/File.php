<?php

namespace projectws\libs;

use projectws\Loader;

class File {

    public static function listModels() {
        $dir = new \DirectoryIterator(Loader::getAppPath() . Loader::getDir('MODELS'));

        $models = [];

        foreach ($dir as $info) {
            if(!$info->isDot()) {
                $models[] = preg_replace("/(.php)$/", "", $info->getFilename());
            }
        }

        return $models;
    }

    public static function listEvents() {
        $dir = new \DirectoryIterator(Loader::getAppPath() . Loader::getDir('EVENTS'));

        $events = [];

        foreach ($dir as $info) {
            if(!$info->isDot()) {
                $event = Loader::getDir('APP') . Loader::getDir('EVENTS') . preg_replace("/(.php)$/", "", $info->getFilename());
                $events[] = preg_replace("/\//", "\\", $event);
            }
        }

        return $events;
    }

}