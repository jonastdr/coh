<?php

namespace projectws\libs;

use projectws\Loader;

class Session {

    public static function set_flashdata($name, $value) {
        $app = Loader::getConfig('NAME');

        $name = $app . '.' . $name;

        $_SESSION[$name] = serialize($value);
    }

    public static function get_flashdata($name) {
        $app = Loader::getConfig('NAME');

        $name = $app . '.' . $name;

        if(isset($_SESSION[$name]))
            $value = unserialize($_SESSION[$name]);
        else
            $value = null;

        if(isset($_SESSION[$name]))
            session_unset($_SESSION[$name]);

        return $value;
    }
    
    public static function get($name) {
        $app = Loader::getConfig('NAME');
    
        $name = $app . '.' . $name;
    
        if(isset($_SESSION[$name]))
            $value = unserialize($_SESSION[$name]);
        else
            $value = null;
    
        return $value;
    }
    
    public static function set($name, $value) {
        $app = Loader::getConfig('NAME');
    
        $name = $app . '.' . $name;
    
        $_SESSION[$name] = serialize($value);
    }
    
    public static function destroy($name) {
        $app = Loader::getConfig('NAME');
    
        $name = $app . '.' . $name;
        
        if(isset($_SESSION[$name])) {
            session_unset($_SESSION[$name]);
            
            return true;
        }
        
        return false;
    }

}
