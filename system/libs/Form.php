<?php
/**
 * Created by PhpStorm.
 * User: jonas
 * Date: 30/06/15
 * Time: 23:14
 */

namespace projectws\libs;

use projectws\libs\Url;

/**
 * Class Form
 * @package projectws\libs
 */

class Form {

    /**
     * Método construtor de tags
     * @param $tag
     * @param $params
     * @param bool $close
     * @return string
     */
    private static function _tag($tag, $params, $texto = false) {
        if($texto !== false)
            $texto = '>' . $texto . '</' . $tag . '>';
        else
            $texto = '>';

        $parametros = [];

        foreach ($params as $nome => $valor)
            if(!is_null($valor))
                $parametros[] = $nome . '="' . $valor . '"';

        return '<' . $tag . ' ' . implode(' ', $parametros) . $texto;
    }

    /**
     * metódo para criar os parametros da tag
     * @param array $parametros
     * @param array $novos
     * @return array
     */
    private static function _params(Array $parametros, Array $novos) {
        foreach ($novos as $nome => $valor)
            $parametros[$nome] = $valor;

        return $parametros;
    }

    private static function _input($type, $name, $valor) {
        $parametros = [
            'type' => $type,
            'id' => $name,
            'name' => $name,
            'value' => $valor
        ];

        return $parametros;
    }

    public static function open($action = '', $method = 'POST', $params = array()) {
        if(!preg_match('/^(GET|POST)$/i', $method)) {
            new Exception('Metodo inválido.');
        }

        $parametros = [
            'action' => $action,
            'method' => strtoupper($method)
        ];

        //Usado para formularios com upload
        foreach ($params as $param => $value) {
            if($param == 'file' && $value === true) {
                unset($params[$param]);

                $params["enctype"] = "multipart/form-data";
            }
        }

        $parametros = self::_params($parametros, $params);

        return self::_tag('form', $parametros);
    }

    public static function close() {
        return '</form>';
    }

    /**
     * Cria template da tag label
     * @param string $name
     * @param string $inner
     * @param array $params
     * @return string
     */
    public static function label($name = '', $inner = '', $params = array()) {
        $parametros = [
            'for' => $name
        ];

        $parametros = self::_params($parametros, $params);

        return self::_tag('label', $parametros, $inner);
    }

    /**
     * Cria template da tag button
     * @param string $name
     * @param string $inner
     * @param array $params
     * @return string
     */
    public static function button($name = '', $inner = 'Ok', $params = array()) {
        $parametros = [
            'id' => $name,
            'name' => $name
        ];

        $parametros = self::_params($parametros, $params);

        return self::_tag('button', $parametros, $inner);
    }

    /**
     * Cria template de input tipo text
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function text($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('text', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template de input tipo password
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function password($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('password', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template de input tipo number
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function number($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('number', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template de input tipo radio
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function radio($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('radio', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template de input tipo checkbox
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function checkbox($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('checkbox', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template de input tipo range
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function range($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('range', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template do input tipo time
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function time($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('time', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template do input tipo hidden
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function hidden($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('hidden', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template do input tipo color
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function color($name = '', $valor = null, $params = array()) {
        $parametros = self::_params(self::_input('color', $name, $valor), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template do input tipo file
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function file($name = '', $params = array()) {
        $parametros = self::_params(self::_input('file', $name, null), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template do input tipo tel
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function tel($name = '', $params = array()) {
        $parametros = self::_params(self::_input('tel', $name, null), $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template de input tipo submit
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function submit($valor = null, $params = array()) {
        $parametros = self::_params(['type' => 'submit', 'value' => $valor], $params);

        return self::_tag('input', $parametros);
    }

    /**
     * Cria template da tag textarea
     * @param string $name
     * @param null $valor
     * @param array $params
     * @return string
     */
    public static function textarea($name = '', $valor = null, $params = array()) {
        if(is_bool($valor)) {
            new Exception("O valor não poder ser booleano!");

            return null;
        }

        $parametros = [
            'id' => $name,
            'name' => $name,
        ];

        $parametros = self::_params($parametros, $params);

        return self::_tag('textarea', $parametros, $valor);
    }

    /**
     * Cria template da tag select
     * @param string $name
     * @param array $options
     * @param null $default
     * @param array $params
     * @return string
     */
    public static function select($name = '', $options = [], $default = null, $params = array()) {
        $parametros = [
            'id' => $name,
            'name' => $name,
            'valor' => $default
        ];

        $parametros = self::_params($parametros, $params);

        $option = [];

        foreach ($options as $value => $texto) {
            $paramOption = [];

            $paramOption["value"] = $value;

            if($default !== null && $value == $default)
                $paramOption["selected"] = "selected";

            //Cria um grupo
            if(is_array($texto)) {
                $optoption = [];

                foreach ($texto as $value2 => $texto2) {
                    $paramOptOption["value"] = $value2;

                    if ($default !== null && $value2 == $default)
                        $paramOptOption["selected"] = true;

                    $optoption[] = self::_tag('option', $paramOptOption, $texto2);
                }

                $option[] = self::_tag('optgroup', ['label' => $value]) . implode($optoption) . '</optgroup>';
            } else {
                $option[] = self::_tag('option', $paramOption, $texto);
            }
        }

        return self::_tag('select', $parametros) . implode($option) . '</select>';
    }
}