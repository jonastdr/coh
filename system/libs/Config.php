<?php

namespace projectws\libs;

use projectws\Loader;

/**
 * Classe responsavel por obter dados de configurações da aplicação
 * Class Config
 * @package projectws\libs
 */
class Config {

    /**
     *
     * @return string Base path dos arquivos de config da aplicação
     */
    protected static function getBaseConfig() {
        return Loader::getBasePath() . 'config/';
    }

    /**
     * Retorna uma config
     * @param string $name
     * @return mixed
     */
    public static function get($name = null) {
        $file = static::getBaseConfig() . $name . '.php';

        if(!file_exists($file)) {
            if(is_null($name))
                $name = 'undefined';

            Loader::getLang('Config', 'no_config', 'Config', $name);

            return;
        }

        return require($file);
    }

}