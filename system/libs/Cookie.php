<?php

namespace projectws\libs;

class Cookie {

    /**
     * Define um cookie, por padrão é definido com 2h
     * @param $name
     * @param $value
     * @param int $expireTime
     * @param string $path
     * @return mixed
     */
    public static function set($name, $value = null, $expireTime = 7200, $path = '/') {
        if($value == null)
            new Exception('Deve definir um cookie deve informar o segundo parametro!');

        setcookie($name, base64_encode(serialize($value)), time() + $expireTime, $path);

        return self::get($name);
    }

    /**
     * Destrói cookie
     * @param $name
     */
    public static function destroy($name = '') {
        setcookie($name, '', time());
    }

    public static function get($name) {
        return unserialize(base64_decode(filter_input(INPUT_COOKIE, $name)));
    }

}
