<?php

namespace projectws\libs;

use projectws\Loader;

class Upload {

    private $files = [];
    private $path;
    private $maxSize = 5000000;

    private $multiple = false;

    private $types = [
        'image/png',
        'image/jpeg',
        'audio/mp3'
    ];

    private $thumb = false;
    private $thumbWidth = 80;
    private $thumbPrefix = 'thumb';

    private $generateFiles = [];

    //infos para o arquivo atual
    private $typeAtual;

    private $msg = [];

    public function __construct($files) {
        $this->setPath();

        if(array_key_exists(0, $files))
            $this->files = $files;
        else
            $this->files = [$files];
    }

    /**
     * Define que o upload é de multiple
     */
    public function multiple() {
        $this->multiple = true;
    }

    /**
     * Define o tamanho maximo em Mb
     * @param $size
     */
    public function setMaxSize($size) {
        $this->maxSize = 100000 * $size;
    }

    /**
     * Define aonde o arquivo será enviado
     * @param $path
     */
    public function setPath($path = '') {
        $this->path = $path;
    }

    /**
     * Define se terá miniatura e define o tamanho
     * @param int $width
     */
    public function thumb($width = 80) {
        $this->thumbWidth = $width;

        $this->thumb = true;
    }

    /**
     * Faz o envio do arquivo
     * @return array|bool
     */
    public function send() {
        if($this->multiple)
            return $this->sendMultiple();

        foreach ($this->files as $file) {
            if(!in_array($file['type'], $this->types)) {
                $this->msg[] = "Arquivo não permitido.";
            }

            if($file['error'] != 0) {
                $this->msg[] = "Falha no envio do arquivo.";
            }

            if($file['size'] > $this->maxSize) {
                $this->msg[] = "Limite de tamanho ultrapassado.";
            }

            $navDir = Loader::getStoreDir();

            $baseDir = $navDir . $this->path . '/';

            if(!file_exists($baseDir)) {
                $dirs = explode("/", $this->path);

                foreach ($dirs as $dir) {
                    if(!file_exists($dir))
                        mkdir($navDir . $dir);

                    $navDir .= $dir . "/";
                }
            }

            $genName = sha1_file($file['tmp_name']);
            $type = explode('.', $file['name']);
            $type = '.' . strtolower(end($type));

            $baseFile = $baseDir . $genName . $type;


            if(move_uploaded_file($file['tmp_name'], $baseFile)) {
                if($this->thumb && preg_match('/image/', $file['type'])) {
                    $this->nameAtual = $genName;
                    $this->typeAtual = preg_replace('/\./', '', $type);

                    $thumb = $this->makeThumb($genName, $type);
                }

                if(isset($thumb))
                    $this->generateFiles[] = [$this->path . '/' . $genName . $type, $thumb];
                else
                    $this->generateFiles[] = $this->path . '/' . $genName . $type;
            }
        }

        if(count($this->generateFiles) > 0)
            return true;

        return false;
    }

    /**
     * Retorna os arquivos enviados
     * @return array
     */
    public function getFiles() {
        return $this->generateFiles;
    }

    /**
     * Retonar as mensagens de falha
     * @return array
     */
    public function failMessages() {
        return $this->msg;
    }

    /**
     * Faz o envio do arquivo
     * @return array|bool
     */
    private function sendMultiple() {
        foreach ($this->files['name'] as $i => $name) {
            if($this->files['error'][$i] != 0) {
                new Exception("Falha no envio do arquivo!");
            }

            if($this->files['size'][$i] > $this->maxSize) {
                new Exception("Limite de tamanho ultrapassado!");
            }

            if(!file_exists(Loader::getBasePath() . $this->path)) {
                new Exception("O local definido para envio do arquivo não existe!");

                continue;
            }

            $genName = sha1_file($this->files['tmp_name'][$i]);
            $type = explode('.', $name);
            $type = '.' . strtolower(end($type));

            $baseFile = Loader::getBasePath() . $this->path . '/' . $genName . $type;

            if(move_uploaded_file($this->files['tmp_name'][$i], $baseFile)) {
                if($this->thumb && preg_match('/image/', $this->files['type'][$i])) {
                    $this->nameAtual = $genName;
                    $this->typeAtual = preg_replace('/\./', '', $type);

                    $thumb = $this->makeThumb($genName, $type);
                }

                if(isset($thumb))
                    $this->generateFiles[] = [$this->path . '/' . $genName . $type, $thumb];
                else
                    $this->generateFiles[] = $this->path . '/' . $genName . $type;
            }
        }

        if(count($this->generateFiles) > 0)
            return $this->generateFiles;

        return false;
    }

    /**
     * Cria uma miniatura do arquivo
     * @param $name
     * @param $type
     * @return string
     */
    private function makeThumb($name, $type) {
        if($this->typeAtual == 'jpg') {
            $this->typeAtual = 'jpeg';
        }

        $baseFile = Loader::getBasePath() . $this->path . '/' . $name . $type;

        //Cria uma nova imagem
        $imgCreate = call_user_func('imagecreatefrom' . $this->typeAtual, $baseFile);

        //Dimensoes reais do arquivo
        $width = imagesx($imgCreate);
        $height = imagesy($imgCreate);

        //Calculo usado para manter o aspect ratio
        $thumbHeight = ($this->thumbWidth/$width) * $height;

        $newImg = imagecreatetruecolor($this->thumbWidth, $thumbHeight);

        //Criar transparencia de png
        if($this->typeAtual == 'png') {
            imagecolortransparent($newImg, imagecolorallocate($newImg, 0, 0, 0));
        }

        imagecopyresampled($newImg, $imgCreate, 0, 0, 0, 0, $this->thumbWidth, $thumbHeight, $width, $height);

        $thumb = $this->path . '/' . $this->nameAtual . '_' . $this->thumbPrefix . $type;

        call_user_func('image' . $this->typeAtual, $newImg, Loader::getBasePath() . $thumb);

        return $thumb;
    }

}