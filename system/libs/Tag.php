<?php

namespace projectws\libs;

use projectws\libs\Url;
use projectws\libs\Request;

class Tag {

    /**
     * Constrói a tag
     * @param $tag
     * @param string $propriedades
     * @param bool $fechar
     * @return string
     */
    private static function _tag($tag, $propriedades = '', $fechar = false, $inner = null) {
        if($inner !== null){
            return "<$tag $propriedades>$inner</$tag>\n";
        }

        if($fechar) {
            return "<$tag $propriedades />\n";
        } else {
            if(empty($propriedades))
                return "<$tag>\n";

            return "<$tag $propriedades>\n";
        }
    }

    /**
     * Cria template das propriedades da tag
     * @param $propriedades
     * @param $padroes
     * @param string $sep
     * @return string
     */
    private static function _prop($propriedades, $padroes, $sep = ' ') {
        $result = array();

        //PARA RETORNAR AS CHAVES NUMERICAS
        $defKeys = array_keys($padroes);

        //HTML INTERNO
        $html = [];

        $i = 0;
        foreach ($propriedades as $propriedade => $valor) {
            if($propriedade === $i) {
                $defKeys[0] = explode(":", $defKeys[$i]);

                if(isset($defKeys[0][1])) {
                    $html[] = $valor;
                } else {
                    $result[] = $defKeys[$propriedade][0] . '="' . $valor . '"';

                    if($defKeys[$propriedade][0] == 'name') {
                        if(Request::getPost($valor))
                            $result[] = 'value' . '="' . Request::getPost($valor) . '"';
                        elseif(Request::get($valor))
                            $result[] = 'value' . '="' . Request::get($valor) . '"';
                    }
                }
            } else {
                $result[] = $propriedade . '="' . $valor . '"';
            }

            $i++;
        }

        if(count($html) == 0)
            return implode($sep, $result);

        return [
            implode($sep, $result),
            implode(' ', $html)
        ];
    }

    /**
     * Criar a tag de formulario
     * @param array $propriedades
     * @return string
     */
    public static function form(array $propriedades) {
        if (isset($propriedades[0])) {
            $propriedades[0] = Url::getUrl() . $propriedades[0];
        }

        $prop = self::_prop($propriedades, ['action' => '', 'method' => 'POST']);

        return self::_tag('form', $prop);
    }

    /**
     * fecha a tag form
     * @return string
     */
    public static function endform() {
        return self::_tag('/form');
    }

    /**
     * Criar a tag com o tipo texto de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function textField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="text"', $prop, true);
    }

    /**
     * Criar a tag com o tipo number de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function numberField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="number"', $prop, true);
    }

    /**
     * Criar a tag com o tipo texto de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function dateField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="number"', $prop, true);
    }

    /**
     * Criar a tag com o tipo time de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function timeField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="time"', $prop, true);
    }

    /**
     * Criar a tag com o tipo senha de Campo de entrada
     * @param array $propriedades
     * @return string]
     */
    public static function passwordField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="password"', $prop, true);
    }

    /**
     * Criar a tag com o tipo checkbox de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function checkField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="checkbox"', $prop, true);
    }

    /**
     * Criar a tag com o tipo file de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function fileField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="file"', $prop, true);
    }

    /**
     * Criar a tag com o tipo submit de Campo de entrada
     * @param array $propriedades
     * @return string
     */
    public static function submitButton(array $propriedades) {
        $prop = self::_prop($propriedades, ['value' => '']);

        return self::_tag('input type="submit"', $prop, true);
    }

    /**
     * Cria tag input hidden
     * @param array $propriedades
     * @return string
     */
    public static function hiddenField(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '']);

        return self::_tag('input type="hidden"', $prop, true);
    }

    /**
     * Criar a tag <a>
     * @param array $propriedades
     * @return string
     */
    public static function linkTo(array $propriedades) {
        $prop = self::_prop($propriedades, ['href' => '', 'inner:true' => '']);

        return self::_tag('a', $prop[0], false, $prop[1]);
    }

    /**
     * @param array $propriedades
     * @return string
     */
    public static function textarea(array $propriedades) {
        $prop = self::_prop($propriedades, ['name' => '', 'inner:true' => '']);

        return self::_tag('a', $prop[0], false, $prop[1]);
    }

}
