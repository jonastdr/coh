<?php

namespace projectws\libs;

use projectws\App;
use projectws\Loader;

class Url {

    private static $urlBase = 'http://localhost/';

    public static function setBaseUrl($url) {
        self::$urlBase = $url;
    }

    public static function getBaseUrl() {
        return self::$urlBase . Loader::getConfig('BASE');
    }

    public static function getUrl() {
        return Loader::getConfig('BASE');
    }

    public static function getBasePath() {
        return Loader::getBasePath();
    }

}
