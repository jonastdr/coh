<?php

namespace projectws\libs\bridges;

use projectws\libs\orm\Builder;
use projectws\libs\orm\FailException;
use projectws\libs\orm\GenericModel;

/**
 * Class DB
 * @package projectws\libs\bridges
 * @method static Builder select($field = null) constrói as colunas visíveis na consulta SQL
 * @method static Builder from(string $table = null, string $alias = null)
 * @method static Builder alias(string $alias = null)
 * @method static Builder where(string $column, string $operator = null, $value = null, bool $slashes = true)
 * @method static Builder orWhere(string $colOrStr, $operator = null, $value = null, $slashes = true)
 * @method static Builder whereIn(string $column, $values)
 * @method static Builder orWhereIn(string $column, $values)
 * @method static Builder whereNotIn(string $column, $values)
 * @method static Builder orWhereNotIn(string $column, $values)
 * @method static Builder whereNull(string $column)
 * @method static Builder orWhereNull(string $column)
 * @method static Builder whereNotNull(string $column)
 * @method static Builder orWhereNotNull(string $column)
 * @method static Builder whereBetween(string $column, $value1, $value2)
 * @method static Builder inValues(array $values)
 * @method static Builder join(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder leftJoin(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder rightJoin(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder fullOuterJoin(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder crossJoin(string $tabela)
 * @method static Builder groupBy(string $group)
 * @method static Builder orderBy(string $order)
 * @method static Builder limit(int $limit)
 * @method static Builder offset(int $offset)
 */
class DB {

    /**
     * Retorna um model genérico
     * @return GenericModel
     */
    private static function genericModel() {
        return new GenericModel();
    }

    /**
     * Inicia uma transação SQL
     * @return $this|null
     */
    public static function begin() {
        return static::genericModel()->beginTransaction();
    }

    /**
     * Faz commit da transação atual
     * @return bool|string
     */
    public static function commit() {
        return static::genericModel()->commit();
    }

    /**
     * Cancela a transação atual
     * @return bool|string
     */
    public static function rollback() {
        return static::genericModel()->rollBack();
    }

    /**
     * Usado para atualização de várias tabelas, se existir alguma falha
     * executa $error($e), se tiver successo executa $success()
     * @param callable $closure
     * @param callable $success
     * @param callable $fail
     * @return mixed
     */
    public static function saveOrFail(callable $closure, callable $success, callable $error) {
        $fail = new FailException();

        //Tentativa de sucesso
        try {
            //Inicia as transações
            DB::begin();

            //Executa as regras
            $closureReturn = $closure($fail);

            //Se existir alguma falha dispara uma excessão
            if($fail->hasMessage())
                throw new FailException();

            DB::commit();

            //Executa a closure de sucesso
            return $success($closureReturn);
        } catch (FailException $e) {
            DB::rollback();

            //Executa a closure de falha
            return $error($e->getFailMessages());
        }
    }

    /**
     * @param $method
     * @param $parametros
     * @return mixed
     */
    public static function __callStatic($method, $parametros) {
        $generic = static::genericModel();

        return call_user_func_array([$generic, $method], $parametros);
    }

}