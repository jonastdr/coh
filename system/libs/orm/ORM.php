<?php

namespace projectws\libs\orm;

use projectws\libs\Config;
use projectws\libs\Database;
use projectws\libs\Exception;
use projectws\libs\orm\relationship\HasOne;
use projectws\libs\orm\relationship\HasMany;
use projectws\libs\orm\relationship\BelongsTo;
use projectws\libs\orm\relationship\BelongsToMany;
use projectws\Loader;
use projectws\mvc\Model;

/**
 * Class ORM
 * @package projectws\libs\orm
 * @method static ModelCollection find($where) Procura por um model pela primary key
 * @method static static findFirst($where = null) Procura por um model pela primary key
 * @method static Builder lists($text = null, $value = null)
 * @method static Builder select($field = null)
 * @method static Builder from(string $table = null, string $alias = null)
 * @method static Builder alias(string $alias = null)
 * @method static Builder where(string $column, string $operator = null, $value = null, bool $slashes = true)
 * @method static Builder orWhere(string $colOrStr, $operator = null, $value = null, $slashes = true)
 * @method static Builder whereIn(string $column, $values)
 * @method static Builder orWhereIn(string $column, $values)
 * @method static Builder whereNotIn(string $column, $values)
 * @method static Builder orWhereNotIn(string $column, $values)
 * @method static Builder whereNull(string $column)
 * @method static Builder orWhereNull(string $column)
 * @method static Builder whereNotNull(string $column)
 * @method static Builder orWhereNotNull(string $column)
 * @method static Builder whereBetween(string $column, $value1, $value2)
 * @method static Builder inValues(array $values)
 * @method static Builder join(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder leftJoin(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder rightJoin(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder fullOuterJoin(string $tabela, $col1 = null, $operator = null, $col2 = null)
 * @method static Builder crossJoin(string $tabela)
 * @method static Builder groupBy(string $group)
 * @method static Builder orderBy(string $order)
 * @method static Builder limit(int $limit)
 * @method static Builder offset(int $offset)
 * @method static Builder update(array $columns, array $where = [])
 * @method static Builder insert(array $column = [])
 * @method static Builder delete($column)
 * @method static string raw()
 * @method static static row()
 * @method static ModelCollection rows()
 * @property static $*
 * @author Jonas <tortato.jonas@gmail.com>
 */
abstract class ORM {

    protected static $db;
    private static $instance = null;
    protected $driver;

    /**
     * Campos definidos no modelo
     * @var array
     */
    protected $fields = [];

    /**
     * Campos que por padrão não mostram em retorno
     * @var array
     */
    protected $hidden = [];
    
    /**
     * Usado para definir se o model tem a ação de exclusão lógica
     * @var bool
     */
    protected $soft_delete = false;
    
    /**
     * Nome da tabela
     * string @var
     */
    protected $table;

    /**
     * Indices da tabela
     * array @var
     */
    protected $indices = [];

    /**
     * PRIMARY KEY
     * @var
     */
    protected $pk;

    /**
     * FOREIGNKEY
     * [
     *  'id_foreignkey' => ['table', 'id']
     * ]
     * @var array
     */
    protected $fk = [];

    /**
     * Se estiver ativo ao executar um insert salva os campos de data_criacao, data_alteracao
     * @var bool
     */
    protected $timestamp = false;
    /**
     * Define se irá atualizar
     * @var bool
     */
    protected $update = false;
    /**
     * Atributos como colunas
     * @var array
     */
    private $_original = [];
    /**
     * Valor da chave primaria
     * @var null|int
     */
    private $_value_pk = null;
    /**
     * Atributos alterados do original
     * @var array
     */
    private $_changed = [];
    /**
     * ID inserido
     * @var bool
     */
    private $insertId = false;
    /**
     * Data de atualização de timestamp
     * @var null
     */
    private $_cacheTimestamp = null;

    /**
     * Quantidade retornado
     * @var int
     */
    private $count = 0;

    /**
     * Filtros do Model
     * @var
     */
    private $filters = [];

    /**
     * ORM constructor.
     */
    public function __construct() {
        self::$instance = $this;

        $table = explode('\\', get_called_class());

        if(!$this->table)
            $this->table = strtolower(preg_replace('/([^A-Z])([A-Z])/', '$1_$2', end($table)));
    }

    /**
     * Chamada de funções de query static
     * @param $method
     * @param $parametros
     * @return mixed
     */
    public static function __callStatic($method, $parametros) {
        if($method !== false) {
            return call_user_func_array([new static, $method], $parametros);
        }

        exit;
    }

    /**
     * Retorna os campos e suas propriedades
     * @return array
     */
    public function getFields() {
        return $this->fields;
    }

    /**
     * Retorna os campos e suas propriedades
     * @return array
     */
    public function getHiddenFields() {
        return $this->hidden;
    }

    public function getVisibleFields() {
        return array_diff();
    }
    
    /**
     * Retorna o valor da chave primaria
     * @return string
     */
    public function getValuePrimaryKey() {
        return $this->_value_pk;
    }

    /**
     * Retorna as chaves estrangeiras
     * @return array
     */
    public function getForeignKey() {
        return $this->fk;
    }

    /**
     * Retorna se está ativo timestamp
     * @return bool
     */
    public function getTimestamp() {
        return $this->timestamp;
    }

    /**
     * Retorna quantidade de resultados
     * @return int
     */
    public function count() {
        return $this->count;
    }

    /**
     * Inicializa uma cache da consulta SQL
     * @param $time timestamp
     * @return $this
     */
    public function cache($time) {
        $this->_cacheTimestamp = time() + $time;

        return $this;
    }

    /**
     * Lista colunas da tabela pelo modelo
     * @return array
     */
    public function getColumns() {
        $fields = [];

        foreach ($this->fields as $a => $b) {
            if (is_array($b)) {
                $fields[] = $a;
            } else {
                $fields[] = $b;
            }
        }

        return $fields;
    }

    /**
     * Faz instrução de UPDATE caso tenha dados a ser alterado, se não conter dados para alterar
     * faz INSERT
     * @return boolean
     */
    public function save() {
        if(count($this->_changed) == 0) {
            return null;
        }

        if ($this->count > 0) {
            $builder = $this->getBuilder();

            $where = [];
	        
            //Seta o where com a chave primaria
	        if($this->_value_pk) {
		        $where[$this->getPrimaryKey()] = $this->_value_pk;
	        } else {
		        /**
		         * Indices são usados geralmente para consultas cruas
		         * ou que não contenham chave primaria
		         * Ex.: tabelas de relacionamento
		         */
	        	$indices = $this->indices;
		
		        foreach ($indices as $indice) {
			        $where[$indice] = $this->{$indice};
		        }
	        }
	        	
	        
            $changed = $this->_changed;

            //reseta as alterações
            $this->refreshModel();
	        
            return $builder->update($changed, $where);
        } else {

            $builder = $this->getBuilder();

            $changed = $this->_changed;

            //reseta as alterações
            $this->refreshModel();

            return $builder->insert($changed);
        }
    }

    /**
     * Retorna o construtor de SQL
     * @return Builder
     */
    protected function getBuilder() {
        $builder = new Builder($this->table);

        //define o model selecionado
        $builder->setModel($this);
        //define os filtros para o uso de métodos save, delete, trash, restore
        $builder->setFilters($this->filters);

        return $builder;
    }
    
    /**
     * Retorna a chave primaria
     * @return string
     */
    public function getPrimaryKey() {
        return $this->pk;
    }

    /**
     * Atualiza as informações dos campos do model
     */
    private function refreshModel() {
        foreach ($this->_changed as $field => $value) {
            $this->_original[$field] = $value;
        }

        $this->_changed = [];
    }

    /**
     * Verifica se o model tem exclusão lógica
     * @return bool
     */
    public function isSoftDelete() {
        if($this->soft_delete === true)
            return true;
        
        return false;
    }

    /**
     * Faz um exclusão lógica de um model
     * @return Builder
     */
    public function trash() {
        $config = Config::get('orm');
	
	    $where = [];
	    
	    if($this->_value_pk)
            $where[$this->getPrimaryKey()] = $this->_value_pk;

        $builder = $this->getBuilder();

        return $builder->update([
            $config['trash'] => date('Y-m-d H:i:s')
        ], $where, true);
    }

    /**
     * Restaura um model que foi executado uma exclusão lógica
     * @return Builder
     */
    public function restore() {
        $config = Config::get('orm');
	
	    $where = [];
	    
	    if($this->_value_pk)
            $where[$this->getPrimaryKey()] = $this->_value_pk;

        $builder = $this->getBuilder();

        return $builder->update([
            $config['trash'] => null
        ], $where, true);
    }
    
    /**
     * Executa a sql | Execução de SQL sem retorno
     * @param string $sql
     * @return $this|null
     */
    public function execute($sql = null, $inserted = false) {
        if (!is_null($sql)) {
            $query = static::$db->query($sql);

            if(Database::getInstance()->getConfig('DEBUG'))
                $this->depuracao($sql);

            if (!$query) {
                $erro = static::$db->errorInfo();

                new Exception($erro[2] . ' ' . $sql);

                return false;
            }
    
            /**
             * Retorna o id inserido
             */
            if($inserted) {
                if ($this->driver == "mysql") {
                    $this->insertId = static::$db->lastInsertId();
                } elseif ($this->driver == "pgsql") {
                    $fore = $query->fetch();

                    if (isset($fore[0]))
                        $this->insertId = $fore[0];
                    else
                        $this->insertId = true;
                }

                return $this->insertId;
            } else {
                return true;
            }
        } else {
            return null;
        }
    }

    /**
     * @param $sql
     */
    private function depuracao($sql) {
        echo console_log($sql);
    }

    /**
     * Inicio de transação
     * @return mixed
     */
    public function beginTransaction() {
        return static::$db->beginTransaction();
    }

    /**
     * Executa COMMIT
     * @return mixed
     */
    public function commit() {
        return static::$db->commit();
    }

    /**
     * Executa ROLLBACK
     * @return mixed
     */
    public function rollBack() {
        return static::$db->rollback();
    }

    /**
     * Traz vários resultados de uma SQL
     * @param string $sql
     * @param array $filters
     * @return array|null|ModelCollection|ORM
     */
    public function results($sql, Array $filters = []) {
        return $this->result($sql, false, $filters);
    }

    /**
     * Traz o resultado de uma SQL
     * @param string $sql
     * @param bool|true $row
     * @param array $filters
     * @return $this|array|null|ModelCollection|ORM
     */
    public function result($sql, $row = true, Array $filters = []) {
        //Se inicializar cache faz verificação
        if(!is_null($this->_cacheTimestamp)) {
            $cacheFile = $this->getCacheFile($sql);

            //Se existir arquivo cache retorna
            if (!is_null($cacheFile))
                return $cacheFile;
        }

        $query = static::$db->query($sql);

        $this->filters = $filters;

        if(Database::getInstance()->getConfig('DEBUG'))
            $this->depuracao($sql);

        $erro = static::$db->errorInfo();

        if ($erro[2]) {
            new Exception($erro[2] . ' ' . $sql);

            return null;
        } else {
            if($row)
                return $this->resultOne($query, $sql);

            return $this->resultMore($query, $sql);
        }
    }

    /**
     * Usado para pegar o arquivo ao invés de executar uma query no banco de dados
     * @param $sql
     * @return null
     */
    private function getCacheFile($sql) {
        $cacheFile = Loader::getBasePath() . 'store/objects/' . md5($sql) . '.php';

        if(file_exists($cacheFile)) {
            if(unserialize(file_get_contents($cacheFile))->timestamp > time()) {
                return unserialize(file_get_contents($cacheFile))->objeto;
            }
        }

        return null;
    }

    /**
     * Traz o resultado de somente 1 linha da consulta SQL
     * @param $query
     * @param $sql
     * @return $this|null
     */
    private function resultOne($query, $sql) {
        $this->count = $query->rowCount();

        if($query->rowCount() > 0) {
            $fore = $query->fetch(\PDO::FETCH_OBJ);

            foreach ($fore as $col => $val) {
                //Verifica se é o valor da chave primaria e define o valor na propriedade do sistema
                if($this->getPrimaryKeyInnerSelect() == $col) {
                    $this->_value_pk = $val;

                    continue;
                }

                $this->_original[$col] = $val;
            }

            //Verificação de arquivo de cache
            $this->registerCache($this, $sql);
        }

        return $this;
    }

    /**
     * Retorna o nome da variavel primary key do sistema interno
     * @return string|void
     */
    private function getPrimaryKeyInnerSelect() {
        $table = $this->table;

        $pk = $this->getPrimaryKey();

        if(!$pk)
            return;

        return "_{$pk}_{$table}";
    }

    /**
     * Verificação de arquivo cache
     * @param $obj
     */
    private function registerCache($obj, $sql) {
        $cacheFile = Loader::getBasePath() . 'store/objects/' . md5($sql) . '.php';

        if(!is_null($this->_cacheTimestamp)) {
            if(file_exists($cacheFile)) {
                if(unserialize(file_get_contents($cacheFile))->timestamp < time())
                    $this->putCacheFile($obj, $sql);
            }
            elseif(!file_exists($cacheFile))
                $this->putCacheFile($obj, $sql);
        } else {
            if(file_exists($cacheFile) && Database::getInstance()->getConfig('DROP_CACHE_EXPIRE'))
                unlink($cacheFile);
        }
    }

    /**
     * Adiciona um arquivo do objeto para cache
     * @param $objeto
     */
    private function putCacheFile($objeto, $sql) {
        $obj = new \StdClass();

        $obj->timestamp = $this->_cacheTimestamp;
        $obj->objeto = $objeto;

        $novoArquivo = Loader::getBasePath() . 'store/objects/' . md5($sql) . '.php';

        file_put_contents($novoArquivo, serialize($obj));
    }

    /**
     * Traz o resultado de uma ou mais linhas da SQL
     * @param $query
     * @method Teste testing()
     * @return ModelCollection
     */
    private function resultMore($query, $sql) {
        $rows = new ModelCollection();

        if($query->rowCount() > 0) {
            $fetch = $query->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($fetch as $cols) {
                $instance = new $this;

                $instance->count = $query->rowCount();

                foreach ($cols as $col => $val) {
                    //Verifica se é o valor da chave primaria e define o valor na propriedade do sistema
                    if($this->getPrimaryKeyInnerSelect() == $col) {
                        $instance->_value_pk = $val;

                        continue;
                    }

                    $instance->_original[$col] = $val;
                }

                $rows[] = $instance;
            }

            //Verificação de arquivo de cache
            $this->registerCache($rows, $sql);
        }

        return $rows;
    }

    /**
     *
     * @return Entity
     */
    public function onlyFields() {
        return new Entity($this->toArray());
    }

    /**
     * Retorna os dados em formato de array
     * @return array
     */
    public function toArray() {
        $fileds = $this->_original;

        foreach ($this->_changed as $name => $value) {
            $fileds[$name] = $value;
        }

        return $fileds;
    }

    /**
     * 1 - 1
     * Traz a relação de um objeto relacionado ao objeto atual
     * @param string $model
     * @param string $foreign_key
     * @param string $primary_key
     * @return Builder
     */
    public function hasOne($model, $foreign_key = null, $primary_key = null) {
        $hasOne = new HasOne($model, $this, $foreign_key, $primary_key);

        return $hasOne->result();
    }

    /**
     * 1 - N
     * Traz a relação de varios objetos relacionados ao objeto atual
     * @param string $model
     * @param string $foreign_key
     * @param string $primary_key
     * @return Builder
     */
    public function hasMany($model, $foreign_key = null, $primary_key = null) {
        $hasMany = new HasMany($model, $this, $foreign_key, $primary_key);

        return $hasMany->result();
    }

    /**
     * 1 - 1
     * Traz a relação a quem o objeto pertence
     * @param string $model
     * @param string $foreign_key
     * @param string $primary_key
     * @return Builder
     */
    public function belongsTo($model, $foreign_key = null, $primary_key = null) {
        $belongsTo = new BelongsTo($model, $this, $foreign_key, $primary_key);

        return $belongsTo->result();
    }

    /**
     * N - N
     * Pertence a muitos
     * @param string Model $model
     * @param string $pivotTable
     * @param string $foreign_key chave estrangeira da tabela
     * @param string $primary_key primary_key da tabela a que se refere a chave estrangeira
     * @param string $foreign_key_this chave estrangeira da segunda tabela
     * @param string $primary_key_this primary_key da tabela atual a que se refere a chave estrangeira
     * @return Builder
     */
    public function belongsToMany($model, $pivotTable = null, $foreign_key = null, $primary_key = null, $foreign_key_this = null, $primary_key_this = null) {
        $belongsToMany = new BelongsToMany($model, $this, $pivotTable, $foreign_key, $primary_key, $foreign_key_this, $primary_key_this);

        return $belongsToMany->result();
    }

    /**
     * Retorna os indices da tabela
     * @return mixed
     */
    public function getIndices() {
        return $this->indices;
    }

    /**
     * Usado para pegar dados do modelo
     * @param $name
     * @return null|ModelCollection|Model
     */
    public function __get($name) {
        if($this->existsField($name)) {
            if (isset($this->_changed[$name]))
                return $this->_changed[$name];
            elseif (isset($this->_original[$name]))
                return $this->_original[$name];

            return null;
        } else {
            if(method_exists($this, $name))
                return $this->resultRelationship($name);

            return null;
        }
    }

    /**
     * Usada para definir dados do modelo
     * @param $name
     * @param $value
     */
    public function __set($name, $value) {
        if($this->existsField($name))
            $this->_changed[$name] = $value;
        else
            $this->{$name} = $value;
    }

    /**
     * Verifica a existência de um campo no modelo
     * @param $nome
     * @return bool
     */
    private function existsField($nome) {
        foreach ($this->fields as $a => $b) {
            if(is_array($b)) {
                $colunaNome = $a;
            } else {
                $colunaNome = $b;
            }

            if($nome == $colunaNome)
                return true;
        }

        if(array_key_exists($nome, $this->_original))
            return true;

        if(array_key_exists($nome, $this->_changed))
            return true;

        return false;
    }

    /**
     * Traz o resultado de um relacionamento
     * @param $name
     * @return ModelCollection|Model
     */
    private function resultRelationship($name) {
        $result = [
            'hasOne' => 'row',
            'hasMany' => 'rows',
            'belongsTo' => 'row',
            'belongsToMany' => 'rows'
        ];

        //Chama o relacionamento feito pelo usuário
        $relationship = $this->{$name}();

        //Nome do tipo de relacionamento
        $nameRelationship = $relationship->getRelationship();

        return $relationship->{$result[$nameRelationship]}();
    }

    /**
     * Chamada de funções de query
     * @param $method
     * @param $parametros
     * @return mixed
     */
    public function __call($method, $parametros) {
        if(in_array($method, ['updateTables'])) {
            $builder = $this->getTableBuilder();
        } else {
            $builder = $this->getBuilder();
        }

        if($method !== false) {
            return call_user_func_array([$builder, $method], $parametros);
        }

        exit;
    }

    /**
     * Retorna o construtor de Tabelas
     * @return TableBuilder
     */
    protected function getTableBuilder() {
        $builder = new TableBuilder($this->table, static::$db);

        $builder->setModel($this);
        $builder->setUpdate($this->update);

        return $builder;
    }
}