<?php

namespace projectws\libs\orm;

class FailException extends \Exception {

    private static $messages = [];

    public function __construct()
    {

    }

    public function addMessage($message) {
        static::$messages[] = $message;
    }

    public function hasMessage() {
        return count(static::$messages) ? true : false;
    }

    public function getFailMessages() {
        return static::$messages;
    }

}