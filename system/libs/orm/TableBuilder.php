<?php

namespace projectws\libs\orm;

use projectws\libs\Database;
use projectws\Loader;
use projectws\mvc\Model;

class TableBuilder {

    protected static $db;

    /**
     * Model
     * @var
     */
    private $model;

    /**
     * Nome da tabela
     * @var
     */
    private $table;

    /**
     * SQL
     * @var
     */
    private $sql;

    /**
     * TRUE para atualização
     * bool @var
     */
    private $update;

    /**
     * TableBuilder constructor.
     * @param $table
     */
    public function __construct($table, $db) {
        static::$db = $db;

        $this->table = $table;
    }

    /**
     * Define o model
     * @param Model $model
     */
    public function setModel(Model $model) {
        $this->model = $model;
    }

    /**
     * Seta definição se é possivel fazer atualização da tabela
     * @param $update
     */
    public function setUpdate($update) {
        $this->update = $update;
    }

    /**
     * Usado para os tipos de dados de colunas : PDO
     * @param $orig
     * @return null
     */
    private function traduzirNativeType($orig) {
        $orig = strtoupper($orig);

        $trans = [
            'VAR_STRING' => 'VARCHAR',
            'STRING' => 'VARCHAR',
            'VARCHAR' => 'VARCHAR',
            'BLOB' => 'BLOB',
            'INT4' => 'INT',
            'LONGLONG' => 'INT',
            'LONG' => 'INT',
            'SHORT' => 'INT',
            'DATETIME' => 'DATETIME',
            'DATE' => 'DATE',
            'DOUBLE' => 'DOUBLE',
            'TIMESTAMP' => 'TIMESTAMP'
        ];

        if(isset($trans[$orig]))
            return $trans[$orig];

        return null;
    }

    /**
     * Usado para criação de tabelas
     * @return bool
     */
    public function make() {
        $ct = "CREATE TABLE {$this->table} (\n    ";

        $colunas = [];

        //usado para verificação se existe
        $fieldsThisModel = $this->model->getFields();

        //chave primaria
        $primary_key = $this->model->getPrimaryKey();

        //chaves estrangeiras
        $foreign_keys = $this->model->getForeignKey();

        //se b nao for array é considerado como nome da coluna
        foreach ($fieldsThisModel as $a => $b) {
            $length = null;
            $default = null;
            $PkFk = null;

            if (is_array($b)) {
                $colunaNome = $a;
                //TIPAGEM DO CAMPO
                $type = strtoupper($b[0]);

                //TAMANHO DO CAMPO
                $length = !empty($b[1]) ? "({$b[1]})" : "";

                //VALOR DEFAULT
                if (!empty($b[2])) {
                    $default = " DEFAULT " . $this->returnValue($b[2]);
                } else {
                    $default = null;
                }
            } else {
                $colunaNome = $b;

                //TIPAGEM DO CAMPO
                $type = "INT";
            }

            //define a primary key da tabela a ser criada
            if ($colunaNome == $primary_key) {
                $PkFk = " PRIMARY KEY";
            } else {
                //Verifica os indices de chaves estrangeiras pra fazer a referencia
                foreach ($foreign_keys as $foreign_key => $data) {
                    if ($colunaNome == $foreign_key) {
                        $references = $data[0];
                        $pk = isset($data[1]) ? $data[1] : 'id';

                        $PkFk = " REFERENCES $references($pk)";

                        unset($foreign_keys[$foreign_key]);

                        break;
                    }
                }
            }

            //ADD CAMPO
            $colunas[] = $colunaNome . " " . $type . $length . $default . $PkFk;
        }
        $ct .= implode(",\n    ", $colunas) . "\n);";

        $this->sql = $ct;

        $this->model->execute($this->sql);

        return true;
    }

    /**
     * Cria chave primaria
     * @return bool
     */
    private function makePrimaryKey() {
        $indices = $this->model->getIndices();

        $columns = $this->model->getColumns();

        if(count($indices)) {
            $primaryKey = $indices[0];

            //Se não existir a coluna não cria
            if(array_search($primaryKey, $columns) === false)
                return false;

            $this->sql = "ALTER TABLE {$this->table} ADD PRIMARY KEY ($primaryKey)";

            $this->model->execute();

            return true;
        }

        return false;
    }

    /**
     * Usado para alteração de tabela
     * @return bool
     */
    public function alter() {
        $at = "ALTER TABLE {$this->table}\n";

        //Consulta os dados da tabela
        $query = static::$db->query("SELECT * FROM {$this->table} LIMIT 0");

        $fields = [];
        $fieldNames = [];
        for ($i = 0; $i < $query->columnCount(); $i++) {
            $name = $query->getColumnMeta($i)['name'];

            $fields[$name] = $query->getColumnMeta($i);

            $fields[$name]['type'] = $this->traduzirNativeType($fields[$name]['native_type']);

            $fieldNames[] = $name;
        }

        $alteracoes = [];
        $anterior = "";
        //usado para verificação se existe
        $fieldsThisModel = $this->model->getFields();

        foreach ($fieldsThisModel as $a => $b) {
            $length = "";
            $default = "";

            if(is_array($b)) {
                $colunaNome = $a;

                if($b[0] == 'serial')
                    $b[0] = 'int';

                //tipagem da coluna
                $type = " " . strtoupper($b[0]);

                //tamanho
                $length = !empty($b[1]) ? "({$b[1]})" : "";

                //valor default
                $default = !empty($b[2]) ? " DEFAULT '{$b[2]}'" : "";
            } else {
                $colunaNome = $b;
                $type = " INT";
            }

            $colunaNome = strtolower($colunaNome);

            //Usado para fazer o add na tabela
            $busca = array_search($colunaNome, $fieldNames);

            //somente para mysql
            $after = !empty($anterior) ? " AFTER " . $anterior : "";

            if($busca === false) {
                //POSTGRES não tem instrução AFTER COLUMN
                if(Database::getInstance()->getConfig('DRIVER') == "pgsql")
                    $after = "";

                $alteracoes[] = "ADD $colunaNome$type$after";

                continue;
            }

            if(!is_null($fields[$colunaNome]['type']) && $fields[$colunaNome]['type'] != trim($type)) {
                if(Database::getInstance()->getConfig('DRIVER') == "mysql") {
                    $ALTER = "MODIFY";
                    $colunaNome = $colunaNome . " " . $colunaNome;
                } else {
                    $ALTER = "ALTER COLUMN";
                    $type = " TYPE $type";
                }

                $alteracoes[] = "$ALTER $colunaNome $type$length";

                continue;
            }

            //usado para criação do AFTER COLUMN
            $anterior = $colunaNome;
        }

        //Se tiver alterações prossegue executando o ALTER TABLE
        if(count($alteracoes)) {
            $at .= implode(",\n", $alteracoes) . $default . ";";

            $this->sql = $at;

            $this->model->execute($this->sql);

            return true;
        }

        return false;
    }

    /**
     * Faz uma atualização da tabela | CREATE TABLE | ALTER TABLE
     * @return bool|null|string
     */
    public function updateTables() {
        if($this->update === false)
            return false;

        static::$db->query("SELECT * FROM {$this->table} LIMIT 0");

        $erro = static::$db->errorInfo();

        if($erro[2]) {
            $this->make();

            return Loader::getLang('Model', 'model_create_in_db', get_called_class());
        }

        if($this->alter())
            return Loader::getLang('Model', 'model_alter_in_db', get_called_class());

        return false;
    }

    /**
     * Constrói corretamente o valor para evitar o envio de SQL Injection
     * @param $valor
     * @return mixed
     */
    private function returnValue($valor) {
        if(is_null($valor))
            return 'null';

        if(is_integer($valor))
            return $valor;

        if(Database::getInstance()->getConfig('DRIVER') == 'pgsql')
            return "'" . preg_replace("/'/", "''", $valor). "'";

        elseif(Database::getInstance()->getConfig('DRIVER') == 'mysql')
            return "'" . preg_replace("/'/", "\\'", $valor) . "'";

        //se não for nenhum dos drivers retorna padrão
        return preg_replace("/'/", "\\'", $valor);
    }

}