<?php

namespace projectws\libs\orm;
use projectws\libs\Exception;

/**
 * Classe usada para a construção de join ORM
 * Class Join
 * @package projectws\libs\orm
 */
class Join {
	
	/**
	 * Re-utiliza os métodos de WHERE
	 */
	use Where;
	
	private $type = 'INNER';
	
	private $table = '';
	
	private $on = "";
	
	private $where = [];
	
	public function __construct($table, $type = 'INNER') {
		$this->setTable($table);
		$this->setType($type);
	}
	
	/**
	 * Faz a construção das chaves relacionadas do JOIN
	 * @param $cond1
	 * @param $operator
	 * @param $cond2
	 * @return $this
	 */
	public function on($cond1, $operator, $cond2) {
		$this->on = "$cond1 $operator $cond2";
		
		return $this;
	}
	
	/**
	 * Faz um filtro na sql
	 * @param string $column
	 * @param null $operator
	 * @param null $value
	 * @param bool $slashes
	 * @param string $next
	 * @return $this
	 */
	public function where($column, $operator = null, $value = null, $slashes = false, $next = 'AND') {
		if(!is_numeric($value) && $slashes){
			$value = "'" . addslashes($value) . "'";
		}
		
		$this->addStruct('where', ["$column $operator $value", $next]);
		
		return $this;
	}
	
	/**
	 * Retorna a construção do join
	 * @return string
	 */
	public function __toString() {
		if(is_null($this->table))
			new Exception('nome da tabela não informado.');
		
		return "{$this->type} JOIN {$this->table} ON {$this->on} {$this->constructWhere()}";
	}
	
	/**
	 * Define o nome da tabela do relacionamento
	 * @param $name
	 */
	private function setTable($name) {
		$this->table = $name;
	}
	
	/**
	 * Define o tipo de JOIN
	 * @param $type
	 */
	private function setType($type) {
		$this->type = $type;
	}
	
	/**
	 * Faz a construção do WHERE para o JOIN
	 * @return string
	 */
	private function constructWhere() {
		$wheres = [];
		
		foreach ($this->where as $index => $where) {
			if($index > 0)
				$wheres[] =  " " . $where[1] . " " . $where[0];
			else
				$wheres[] = $where[0];
		}
		
		if(count($wheres))
			return 'AND ' . implode('', $wheres);
		
		return '';
	}
	
	/**
	 * Adiciona valor a estrutura da SQL
	 * @param $tipo
	 * @param $valor
	 * @return mixed
	 */
	private function addStruct($tipo, $valor) {
		if(is_array($this->{$tipo}))
			$this->{$tipo}[] = $valor;
		else
			$this->{$tipo} = $valor;
		
		return $valor;
	}
	
}