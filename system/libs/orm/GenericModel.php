<?php

namespace projectws\libs\orm;

use projectws\mvc\Model;

/**
 * Model Genérico usado para acesso sem referência a uma tabela
 * Class GenericModel
 * @package projectws\libs\orm
 */
class GenericModel extends Model {

    /**
     * GenericModel não é atualizado
     * @var bool
     */
    protected $update = false;

}