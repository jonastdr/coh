<?php

namespace projectws\libs\orm;

use projectws\libs\Collection;
use projectws\mvc\Model;

/**
 * Usado para fazer listagem de objetos do tipo Model
 * Class ModelList
 * @package projectws\libs\model
 */
class ModelCollection extends Collection {

    /**
     * Retorna array de objetos simples
     * @return array
     */
    public function onlyFields() {
        return $this->each(function ($item) {
            return $item->onlyFields();
        });
    }

    /**
     * Retorna como array
     * @return array
     */
    public function toArray() {
        return $this->each(function ($item) {
            return $item->toArray();
        });
    }

    public function toJson() {
        return json_encode($this->onlyFields());
    }

    /**
     * Adiciona model a coleção de models
     */
    public function offsetSet($offset, $value) {
        if(!$value instanceof Model)
            return null;

        if(is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    /**
     * Se o metodo não existir chama o método de todos da lista
     * @param $method
     * @param $parametros
     * @return $this
     */
    public function __call($method, $parametros) {
        foreach ($this->items as $item) {
            call_user_func_array([$item, $method], $parametros);
        }

        return $this;
    }
}