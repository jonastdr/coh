<?php

namespace projectws\libs\orm;

use projectws\libs\Config;
use projectws\libs\Database;
use projectws\libs\Exception;
use projectws\Loader;
use projectws\mvc\Model;

class Builder {
	
	/**
	 * Utiliza os métodos de WHERE
	 */
	use Where;

    /**
     * Possíveis operadores
     * @var array
     */
    protected $operators = [
        '=',
        '<',
        '>',
        '<=',
        '>=',
        '<>',
        '!=',
        'like',
        'not like',
        'between',
        'ilike',
        '&',
        '|',
        '^'
    ];
    /**
     *
     * @var \projectws\mvc\Model
     */
    private $model;
    private $sql;
    /**
     * Estrutura da SQL
     * @var array
     */
    private $structure = [
        'select' => [],
        'from' => null,
        'alias' => null,
        'join' => [],
        'where' => [],
        'group' => [],
        'order' => [],
        'limit' => null,
        'offset' => null,
        'count' => 0
    ];
    /**
     * Define o nome do relacionamento
     * string @var
     */
    private $relationship;

    /**
     * Builder constructor.
     * @param string|null $table
     */
    public function __construct($table = null) {
        if(!is_null($table))
            $this->addStruct('from', $table);
    }

    /**
     * Adiciona valor a estrutura da SQL
     * @param $tipo
     * @param $valor
     * @return mixed
     */
    private function addStruct($tipo, $valor) {
        if(is_array($this->structure[$tipo]))
            $this->structure[$tipo][] = $valor;
        else
            $this->structure[$tipo] = $valor;

        return $valor;
    }

    /**
     * Define os filtros padrões
     * Usado para retorno ao construtor
     * @param array $filters
     */
    public function setFilters(Array $filters = []) {
        $this->structure['where'] = $filters;
    }

    /**
     * Retorna o nome do relationamento setado
     * @return string mixed
     */
    public function getRelationship() {
        return $this->relationship;
    }

    /**
     * Define o nome do relacionamento
     * @param $name nome do relacionamento
     * @return $this
     */
    public function setRelationship($name) {
        $this->relationship = $name;

        return $this;
    }

    /**
     * Constroi uma SQL
     * @param $sql
     * @param array $params
     * @param bool|false $row
     * @return array|null|QueryBuilder
     */
    public function query($sql, $params = [], $row = false) {
        $sql = preg_replace_callback('/:(\w*)/', function ($variavel) use ($params) {
            return $this->constructParams($params, $variavel);
        }, $sql);

        $this->sql = $sql;

        //Lista somente colunas
        if($row)
            return $this->model->result($this->sql);

        //retorna lista de models
        return $this->model->results($this->sql);
    }

    /**
     * Usado para expressões regulares de parametros de SQL
     * @param $params parametros de uma SQL
     * @param $variavel variavel a ser procurada
     * @return mixed
     */
    private function constructParams($params, $variavel) {
        foreach ($params as $nome => $valor) {
            if($nome == $variavel[1])
                return $this->returnValue($valor);
        }
    }

    /**
     * Constrói corretamente o valor para evitar o envio de SQL Injection
     * @param $valor
     * @return mixed
     */
    private function returnValue($valor) {
        if(is_null($valor))
            return 'null';
	    
	    if(is_numeric($valor))
	    	return $valor;

        if(is_integer($valor))
            return $valor;

        if(Database::getInstance()->getConfig('DRIVER') == 'pgsql')
            return "'" . preg_replace("/'/", "''", $valor). "'";

        elseif(Database::getInstance()->getConfig('DRIVER') == 'mysql')
            return "'" . preg_replace("/'/", "\\'", $valor) . "'";

        //se não for nenhum dos drivers retorna padrão
        return preg_replace("/'/", "\\'", $valor);
    }

    /**
     * Constroi uma instrucao SELECT somente passando como parametro um array ou uma string
     * @param null $where
     * @return array|null
     */
    public function find($where = null) {
        return $this->filtro($where)->rows();
    }

    /**
     * Lista os resultados da sql como objeto
     * @return ModelCollection|null
     */
    public function rows() {
        $this->get();

        return $this->model->results($this->sql, $this->structure['where']);
    }

    /**
     * Constroi uma SQL e reseta as propriedades de construtor
     * @param type $tabela
     * @return \projectws\mvc\Model
     */
    protected function get() {
        $this->isSoftDelete();
        
        $SQL = [];

        $SQL[] = "SELECT " . $this->constructSelect();
        $SQL[] = "FROM " . $this->structure['from'] . (!empty($this->structure['alias']) ? " " . $this->structure['alias'] : "");
        $SQL[] = (count($this->structure['join']) == 0 ? '' : implode("\n", $this->structure['join']));
        $SQL[] = (count($this->structure['where']) == 0 ? '' : ' WHERE ' . $this->constructWhere());
        $SQL[] = (count($this->structure['group']) == 0 ? '' : ' GROUP BY ' . implode(', ', $this->structure['group']));
        $SQL[] = (count($this->structure['order']) == 0 ? '' : ' ORDER BY ' . implode(', ', $this->structure['order']));
        $SQL[] = (!$this->structure['limit'] ? '' : ' LIMIT ' . $this->structure['limit']) . ' ';
        $SQL[] = (!$this->structure['offset'] ? '' : ' OFFSET ' . $this->structure['offset']) . ' ';

        $this->sql = trim(implode("\n", $SQL));

        return $this;
    }

    private function isSoftDelete() {
        if($this->model->isSoftDelete()) {
            $config = Config::get('orm');
            
            $this->whereNull($this->fromOrAlias() . "." . $config['trash']);
        }
    }

    /**
     * Retornar o alias ou o nome da tabela
     * @return mixed
     */
    public function fromOrAlias() {
        return is_null($this->structure['alias']) ? $this->from() : $this->alias();
    }

    /**
     * Define o from de um select
     * @param string $table
     * @param string $alias
     * @return \projectws\mvc\Model
     */
    public function from($table = null, $alias = null) {
        if($table instanceof Builder) {
            $table = $this->constructSqlRaw($table);

            $this->alias($this->from());
        }

        if(is_null($table))
            return $this->structure['from'];

        $this->addStruct('from', $table);

        //Se existir alias já o retorna
        if(!is_null($alias))
            return $this->alias($alias);

        return $this;
    }

    /**
     * Retorna uma sql parcial
     * @param $model
     * @param null $alias
     * @param bool|true $parenteses
     * @return string
     */
    private function constructSqlRaw($model, $alias = null, $parenteses = true) {
        $model->get();

        $return = preg_replace('/\n/', ' ', $model->sql);

        if($parenteses)
            $return = '(' . $return . ')';

        return $return . (is_null($alias) ? '' : ' as ' . $alias);
    }

    /**
     * alias da tabela
     * @param null $alias
     * @return $this
     */
    public function alias($alias = null) {
        if(is_null($alias)) {
            return $this->structure['alias'];
        }

        $this->addStruct('alias', $alias);

        return $this;
    }

    /**
     * Constrói os fields do select já formatado
     * @return string
     */
    private function constructSelect() {
        //Verifica se o usuário programador definiu os campos manualmente
        if(count($this->structure['select']) == 0) {
            $fieldsThisModel = $this->model->getFields();

            $fields = [];

            //se b nao for array é considerado como nome da coluna
            foreach ($fieldsThisModel as $a => $b) {
                if (is_array($b)) {
                    $colunaNome = $a;
                } else {
                    $colunaNome = $b;
                }

                $fields[] = $colunaNome;
            }

            //Escode os campos escondidos
            $fields = array_diff($fields, $this->model->getHiddenFields());

            //informa o alias da tabela
            $alias = $this->fromOrAlias();

            foreach ($fields as $a => $b) {
                //Verifica o nome do campo || utilizado para identificar o nome do campo
                if(is_array($b))
                    $colunaNome = $a;
                else
                    $colunaNome = $b;

                //Define campo a campo
                $this->select("$alias.$colunaNome");
            }

            //Caso não contenha nenhum field retorna o asterisco para todos
            if(count($fields) == 0) {
                $this->select("$alias.*");
            }
        }

        //Define a primary key como um alias para não quebrar o vinculo ao alterar um select fields
        $this->setPrimaryKeyInnerSelect();

        return implode(", ", $this->structure['select']);
    }

    /**
     * campos visíveis
     * @param type $field
     * @return \projectws\mvc\Model
     */
    public function select($field = null) {
        foreach (func_get_args() as $arg)
            $this->addStruct('select', $arg);

        return $this;
    }

    /**
     * Define o nome da primary key na select para uso interno do sistema
     */
    private function setPrimaryKeyInnerSelect() {
        $table = $this->structure['from'];
        $nick = $table;

        if(!empty($this->structure['alias'])) {
            $nick = $this->structure['alias'];
        }

        $pk = $this->model->getPrimaryKey();

        if(!$pk)
            return;

        $alias = $this->getPrimaryKeyInnerSelect();

        //Não pode existir agrupamento para esta execução | MySQL aceita
        if(count($this->structure['group']) == 0 && Database::getInstance()->getConfig('DRIVER') == 'mysql')
            $this->select("$nick.$pk as $alias");
    }

    /**
     * Retorna o nome da variavel primary key do sistema interno
     * @return string|void
     */
    public function getPrimaryKeyInnerSelect() {
        $table = $this->structure['from'];

        $pk = $this->model->getPrimaryKey();

        if(!$pk)
            return;

        return "_{$pk}_{$table}";
    }

    /**
     * Contrói parte do WHERE da SQL
     * @return string
     */
    public function constructWhere() {
        $wheres = [];

        foreach ($this->structure['where'] as $index => $where) {
            if($index > 0)
                $wheres[] =  " " . $where[1] . " " . $where[0];
            else
                $wheres[] = $where[0];
        }

        return implode('', $wheres);
    }

    /**
     * Usado para criar filtros WHERE na consulta SQL
     * @param null $where
     * @return $this
     */
    private function filtro($where = null) {
        //Filtro da pesquisa
        if(is_array($where)){
            foreach ($where as $campo => $valor) {
                $this->where($campo, "=", $valor);
            }
        } else {
            //insere como busca pelo indice ID
            if(is_numeric($where)) {
                $indices = $this->model->getIndices();

                if(count($indices))
                    $this->where($this->fromOrAlias() . '.' . $indices[0], '=', $where);
                else
                    $this->where($this->fromOrAlias() . '.' . 'id', '=', $where);
            }

            elseif(!empty($where))
                $this->where($where);
        }

        return $this;
    }

    /**
     * Constroi uma instrucao SELECT somente passando como parametro um array ou uma string com limit 1
     * @param $where
     * @return ORM
     */
    public function findFirst($where = null) {
        return $this->filtro($where)->row();
    }

	/**
	 * Lista somente 1 resultado
	 * @return $this|array|null|ModelCollection|ORM
	 */
    public function row() {
        $this->get();

        return $this->model->result($this->sql, true, $this->structure['where']);
    }

    /**
     * Limita em 1 posição a query
     * @return ORM
     */
    public function first() {
        return $this->limit(1);
    }

    /**
     * @param type $limit
     * @return \projectws\mvc\Model
     */
    public function limit($limit) {
        $this->addStruct('limit', $limit);

        return $this;
    }

    /**
     * Usado para construir uma lista de uma determinada coluna[$text] e o value opcionalmente usado para criar o indice do array
     * Bastante útil para combobox
     * @param null $text
     * @param null $value
     * @return array|void
     */
    public function lists($text = null, $value = null) {
        if(is_null($text)) {
            new Exception(Loader::getLang('Model', 'informed_value_text'));

            return;
        }

        $this->get();

        $rows = $this->rows();

        $retorno = [];

        $i = 0;

        foreach ($rows as $row) {
            $obj = new Entity($row->_original);

            if(is_null($value))
                $valor = $i++;
            else
                $valor = $obj->{$value};

            $retorno[$valor] = $obj->{$text};
        }

        return $retorno;
    }

    /**
     * Constrói um LEFT JOIN
     * @param $tabela
     * @param null $uniao
     * @return \projectws\mvc\Model
     */
    public function leftJoin($tabela, $col1 = null, $operator = null, $col2 = null) {
        return $this->join($tabela, $col1, $operator, $col2, 'LEFT');
    }

    /**
     * Constrói um JOIN
     * @param $tabela
     * @param null $col1
     * @param null $operator
     * @param null $col2
     * @param string $type
     * @return \projectws\mvc\Model
     */
    public function join($tabela, $col1 = null, $operator = null, $col2 = null, $type = 'INNER') {
        if($tabela instanceof Builder)
            $tabela = $this->constructSqlRaw($tabela, $tabela->fromOrAlias());

	    if(gettype($col1) == "object") {
	    	$construct = $this->closureJoin($tabela, $col1, $type);
		    
		    $this->addStruct('join', $construct);
		    
		    return $this;
	    }
	    
        if(is_null($col1) && is_null($operator) && is_null($col2)) {
            $alias = empty($this->structure['alias']) ? $this->structure['from'] : $this->structure['alias'];

            $uniao = "$tabela.id = $alias.id_$tabela";
        } else {
            $uniao = $col1 . $operator . $col2;
        }
	    
        $this->addStruct('join', $type . " JOIN {$tabela} ON {$uniao}");

        return $this;
    }

	/**
	 * Cria um join mais versatil
	 * @param $tabela
	 * @param \Closure $joinFunc
	 * @return Join
	 */
	private function closureJoin($tabela, \Closure $joinFunc, $type) {
		$joinClass = new Join($tabela, $type);
		
		$joinFunc($joinClass);
		
		return $joinClass;
	}

    /**
     * Constrói um RIGHT JOIN
     * @param $tabela
     * @param null $col1
     * @param null $operator
     * @param null $col2
     * @return \projectws\mvc\Model
     */
    public function rightJoin($tabela, $col1 = null, $operator = null, $col2 = null) {
        return $this->join($tabela, $col1, $operator, $col2, 'RIGHT');
    }

    /**
     * Constrói um FULL OUTER JOIN
     * @param $tabela
     * @param null $col1
     * @param null $operator
     * @param null $col2
     * @return \projectws\mvc\Model
     */
    public function fullOuterJoin($tabela, $col1 = null, $operator = null, $col2 = null) {
        return $this->join($tabela, $col1, $operator, $col2, 'FULL OUTER');
    }

    /**
     * Constrói um CROSS JOIN
     * @param $tabela
     * @return \projectws\mvc\Model
     */
    public function crossJoin($tabela) {
        $this->addStruct('join', "CROSS JOIN {$tabela}");

        return $this;
    }

    /**
     * @param type $group
     * @return \projectws\mvc\Model
     */
    public function groupBy($group) {
        $groups = func_get_args();

        foreach ($groups as $group) {
            $this->addStruct('group', $group);
        }

        return $this;
    }

    /**
     * @param type $order
     * @return \projectws\mvc\Model
     */
    public function orderBy($order) {
        $this->addStruct('order', $order);

        return $this;
    }

    /**
     * @param type $offset
     * @return \projectws\mvc\Model
     */
    public function offset($offset) {
        $this->addStruct('offset', $offset);

        return $this;
    }

    /**
     * Atualiza um model
     * @param array $columns
     * @param array $where
     * @param bool $anyFields Se estiver ativo aceita o update de qualquer campo que não contenha em $fields
     * @return $this
     */
    public function update(array $columns, array $where = [], $anyFields = false) {
        $dados = [];

        foreach ($columns as $coluna => $valor) {
            if(in_array($coluna, $this->model->getColumns()) || $anyFields === true) {
                if ($valor instanceof Builder) {
                    $dados[] = $this->constructSqlRaw($valor);
                } elseif (is_string($valor)) {
                    $dados[] = $coluna . "=" . $this->returnValue($valor);
                } elseif (is_null($valor)) {
                    $dados[] = $coluna . '=NULL';
                } elseif (is_bool($valor)) {
                	$dados[] = $coluna . '=' . ($valor ? 'TRUE' : 'FALSE');
                } else {
                    $dados[] = $coluna . '=' . $valor;
                }
            }
        }

        //Se estiver ativo o timestamp salva automáticamente o 'alterado em' na tabela
        if($this->model->getTimestamp()) {
            $config = Config::get('orm');

            $dados[] = $config['timestamp_updated'] . '=' . $this->returnValue(date('Y-m-d H:i:s'));
        }

        $dados = implode(", ", $dados);

        if(empty($dados)) {
            return $this;
        }

        //Faz a construção dos filtros
        foreach ($where as $name => $value) {
            $this->where($name, '=', $value);
        }
        
        if($this->constructWhere() == "") {
	        new Exception(Loader::getLang("Model", "no_have_where", "UPDATE {$this->structure['from']} SET $dados..."));
	
	        return null;
        }

        $where = " WHERE " . $this->constructWhere();

        $this->sql = "UPDATE {$this->structure['from']} SET $dados$where";
        
        return $this->model->execute($this->sql);
    }
	
    /**
     * Faz insert de um novo model
     * @param array $column
     * @return null
     */
    public function insert(array $column = []) {
        $nomes = [];
        $valores = [];

        //constrói os SETERS
        foreach ($column as $coluna => $valor) {
            if(in_array($coluna, $this->model->getColumns())) {
                $nomes[] = $coluna;
    
                if (is_string($valor)) {
                    $valores[] = $this->returnValue($valor);
                } elseif (is_null($valor)) {
                    $valores[] = 'NULL';
                } elseif (is_bool($valor)) {
                    $valores[] = ($valor ? 'TRUE' : 'FALSE');
                } else {
                    $valores[] = $valor;
                }
            }
        }

        //Se estiver ativo o timestamp salva automáticamente o 'criado em' na tabela
        if($this->model->getTimestamp()) {
            $config = Config::get('orm');

            $nomes[] = $config['timestamp_created'];
            $valores[] = $this->returnValue(date('Y-m-d H:i:s'));
        }

        $nomes = implode(", ", $nomes);
        $valores = implode(", ", $valores);

        if(empty($nomes)) {
            new Exception(Loader::getLang("Model", "no_values_insert"));

            return null;
        }

        $this->sql = "INSERT INTO {$this->structure['from']} ($nomes) VALUES ($valores)";

        if(Config::get('database')['DRIVER'] == "pgsql") {
            $pk = $this->model->getPrimaryKey();
            if($pk)
                $this->sql .= "RETURNING " . $pk;
        }

        return $this->model->execute($this->sql, true);
    }

    /**
     * Método para fazer a deleção do model
     * @param array|int $column
     * @return mixed
     */
    public function delete($column = []) {
        //Aceitar definição direta da chave primaria
        if(is_numeric($column))
            $where[$this->model->getPrimaryKey()] = $column;
        else
            $where = $column;

        //Faz a construção dos filtros
        foreach ($where as $name => $value) {
            $this->where($name, '=', $value);
        }

        //Se não existir filtros usa os indices
        if(count($this->structure['where']) == 0) {
        	$valPK = $this->model->getValuePrimaryKey();
        	
	        if($valPK > 0)
                $this->where($this->model->getPrimaryKey(), '=', $valPK);
        }

        if(count($this->structure['where']) == 0) {
            $indices = $this->model->getIndices();
	
	        foreach ($indices as $indice) {
		        $this->where($indice, '=', $this->model->{$indice});
	        }
        }

        $where = " WHERE " . $this->constructWhere();

        $this->sql = "DELETE FROM {$this->structure['from']}$where";

        return $this->model->execute($this->sql);
    }

    /**
     * Raw
     * @return $this
     */
    public function raw() {
        return $this;
    }

    /**
     * Retorna a sql construida
     */
    public function preview() {
        $this->get();

        pd($this->sql);
    }

    /**
     * Chamada de procedure
     * @param $procedure
     * @param array $params
     * @return bool|string
     */
    protected function call($procedure, $params = []) {
        foreach ($params as $index => $value)
            $params[$index] = $this->returnValue($value);

        $params = implode(", ", $params);

        if(Database::getInstance()->getConfig('DRIVER') == 'mysql')
            $this->sql = "CALL $procedure($params)";
        elseif(Database::getInstance()->getConfig('DRIVER') == 'pgsql')
            $this->sql = "SELECT $procedure($params)";
        else
            $this->sql = "SELECT $procedure($params)";

        return $this->execute();
    }
	
    /**
     * Closure torna-se $this
     * @param $closure
     * @return $this
     */
    private function closure($closure) {
        $closure($this);

        return $this;
    }

    /**
     * Retorna um filtro raw
     * @param \Closure $closure
     * @return string
     */
    private function closureWhere(\Closure $closure) {
        $builder = new Builder();
        $builder->setModel(new GenericModel());

        $closure($builder);

        return $builder->constructWhere();
    }

    /**
     * Define o model
     * @param Model $model
     */
    public function setModel(Model $model) {
        $this->model = $model;
    }

    /**
     * Usado para fazer depuração das SQL
     */
    private function depuracao() {
        echo console_log($this->sql);
    }
}