<?php

namespace projectws\libs\orm\relationship;

use projectws\libs\orm\ORM;
use projectws\Loader;

/**
 * Classe usada para linkar o objeto a um outro objeto com vários resultados pela chave estrangeira e
 * pela chave local da tabela
 * Class HasMany
 * @package projectws\libs\model
 */
class HasMany {

    //Objeto a ser relacionado
    private $relation;
    /**
     * Nome do modelo
     * @var ORM $model
     */
    private $model;
    //Nome do campo estrangeiro ex.: ID_TABELA
    private $foreign_key;
    //Nome do campo local Ex.: ID
    private $primary_key;

    /**
     * Indica se irá pegar o valor de _value_pk de model
     * @var bool
     */
    private $getPrk = true;

    /**
     * HasMany constructor.
     * @param $nome
     * @param $model
     * @param null $foreign_key
     * @param null $primary_key
     */
    public function __construct($nome, $model, $foreign_key = null, $primary_key = null) {
        $nome = preg_replace('/\//', '\\', Loader::getDir('APP') . Loader::getDir('MODELS') . $nome);

        $this->relation = new $nome();
        $this->model = $model;

        if(!is_null($foreign_key))
            $this->getPrk = false;

        $this->setForeignKey($foreign_key);
        $this->setPrimaryKey($primary_key);
    }

    /**
     * @param string $foreign_key
     */
    public function setForeignKey($foreign_key) {
        if(is_null($foreign_key))
            $foreign_key = 'id_' . $this->model->from();

        $this->foreign_key = $foreign_key;
    }

    /**
     * @param string $local_key
     */
    public function setPrimaryKey($primary_key) {
        if(is_null($primary_key))
            $primary_key = 'id';

        $this->primary_key = $primary_key;
    }

    /**
     * Traz o resultado do objeto
     * @return mixed
     */
    public function result() {
        $foreign_key_value = $this->getForeignKeyValue();

        return $this->relation($foreign_key_value);
    }

    /**
     * Seleciona o valor da chave estrangeira
     * @return null
     */
    private function getForeignKeyValue() {
    	$value = $this->model->getValuePrimaryKey();
    	
        if($this->getPrk && $value)
            return $this->model->getValuePrimaryKey();

        return $this->model->{$this->primary_key};
    }

    /**
     * Faz a relação do objeto
     * @param $nome
     * @param $foreign_key_value
     * @return mixed
     */
    private function relation($foreign_key_value) {
        return $this->relation->setRelationship('hasMany')->where($this->foreign_key, '=', $foreign_key_value);
    }
}