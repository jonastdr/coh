<?php

namespace projectws\libs\orm\relationship;

use projectws\Loader;
use projectws\mvc\Model;

/**
 * Classe usada para linkar o objeto a um outro objeto pela chave estrangeira e pela chave local da tabela
 * Inverso de HasOne
 * Class BelongsTo
 * @package projectws\libs\model
 */
class BelongsTo {

    //Objeto a ser relacionado
    private $relation;
    //Nome do modelo
    private $model;
    //Nome do campo estrangeiro ex.: ID_TABELA
    private $foreign_key;
    //Nome do campo local Ex.: ID
    private $primary_key;

    /**
     * BelongsTo constructor.
     * @param $nome
     * @param Model $model
     * @param null $local_key
     * @param null $foreign_key
     */
    public function __construct($nome, Model $model, $foreign_key = null, $primary_key = null)
    {
        $nome = preg_replace('/\//', '\\', Loader::getDir('APP') . Loader::getDir('MODELS') . $nome);

        $this->relation = new $nome();
        $this->model = $model;
        $this->setForeignKey($foreign_key);
        $this->setPrimaryKey($primary_key);
    }

    /**
     * @param string $foreign_key
     */
    public function setForeignKey($foreign_key) {
        if(is_null($foreign_key))
            $foreign_key = 'id_' . $this->relation->from();

        $this->foreign_key = $foreign_key;
    }

    /**
     * @param string $local_key
     */
    public function setPrimaryKey($primary_key) {
        if(is_null($primary_key))
            $primary_key = 'id';

        $this->primary_key = $primary_key;
    }

    /**
     * Traz o resultado do objeto
     * @return mixed
     */
    public function result() {
        $foreign_key_value = $this->getForeignKeyValue();

        return $this->relation($foreign_key_value);
    }

    /**
     * Seleciona o valor da chave estrangeira
     * @return null
     */
    private function getForeignKeyValue() {
        return $this->model->{$this->foreign_key};
    }

    /**
     * Faz a relação do objeto
     * @param $nome
     * @param $foreign_key_value
     * @return mixed
     */
    private function relation($foreign_key_value) {
        return $this->relation->setRelationship('belongsTo')->where($this->primary_key, '=', $foreign_key_value);
    }

}