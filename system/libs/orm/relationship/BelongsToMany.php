<?php

namespace projectws\libs\orm\relationship;

use projectws\Loader;

/**
 * Class BelongsToMany
 * @package projectws\libs\model
 */
class BelongsToMany {

    //Nome do modelo
    private $model;
    //Tabela intermediaria
    private $pivot;
    //Nome do campo estrangeiro Ex.: ID
    private $foreign_key;
    //Nome do campo que refere ao campo estrangeiro ex.: ID_TABELA
    private $primary_key;
    //Nome do campo estrangeiro da segunda tabela Ex.: ID
    private $foreign_key_this;
    //Nome do campo que refere ao campo estrangeiro da segunda tabela ex.: ID_TABELA
    private $primary_key_this;
    //Nome do
    //objeto relacionado
    private $relation;

    /**
     * BelongsToMany constructor.
     * @param $nome
     * @param $model
     * @param null $pivotTable
     * @param null $foreign_key
     * @param null $primary_key
     */
    public function __construct($nome, $model, $pivotTable = null, $foreign_key = null, $primary_key = null, $foreign_key_this = null, $primary_key_this = null) {
        $nome = preg_replace('/\//', '\\', Loader::getDir('APP') . Loader::getDir('MODELS') . $nome);

        $this->relation = new $nome();

        $this->model = $model;
        $this->setPivotTable($pivotTable);
        $this->setForeignKey($foreign_key);
        $this->setPrimaryKey($primary_key);
        $this->setForeignKeyThis($foreign_key_this);
        $this->setPrimaryKeyThis($primary_key_this);
    }

    /**
     * Define o valor da chave estrangeira da tabela a ser relacionada
     * @param string $name
     */
    private function setForeignKey($name) {
        if(is_null($name))
            $name = 'id_' . $this->relation->from();

        $this->foreign_key = $name;
    }

    /**
     * Define o valor da chave que se refere a chave estrangeira da tabela a ser relacionada
     * @param string $name
     */
    private function setPrimaryKey($name) {
        if(is_null($name))
            $name = 'id';

        $this->primary_key = $name;
    }

    /**
     * Define o valor da chave estrangeira da tabela a ser relacionada do objeto de partida
     * @param string $name
     */
    private function setForeignKeyThis($name) {
        if(is_null($name))
            $name = 'id_' . $this->model->from();

        $this->foreign_key_this = $name;
    }

    /**
     * Define o valor da chave que se refere a chave estrangeira da tabela a ser relacionada do objeto de partida
     * @param string $name
     */
    private function setPrimaryKeyThis($name) {
        if(is_null($name))
            $name = 'id';

        $this->primary_key_this = $name;
    }

    /**
     * Define o nome da tabela intermediaria
     * @param string $name
     */
    private function setPivotTable($name) {
        if(is_null($name))
            $name = $this->relation->from() . "_" . $this->model->from();

        $this->pivot = $name;
    }

    /**
     * Traz o resultado do objeto
     * @return mixed
     */
    public function result() {
        $primary_key_value = $this->getForeignKeyValue();

        return $this->relation($primary_key_value);
    }

    /**
     * Seleciona o valor da chave estrangeira
     * @return null
     */
    private function getForeignKeyValue() {
        return $this->model->getValuePrimaryKey();
    }

    /**
     * Faz a relação do objeto
     * @param $nome
     * @param $primary_key_value
     * @return mixed
     */
    private function relation($primary_key_value) {
        return $this->relation
            ->setRelationship('belongsToMany')
            ->alias('a')
            ->join($this->pivot . ' pivot', 'pivot.' . $this->foreign_key, '=' . 'a.', $this->primary_key_this)
            ->join($this->model->from() . ' b', 'b.' . $this->primary_key_this, '=', 'pivot.' . $this->foreign_key_this)
            ->where('b.' . $this->primary_key_this, '=', $primary_key_value);
    }
}