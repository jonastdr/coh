<?php

namespace projectws\libs\orm;

/**
 * Construção de WHERE
 * Class Where
 * @package projectws\libs\orm
 */
trait Where {
	
	/**
	 * Faz um filtro na sql
	 * @param string $column
	 * @param null $operator
	 * @param null $value
	 * @param bool $slashes
	 * @param string $next
	 * @return $this|Builder
	 */
	public function where($column, $operator = null, $value = null, $slashes = true, $next = 'AND') {
		//SE O FORMATO FOR DA INSTANCIA BUILDER IGUALA A UMA SQL CRUA
		if($value instanceof Builder) {
			$value = $this->constructSqlRaw($value);
			
			$this->addStruct('where', ["$column $operator $value", $next]);
			
			return $this;
		}
		
		if(gettype($column) == "object") {
			$whereRaw = $this->closureWhere($column);
			
			$this->addStruct('where', ["(" . $whereRaw . ")", $next]);
			
			return $this;
		}
		
		if (!$operator && !$value) {
			$this->addStruct('where', [$column, $next]);
			
			return $this;
		}
		
		if(!is_numeric($value) && $slashes){
			$value = "'" . addslashes($value) . "'";
		}
		
		$this->addStruct('where', ["$column $operator $value", $next]);
		
		return $this;
	}
	
	/**
	 * Faz um filtro na sql com o proximo procedendo de OR
	 * @param string $column
	 * @param null $operator
	 * @param null $value
	 * @param bool|true $slashes
	 * @return $this|Builder
	 */
	public function orWhere($column, $operator = null, $value = null, $slashes = true) {
		return $this->where($column, $operator, $value, $slashes, 'OR');
	}
	
	/**
	 * relativo a
	 * WHERE column IN (1,2,3,4...)
	 * @param $column
	 * @param array|Model $values
	 * @return $this|Builder
	 */
	public function whereIn($column, $values, $next = 'AND') {
		if($values instanceof Builder) {
			$values = $this->constructSqlRaw($values);
		} elseif(is_array($values)) {
			$values = '(' . implode(', ', $this->inValues($values)) . ')';
		} else {
			new Exception(Loader::getLang('Model', 'value_incorret_in_method', 'values', 'whereIn'));
		}
		
		$this->addStruct('where', ["($column in $values)", $next]);
		
		return $this;
	}
	
	/**
	 * relativo a
	 * WHERE column IN (1,2,3,4...) OR...
	 * @param $column
	 * @param $values
	 * @return $this|Builder
	 */
	public function orWhereIn($column, $values) {
		return $this->whereIn($column, $values, 'OR');
	}
	
	/**
	 * relativo a
	 * WHERE column NOT IN (1,2,3,4...)
	 * @param $column
	 * @param array|Model $values
	 * @return $this|Builder
	 */
	public function whereNotIn($column, $values, $next = 'AND') {
		if($values instanceof Builder) {
			$values = $this->constructSqlRaw($values);
		} elseif(is_array($values)) {
			$values = '(' . implode(', ', $this->inValues($values)) . ')';
		} else {
			new Exception(Loader::getLang('Model', 'value_incorret_in_method', 'values', 'whereNotIn'));
		}
		
		$this->addStruct('where', ["($column not in $values)", $next]);
		
		return $this;
	}
	
	/**
	 * relativo a
	 * WHERE column NOT IN (1,2,3,4...) OR...
	 * @param $column
	 * @param $values
	 * @return $this|Builder
	 */
	public function orWhereNotIn($column, $values) {
		return $this->whereNotIn($column, $values, 'OR');
	}
	
	/**
	 * Relativo a
	 * WHERE column IS NULL
	 * @param $column
	 * @return $this|Builder
	 */
	public function whereNull($column, $next = 'AND') {
		$this->addStruct('where', ["($column is null)", $next]);
		
		return $this;
	}
	
	/**
	 * Relativo a
	 * WHERE column IS TRUE|FALSE
	 * @param $column
	 * @param bool $cond
	 * @param string $next
	 * @return $this
	 */
	public function whereBool($column, $cond = true, $next = 'AND') {
		if($cond)
			$this->addStruct('where', ["($column is true)", $next]);
		else
			$this->addStruct('where', ["($column is false)", $next]);
		
		return $this;
	}
	
	/**
	 * Relativo a
	 * WHERE column IS TRUE
	 * @param $column
	 * @param string $next
	 * @return Builder
	 */
	public function whereTrue($column, $next = 'AND') {
		return $this->whereBool($column, true, $next);
	}
	
	/**
	 * Relativo a
	 * WHERE column IS FALSE
	 * @param $column
	 * @param string $next
	 * @return Builder
	 */
	public function whereFalse($column, $next = 'AND') {
		return $this->whereBool($column, false, $next);
	}
	
	/**
	 * Relativo a
	 * WHERE column IS NULL OR...
	 * @param $column
	 * @return $this|Builder
	 */
	public function orWhereNull($column) {
		return $this->whereNull($column, 'OR');
	}
	
	/**
	 * @param $column
	 * @return $this|Builder
	 */
	public function whereNotNull($column, $next = 'AND') {
		$this->addStruct('where', ["($column is not null)", $next]);
		
		return $this;
	}
	
	/**
	 * Relativo a
	 * WHERE column IS NOT NULL OR...
	 * @param $column
	 * @return $this|Builder
	 */
	public function orWhereNotNull($column) {
		return $this->whereNotNull($column, 'OR');
	}
	
	/**
	 * WHERE column BETWEEN .....
	 * @param $column
	 * @param $value1
	 * @param $value2
	 * @param string $next
	 * @return $this
	 */
	public function whereBetween($column, $value1, $value2, $next = 'AND') {
		$value1 = $this->returnValue($value1);
		$value2 = $this->returnValue($value2);
		
		$this->addStruct('where', ["($column BETWEEN $value1 AND $value2)", $next]);
		
		return $this;
	}
	
	/**
	 * Usado para construcao de parametros IN
	 * @param $values
	 * @return array
	 */
	private function inValues($values) {
		return array_map(function ($value) {
			return $this->returnValue($value);
		}, $values);
	}
	
}