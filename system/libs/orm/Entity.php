<?php

namespace projectws\libs\orm;

class Entity {

    public function __construct(array $argumentos = array()) {
        if (!empty($argumentos)) {
            foreach ($argumentos as $propriedade => $valor) {
                $this->{$propriedade} = $valor;
            }
        }
    }

}