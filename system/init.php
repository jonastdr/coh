<?php

use projectws\libs\Exception;

require(__DIR__ . '/Loader.php');

//inicia o arquivo dos helpers global
require(__DIR__ . '/helpers/global.php');

//arquivos de envio de e-mail
require(__DIR__ . '/../thirdparty/phpmailer/phpmailer/PHPMailerAutoload.php');

try {
    //iniciao sessao
    session_start();

    //Carrega arquivo de configuração de ambiente
    $config = require(__DIR__ . '/../config/environment.php');
    $dbConfig = require(__DIR__ . '/../config/database.php');

    $loader = new projectws\Loader($config, __DIR__ . '/../');
    
    //registra os diretorios padroes
    $loader->registerDirs($config['FOLDERS']);
    
    //Inicia a injecao de dependencias
    $di = new projectws\di\FactoryDefault();

    $dependency = require(__DIR__ . '/../config/dependencies.php');
    
    foreach ($dependency as $nome => $valor) {
        $di[$nome] = function() {
            global $valor;

            if(is_string($valor)){
                return new $valor();
            }
            elseif(is_array($valor)){
                
            }
        };
    }
    
    //Cria as configurações do banco de dados
    $database = new projectws\libs\Database($dbConfig);
    $database->connect();

    //Define a URL padrao do aplicativo
    $https = filter_input(INPUT_SERVER, 'HTTPS');

    $scheme = (!empty($https) AND $https != 'off') ? 'https' : 'http';
    $server = filter_input(INPUT_SERVER, 'SERVER_NAME') . "/";

    $url = new projectws\libs\Url();
    $url->setBaseUrl($scheme. "://" . $server);
    
    //Inicia a aplicação
    $Aplicacao = new projectws\App($di);

    echo $Aplicacao->getContent();

} catch (Exception $e) {
    echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}