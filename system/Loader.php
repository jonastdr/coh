<?php

namespace projectws;

use projectws\libs\Exception;

class Loader {

    private $dirs = array();
    private $config;
    private static $instance;
    private $base;
    
    public function __construct($config, $base) {
        $this->setBasePath($base);

        self::$instance = $this;
        
        $this->config = $config;

        //funcao para selecionar a classe
        spl_autoload_register(array($this, 'load'));
    }
    
    public static function getInstance() {
        return self::$instance;
    }
    
    public function registerDirs(array $dirs) {
        foreach ($dirs as $key => $value) {
            $this->dirs[$key] = $value;
        }
    }
    
    public static function getDir($nome) {
        $loader = self::getInstance();

        $dir = isset($loader->dirs[strtoupper($nome)]) ? $loader->dirs[strtoupper($nome)] : null;

        if($dir === null) {
            new Exception(self::getLang('Loader', 'dir_not_exist', $nome));

            return null;
        }

        return $dir;
    }
    
    /**
     * Retorna um diretório existente de armazenamento
     * @param string $nome
     * @return string
     */
    public static function getStoreDir($nome = '') {
        if($nome)
            $nome = $nome . "/";
        
        $dir = Loader::getBasePath() . 'store/' . $nome;
        
        if(!file_exists($dir)) {
            new Exception(self::getLang('Loader', 'dir_not_exist', $nome));
        }
        
        return $dir;
    }

    public static function getConfig($nome) {
        $loader = self::getInstance();

        $config = isset($loader->config[strtoupper($nome)]) ? $loader->config[strtoupper($nome)] : null;

        if($config === null) {
            new Exception(self::getLang('Loader', 'config_not_exist', $nome));

            return null;
        }

        return $loader->config[strtoupper($nome)];
    }

    public static function getConfigSytem($nome) {
        $loader = self::getInstance();

        $configSystem = $loader->getBasePath() . 'config/system.php';

        if(!file_exists($configSystem)) {
            new Exception(self::getLang('Loader', 'config_sys_not_exist', $nome));

            return null;
        }

        $configSystem = require($configSystem);

        return $configSystem[strtoupper($nome)];
    }

    private function setBasePath($base) {
        $this->base = $base . (substr($base, -1, 1) == '/' ? '' : '/');
    }

    public static function getBasePath() {
        $loader = self::getInstance();

        return $loader->base;
    }

    public static function getAppPath() {
        $loader = self::getInstance();

        return $loader->getBasePath() . $loader->getDir('APP');
    }

    public static function getLang($type, $lang) {
        $loader = self::getInstance();

        $langUser = null;

        $params = func_get_args();
        array_shift($params);
        array_shift($params);

        $userDefault = $loader->getBasePath() . $loader->getDir('APP') . $loader->getDir('LANGUAGES') . $loader->getConfigSytem('LANG') . '/' . $type . '.php';

        if(file_exists($userDefault))
            $langUser = require($userDefault);

        if(isset($langUser[$lang]))
            if(count($params) == 0)
                return $langUser;
            else
                return vsprintf($langUser[$lang], $params);

        $systemLang = $loader->getBasePath() . 'system/lang/' . $type . '.php';

        if (!file_exists($systemLang)) {
            new Exception("O arquivo de linguagem padrão do sistema foi excluído!");

            return null;
        }

        $systemLang = require($systemLang);

        if(isset($systemLang[$lang]))
            if(count($params) == 0)
                return $systemLang[$lang];
            else
                return vsprintf($systemLang[$lang], $params);


        new Exception("Language $type:$lang não existe!");

        return null;
    }

    /**
     * Método usado para carregar classes
     * @param $classe
     * @throws \Exception
     */
    private function load($classe) {
        $caminho = explode('\\', $classe);

        //carrega dependencias
        $aliases = $this->getConfig('ALIAS');

        if(count($caminho) == 1) {
            if(array_key_exists($caminho[0], $aliases)) {
                //Cria um alias da classe
                class_alias($aliases[$caminho[0]], $caminho[0]);

                $caminho = explode("\\", $aliases[$caminho[0]]);
            }
        }

        if($caminho[0] == 'projectws') {
            $caminho[0] = 'system';
        }

        //CAMINHO DO ARQUIVO INDEX
        $doc_root = $this->getBasePath();

        $path = implode(DIRECTORY_SEPARATOR, $caminho);

        if (!file_exists($doc_root . $path . '.php')) {
            return new Exception($this->getLang('Loader', 'lib_not_exist', $classe), 500);
        }

        require_once($doc_root . $path . '.php');
    }

    /**
     * Carrega helper para utilização
     * @param $nome
     * @throws \Exception
     */
    public static function loadHelper($nome) {
        $instance = self::getInstance();

        //CAMINHO DO ARQUIVO INDEX
        $doc_root = $instance->getBasePath();

        $path = $instance->dirs['APP'] . $instance->dirs['HELPERS'] . $nome . '.php';

        if(!file_exists($doc_root . $path))
            return new Exception(self::getLang('Loader', 'helper_not_exist', $nome), 500);

        require_once($doc_root . $path);
    }
}
