<?php

if(!function_exists('route')) {
    /**
     * Retonar a rota pelo alias
     * @param string $nome
     * @return mixed|null
     */
    function route($name = '') {
        if($name == '')
            return null;

        $parametros = func_get_args();

        $route = call_user_func_array(['projectws\libs\Router', 'alias'], $parametros);

        $route = preg_replace('/^\/$/', '', $route);

        return \projectws\libs\Url::getBaseUrl() . $route;
    }
}

if(!function_exists('helper')) {
    /**
     * Carrega arquivo de helpers
     * @param string $nome
     * @return null|void
     * @throws \Exception
     */
    function helper($nome = '') {
        if($nome == '')
            return null;

        return projectws\Loader::loadHelper($nome);
    }
}

if(!function_exists('view')) {
    /**
     * retorna uma view
     * @param string $nome
     * @param array $params
     * @return null|string
     * @throws Exception
     */
    function view($nome = '', $params = array()) {
        if($nome == '')
            return null;

        $view = new projectws\mvc\View();

        return $view->render($nome, $params);
    }
}

if(!function_exists('redirect')) {
    /**
     * Função para redirecionamento de página
     * @param $nome
     * @return null
     */
    function redirect($nome = null) {
        return projectws\libs\Response::redirect($nome);
    }
}

if(!function_exists('env')) {
    /**
     * Retorna a config
     * @param string $config
     * @return null
     */
    function env($config = '') {
        if($config == '')
            return null;

        return projectws\Loader::getConfig($config);
    }
}

if(!function_exists('lang')) {
    /**
     * Retorna uma tradução de mensagem
     * @param string $type
     * @param string $msg
     * @return mixed|null
     */
    function lang($type = '', $msg = '') {
        if($type == '' OR $msg == '')
            return null;

        $parametros = func_get_args();

        return call_user_func_array(['projectws\Loader', 'getLang'], $parametros);
    }
}

if(!function_exists('coalesce')) {
    /**
     * Retornar o primeiro valor não nulo
     * @param null $param1
     * @param null $param2
     * @return $param|null
     */
    function coalesce($param1 = null, $param2 = null) {
        $parametros = func_get_args();

        foreach ($parametros as $param)
            if($param != null)
                return $param;

        return null;
    }
}

if(!function_exists('console_log')) {
    /**
     * depuração de código para console.log javascript
     * @param string $var
     * @return string
     */
    function console_log($var = '') {
        return "<script>console.log(" . json_encode($var) . ");</script>";
    }
}

if(!function_exists('between')) {
    /**
     * Para verificar se o valor está entre um valor e outro
     * @param $var
     * @param $cond1
     * @param $cond2
     * @return bool
     */
    function between($var, $cond1, $cond2) {
        if($var >= $cond1 && $var <= $cond2)
            return true;

        return false;
    }
}

if(!function_exists('in')) {
    /**
     * Verifica se existe algum valor na variavel
     * @param $var
     * @return bool
     */
    function in($var, $value1, $value2) {
        $parametros = func_get_args();

        //elimina o var para não comparar
        array_shift($parametros);

        foreach ($parametros as $param) {
            if($var == $param)
                return true;
        }

        return false;
    }
}

if(!function_exists('get_file')) {
    /**
     * seleciona o primeiro arquivo encontrado. Helper usado para adicionar imagem
     * @param $arquivo
     * @return string
     */
    function get_file($arquivo) {
        $params = func_get_args();

        foreach($params as $param) {
            $param = \projectws\libs\Url::getBaseUrl() . 'public/' . $param;

            $headers = get_headers($param);

            if($headers[0] == 'HTTP/1.1 200 OK')
                return $param;
        }

    }
}

if(!function_exists('encrypt')) {
    /**
     * Função usada para criptografar string
     * @param $pass
     * @return null|string
     */
    function encrypt($pass) {
        if($pass == '')
            return null;

        return \projectws\libs\Encrypt::make($pass);
    }
}

if(!function_exists('timesToDateTime')) {
    /**
     * Converte um timestamp_unix para DATETIME
     * @param null $timestamp
     * @return bool|null|string DATETIME
     */
    function timesToDateTime($timestamp = null) {
        if($timestamp == null)
            return null;

        return date('Y-m-d H:i:s', $timestamp);
    }
}

if(!function_exists('timesToDate')) {
    /**
     * Converte um timestamp_unix para DATE
     * @param null $timestamp
     * @return bool|null|string DATETIME
     */
    function timesToDate($timestamp = null) {
        if($timestamp == null)
            return null;

        return date('Y-m-d', $timestamp);
    }
}

if(!function_exists('timesToView')) {
    /**
     * Converte um timestamp_unix para DATE
     * @param null $timestamp
     * @return bool|null|string DATETIME
     */
    function timesToView($timestamp = null, $time = false) {
        if($timestamp == null)
            return null;

        if($time)
            $time = " " . env('TIME_FORMAT_PREVIEW');

        return date(env('DATE_FORMAT_PREVIEW') . $time, $timestamp);
    }
}

if(!function_exists('equals')) {
    /**
     * Usado para conferir se varios resultados é igual ao valor necessário
     * @param $value
     * @param array $needle
     * @return bool
     */
    function equals($value, Array $needle) {
        foreach ($needle as $valor) {
            if($value !== $valor) {
                return false;
            }
        }

        return true;
    }
}

if(!function_exists('dd')) {
    /**
     * Dump and Die
     * Usado para testes de variaveis | ao executar a aplicação é encerrada
     * @param $value
     */
    function dd($value) {
        $params = func_get_args();

        echo '<pre>';
        var_dump($params);
        echo '</pre>';

        die();
    }
}

if(!function_exists('pd')) {
    /**
     * Print and Die
     * Usado para testes de variaveis | ao executar a aplicação é encerrada
     * @param $value
     */
    function pd($value) {
        $params = func_get_args();

        echo '<pre>';

        if(count($params) == 1)
            print_r($params[0]);
        else
            print_r($params);

        echo '</pre>';

        die();
    }
}

if(!function_exists('url')) {
    /**
     * Retorna a url dentro do projeto
     * @param null $url
     * @return string
     */
    function url($url = null) {
        return \projectws\libs\Url::getBaseUrl() . $url;
    }
}

if(!function_exists('removefile')) {
    /**
     * Usado para remover arquivo do projeto
     * @param null $file
     * @return null]
     */
    function removeFile($file = null) {
        if($file == null)
            return null;

        if(!file_exists(\projectws\Loader::getBasePath() . $file)) {
            new \projectws\libs\Exception("O arquivo não existe!");

            return false;
        }

        unlink(\projectws\Loader::getBasePath() . $file);

        return true;
    }
}