<?php

namespace projectws\mvc;
use projectws\libs\Database;
use projectws\libs\orm\ORM;

/**
 * Classe para construir um modelo de uma tabela de banco de dados
 * @author Jonas <tortato.jonas@gmail.com>
 */
abstract class Model extends ORM {

    /**
     * driver DB
     * @var void
     */
    protected $driver;

    /**
     * Model constructor.
     */
    public function __construct() {
        parent::__construct();

        //Define o driver de banco de dados
        $this->driver = Database::getInstance()->getConfig('DRIVER');

        //Retorna a conexão atual
        static::$db = Database::getConnection();
    }
}
