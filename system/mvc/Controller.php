<?php

namespace projectws\mvc;

use projectws\App;
use projectws\libs\Url;
use projectws\libs\Assets;
use projectws\mvc\View;

abstract class Controller {

    public $di;
    public $view;
    public $assets;
    public $parameter;

    public function __construct() {

        $app = App::getInstance();
        
        $dependencias = $app->getDependencies();
        
        foreach ($dependencias as $key => $value) {
            $this->$key = $value;
        }

        $this->assets = new Assets();

        $this->view = new View();

        //método construtor
        if(method_exists($this, 'begin'))
            $this->begin();
    }
    
    protected function res() {
    	
    }

}
