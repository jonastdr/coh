<?php

namespace projectws\mvc;

use projectws\App;
use projectws\libs\Exception;
use projectws\Loader;
use projectws\libs\Assets;
use projectws\libs\tag;
use projectws\libs\ViewCompiler;

class View {

    public $vars;
    private $controller;
    private $action;
    private $app;
    private $assets;
    private $loader;
    private $arquivo;

    public function __get($property) {
        if (isset($this->vars[$property])) {
            return $this->vars[$property];
        }
        return null;
    }

    public function __set($property, $value) {
        $this->vars[$property] = $value;
    }

    public function __construct() {
        $this->loader = Loader::getInstance();

        $this->app = App::getInstance();

        $this->assets = Assets::getInstance();
        
        //Carrega todas as dependencias
        $dependencias = $this->app->getDependencies();
        
        foreach ($dependencias as $key => $value) {
            $this->$key = $value;
        }
        
        $this->controller = $this->app->getController();
        $this->action = $this->app->getAction();
    }

    public function render($file, $variaveis = array())
    {
        if (count($variaveis)) {
            foreach ($variaveis as $nome => $valor)
                $this->vars[$nome] = $valor;
        }

        if ($this->vars) {
            foreach ($this->vars as $nome => $valor) {
                $$nome = $valor;
            }
        }

        //Base do arquivo
        $base = $this->loader->getBasePath();

        //verifica se o arquivo é do tipo template edge
        if(preg_match('/(\.edge)/', $file)) {
            $compiler = new ViewCompiler($file);

            //se estiver no ambiente de produção e o arquivo compilado existir não executa novamente a compilação
            //se estiver em ambimentes de testes faz as verificações para compilar o novo arquivo
            if((!$compiler->existCompile() && Loader::getConfig('ENV') == 'master') OR Loader::getConfig('ENV') != 'master') {
                //executa os replaces de funcoes e echo
                $novo = $compiler->make();

                if ($novo == "") {
                    new Exception(Loader::getLang('View', 'failed_compile', $file));

                    return null;
                }

                //compila o arquivo ou executa se não conter diferença
                if ($compiler->se_compilar($novo))
                    $compiler->compilar();
            }

            $this->arquivo = $compiler->file();
        } else {
            $app = $this->loader->getDir('APP');
            $dirView = $this->loader->getDir('VIEWS');

            $this->arquivo = $app . $dirView . $file . '.php';
        }

        if(!file_exists($base . $this->arquivo)) {
            new Exception(Loader::getLang('View', 'include_not_found', $this->arquivo));

            return null;
        } else {
            //inicia o buffer
            ob_start();
            //carrega o arquivo e lanca conteudo para view
            require_once($base . $this->arquivo);
            $view = ob_get_contents();
            ob_end_clean();

            return $view;
        }
    }

}
