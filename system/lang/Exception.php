<?php

return [
    "no_permission" => "Sem permissão para criar/alterar o arquivo!",
    "log_not_exist" => "A pasta de log não existe ou não tem permissão para criar/alterar o arquivo!"
];