<?php
return [
    "action_not_informed" => "Ação não informada para rota '%s'!",
    "last_param_not_function" => "O ultimo parâmetro do grupo de rota '%s' deve ser uma função.",
    "route_not_exist" => "Rota '%s' não existe!"
];