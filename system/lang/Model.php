<?php

return [
    "informed_indice" => "Deve informar os indices no modelo %s.",
    "no_values_change" => "Não foi informado nenhum valor para alterar!",
    "no_values_insert" => "Não foi informado nenhum valor para inserir!",
    "model_create_in_db" => "Modelo '%s' criado na base de dados!",
    "model_alter_in_db" => "Modelo '%s' alterado na base de dados!",
    "model_update_no_seters" => "Não foi definido seters no UPDATE",
    "informed_text" => "Deve informar o texto na lista",
    "from_not_define" => "Deve informar a tabela na chamada %s::from(nome)",
    "alias_not_define" => "Deve informar o alias na chamada %s::alias(nome)",
    "method_not_exist" => "O método '%s' não existe no modelo %s!",
    "value_incorret_in_method" => "Parametro '%s' incorreto no método '%s'",
	"no_have_where" => "Não foi encontrado nenhum filtro. '%s'"
];