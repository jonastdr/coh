<?php

return [
    "events_file_not_found" => "O arquivo de configurações de eventos não foi encontrado!",
    "event_not_exist" => "O evento '%s' não existe!",
    "event_file_not_exist" => "O arquivo de evento não existe",
    "controller_not_exist" => "Controller '%s' não existe!",
    "method_not_exist_in_controller" => "Método '%s' não existe no Controller '%s!'"
];