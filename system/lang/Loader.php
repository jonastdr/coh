<?php
return [
    "dir_not_exist" => "O diretório '%s' não existe no arquivo de configuração!",
    "config_not_exist" => "O arquivo de configuração '%s' não existe!",
    "config_sys_not_exist" => "O arquivo de configuração de sistema '%s' não existe!",
    "lib_not_exist" => "Biblioteca '%s' não existe!",
    "helper_not_exist" => "Helper '%s' não existe!"
];