<?php

return [
    "required" => "O campo '%s' é obrigatório.",
    "min" => "O valor do campo '%s' deve ser maior que '%s'.",
    "max" => "O valor do campo '%s' deve ser menor que '%s'.",
    "minstr" => "O campo %s deve ter no mínimo '%s' caracteres. Você digitou '%s'.",
    "maxstr" => "O campo %s deve ter no máximo '%s' caracteres. Você digitou '%s'.",
    "email" => "O campo '%s' não contém um email válido.",
    "digit" => "O campo '%s' deve conter somente dígitos.",
    "ip" => "O campo '%s' deve conter um IP.",
    "unique" => "Valor já cadastrado no campo '%s'.",
    "function_not_exist" => "A função %s não existe",
	"no_informed_field" => "Não foi informado o campo a ser unico."
];