<?php

/////////////////////////////////////////////
//      Traduções para classe Console      //
/////////////////////////////////////////////

return [
    //Hello
    "hello1" => "Sistema de configuracao do framework ProjectWs",
    "hello2" => "Lista de comandos:",
    "hello3" => "projectws make:controller controller_nome:index,login,logout.. Cria um novo controller",
    "hello4" => "projectws make:model modelo_nome:id,coluna1,coluna2.. Cria um novo modelo",
    "hello5" => "projectws make:view index/inicio Cria uma nova view",
    "hello6" => "projectws make:event event_nome Cria uma nova view",
    "hello7" => "projectws make:middleware middleware_nome Cria um novo middleware",
    "hello8" => "projectws remove:controller controller_name Remove um controller",
    "hello9" => "projectws remove:model model_nome Remove um modelo",
    "hello10" => "projectws remove:view view_nome Remove a view",
    "hello11" => "projectws remove:middleware middleware_nome Remove um middleware",
    "hello12" => "projectws remove:event event_nome Remove um evento",
    "hello13" => "projectws db:update Atualiza tabelas",
    "hello14" => "projectws log 22-07-2015 Visualiza o log",
    //CONTROLLER
    "controller_exist" => "Controller '%s' já existe!",
    "controller_not_exist" => "Controller '%s' não existe!",
    "controller_create" => "Controller '%s' criado com sucesso!",
    "controller_help" => "Qual é o nome do Controller?",
    "controller_remove" => "Controller '%s' removido!",
    "controller_remove_error" => "Não foi possível remover o controller '%s'!",
    "controller_remove_help" => "Qual o nome do controller a ser removido?",
    //MODEL
    "model_exist" => "Modelo '%s' já existe",
    "model_create" => "Modelo '%s' criado com sucesso!",
    "model_help" => "Qual é o nome do Model?",
    "column_duplicate" => "Erro: colunas duplicadas",
    "column_incorrect" => "Nome de coluna incorreto!",
    "model_remove" => "Modelo '%s' removido!",
    "model_remove_error" => "Não foi possível remover o modelo '%s'!",
    "model_remove_help" => "Qual o nome do modelo a ser removido?",
    //VIEW
    "view_exist" => "View '%s' já existe!",
    "view_create" => "View '%s' criada com sucesso!",
    "view_help" => "Qual é o nome da View?",
    "view_remove" => "Modelo '%s' removido!",
    "view_remove_error" => "Não foi possível remover a view '%s'!",
    "view_remove_help" => "Qual o nome da view a ser removido?",
    //EVENT
    "event_exist" => "Evento '%s' já existe!",
    "event_create" => "Evento '%s' criado com sucesso!",
    "event_help" => "Qual é o nome do Evento?",
    "event_remove" => "Evento '%s' removido!",
    "event_remove_error" => "Não foi possível remover o evento '%s'!",
    "event_remove_help" => "Qual o nome do evento a ser removido?",
    //ROUTES
    "route_list_param" => "Lista de rotas %s:%s do sistema:",
    "route_list_all" => "Lista de todas as rotas do sistema:",
    "route_none" => "Não há nenhuma rota!",
    "routes_file_not_exist" => "O arquivo de rota não existe",
    //LOG
    "log_not_exit" => "O arquivo %s não existe!",
    //OTHER
    "whats" => "Você quis dizer?",
    "command_not_exist" => "Este comando não existe!",
    //MIGRATE
    "migrate_no_changes" => "Nenhuma alteracao!",
    //MIDDLEWARE
    "middleware_exist" => "Middleware '%s' já existe!",
    "middleware_create" => "Middleware '%s' criado com sucesso!",
    "middleware_help" => "Qual é o nome do Middleware?",
    "middleware_remove" => "Middleware '%s' removido!",
    "middleware_remove_error" => "Não foi possível remover o middleware '%s'!",
    "middleware_remove_help" => "Qual o nome do middleware a ser removido?",
];