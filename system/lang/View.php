<?php

return [
    "include_not_found" => "a View '%s' não existe!",
    "failed_compile" => "Falha na compilação do arquivo %s!"
];