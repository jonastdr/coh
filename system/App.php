<?php

namespace projectws;

use app\events\Recursos;
use projectws\libs\Exception;
use projectws\libs\File;
use projectws\libs\Minification;
use projectws\libs\Request;
use projectws\libs\Url;
use projectws\libs\Router;
use projectws\libs\Validator;
use projectws\Loader;

class App {

    private $content;
    private $controller;
    private $action;
    private $parameters;
    private $dependencies;
    private static $instance;

    public function __construct($di = null) {
        self::$instance = $this;

        $this->dependencies = $di->getContent();

        $this->init();
    }

    /**
     * Definição da construção de erros
     */
    private function exceptions() {
        error_reporting(-1);
        ini_set('display_errors', 'Off');

        set_error_handler(['projectws\libs\Exception', 'error_handler']);

        set_exception_handler(['projectws\libs\Exception', 'exception_handler']);

        register_shutdown_function(['projectws\libs\Exception', 'shutdown_function']);
    }

    private function execFileRoutes() {
        require(Loader::getAppPath() . 'routes.php');
    }

    /**
     * Inicia a aplicação
     */
    private function init() {
        //define o timezone da aplicação
        date_default_timezone_set(Loader::getConfig('TIME_ZONE'));

        $this->exceptions();

        $this->execFileRoutes();

        //Aciona os eventos ativados
        $this->fireEvents();

        $url = empty($_GET['_url']) ? '/' : $_GET['_url'];

        //adiciona o conteudo do sistema
        $this->content = $this->construc_route($url);

        //minifica arquivos
        $min = new Minification();

        $min->run();
    }

    /**
     * Seleciona o arquivo com os eventos
     * @return string
     */
    private function getEvents() {
        $file = Loader::getAppPath() . Loader::getDir('events') . 'events.php';

        if(!file_exists($file)) {
            new Exception(Loader::getLang('App', 'event_file_not_exist'));

            return;
        }

        return require($file);
    }

    /**
     * Aciona os eventos ativos
     */
    private function fireEvents() {
        $eventos = $this->getEvents();

        foreach ($eventos as $i => $evento) {
            if(!class_exists($evento)) {
                new Exception(Loader::getLang('App', 'event_not_exist', $evento));
            } else {
                $evento = new $evento();
            }

            if($evento->isActive() === true && $evento->rules() === true) {
                //Aciona o evento
                $evento->run();
            }
        }

    }

    /**
     * Verifica a rota pela url
     * @param $url
     * @return mixed|null|string
     */
    private function construc_route($url) {
        //define com padrão a exibir exceção
        $exeption = true;

        foreach (Router::getRoutes() as $route) {
            //url do navegador
            $expString = $this->urlString($url);

            //url do router
            $routerString = $this->urlString($route['http']);

            $route['params'] = [];

	        $route['paramsCount'] = 0;
	        
            foreach ($routerString as $i => $str) {
	            if(preg_match('/\{(.*?)\}/', $str)) {
		            $route['paramsCount']++;
	            }
            	
                if(preg_match('/\{(.*?)\}/', $str) && isset($expString[$i])) {
                    $routerString[$i] = $expString[$i];

                    $route['params'][] = $expString[$i];
                }

                //Parametros opcionais
                elseif(preg_match('/\{(.*?\?)\}/', $str)) {
                    unset($routerString[$i]);
                }
            }

            if(implode('/', $expString) === implode('/', $routerString) && Request::isMethod($route['method']) && ($route['ajax'] == false OR ($route['ajax'] == true && Request::isAjax()))) {
                if(!$route['action'] instanceof \Closure)
                    $route['action'] = explode('/', $route['action']);

                $action = $this->action($route);

                if(is_array($action) || is_object($action) || $route['response_type'] == 'json') {
                    header('Content-Type: application/json');

                    return json_encode($action);
                }

                return $action;
            }
        }

        if($exeption)
            new Exception(Loader::getLang('Router', 'route_not_exist', $url));
    }

    private function urlString($valor) {
        $matchs = preg_split('/\\\|\//', $valor);

        return isset($matchs) ? $matchs : [];
    }

    /**
     * constrói a ação da rota
     * @param array $route
     * @throws \Exception
     */
    private function action(Array $route) {
        //usado para passar parametros do método run do middleware
        $middlewareParams = null;

        if(isset($route['middleware']) && $route['middleware'] instanceof \Closure) {
            $middleware = new Validator();

            if(!$route['middleware']($middleware))
                return null;
        }
        elseif(is_string($route['middleware'])) {
            $middleware = new $route['middleware']();

            if($middleware instanceof Validator) {
                $middleware->run();

                if ($middleware->fail()) {
                    $middleware->unauthorized();

                    return null;
                }
            } else {
                $middlewareParams = $middleware->run();

                if($middlewareParams === false) {
                    if(method_exists($middleware, "unauthorized"))
                        return $middleware->unauthorized();

                    return null;
                }
            }
        }

        //Verifica se irá passar por uma função comum e já retorna a ação
        if($route['action'] instanceof \Closure)
            return $route['action']();

        //CONFIG NAMESPACE
        $APP_FOLDER = preg_replace('/\/|\\\/', '\\', Loader::getDir('APP'));
        $CONTROLLER_FOLDER = preg_replace('/\/|\\\/', '\\', Loader::getDir('CONTROLLERS'));

        //Define o nome do controlador
        $this->controller = $APP_FOLDER . $CONTROLLER_FOLDER . $route['action'][0];

        //Define o nome da ação
        $this->action = $route['action'][1];

        //Carrega a Classe controladora
        $app = new $this->controller();

        //Verifica se o método existe no controlador
        if(!method_exists($app, $this->action)) {
            new Exception(Loader::getLang('App', 'method_not_exist_in_controller', $this->action, $this->controller));

            return null;
        }

        //Parametros de rotas enviados para o controller
        $params = $route['params'];

        //Passa o retorno do middleware para o controller como parametro
        if($middlewareParams && !is_bool($middlewareParams)) {
            $params[$route['paramsCount']] = $middlewareParams;
        }

        if (count($params) > 0) {
            return call_user_func_array([$app, $this->action], $params);
        } else {
            return $app->{$this->action}();
        }
    }

    /**
     * Retorna a instancia da classe App
     * @return \projectws\App
     */
    public static function getInstance() {
        return self::$instance;
    }

    /**
     * Retorna o controller
     * @return mixed
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * Retorna a Action
     * @return mixed
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * Retorna oos parametros passados para o controller
     * @return mixed
     */
    public function getParameters() {
        return $this->parameters;
    }

    /**
     * Retorna o conteudo da aplicação
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Retorna as dependencias do app, acessadas pelo $this
     * @return mixed
     */
    public function getDependencies(){
        return $this->dependencies;
    }
    
    /**
     * Retorna um diretório da aplicação
     * @param $name
     * @return string
     */
    public static function getAppDir($name) {
        return Loader::getAppPath() . Loader::getDir($name);
    }

    /**
     * Retorna um diretório de armazenamento
     * @param string $name
     * @return string
     */
    public static function getStoreDir($name = '') {
        return Loader::getStoreDir($name);
    }
}
