<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <title>Projectws - Error</title>
    <style>
        @media (min-width: 1000px) {
            #errors {
                width: 960px;
            }
        }

        #errors {
            margin: 0 auto;
            border: 1px solid #d3d3d3;
            border-radius: 10px;
            background-color: #dadada;
            color: #474747;
        }

        #errors h1 {
            font-size: 22px;
            text-align: center;
        }

        #errors ul {
            padding: 0;
        }

        #errors li {
            list-style: none;
            font-size: 16px;
            text-align: center;
            margin: 10px;
        }

        #errors small {
            padding: 0 2px;
            color: #aaa;
        }

        #errors span.file {
            color: #800100;
        }
    </style>
</head>
<body>
<div id="errors">
    <h1>Erros de sistema</h1>
    <ul>
    <?php foreach($errors as $error): ?>
        <li><?php echo $error ?></li>
    <?php endforeach ?>
    </ul>
</div>
</body>
</html>