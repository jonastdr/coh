<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <title>Error</title>
    <style>
        h1 {
            margin: 50px 0;
            font-size: 32px;
            text-align: center;
            color: #222;
        }
    </style>
</head>
<body>
    <h1>Ocorreu um erro interno no sistema</h1>
</body>
</html>