<?php

namespace {{APP_FOLDER}}\{{MODEL_FOLDER}};

use projectws\mvc\Model;

/**
 * Class {{MODEL_NAME}}
{{MODEL_COLUNAS_DOC}}
 * @package app\models
 */
class {{MODEL_NAME}} extends Model {

    //Define se o model é atualizado
    protected $update = true;

    //campos filtrados
    protected $fields = [
{{MODEL_COLUNAS}}
    ];

    //primary key da tabela
    protected $pk = 'id';

    //chaves estrangeiras
    protected $fk = [];

    //indices usado como padrão em métodos where
    protected $indices = ['id'];

}