<?php

namespace app\middlewares;

use projectws\libs\Validator;

class {{CLASSE}} extends Validator {

    /**
     * regras de validação
     * @return array
     */
    public function rules() {
        return [
            //'field' => 'required'
        ];
    }

    /**
    * mensagens de cada campo
    * @return array
    */
    public function messages() {
        return [
            //'field' => 'O campo field é obrigatório'
        ];
    }
}