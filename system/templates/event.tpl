<?php

namespace app\events;

use projectws\libs\Events;

class {{CLASSE}} extends Events {

    protected $active = true;

	/**
	 * Executa o evento
	 */
    public function run() {
        //Regras de execução do evento
    }

	/**
	 * Regras a ser executa para validar o Evento
	 */
    public function rules() {
        return false;
    }
}